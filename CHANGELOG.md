# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.11](https://gitlab.com/loki/t2/t2spa/compare/v0.1.10...v0.1.11) (2020-11-13)


### Features

* build and push ([2240809](https://gitlab.com/loki/t2/t2spa/commit/224080916f38afa5bce9158155ec22502a7e6c2b))
* build and push ([3932c14](https://gitlab.com/loki/t2/t2spa/commit/3932c140e16e958afa0ebca5eea13d285943936b))
* build and release ([9d49887](https://gitlab.com/loki/t2/t2spa/commit/9d498876aabf8047fef44e55bc0336fa08e9be68))


### Bug Fixes

* 231 ([5ed41ee](https://gitlab.com/loki/t2/t2spa/commit/5ed41ee722e32ba70472ee212bfa17b724e802fd))
* 353 ([bbb27b2](https://gitlab.com/loki/t2/t2spa/commit/bbb27b27a6ecad6270d6b4cf3f5c20f443b779b6))
* 358 ([5556b92](https://gitlab.com/loki/t2/t2spa/commit/5556b92a3f0b376be7c5bd01244da07d6c5c3a28))
* 361 ([470d6ef](https://gitlab.com/loki/t2/t2spa/commit/470d6efd53d29f2fd0af7014b271c44081dcabed))
* 524 ([bde4c02](https://gitlab.com/loki/t2/t2spa/commit/bde4c0215b1ad021ae64365f83dfca73fcdb44f3))
* 524 ([36766fd](https://gitlab.com/loki/t2/t2spa/commit/36766fd6d6171dbb92f72cb52576e1b2001c4b00))
* 524 ([c5b3fe9](https://gitlab.com/loki/t2/t2spa/commit/c5b3fe9d2122f6f7eac2f266774dd4ddfe987eb5))
* 547 ([540faab](https://gitlab.com/loki/t2/t2spa/commit/540faab588a027535e545e406ac02f8246f1558d))

### [0.1.10](https://gitlab.com/loki/t2/t2spa/compare/v0.1.9...v0.1.10) (2020-11-09)


### Features

* build and push ([d690f2e](https://gitlab.com/loki/t2/t2spa/commit/d690f2ee16eed5ac04faa3febccec54b7c702584))
* build and push ([ace2572](https://gitlab.com/loki/t2/t2spa/commit/ace2572c5e9d6d73799830192f5b91f647f511dc))

### [0.1.9](https://gitlab.com/loki/t2/t2spa/compare/v0.1.8...v0.1.9) (2020-11-06)


### Features

* build and push ([bd5abee](https://gitlab.com/loki/t2/t2spa/commit/bd5abee56063c07f67c9b698f039c468ecaf9e81))
* build and push ([4142225](https://gitlab.com/loki/t2/t2spa/commit/4142225687b8677f2e609dc1ba32d2047a242594))
* build and push ([cc9d9c1](https://gitlab.com/loki/t2/t2spa/commit/cc9d9c1016def3bb2df29566d88313cbdc338547))
* build and push ([73c40f2](https://gitlab.com/loki/t2/t2spa/commit/73c40f228670637afa81b13ea70a0e1b33a72f8c))
* build and release ([52d723a](https://gitlab.com/loki/t2/t2spa/commit/52d723adcebbe57e6feb51ec97148501e624f544))
* update account ([b97ec3f](https://gitlab.com/loki/t2/t2spa/commit/b97ec3ff299a650d7d341841f974dd5752dc2b17))


### Bug Fixes

* end of line ([1069326](https://gitlab.com/loki/t2/t2spa/commit/10693268cb36d8832640c62fdc54948551cbe360))
* remove autofocus list ([b4d5940](https://gitlab.com/loki/t2/t2spa/commit/b4d594013aa5f1d577e93590d048540c1f976638))

### [0.1.8](https://gitlab.com/loki/t2/t2spa/compare/v0.1.7...v0.1.8) (2020-10-19)


### Features

* build and push ([49b5156](https://gitlab.com/loki/t2/t2spa/commit/49b515662e658285b05668b6caa385fe6a1874c7))
* build and push ([7dcf6a7](https://gitlab.com/loki/t2/t2spa/commit/7dcf6a7c4fca21c9f653574a3d165f3bb1c3c7df))
* build and push ([6ab94f3](https://gitlab.com/loki/t2/t2spa/commit/6ab94f373d38eea488cfb61b1e0429d7d246b640))
* build and push ([d07ea52](https://gitlab.com/loki/t2/t2spa/commit/d07ea52b1b00e9e5ab62a0b6f686f777b81fa259))
* build and release ([f025da4](https://gitlab.com/loki/t2/t2spa/commit/f025da4f21c8bbb78e7c3944fcab8558ab57eaca))
* update feedback ([b34fd25](https://gitlab.com/loki/t2/t2spa/commit/b34fd25b113c32183a132e71175d517f557588e8))


### Bug Fixes

* 353 ([08f966d](https://gitlab.com/loki/t2/t2spa/commit/08f966d1ac0d8e5739413e6e0425b16768ad0a7d))
* 363 ([5c43792](https://gitlab.com/loki/t2/t2spa/commit/5c43792629c2e46b0327f80831a30db082859470))
* 378 ([7bceedc](https://gitlab.com/loki/t2/t2spa/commit/7bceedc0e4e25dc95a746fdd96bc0109ed25fcde))
* 382 + 363 ([39693ff](https://gitlab.com/loki/t2/t2spa/commit/39693ff489af46efdf995026d7a3f5e54523a327))
* 418 ([4b23211](https://gitlab.com/loki/t2/t2spa/commit/4b23211f3cc36b410fa93d68d9ce94f587a5cfe4))
* 424 ([eea015a](https://gitlab.com/loki/t2/t2spa/commit/eea015a8980b5477077e24eae2f25b5fde0e215f))
* 425 ([cabf23e](https://gitlab.com/loki/t2/t2spa/commit/cabf23ed60a069ce2201f7603bd18a439091933e))
* 428 ([5f5677b](https://gitlab.com/loki/t2/t2spa/commit/5f5677b31823b8868d7b2e5bfe3356abe5bde32e))
* 438 ([e980cdc](https://gitlab.com/loki/t2/t2spa/commit/e980cdc442232c253b2ddb86d5b1c7a519c9403d))
* 439 ([bc23aca](https://gitlab.com/loki/t2/t2spa/commit/bc23acaad9eb4e8f5cb2fdd472423b883f516070))
* 441 ([dce9134](https://gitlab.com/loki/t2/t2spa/commit/dce9134154b501727c9511f336460a22c19d814c))
* 443 ([e442cdd](https://gitlab.com/loki/t2/t2spa/commit/e442cdd941a85822aab271fde5bf2479d1d9b6d4))
* 461 - 451 ([7c98082](https://gitlab.com/loki/t2/t2spa/commit/7c98082c50e63e623f2f749f4bcb2be7dda7bacb))
* bugs ([2667479](https://gitlab.com/loki/t2/t2spa/commit/2667479b1ebbecdab04d4511a8336216337b2a94))
* env ([73457f2](https://gitlab.com/loki/t2/t2spa/commit/73457f28e4dfa9fa8df78b358fa20d1bbf8a6cf4))
* search advanced form ([df800d9](https://gitlab.com/loki/t2/t2spa/commit/df800d9e8ad45c3f22e5498814abba53f9ffe9d2))
* service worker ([22a6a45](https://gitlab.com/loki/t2/t2spa/commit/22a6a45724ac9bbdf91c580310fae83032cfe96c))
* service worker ([04988ba](https://gitlab.com/loki/t2/t2spa/commit/04988ba8ba3338a92e28f134814c17b2297ce308))
* **ci:** remove auto deploy ([fe7b0c7](https://gitlab.com/loki/t2/t2spa/commit/fe7b0c7de3880b1fc7608c933b29ba8a9494961d))
* update page title ([3139005](https://gitlab.com/loki/t2/t2spa/commit/313900518739a89e68233c8fe51f29453389b9a7))
* **grid:** method overloading onSearch ([b959299](https://gitlab.com/loki/t2/t2spa/commit/b9592999f9196800a36289649487b3af4aae8b3a))

### [0.1.7](https://gitlab.com/loki/t2/t2spa/compare/v0.1.6...v0.1.7) (2020-09-12)


### Features

* build and release ([837ed00](https://gitlab.com/loki/t2/t2spa/commit/837ed0069a5149dcaf10b98359b2827136c73d40))

### [0.1.6](https://gitlab.com/loki/t2/t2spa/compare/v0.1.5...v0.1.6) (2020-09-11)


### Features

* build and push ([9390fc2](https://gitlab.com/loki/t2/t2spa/commit/9390fc27cf0759599178e340d81df840ddd8883f))
* pre update ([1b5c0b9](https://gitlab.com/loki/t2/t2spa/commit/1b5c0b98a3aed71ab5b70f018c51b3ccf1eaeb38))
* update ([f9477f0](https://gitlab.com/loki/t2/t2spa/commit/f9477f030e3c320544ab2fe523519be280f6cdb9))
* update ([874b87a](https://gitlab.com/loki/t2/t2spa/commit/874b87a2e212d857cb463f347ef1998438758884))
* update ui ([8755e65](https://gitlab.com/loki/t2/t2spa/commit/8755e6598b2b820700d0796b5f51e775e0f2176f))


### Bug Fixes

* bugs ([edbdf23](https://gitlab.com/loki/t2/t2spa/commit/edbdf23416f631041ad04b6597737ec01fe571f8))
* update ([521b6a1](https://gitlab.com/loki/t2/t2spa/commit/521b6a13314c1a916d9669f11b857fe630c23f42))
* update ([5066443](https://gitlab.com/loki/t2/t2spa/commit/50664433b502a20c70ca09d94504cc1725c11f74))

### [0.1.5](https://gitlab.com/loki/t2/t2spa/compare/v0.1.4...v0.1.5) (2020-07-22)


### Features

* loki ([a17555e](https://gitlab.com/loki/t2/t2spa/commit/a17555ead390785c851fe026d55719c3f199d1fe))

### [0.1.4](https://gitlab.com/loki/t2/t2spa/compare/v0.1.3...v0.1.4) (2020-07-22)


### Features

* new a10 suck ([468eb2f](https://gitlab.com/loki/t2/t2spa/commit/468eb2fcca8e6f61e4b6ba5625228218b4f27453))
* responsive ([2397e83](https://gitlab.com/loki/t2/t2spa/commit/2397e83ff21e6a1613e59f0da685b6c2a57dd930))
* update ([7bc08f1](https://gitlab.com/loki/t2/t2spa/commit/7bc08f125fc18ebb6ea6efbd511ca62955b38a13))
* update ([7ebc650](https://gitlab.com/loki/t2/t2spa/commit/7ebc650783dade7dee81c55d26d581d3d9bb4e2d))
* update a10 ([0924437](https://gitlab.com/loki/t2/t2spa/commit/09244373094324cf26d4130a79579e60c04fbde9))


### Bug Fixes

* bug ([0a168c5](https://gitlab.com/loki/t2/t2spa/commit/0a168c5ff82bc8b3ccaa587268971aaa1694fa94))
* cicd ([f4db9b8](https://gitlab.com/loki/t2/t2spa/commit/f4db9b8d66bc09c1aed7cb61f0aab9da4db9661b))
* hotfix ([95258e7](https://gitlab.com/loki/t2/t2spa/commit/95258e722cca1ab289f7570af7a21524b5cd9337))

### [0.1.3](https://gitlab.com/loki/t2/t2spa/compare/v0.1.2...v0.1.3) (2020-07-16)


### Features

* 3006 ([13d546e](https://gitlab.com/loki/t2/t2spa/commit/13d546e3ece92b2018c50e17c2420929c8740397))
* add api ([4f43497](https://gitlab.com/loki/t2/t2spa/commit/4f43497a535301965fc14d426d7a3d736ae6ed2e))
* fix bug ([0bc2ad3](https://gitlab.com/loki/t2/t2spa/commit/0bc2ad3ba4a137143c35b35e76cb860dd48e418d))
* fix bugsss ([811d150](https://gitlab.com/loki/t2/t2spa/commit/811d15026ab7139f48eab103ec0fd84a7387ef7e))
* go away ([3955958](https://gitlab.com/loki/t2/t2spa/commit/3955958bfaf2213c2727d4902d0420f9dd70cdb4))
* register partner ([50ebbba](https://gitlab.com/loki/t2/t2spa/commit/50ebbba6775f3915c52f7e277a1989b4555fc5d3))
* remove moment ([9189306](https://gitlab.com/loki/t2/t2spa/commit/918930695726843bd2533731fbfa44a0a63c6969))
* scroll table ([bc1ca2e](https://gitlab.com/loki/t2/t2spa/commit/bc1ca2e53a9c505b0a2543c9a8a026f0bb4c1752))
* update class ([7f50fa4](https://gitlab.com/loki/t2/t2spa/commit/7f50fa4b79b1da6b4e252ef852777264967ec25f))
* update class ([2da56fa](https://gitlab.com/loki/t2/t2spa/commit/2da56fa2019a3542ee735a7f8f419c6504b69643))
* update sonpn ([4ccd181](https://gitlab.com/loki/t2/t2spa/commit/4ccd1816c1a6f91094668243e9ab2338564833fa))
* update ui ([f3e0d84](https://gitlab.com/loki/t2/t2spa/commit/f3e0d84e89a758e850c0e3e66640be182162146f))


### Bug Fixes

* bug ([17ca97f](https://gitlab.com/loki/t2/t2spa/commit/17ca97fc91eb301c0fdfea545c49fcf7c8e27841))
* bug ([0873104](https://gitlab.com/loki/t2/t2spa/commit/0873104b272a547f695be2a75a546140257f95e8))
* bug 1006 ([5b306b8](https://gitlab.com/loki/t2/t2spa/commit/5b306b8eb208a8e0604b992d46a683152d74cc42))
* bugs ([319d961](https://gitlab.com/loki/t2/t2spa/commit/319d9615327828da52e0845591aeb16e09535334))
* build ([2e9f796](https://gitlab.com/loki/t2/t2spa/commit/2e9f796952fa528d49f7e0a39285a5dfee1ca899))
* fee ([fd998e9](https://gitlab.com/loki/t2/t2spa/commit/fd998e98e0b5332255f1d4154daef97ee73e7ced))
* hotfix ([aaaa8ef](https://gitlab.com/loki/t2/t2spa/commit/aaaa8ef9cc6971085ba34a7f964b10cccd155151))
* hotfix ([c37f31a](https://gitlab.com/loki/t2/t2spa/commit/c37f31ad337286eafbdab46a15fd00fa936ce74e))

### [0.1.2](https://gitlab.com/loki/t2/t2spa/compare/v0.3.4...v0.1.2) (2020-05-16)


### Features

* build docker ([2563e77](https://gitlab.com/loki/t2/t2spa/commit/2563e778f36862e7ecc0dc462f9dfb57c82ea998))
* code day 08-05 ([904e7ef](https://gitlab.com/loki/t2/t2spa/commit/904e7ef686db80f9f624b0db7defae1fd36359aa))
* restructure ([0d65656](https://gitlab.com/loki/t2/t2spa/commit/0d65656780f4bd13c6f1b47021aa2d9874ccae25))
* restructure ([7ca9b20](https://gitlab.com/loki/t2/t2spa/commit/7ca9b207bb3089d46db92bc023ad99201cfb8cfc))
* save ([be6a336](https://gitlab.com/loki/t2/t2spa/commit/be6a3362e78e9bccf0dfa8d98b0f45f99baa6129))
* save ([482d5dd](https://gitlab.com/loki/t2/t2spa/commit/482d5dd00179e615eb23ad6a35e11327bf01c604))
* stop ([c378f3d](https://gitlab.com/loki/t2/t2spa/commit/c378f3df2841a8a44f45b70bc7915f67fa1847cd))
* summary ([5326a5a](https://gitlab.com/loki/t2/t2spa/commit/5326a5afe0cc0651efdd6b23b742f4ab1d29d95e))
* sync new structure ([b83a8d4](https://gitlab.com/loki/t2/t2spa/commit/b83a8d4c6c35f726b05102743693e7c0e62ab4f4))
* template user ([00e9688](https://gitlab.com/loki/t2/t2spa/commit/00e9688daada50df443ce8928a6526f2c49bf80b))
* update 9 ([789fb12](https://gitlab.com/loki/t2/t2spa/commit/789fb12fd115f749aff8f93ab9089654dec4d9ab))
* update account ([3919211](https://gitlab.com/loki/t2/t2spa/commit/39192110d86332ed67934da0074886a8b9fc8310))
* update build ([e592aff](https://gitlab.com/loki/t2/t2spa/commit/e592affcd2f3b61021ff08905910de915ded4c67))
* update ci ([e26e562](https://gitlab.com/loki/t2/t2spa/commit/e26e562c66214cfced52931ac5030172f3224f7c))
* update code style ([25e7c37](https://gitlab.com/loki/t2/t2spa/commit/25e7c37ba73bc175646e8f69f644ac8c0c5152da))
* update script ([c6a387a](https://gitlab.com/loki/t2/t2spa/commit/c6a387ace5289e6886ecf16e8b3d3d2d38cbd53b))
* update style ([7a815b6](https://gitlab.com/loki/t2/t2spa/commit/7a815b6146833e5000c3749f9dfaaacc6829514b))
* update ui ([b2f5e2d](https://gitlab.com/loki/t2/t2spa/commit/b2f5e2d0a14a8388b97db4ff62481f5ce7180656))
* zip output ([6eba4c8](https://gitlab.com/loki/t2/t2spa/commit/6eba4c8f95bd1049d759d67aa99ab082f0b69bc7))


### Bug Fixes

* ci-cd config ([e648ce3](https://gitlab.com/loki/t2/t2spa/commit/e648ce3996332a0d59d764047212d44aafbd3662))

### [0.3.4](https://gitlab.com/loki/t2/t2spa/compare/v0.3.3...v0.3.4) (2020-04-27)


### Features

* draft ([689ff8a](https://gitlab.com/loki/t2/t2spa/commit/689ff8a2126f2e84f3334dac84cafbc41209ea7c))
* new server ([6392733](https://gitlab.com/loki/t2/t2spa/commit/63927338bc08e3b24e7523326512b4613679fa72))
* post manager ([553e04f](https://gitlab.com/loki/t2/t2spa/commit/553e04fd4769fcf5538c56c0e4f881aa9961b951))
* remove module not use ([8d02917](https://gitlab.com/loki/t2/t2spa/commit/8d0291788b2196cbf0132b9cf1e6ac51b2d4ad1b))

### [0.3.3](https://gitlab.com/loki/t2/t2spa/compare/v0.3.2...v0.3.3) (2020-04-17)


### Features

* complete deposit ([48e7770](https://gitlab.com/loki/t2/t2spa/commit/48e777024f3c88ae2abf7c6557f6aa317266fe56))
* complete feedback ([321b46b](https://gitlab.com/loki/t2/t2spa/commit/321b46bba6e171bae1e303446d8bd7958c9cf253))
* parnnerfee ([2f14af0](https://gitlab.com/loki/t2/t2spa/commit/2f14af0d4eeb7e2b61a213e507efaed9e1f6bad0))
* update deposit ([17fd5c5](https://gitlab.com/loki/t2/t2spa/commit/17fd5c5fcf25ef5f34ad2a5257b783afa79d6283))
* update fe ([0178d30](https://gitlab.com/loki/t2/t2spa/commit/0178d306756ac4506ebb00e7a4e84f2594c1c3f7))
* update fee ([4be9e0d](https://gitlab.com/loki/t2/t2spa/commit/4be9e0d9cea8419862b1b3f101ed1786cff67bb3))


### Bug Fixes

* cicd ([41c0896](https://gitlab.com/loki/t2/t2spa/commit/41c0896b089f74831e6eac2862474e837bd5cea1))

### 0.3.2 (2020-03-15)


### Features

* first commit ([a3f7e9a](https://gitlab.com/loki/t2/t2spa/commit/a3f7e9ac21fb27487b90dfe7c6c91a7a1e872be1))
* update ui ([20016a3](https://gitlab.com/loki/t2/t2spa/commit/20016a3d61205ff65fb61a9034d26f08f131bd33))
