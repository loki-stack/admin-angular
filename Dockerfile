

FROM  garthk/unzip AS build

WORKDIR /unzip

COPY loki-ui.zip /unzip

RUN unzip /unzip/loki-ui.zip

FROM nginx:alpine as final

COPY _nginx/default_ssl.conf /etc/nginx/conf.d/default.conf
COPY _nginx/ssl/* /etc/nginx/ssl/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /unzip /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off;"]
