```
    ---.
   -..  \
     _|_|_
    /  O    \
    \_______/
       /   \
       \/   \
       /'---'\
  ____/  |     \_____
       __/\____/      \_
            |            \
          / \__  /\      '_
          /     \__ \        \
          \        \_\_________\
           \          \     \
            \          \
```

## Requirements

- Node v11.10.1
- yarn v1.12.3

## Conventions

#### Coding Convention

- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)

#### Commit Convention

- [Commitlint](https://github.com/conventional-changelog/commitlint#what-is-commitlint)

- Commit Pattern:

  `type(scope?): subject  #scope is optional`

  Example:
  
        fix(pos): change dev url
        feat(reservation): add Calendar Component

- Common types: build, ci, chore, docs, feat, fix, perf, refactor, revert, style, test

## Tools

- [yarn](https://yarnpkg.com/en/)
  
  to manage npm dependencies.
  
- [eslint](https://eslint.org/)

  for enforcing rules for writing javascript.

- [prettier](https://github.com/prettier/prettier)

  for formatting code with specified rules with help of eslint.

- [babel](https://github.com/babel/babel)

  for transpiling javascript.

- [husky](https://github.com/typicode/husky)

  for adding git hooks.

- [lint-staged](https://github.com/okonet/lint-staged)

  for processing git staged files.

- [storybook](https://github.com/storybooks/storybook)

  for designing and developing components.
  
## Project Structure

```
├── _mock                                       # Local Mock Data
├── src
│   ├── app
│   │   ├── core
│   │   │   ├── i18n
│   │   │   ├── net
│   │   │   │   └── default.interceptor.ts      # Default HTTP interceptor
│   │   │   ├── services
│   │   │   │   └── startup.service.ts          # Initialize project configuration
│   │   │   └── core.module.ts                  # Core module file
│   │   ├── layout                              # Layout files
│   │   ├── routes
│   │   │   ├── **                              # Business directory
│   │   │   ├── routes.module.ts                # Business routing module
│   │   │   └── routes-routing.module.ts
│   │   ├── shared                              # Shared module
│   │   │   └── shared.module.ts
│   │   ├── app.component.ts                    # Root component
│   │   └── app.module.ts                       # Root module
│   │   └── delon.module.ts                     # @delon modules import
│   ├── assets                                  # Local static files
│   ├── environments                            # Environment configuration
│   ├── styles                                  # Project styles
└── └── style.less                              # Style entry
```

# Schematic

## Create Template Page

# Best practice

- Select multi line vs code : shift + ctrl + up

# Gen swagger

```
export NODE_TLS_REJECT_UNAUTHORIZED=0
node_modules/.bin/ng-swagger-gen -o src/app/services/admin -c ng-swagger-gen-admin.json
```
