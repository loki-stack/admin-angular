import { MockRequest, MockStatusError } from '@delon/mock';
export const adminAPI = {


    //#region Module: BsdLogs 
    // Function: Get filter and pagination 
    // OperationId: BsdLogs_GetFilter 
    'GET /api/v1/bsd/logs': (req: MockRequest) => {

        // {"tags":["BsdLogs"],"summary":"Get filter and pagination","operationId":"BsdLogs_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/LogsModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/LogsModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/LogsModelResponsePagination"}}}}}}
    },
    // Function: Get all 
    // OperationId: BsdLogs_GetAll 
    'GET /api/v1/bsd/logs/all': (req: MockRequest) => {

        // {"tags":["BsdLogs"],"summary":"Get all","operationId":"BsdLogs_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/LogsModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/LogsModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/LogsModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: BsdNavigation 
    // Function: Get tree 
    // OperationId: BsdNavigation_GetTree 
    'GET /api/v1/bsd/navigations/tree': (req: MockRequest) => {

        // {"tags":["BsdNavigation"],"summary":"Get tree","operationId":"BsdNavigation_GetTree","responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/NavigationModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/NavigationModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/NavigationModelListResponseObject"}}}}}}
    },
    // Function: Create 
    // OperationId: BsdNavigation_Create 
    'POST /api/v1/bsd/navigations': (req: MockRequest) => {

        // {"tags":["BsdNavigation"],"summary":"Create","operationId":"BsdNavigation_Create","requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/NavigationCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/NavigationCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/NavigationCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/NavigationCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/NavigationModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/NavigationModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/NavigationModelResponseObject"}}}}}}
    },
    // Function: Update 
    // OperationId: BsdNavigation_Update 
    'PUT /api/v1/bsd/navigations/{id}': (req: MockRequest) => {

        // {"tags":["BsdNavigation"],"summary":"Update","operationId":"BsdNavigation_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/NavigationUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/NavigationUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/NavigationUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/NavigationUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: BsdNavigation_Delete 
    'DELETE /api/v1/bsd/navigations/{id}': (req: MockRequest) => {

        // {"tags":["BsdNavigation"],"summary":"Delete","operationId":"BsdNavigation_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    //#endregion


    //#region Module: BsdParameter 
    // Function: Create 
    // OperationId: BsdParameter_Create 
    'POST /api/v1/bsd/parameters': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Create","operationId":"BsdParameter_Create","requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/ParameterCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/ParameterCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}}}}}}
    },
    // Function: Delete range 
    // OperationId: BsdParameter_DeleteRange 
    'DELETE /api/v1/bsd/parameters': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Delete range","operationId":"BsdParameter_DeleteRange","parameters":[{"name":"listId","in":"query","description":"List Id records","schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}}}}}}
    },
    // Function: Get filter and pagination 
    // OperationId: BsdParameter_GetFilter 
    'GET /api/v1/bsd/parameters': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Get filter and pagination","operationId":"BsdParameter_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponsePagination"}}}}}}
    },
    // Function: Update 
    // OperationId: BsdParameter_Update 
    'PUT /api/v1/bsd/parameters/{id}': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Update","operationId":"BsdParameter_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/ParameterUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/ParameterUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: BsdParameter_Delete 
    'DELETE /api/v1/bsd/parameters/{id}': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Delete","operationId":"BsdParameter_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    // Function: Get by Id 
    // OperationId: BsdParameter_GetById 
    'GET /api/v1/bsd/parameters/{id}': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Get by Id","operationId":"BsdParameter_GetById","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterModelResponseObject"}}}}}}
    },
    // Function: Get by code value 
    // OperationId: BsdParameter_GetByCode 
    'GET /api/v1/bsd/parameters/code/{code}': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Get by code value","operationId":"BsdParameter_GetByCode","parameters":[{"name":"code","in":"path","description":"","required":true,"schema":{"type":"string","description":"","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    // Function: Get all 
    // OperationId: BsdParameter_GetAll 
    'GET /api/v1/bsd/parameters/all': (req: MockRequest) => {

        // {"tags":["BsdParameter"],"summary":"Get all","operationId":"BsdParameter_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ParameterModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ParameterModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ParameterModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: IdmRight 
    // Function: Get by id with detail 
    // OperationId: IdmRight_GetDetail 
    'GET /api/v1/idm/rights/{id}/detail': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get by id with detail","operationId":"IdmRight_GetDetail","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightDetailModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RightDetailModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightDetailModelListResponseObject"}}}}}}
    },
    // Function: Get users map right 
    // OperationId: IdmRight_GetUserMapRight 
    'GET /api/v1/idm/rights/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get users map right","operationId":"IdmRight_GetUserMapRight","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}}}}}}
    },
    // Function: Add right map users 
    // OperationId: IdmRight_AddRightMapUser 
    'POST /api/v1/idm/rights/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Add right map users","operationId":"IdmRight_AddRightMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete right map users 
    // OperationId: IdmRight_DeleteRightMapUser 
    'DELETE /api/v1/idm/rights/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Delete right map users","operationId":"IdmRight_DeleteRightMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Get roles map right 
    // OperationId: IdmRight_GetRoleMapRight 
    'GET /api/v1/idm/rights/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get roles map right","operationId":"IdmRight_GetRoleMapRight","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}}}}}}
    },
    // Function: Add rights map role 
    // OperationId: IdmRight_AddRightMapRole 
    'POST /api/v1/idm/rights/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Add rights map role","operationId":"IdmRight_AddRightMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete right map roles 
    // OperationId: IdmRight_DeleteRightMapRole 
    'DELETE /api/v1/idm/rights/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Delete right map roles","operationId":"IdmRight_DeleteRightMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Create 
    // OperationId: IdmRight_Create 
    'POST /api/v1/idm/rights': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Create","operationId":"IdmRight_Create","parameters":[{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/RightCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/RightCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}}}}}}
    },
    // Function: Delete range 
    // OperationId: IdmRight_DeleteRange 
    'DELETE /api/v1/idm/rights': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Delete range","operationId":"IdmRight_DeleteRange","parameters":[{"name":"listId","in":"query","description":"List Id records","schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}}}}}}
    },
    // Function: Get filter and pagination 
    // OperationId: IdmRight_GetFilter 
    'GET /api/v1/idm/rights': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get filter and pagination","operationId":"IdmRight_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RightModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightModelResponsePagination"}}}}}}
    },
    // Function: Update 
    // OperationId: IdmRight_Update 
    'PUT /api/v1/idm/rights/{id}': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Update","operationId":"IdmRight_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/RightUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/RightUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: IdmRight_Delete 
    'DELETE /api/v1/idm/rights/{id}': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Delete","operationId":"IdmRight_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    // Function: Get by Id 
    // OperationId: IdmRight_GetById 
    'GET /api/v1/idm/rights/{id}': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get by Id","operationId":"IdmRight_GetById","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightModelResponseObject"}}}}}}
    },
    // Function: Get all 
    // OperationId: IdmRight_GetAll 
    'GET /api/v1/idm/rights/all': (req: MockRequest) => {

        // {"tags":["IdmRight"],"summary":"Get all","operationId":"IdmRight_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RightModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RightModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RightModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: IdmRole 
    // Function: Get by id with detail 
    // OperationId: IdmRole_GetDetail 
    'GET /api/v1/idm/roles/{id}/detail': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get by id with detail","operationId":"IdmRole_GetDetail","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleDetailModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RoleDetailModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleDetailModelListResponseObject"}}}}}}
    },
    // Function: Get users map role 
    // OperationId: IdmRole_GetUserMapRole 
    'GET /api/v1/idm/roles/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get users map role","operationId":"IdmRole_GetUserMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseUserModelListResponseObject"}}}}}}
    },
    // Function: Add users map role 
    // OperationId: IdmRole_AddUserMapRole 
    'POST /api/v1/idm/roles/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Add users map role","operationId":"IdmRole_AddUserMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete users map role 
    // OperationId: IdmRole_DeleteUserMapRole 
    'DELETE /api/v1/idm/roles/{id}/user': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Delete users map role","operationId":"IdmRole_DeleteUserMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Get rights map role 
    // OperationId: IdmRole_GetRightMapRole 
    'GET /api/v1/idm/roles/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get rights map role","operationId":"IdmRole_GetRightMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}}}}}}
    },
    // Function: Add rights map role 
    // OperationId: IdmRole_AddRightMapRole 
    'POST /api/v1/idm/roles/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Add rights map role","operationId":"IdmRole_AddRightMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete rights map role 
    // OperationId: IdmRole_DeleteRightMapRole 
    'DELETE /api/v1/idm/roles/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Delete rights map role","operationId":"IdmRole_DeleteRightMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Create 
    // OperationId: IdmRole_Create 
    'POST /api/v1/idm/roles': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Create","operationId":"IdmRole_Create","parameters":[{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/RoleCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/RoleCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}}}}}}
    },
    // Function: Delete range 
    // OperationId: IdmRole_DeleteRange 
    'DELETE /api/v1/idm/roles': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Delete range","operationId":"IdmRole_DeleteRange","requestBody":{"description":"List Id records","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}}}}}}
    },
    // Function: Get filter and pagination 
    // OperationId: IdmRole_GetFilter 
    'GET /api/v1/idm/roles': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get filter and pagination","operationId":"IdmRole_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleModelResponsePagination"}}}}}}
    },
    // Function: Update 
    // OperationId: IdmRole_Update 
    'PUT /api/v1/idm/roles/{id}': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Update","operationId":"IdmRole_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/RoleUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/RoleUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: IdmRole_Delete 
    'DELETE /api/v1/idm/roles/{id}': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Delete","operationId":"IdmRole_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    // Function: Get by Id 
    // OperationId: IdmRole_GetById 
    'GET /api/v1/idm/roles/{id}': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get by Id","operationId":"IdmRole_GetById","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleModelResponseObject"}}}}}}
    },
    // Function: Get all 
    // OperationId: IdmRole_GetAll 
    'GET /api/v1/idm/roles/all': (req: MockRequest) => {

        // {"tags":["IdmRole"],"summary":"Get all","operationId":"IdmRole_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RoleModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/RoleModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/RoleModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: IdmUser 
    // Function: Check name valid 
    // OperationId: IdmUser_CheckNameAvailability 
    'GET /api/v1/idm/users/check_name/{name}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Check name valid","operationId":"IdmUser_CheckNameAvailability","parameters":[{"name":"name","in":"path","description":"","required":true,"schema":{"type":"string","description":"","nullable":true}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}}}}}}
    },
    // Function: Get by id with detail 
    // OperationId: IdmUser_GetDetail 
    'GET /api/v1/idm/users/{id}/detail': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get by id with detail","operationId":"IdmUser_GetDetail","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserDetailModelListResponseObject"}}}}}}
    },
    // Function: Get roles map user 
    // OperationId: IdmUser_GetRoleMapUser 
    'GET /api/v1/idm/users/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get roles map user","operationId":"IdmUser_GetRoleMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseRoleModelListResponseObject"}}}}}}
    },
    // Function: Add users map role 
    // OperationId: IdmUser_AddUserMapRole 
    'POST /api/v1/idm/users/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Add users map role","operationId":"IdmUser_AddUserMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete user map role 
    // OperationId: IdmUser_DeleteUserMapRole 
    'DELETE /api/v1/idm/users/{id}/role': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Delete user map role","operationId":"IdmUser_DeleteUserMapRole","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Get rights map user 
    // OperationId: IdmUser_GetRightMapUser 
    'GET /api/v1/idm/users/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get rights map user","operationId":"IdmUser_GetRightMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BaseRightModelListResponseObject"}}}}}}
    },
    // Function: Add right map user 
    // OperationId: IdmUser_AddRightMapUser 
    'POST /api/v1/idm/users/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Add right map user","operationId":"IdmUser_AddRightMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Delete right map user 
    // OperationId: IdmUser_DeleteRightMapUser 
    'DELETE /api/v1/idm/users/{id}/right': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Delete right map user","operationId":"IdmUser_DeleteRightMapUser","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Enable right map user 
    // OperationId: IdmUser_EnableRightMapUser 
    'DELETE /api/v1/idm/users/{id}/right/enable/{rightId}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Enable right map user","operationId":"IdmUser_EnableRightMapUser","parameters":[{"name":"id","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}},{"name":"rightId","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Disable right of user 
    // OperationId: IdmUser_DisableRightMapUser 
    'DELETE /api/v1/idm/users/{id}/right/disable/{rightId}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Disable right of user","operationId":"IdmUser_DisableRightMapUser","parameters":[{"name":"id","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}},{"name":"rightId","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Check right map user 
    // OperationId: IdmUser_CheckRightMapUser 
    'GET /api/v1/idm/users/{id}/right/check': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Check right map user","operationId":"IdmUser_CheckRightMapUser","parameters":[{"name":"id","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}},{"name":"rightId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/BooleanResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/BooleanResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/BooleanResponseObject"}}}}}}
    },
    // Function: Update system right, role map user in list application 
    // OperationId: IdmUser_UpdateSystemRightRoleMapApps 
    'PUT /api/v1/idm/users/{id}/right_role/system': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Update system right, role map user in list application","operationId":"IdmUser_UpdateSystemRightRoleMapApps","parameters":[{"name":"id","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/UpdateSystemRightRoleMapAppsRequest"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UpdateSystemRightRoleMapAppsRequest"}},"application/json":{"schema":{"$ref":"#/components/schemas/UpdateSystemRightRoleMapAppsRequest"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/UpdateSystemRightRoleMapAppsRequest"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/Response"}},"application/json":{"schema":{"$ref":"#/components/schemas/Response"}}}}}}
    },
    // Function: Create 
    // OperationId: IdmUser_Create 
    'POST /api/v1/idm/users': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Create","operationId":"IdmUser_Create","parameters":[{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/UserCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/UserCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}}}}}}
    },
    // Function: Delete range 
    // OperationId: IdmUser_DeleteRange 
    'DELETE /api/v1/idm/users': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Delete range","operationId":"IdmUser_DeleteRange","parameters":[{"name":"listId","in":"query","description":"List Id records","schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}}}}}}
    },
    // Function: Get filter and pagination 
    // OperationId: IdmUser_GetFilter 
    'GET /api/v1/idm/users': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get filter and pagination","operationId":"IdmUser_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserModelResponsePagination"}}}}}}
    },
    // Function: Update password 
    // OperationId: IdmUser_ChangePassword 
    'PUT /api/v1/idm/users/{id}/change_password': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Update password","operationId":"IdmUser_ChangePassword","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"oldPassword","in":"query","description":"","schema":{"type":"string","description":"","nullable":true}},{"name":"password","in":"query","description":"","schema":{"type":"string","description":"","nullable":true}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Update 
    // OperationId: IdmUser_Update 
    'PUT /api/v1/idm/users/{id}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Update","operationId":"IdmUser_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}},{"name":"applicationId","in":"query","description":"","schema":{"type":"string","description":"","format":"uuid","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/UserUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/UserUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: IdmUser_Delete 
    'DELETE /api/v1/idm/users/{id}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Delete","operationId":"IdmUser_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    // Function: Get by Id 
    // OperationId: IdmUser_GetById 
    'GET /api/v1/idm/users/{id}': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get by Id","operationId":"IdmUser_GetById","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserModelResponseObject"}}}}}}
    },
    // Function: Get all 
    // OperationId: IdmUser_GetAll 
    'GET /api/v1/idm/users/all': (req: MockRequest) => {

        // {"tags":["IdmUser"],"summary":"Get all","operationId":"IdmUser_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/UserModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/UserModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/UserModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: MdmNodeUpload 
    // Function: Get filter and pagination 
    // OperationId: MdmNodeUpload_GetFilter 
    'GET /api/v1/core/nodes': (req: MockRequest) => {

        // {"tags":["MdmNodeUpload"],"summary":"Get filter and pagination","operationId":"MdmNodeUpload_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/FileModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/FileModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/FileModelResponsePagination"}}}}}}
    },
    // Function: Upload Base64 
    // OperationId: MdmNodeUpload_UploadFileBase64Physical 
    'POST /api/v1/core/nodes/upload/physical/base64': (req: MockRequest) => {

        // {"tags":["MdmNodeUpload"],"summary":"Upload Base64","operationId":"MdmNodeUpload_UploadFileBase64Physical","parameters":[{"name":"destinationPhysicalPath","in":"query","description":"Destination path (optional)","schema":{"type":"string","description":"Destination path (optional)","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/Base64FileData"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/Base64FileData"}},"application/json":{"schema":{"$ref":"#/components/schemas/Base64FileData"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/Base64FileData"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}}}}}}
    },
    // Function: Upload many file base 64 
    // OperationId: MdmNodeUpload_UploadFileBase64ManyPhysical 
    'POST /api/v1/core/nodes/upload/physical/base64/many': (req: MockRequest) => {

        // {"tags":["MdmNodeUpload"],"summary":"Upload many file base 64","operationId":"MdmNodeUpload_UploadFileBase64ManyPhysical","parameters":[{"name":"destinationPhysicalPath","in":"query","description":"Destination path (optional)","schema":{"type":"string","description":"Destination path (optional)","nullable":true}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"type":"array","items":{"$ref":"#/components/schemas/Base64FileData"},"description":"","nullable":true}},"application/vnd.restful+json":{"schema":{"type":"array","items":{"$ref":"#/components/schemas/Base64FileData"},"description":"","nullable":true}},"application/json":{"schema":{"type":"array","items":{"$ref":"#/components/schemas/Base64FileData"},"description":"","nullable":true}},"application/*+json":{"schema":{"type":"array","items":{"$ref":"#/components/schemas/Base64FileData"},"description":"","nullable":true}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}}}}}}
    },
    // Function: Upload a file 
    // OperationId: MdmNodeUpload_UploadFileBlobPhysical 
    'POST /api/v1/core/nodes/upload/physical/blob': (req: MockRequest) => {

        // {"tags":["MdmNodeUpload"],"summary":"Upload a file","operationId":"MdmNodeUpload_UploadFileBlobPhysical","parameters":[{"name":"destinationPhysicalPath","in":"query","description":"Destination path (optional)","schema":{"type":"string","description":"Destination path (optional)","nullable":true}},{"name":"isResize","in":"query","description":"","schema":{"type":"boolean","description":"","default":false}},{"name":"width","in":"query","description":"","schema":{"type":"integer","description":"","format":"int32","default":160}},{"name":"height","in":"query","description":"","schema":{"type":"integer","description":"","format":"int32","default":160}}],"requestBody":{"content":{"multipart/form-data":{"schema":{"type":"object","properties":{"file":{"type":"string","description":"","format":"binary","nullable":true}}},"encoding":{"file":{"style":"form"}}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/FileUploadResultResponseObject"}}}}}}
    },
    // Function: Upload many file 
    // OperationId: MdmNodeUpload_UploadFileBlobManyPhysical 
    'POST /api/v1/core/nodes/upload/physical/blob/many': (req: MockRequest) => {

        // {"tags":["MdmNodeUpload"],"summary":"Upload many file","operationId":"MdmNodeUpload_UploadFileBlobManyPhysical","parameters":[{"name":"destinationPhysicalPath","in":"query","description":"Destination path (optional)","schema":{"type":"string","description":"Destination path (optional)","nullable":true}}],"requestBody":{"content":{"multipart/form-data":{"schema":{"type":"object","properties":{"formData":{"type":"array","items":{"$ref":"#/components/schemas/StringStringValuesKeyValuePair"},"description":"","nullable":true}}},"encoding":{"formData":{"style":"form"}}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/FileUploadResultListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: SysApplication 
    // Function: Bootstrap data of application 
    // OperationId: SysApplication_Bootstrap 
    'POST /api/v1/sys/applications/bootstrap': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Bootstrap data of application","operationId":"SysApplication_Bootstrap","parameters":[{"name":"overwrite","in":"query","description":"overwrite data","schema":{"type":"boolean","description":"overwrite data"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Create 
    // OperationId: SysApplication_Create 
    'POST /api/v1/sys/applications/{id}': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Create","operationId":"SysApplication_Create","parameters":[{"name":"id","in":"path","description":"","required":true,"schema":{"type":"string","description":"","format":"uuid"}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/ApplicationCreateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationCreateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationCreateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/ApplicationCreateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}}}}}}
    },
    // Function: Update 
    // OperationId: SysApplication_Update 
    'PUT /api/v1/sys/applications/{id}': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Update","operationId":"SysApplication_Update","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/ApplicationUpdateModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationUpdateModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationUpdateModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/ApplicationUpdateModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseUpdate"}}}}}}
    },
    // Function: Delete 
    // OperationId: SysApplication_Delete 
    'DELETE /api/v1/sys/applications/{id}': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Delete","operationId":"SysApplication_Delete","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDelete"}}}}}}
    },
    // Function: Get by Id 
    // OperationId: SysApplication_GetById 
    'GET /api/v1/sys/applications/{id}': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Get by Id","operationId":"SysApplication_GetById","parameters":[{"name":"id","in":"path","description":"Id record","required":true,"schema":{"type":"string","description":"Id record","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponseObject"}}}}}}
    },
    // Function: Delete range 
    // OperationId: SysApplication_DeleteRange 
    'DELETE /api/v1/sys/applications': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Delete range","operationId":"SysApplication_DeleteRange","parameters":[{"name":"listId","in":"query","description":"List Id records","schema":{"type":"array","items":{"type":"string","format":"uuid"},"description":"List Id records","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}},"application/json":{"schema":{"$ref":"#/components/schemas/ResponseDeleteMulti"}}}}}}
    },
    // Function: Get filter and pagination 
    // OperationId: SysApplication_GetFilter 
    'GET /api/v1/sys/applications': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Get filter and pagination","operationId":"SysApplication_GetFilter","parameters":[{"name":"page","in":"query","description":"Page number","schema":{"type":"integer","description":"Page number","format":"int32","default":1}},{"name":"size","in":"query","description":"Number records in one page","schema":{"type":"integer","description":"Number records in one page","format":"int32","default":20}},{"name":"filter","in":"query","description":"Filter Json Object. Ex: \"{}\"","schema":{"type":"string","description":"Filter Json Object. Ex: \"{}\"","default":"{}","nullable":true}},{"name":"sort","in":"query","description":"Sort condition. Ex: \"+id\"","schema":{"type":"string","description":"Sort condition. Ex: \"+id\"","default":"","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponsePagination"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponsePagination"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelResponsePagination"}}}}}}
    },
    // Function: Get all 
    // OperationId: SysApplication_GetAll 
    'GET /api/v1/sys/applications/all': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Get all","operationId":"SysApplication_GetAll","parameters":[{"name":"filter","in":"query","schema":{"type":"string","default":"{}","nullable":true}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}}}}}}
    },
    // Function: Get all owner 
    // OperationId: SysApplication_GetAllOwner 
    'GET /api/v1/sys/applications/owner': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Get all owner","operationId":"SysApplication_GetAllOwner","responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}}}}}}
    },
    // Function: Get all by userId 
    // OperationId: SysApplication_GetAllByUserId 
    'GET /api/v1/sys/applications/user/{userId}': (req: MockRequest) => {

        // {"tags":["SysApplication"],"summary":"Get all by userId","operationId":"SysApplication_GetAllByUserId","parameters":[{"name":"userId","in":"path","description":"Id user","required":true,"schema":{"type":"string","description":"Id user","format":"uuid"}}],"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/ApplicationModelListResponseObject"}}}}}}
    },
    //#endregion


    //#region Module: SysAuthentication 
    // Function: Register JWT 
    // OperationId: SysAuthentication_RegisterJwt 
    'POST /api/v1/authentication/jwt/register': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Register JWT","operationId":"SysAuthentication_RegisterJwt","requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/RegisterModel"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/RegisterModel"}},"application/json":{"schema":{"$ref":"#/components/schemas/RegisterModel"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/RegisterModel"}}}},"responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}}}}}}
    },
    // Function: Get JWT info 
    // OperationId: SysAuthentication_JwtInfo 
    'POST /api/v1/authentication/jwt/info': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Get JWT info","operationId":"SysAuthentication_JwtInfo","responses":{"200":{"description":"Success","content":{"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}},"application/problem+json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}},"application/json":{"schema":{"$ref":"#/components/schemas/LoginResponseResponseObject"}}}}}}
    },
    // Function: Login 
    // OperationId: SysAuthentication_SignInJwt 
    'POST /api/v1/authentication/jwt/login': (req: MockRequest) => {
        return {
        "data": {
            "userId": "00000000-0000-0000-0000-000000000001",
            "tokenString": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ4LXVzZXJOYW1lIjoibXJpLmhhbm5pYmFsQGdtYWlsLmNvbSIsIngtZnVsbE5hbWUiOiJBZG1pbmlzdHJhdG9yIiwieC1hdmF0YXIiOiJodHRwczovL2F2YXRhcnMwLmdpdGh1YnVzZXJjb250ZW50LmNvbS91LzE4NDIxMzEzP3Y9NCIsIngtdXNlcklkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAxIiwieC1hcHBJZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMSIsIngtYXBwIjoiW1wiMVwiXSIsIngtcm9sZSI6IltcIl9BRE1JTlwiXSIsIngtcmlnaHQiOiJbXCJUUlVZX0NBUF9IRV9USE9OR1wiLFwiVFJVWV9DQVBfTUFDX0RJTkhcIl0iLCJ4LWV4cCI6IjE2MTcxMDMwMDQzMzgiLCJ4LWlhdCI6IjE2MTcxNjMwMDQzMzgiLCJleHAiOjE2MTcwODM4MDQsImlzcyI6IkdFTkVBVF9DUk9QIiwiYXVkIjoiR0VORUFUX0NST1AifQ.MVXrYz459AbkWcrTUdxGklvk_wtQKzko_wXpIXHBiF8",
            "refreshToken": "UwfiSS/YgmrWvJi2CK3m4Xtw5ia98jc50BpmPL4Yp4pOyAz4nJrBau+bboYGfET969k3rwElc5XnfcQSQNY3MA==",
            "issuedAt": "2021-03-30T11:16:44.3388549+07:00",
            "expiresAt": "2021-03-31T03:56:44.3388549+07:00",
            "applicationId": "00000000-0000-0000-0000-000000000001",
            "applicationModel": {
            "description": "Default app",
            "isDefault": false,
            "id": "00000000-0000-0000-0000-000000000001",
            "name": "ADMIN",
            "code": "1"
            },
            "listApplication": [
            {
                "description": "Default app",
                "isDefault": false,
                "id": "00000000-0000-0000-0000-000000000001",
                "name": "ADMIN",
                "code": "1"
            }
            ],
            "listRight": [
            {
                "enable": true,
                "inherited": false,
                "inheritedFromRoles": "",
                "listRole": null,
                "listRoleId": [],
                "id": "00000000-0000-0000-0000-000000000001",
                "code": "TRUY_CAP_HE_THONG",
                "name": "Access",
                "applicationId": "00000000-0000-0000-0000-000000000001",
                "createdByUserId": "00000000-0000-0000-0000-000000000001",
                "createdOnDate": "2021-03-29T16:15:32.551745",
                "lastModifiedByUserId": "00000000-0000-0000-0000-000000000001",
                "lastModifiedOnDate": "2021-03-29T16:15:32.551746"
            },
            {
                "enable": true,
                "inherited": false,
                "inheritedFromRoles": "",
                "listRole": null,
                "listRoleId": [],
                "id": "00000000-0000-0000-0000-000000000002",
                "code": "TRUY_CAP_MAC_DINH",
                "name": "Access default",
                "applicationId": "00000000-0000-0000-0000-000000000001",
                "createdByUserId": "00000000-0000-0000-0000-000000000001",
                "createdOnDate": "2021-03-29T16:15:32.551751",
                "lastModifiedByUserId": "00000000-0000-0000-0000-000000000001",
                "lastModifiedOnDate": "2021-03-29T16:15:32.551752"
            }
            ],
            "listRole": [
            {
                "id": "00000000-0000-0000-0000-000000000001",
                "code": "_ADMIN",
                "name": "System admin",
                "isSystem": true
            }
            ]
        },
        "code": 200,
        "message": "Thành công",
        "totalTime": 0
        }
    },
    // Function: Sign-In using OA Facebook 
    // OperationId: SysAuthentication_OaFacebook 
    'GET /api/v1/authentication/fb/oa': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Sign-In using OA Facebook","operationId":"SysAuthentication_OaFacebook","parameters":[{"name":"callbackUrl","in":"query","description":"Url frontend callback. Ex: https//yourclient/callback.html","schema":{"type":"string","description":"Url frontend callback. Ex: https//yourclient/callback.html","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    // Function: Sign-In using OA Google 
    // OperationId: SysAuthentication_OaGoogle 
    'GET /api/v1/authentication/gg/oa': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Sign-In using OA Google","operationId":"SysAuthentication_OaGoogle","parameters":[{"name":"callbackUrl","in":"query","description":"Url frontend callback. Ex: https//yourclient/callback.html","schema":{"type":"string","description":"Url frontend callback. Ex: https//yourclient/callback.html","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    // Function: Call back API OA 
    // OperationId: SysAuthentication_CallBackOa 
    'GET /api/v1/authentication/{type}/callback': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Call back API OA","operationId":"SysAuthentication_CallBackOa","parameters":[{"name":"type","in":"path","required":true,"schema":{"type":"string","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    // Function: Logout OA 
    // OperationId: SysAuthentication_SignOutOa 
    'GET /api/v1/authentication/logout': (req: MockRequest) => {

        // {"tags":["SysAuthentication"],"summary":"Logout OA","operationId":"SysAuthentication_SignOutOa","parameters":[{"name":"callbackUrl","in":"query","schema":{"type":"string","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    //#endregion


    //#region Module: SysCache 
    // Function: Get cache 
    // OperationId: SysCache_GetCollection 
    'GET /api/v1/cache': (req: MockRequest) => {

        // {"tags":["SysCache"],"summary":"Get cache","operationId":"SysCache_GetCollection","responses":{"200":{"description":"Success"}}}
    },
    //#endregion


    //#region Module: SysPush 
    // Function: Get public key 
    // OperationId: SysPush_Get 
    'GET /api/v1/core/push': (req: MockRequest) => {
        return {
  "data": "BKpZNvHT4gxa3EURbyZxkgEazo5yr79Fb8hZG8ZzJQQxle8LCwldSRa5d9lmfJey4I_SVBwq6Chmde2g2USyMgs",
  "code": 200,
  "message": "Thành công",
  "totalTime": 0
}
    },
    // Function: Add new subscription 
    // OperationId: SysPush_Post 
    'POST /api/v1/core/push': (req: MockRequest) => {

        // {"tags":["SysPush"],"summary":"Add new subscription","operationId":"SysPush_Post","requestBody":{"description":"","content":{"application/json-patch+json":{"schema":{"$ref":"#/components/schemas/PushSubscription"}},"application/vnd.restful+json":{"schema":{"$ref":"#/components/schemas/PushSubscription"}},"application/json":{"schema":{"$ref":"#/components/schemas/PushSubscription"}},"application/*+json":{"schema":{"$ref":"#/components/schemas/PushSubscription"}}}},"responses":{"200":{"description":"Success"}}}
    },
    // Function: Test send dummy message 
    // OperationId: SysPush_TestPush 
    'GET /api/v1/core/push/test/{message}': (req: MockRequest) => {

        // {"tags":["SysPush"],"summary":"Test send dummy message","operationId":"SysPush_TestPush","parameters":[{"name":"message","in":"path","description":"","required":true,"schema":{"type":"string","description":"","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },
    // Function: Delete endpoint 
    // OperationId: SysPush_Delete 
    'DELETE /api/v1/core/push/{endpoint}': (req: MockRequest) => {

        // {"tags":["SysPush"],"summary":"Delete endpoint","operationId":"SysPush_Delete","parameters":[{"name":"endpoint","in":"path","description":"","required":true,"schema":{"type":"string","description":"","nullable":true}}],"responses":{"200":{"description":"Success"}}}
    },

    //#endregion

};
