// import { MockRequest } from '@delon/mock';
// import { deepCopy } from '@delon/util';

// interface DemoModel {
//     id: string;
//     ma_noi_bo: string;
//     ten: string;
//     gia_ban: number;
//     gia_von: number;
//     nhom_san_pham: string;
//     loai_san_pham: string;
//     so_luong_thuc_te: number;
//     so_luong_du_bao: number;
//     don_vi_tinh: string;
//     trang_thai: boolean;
//     ngay_tao: string;
//     anh: string;
// }

// const DATA: DemoModel[] = [
//     {
//         id: '3f4730ac-8d61-4c98-80c2-8fba402c4988',
//         ma_noi_bo: 'org.creativecommons.Redhold',
//         ten: 'Croissant, Raw - Mini',
//         gia_von: 3623.66,
//         gia_ban: 1094,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5669,
//         so_luong_du_bao: 9634,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-10-19 08:17:05',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJJSURBVDjLpZNNbxJRFIb7A/wF/A5YunRDovsmRk3cmLAxcdG0uiFuXDSmkBlLFNOmtYFKgibUtqlJG6UjiGksU0oZPgQs0KEwMw4Dw8dQjnPuMCNq48abvJub87zn4547BQBTk7q2CDZdDl1OXdNjOcd3tj/jJ8Eruuxzb2RX+NMpHT/MMUfHJwKbSgv7Bxnm9YciPRMSXRiDsb8ZjOGrwWjNzZ4UOL4pg6IOQLsYEbU6fajWRYgdpLilnYIbY00T08COcCrzTen2NMCj9ocgKgMQdLV7Q3KnqH3YTyQV/1YWTezEAPvCsjGzCTfkPtR/9IGXDNWkHlTFnmWysxfj7q/x2I4NDRxh5juNZf8LPm12ifBkimdAheI0smjgjH3NMtgzlmqCNx5tGnq4Abe9LIHLjS7IHQ3OJRWW1zcYZNFgOnl0LOCwmq0BgTEjgqbQoHSuQrGuEqO+dgFrgXUBWWJwyKaIAZaPcEXoWvD1uQjc8rBQ4FUio4oBLK+8sgycH7+kGUnpQUvVrF4xK4KomwuGQf6sQ14mV5GA8gesFhyB3TxdrjZhNAKSwSzXzIpgrtaBbLUDg+EI9j6nwe3btIZoexBsuHajCU6QjSlfBmaqbZIgr2f3Pl/l7vpyxjOai0S9Zd2R91GFF41Aqa1Z1eAyYeZcRQSPP6jMUlu/FmlylecDCfdqKMLFk3ko8zKZCfacLgmwHWVhnlriZrzv/l7lyc9072XJ9fjFNv10cYWhnvmEBS8tPPH4mVlPmL5DZy7/TP/znX8C6zgR9sd1gukAAAAASUVORK5CYII='
//     },
//     {
//         id: 'bae6e987-4621-4e06-aa81-140b00b10721',
//         ma_noi_bo: 'com.cargocollective.Regrant',
//         ten: 'Pepper - Black, Whole',
//         gia_von: 6346.5,
//         gia_ban: 4601,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 7748,
//         so_luong_du_bao: 8279,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-10-27 21:20:23',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKbSURBVDjLpZNdSNNRGMb/EcjsxiCkMKhGCEaxTAoh60qyIiKaiDTNUklTWatwoLnSbbiaa5vDMefGcv/+29gHa3ObqWvpprTp5kckrpvu2q0XhVZU7OlsF2I32sfFe/G85zy/l/c5HAoA9T+15eHS0lJOIpHI/ScA//liXYd55vOLUGxN6l1Zu8ssNPwxgJgr2x3zENpnofC9Q3fgPTK6VhGo3BZAzBy+OQbpqxXclDnh9o+/FZhmsrpO4cWle3rOloDX01F7ryWMRs0oRBrbfKb3aMg7m9H3n9qg1Oi0vwGWl5f1JKwkCSsZjUY/RCKR9OrqKoYY97e5uTk4HA6QHmQa4xdyF2q1el0ulyclEklSJBJFMklrU6kUCADhcJgbi8UQCARioVAIdru92ufzwWg0VvGZMtwaPgG9QQexWFyTAQuFwo8UmXI4Ho9jamqqYGJiguX3+0vcbvc+m83GMZvNeQaDoXhgYGB343DJV7Gfh2p9Ee6I69kEAoFAMJrdY3Jy8hMBgEzjBoNBWK3W6wLrWfCZ0nQzfRL1puNpkacKzoQG7a4rOKfemzbRRrS0tHRkAWNjY+MjIyMFTqeTZbFYOCaTKb+NKYV3UQf3gjZrtCf60R9qBx3rw21rBU49ZoHXyq3MAjweD5cAwDAM1+VyQafTXWsk+2bMqqAA8vE29L5sQo+/HpLRZuinpag1n0FhN/Vz4zlomv5BwmJptdoilUq1p8Z4BI54P6yzStDRPjx784RAWjE4LUGT9QIKu3K+7++kijcAJCyQsKBUKiGTyXB1kI2Lmv0oV+ejTJGHuuHT0EV60MBUoPBBLi43nA9v+5k211HpjvUbdDkOdlGpQ8Jdx3g8Xs5fAdgPqfUDXTuR30mxN/d/AaKgrAVJC0Z9AAAAAElFTkSuQmCC'
//     },
//     {
//         id: '4d53aa7c-5d97-4f76-9d6a-10559eaa95c6',
//         ma_noi_bo: 'com.instagram.Vagram',
//         ten: 'Spring Roll Wrappers',
//         gia_von: 3221.15,
//         gia_ban: 5575,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2530,
//         so_luong_du_bao: 7874,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-10-17 00:42:57',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALZSURBVBgZBcFLiFVlAADg7zzuPLzjzDjOMINMitIie5gF+UAkIZSgRQuXLZIWrY021dYIggJdJURElJsoqlWRYA9GshGFCNQeOjoTk6bjeOd5zzn/f07flzRNA459ObcHJ3cM9+1fq2prVa2qa+uh7mAZ9xCxiAV8iu9zgDqEvU9ODOx//dkxALBa1kNrZT202I2TZcVyEd28t+Lb66uHcTwHqEMYH+xJwNyDqJUk8oQsp7eV2tqbytJUK+OpyX5bhtojH07Pv58CxKoabOeEmuUy0al4UNDp0umysM5/KxG8eWbW/u1tj4+2xnKAWFUjG3tSqwWr3ShNEzmyjDQjk8gSaiRxyYUbiy7PduZzgFiW40P9mc56sFY00rSRpaQxkaVkGlmGJnNnqXDq7N9LOJYDhLLcNj7Y0uk2AjRkMZE2iGQaeZOqG2IrCmXY/s1rB+6nALEstk0M9VotG0lKliRSpEjw+YUjPjq3RxkKoSjEsoiQwvMnvusXQ09vK1VGUg1qjVrUqDWKUJoc3emVj3dbWeuEUJZLkEMoyrF2u0+aUEPD19OHNXVQ1kEZgy2bHrZzYq/l7qr766/m3VC0ub+SQyyLDXm7R56SpYlYJ0JdOvzYy2JTi3VUa8x35jwxecBKue7S7E+dXW+nI/nB42dGcWLPI1vdXmrcvBO1++iGUmxqtxb+UtVBqCtVrCwVy3Y/dNBKtZb+OjO1kMeyfA4vXLo6Y3E9t1I0qtjo6goxGB/cKtRRbGr/dmaNDEy4PHfe+etTd8vgSB6r6ukXD+3qf+ulfQDg6OnCJ7+8p6xL3VDaMfqofTuOuHhryrk/fl4tokPz7zRX8lhVM7fvdXx29qrhgX7Dg32G271OHv3dxg09entSvXnqmXcHJGm/6Ru/ad89dmrm9AdXIK9D+GLq4rXJqYvXtmEzNmMTNmGor6fV6utr6YxWfvjzR0P/vDGTh7GvAP4H2uh1wse2x/0AAAAASUVORK5CYII='
//     },
//     {
//         id: '935a19fe-a266-41a1-af99-f780fb13dcd4',
//         ma_noi_bo: 'com.sbwire.Pannier',
//         ten: 'Pasta - Penne, Rigate, Dry',
//         gia_von: 324.63,
//         gia_ban: 5009,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 8273,
//         so_luong_du_bao: 9171,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-11-22 18:20:01',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJGSURBVDjLpZNLbMxxEMc//+42Sh/b7cOjEVXV165UDw7iSkIEFxcuEpc2JOJxQkIkHnFz4+Ikri6kRBsbKhLR6ksvJEokdotVVbZLdx4OW0pajcRcJvkl8/3N9zMzgbvzPxGe7/Hs4IkRM42rmqvq7fMbL+34m0DB3OLjFRjxppImGsoaAlHduFAHcwROtV0cn5bpTM5zZKYziMjwQgKBu3Nu6KSbKaqG5rO3RJsDMWEoNfxZRFVVC0SEq7uuR+cwUFMaixswN8wdxwI1wx1i1bGIuuJuPBl9Oj9EVUXdMDeSX98w8W0CccHcKQhCmDn1kQZE5C8CooykRygrjLCsuIYPU2nUNW/Hv6OmCEplSSW7r+10EUFE6WzvCoLf9+BIosNj1TFGPg6iZpgr4oa6UlYYYWXpKswcx0j0PeDugUTwxx6IKOqKaD7rTLGa0vjsJWsH7hBKjZGNljJZE521cPheu4so5UXlqBk5z+WZmCJmtD1LsSldQPPeoyyqi5Md7mLpw266txQeCv/8Ob6sBcV5NTlKrKL110TMjfreBE37T1P08j70nGNJpJw1tbX0P/ejYYCcCIIy+L6fdVXrGR17RTKdQlWpilSx4VOGouV1sP3YLP0zKwhZsDrfQU5QM+KVrYgayXSKG/tuBQDbrmz2bHkJU4OdFN88yPfsGFPAl8kQGiI5Y0HofdGHqDIzom+zYMUGVtcEVY97gtqKxYRDhXz5ILx+F6jjl4N/PedHe1Ydz4wnO0Ia1GrI3zpc2dolF34Ah+h1f9LfEqAAAAAASUVORK5CYII='
//     },
//     {
//         id: '8153a642-d6e9-480c-8325-3d7ac2f0bf8a',
//         ma_noi_bo: 'cc.tiny.Alpha',
//         ten: 'Wine - Red, Metus Rose',
//         gia_von: 6881.55,
//         gia_ban: 6523,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 6849,
//         so_luong_du_bao: 624,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-07-23 08:45:13',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALuSURBVBgZpcFPaJtlHMDx7/PmTZq26b8laZ2m69qJS6dWUVa7oqDOQg9DUPTgwYPgRaenXeZNGEJB2UmLiiCi6MBd1O6iqzgota5sq8uy1nWbrX/Sbm2TkmZJ3vd9nt/jWxARvQh+Pspay//hPv3GqfZMKnGmtSmWwSqwgrEWEwR4XkAQBPh1D8/z0DUPU6/h1+psrpUKy/PXn3B7OltyB+7uzvR27WCbWMs2IxYRi7EWYwQjFmMMRiyBMZyfu7zjO688796RbM6kmyJcWV5FG8O53BJDD2aZvXCF++/ZhQ6EC5eW6O/fzdRUju7dnRgj3JVpI33bzg5XKRBriUYUxYpQ1YqqiVKsBnw7s4gxglgHn0Y8FWNjvUxPpgMtglIK9frH09bRNRyxKCfCgQf2UvJjGBGMWERAG0GL0OpWmZycxatsoP2Atd/LuD/kl5/Ppu1Ysq05kk5nuiKxRlVYnKewuoGvDVobjBECAyOPDXLnvj65lt/KrWyV5er82ePKWsu2w8cnPn1ooPe5bF8PbYkm9nY6/N17ExeJNzWytn6Dc7O5U5+9+dIhQg6hZ499sSvZnniqK53EN5Bo4F9S7S0sLG/Q0Z4kmkgN8yfn0SMnVK24eTLb2xVHuaAcWuOKfyqWp6lWxzmbfw2JftDx+Iuj3xCKLE2f5Pwv5Xf37cmo1bKhrTlGX7oBR/GXie9PsFg8zSP7Bzm4fxQ/dpMS+b63PjnW4hKq++KsrG9ybeUWdS/F5EyeWt1HG8ELDJXqRzxzaBTjGO7bOcLpy58zdO8wS1e/POwQ0kFAYARfaxau/0bpVkDFE6qexfOFUuUGUZXgyf5X2Xbk4Pvs6RwgFHcJ1X2fWs2nO9WA0S5iLWIErTXWRvnp1xL5wjQ/FqY4OvIhY1+/QDzSQKjuEvp57tJC9eZaNhpvRoxGRJDAYLWPaEujGmTm4jTDAw/zVW6cmHKZmjtD6B1lreW/GHrl9jHgZaAF2ALGZ94uHP0DwIeG5DahstsAAAAASUVORK5CYII='
//     },
//     {
//         id: '57b7000e-1b6b-4ca0-8493-8cde30d19879',
//         ma_noi_bo: 'br.com.uol.Zontrax',
//         ten: 'Beer - Blue',
//         gia_von: 5377.5,
//         gia_ban: 8108,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5658,
//         so_luong_du_bao: 8256,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-03-04 19:12:45',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADwSURBVCjPpdGhSwNhHMbxX93f8WpRs/VAGXYF0SA4DgyGsaDBwdQfJoO23dzumsFgfoNcMEwss3gKr0G4uuzSC6dfy8KdDBHkU79PegT5nfw5SEiIiRnQp0dEJdCaBgmO4ZQjYi/YrU0DDXVyVVlHdDlhe7IRIqL1AU/kMww5YK0ummWM6cww5oHNd1FfUDDi44dHCgpaX3KMx3NLzqgk5xqPp4G0cTh6vHFf4rjE4QiQQ1JSLnjlruSFM1JSFpAWFksXRTmiSYN1VlmhicUyj+x/3mBRzitOWcISY5Cwv5NtVdbLLGIwzGGeTVv+/eY3onJlSlwTV9MAAAAASUVORK5CYII='
//     },
//     {
//         id: 'dae14689-56c4-424f-9a41-172bfca6ed11',
//         ma_noi_bo: 'com.tmall.Duobam',
//         ten: 'Lettuce - Romaine',
//         gia_von: 8500.78,
//         gia_ban: 9026,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2867,
//         so_luong_du_bao: 3668,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-11-26 08:40:43',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEXSURBVDjLY/j//z8DJZhhmBpg2POQn2wDDDof8HvOe3osYtXzDzCxuM2vP3gvfn4MJIfXAP22e0Ies58eK9r2+r//3Kf3YOIhq17eK9v95j9ITrv2jhBWA/Ra7kVEr375vXDrq/9+s57eUy+4IY0kJx2w6Nk9kFzE0uffgXIRKAboNtxlC1/+/GPljjdABc9+q+ZcM0Z3qmb5LWOQXOmml/8DZz7+qJB0hQ3FBerFNyNC5z/9nrXqxX+Pvgf35OMuSSPJSXtPfXQPJBc089F3oFwE1jBQTLkiZNtw51jq4qf/XVvuwsPAa9Kjexkrnv8HyclFXxTCGwsyERf4LctvHvPuvAePBf8pDz/Y1N45BpIbKUmZFAwAR3nW32nUrY0AAAAASUVORK5CYII='
//     },
//     {
//         id: '80d9bf6a-aaaa-41bf-bbda-ca57bf005f76',
//         ma_noi_bo: 'com.cbslocal.Otcom',
//         ten: 'Wine - White, Ej',
//         gia_von: 2707.2,
//         gia_ban: 1455,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 4389,
//         so_luong_du_bao: 917,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-07-26 16:00:17',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHnSURBVDjLfVI9bxpBEJ1DKMeHkRUbCWIlSuE0KamuIVUKp4pSuI9cWPwFKhcoBRWlFTBtKij8CyIRIUWWUCQXprdEQcBCCtwdHPtxmVm8aM9HMtLT7e3OvHnzdq0wDEHHhy8/wrOPb+Dn/QwkD0EIqSARnEsoHx/C5bdfMLg8tXRNEoyQQoCUIXD2WMg3X8E34ETKhVkSJdh2Y2JTZBRvCZ8QJCIK8NDzGRzlbGBrEcGr/TS4foBE/1UgaqP7h4v3b/fgpPAMLMmA4Th+IOFP4MH3u9/UpPZPgq+fX4+8FXNfvni+pxRJCdrk1WoFbH7rvjuyR2aNpROGw6HDOb8uFArFxWIBy+VSERASiQTYtq3Q6/XGQRB8qlQqNxEPGGPVfD5fnM/n4Pu+mlWoW5F0Bq7rqv1SqVRENdWYiZhUJjXUGZVsuxM0med5kM1mARWUYx6s1+sDStbFpgfm/6MfB7sItsnm9+magAri7wA3Z9pQPbuWr9eWZSmFlLuLoE/zk9O6qwnaS6fTMJlMSG0/RoBz1QeDwTiVSkEmk4l4QJ3JvGQyCd1ud4wE9dg7oGi32+eopOY4TjGXy6k9rWY6nUKn0xnjbV00m82rnQQUjUbDQZIqdinTzZC5WDRD9BH1Vqt1Y+b/BXpYxDgsNaz2AAAAAElFTkSuQmCC'
//     },
//     {
//         id: '786fd9b5-9581-4492-b793-29d74cfa3b80',
//         ma_noi_bo: 'org.bbb.Kanlam',
//         ten: 'Raisin - Dark',
//         gia_von: 2836.55,
//         gia_ban: 8785,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2899,
//         so_luong_du_bao: 9295,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-10-13 03:29:42',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIpSURBVDjLpZNPSFRRFMZ/749/Kt3IqFTSRoSMmrGIYTTbpEJtjBCCok1Em9JVG1dRC8FFEES5aGFEgRRZWq1iLKKxBiNqLDcltQgmHR9hY6LOu+feFm+YGVsZXbh8nHO53/nud8+xjDH8z3IB7r5avGgMZ8XoBq01okFpjYhGtEGJLtmCKINo/XbgVFPUBdDG9PVEq0P/UvnSvdlwQYFoHQIY/3obpRVKFL5W+OIXUVThrL91AN+XihKCwIeTu85sqPryqsJXUvRARAMwkshsiKB7fw25UgKVJwA40V7H/cl5jh+oL+RGk/P0xIqxl11dr8AXjTYG14HRNxkcx+ZhMoNlg52/ND6VAWMoc6F5+2Zy/l9PMIDrWByL1jI+tcDRaN06BaXxbDqLUnq9AqPBteHpuwUcJ0AIcgBXH93h+/wEyyuLrPk5cmv7gNY8gdIYYyhz4PDeWuIpj85IsS2ujQ2zJAk6DkZpqGnixcwYyU+PifUOX7Eh6DoAx7aIpzwA4imPeMrj+bTH+88PaNkZQWwhsrULsXxie9oAzgcESgUe2NAZCeE6AXZGQhwKh/Cyc5RZVXQ39wFwoeMmjXVhgMqiB8awe0cVP36u0Fi/iW9zvwuzkF3+xUz6Nal0gv6uWww+O02lUwGwmv8FM3l55EtLTvQWXwm+EkRpfNEoUZRXHCE5PUFbuJ0nH4cot1wSH14C3LA2Os6x3m2DwDmgGlgChpLX0/1/AIu8MA7WsWBMAAAAAElFTkSuQmCC'
//     },
//     {
//         id: 'd89448e5-00bd-497e-820c-a97973864326',
//         ma_noi_bo: 'com.usatoday.Domainer',
//         ten: 'Cheese - Colby',
//         gia_von: 2120.1,
//         gia_ban: 5331,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5608,
//         so_luong_du_bao: 1351,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-02-06 12:54:24',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAG/SURBVDjLjZK9T8JQFMVZTUyc3IyJg4mDi87+GyYu6qB/gcZdFxkkJM66qJMGSNRBxDzigJMRQ1jQ4EcQ+SgVKB+FtuL13EdJxNDq8Ev7Xu85797T51nwhqeAH5w6cAxWwDgReX7jwYfdaCIraroptB7NLlVQrOoiGEsL1G06GZyxuILicsMUH3VTlOqGKNUMUdTacj+j1Nng0NGAT2WxYosK1bbIVVoiW27J9V8G57WWKVSczMV5iK+Tudv1vVh5yXdlLQN+os4AFZss2Ob82CCgQmhYHSnmkzf2b6rIhTAaaT2aXZALIRdCLgRtkA1WfYG4iKcVYX52JIs7EYvFmJ8wGiEXQi6EXAhdyn2MxQaPcg68zIETTvzyLsPzWnwqixVbhFwI3RFykes+A9vkIBKX4jCoIxdCLrI4/0OcUXXK4/1dbbDBS088xGGCCzAJCsiF2lanT8xdKNhHXvRarLFBqmcwCrbAhL32+kP3lHguETKRsNlbqUFPeY2OoikW62DNM+jf2ibzQNN0g5ALC75AGiT59oIReQ+cDGyTB+TC4jaYGXiRXMTD3AFogVmnOjeDMRAC025duo7wH74BwZ8JlHrTPLcAAAAASUVORK5CYII='
//     },
//     {
//         id: '92b11ed4-9cff-4920-9247-81bf56e9b430',
//         ma_noi_bo: 'com.mac.Konklux',
//         ten: 'Cauliflower',
//         gia_von: 3974.62,
//         gia_ban: 7711,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 6212,
//         so_luong_du_bao: 1865,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-12-04 06:21:33',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAH8SURBVDjLjZPfS1NhGMdXf0VEQhDUhdCN4X0IYT8ghIJQM0KoC4vushZddLELKyRhQQkSFIKEGEkUCI2oxVhepG5zi1xbc0u3cDs7Z+ec/ezT+x62scmmHvhwDrzP93Pe57znsQE2cR0SdAm6d+GwYL/M1LBVBV35fF4plUqVcrlMK8Q6TqdzYrukJuiW4Vwuh67rdbLZLJlMhmQyaUnigVlC05f4+dbB0tQplp92DsnwPimQBaZpUigUrLtE0zQURSGVSqHF37DhGkVZeQdagszKLJ7HvZtNAhmuIQWGYaCqKps/ZkivPqCwPs/Gp0cYvjnKUTe+F9fMJoFoo96zfJZ9K+sLpP33qRhujPANtr7dJPhqmO/PBxX3+PljTYLtqImPpH13qZge9LUrmLEB1FU7sZd9jJw5MljNthYk/KLnxdFqeAjzdz9Z/z3Ck2fRE36qx9pakAjME1y4Lbb9GTMyTD52GUXsZO3ZadTkL6umrSD4ZZrAezvLH54Q915EjwywtXSH8FQf+t+I9V12FLwe6wE1SmjyAi77Qb6Kt3rGe9H+hKzwrgLH9eMUPE4K3gm8jpPMjRwlHfNTLBbr7Cjo7znA2NVOXA/PsThzi2wyah1pI+0E/9rNQQsqMtM4CyfE36fLhb2ERa0mB7BR0CElexjnGnL0O2T2PyFunSz8jchwAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '605a2bf1-4520-4b10-ba57-5658da23e5f6',
//         ma_noi_bo: 'com.disqus.Solarbreeze',
//         ten: 'Cleaner - Pine Sol',
//         gia_von: 4401.73,
//         gia_ban: 412,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 768,
//         so_luong_du_bao: 9125,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-07-12 19:46:10',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGeSURBVDjLjZLdKwRRGIf3X1wXuNAikiKfWXaVLO1SIjYlG9LYaGKxNSXWx1rnwoUo+Zhdu2vkRsp32ePCvH5nYoaGNaeemjkz5/ee85zX5W6VS4bjuc3uhSzzz4NohnlnT1nHzAlrnz5mbZEj1jJ5yHwRxsS/ROT6jiu4lEuF12+YE5pHd1O2AFHZKXVDSWYL8EcvxKQjaga27AG+ubTxUUllMlOJq9fB1Us+sAJieR5azPJ+Oc0DC2e8N3rCmyYOOFxocOGxAiTVCBhTtMJ08pYXY1i55nChwUXeDGgM7xsBovJ/dErnHC40uNDMAGynr35kj3VJKn98eQOcPzwLCib3gqcCf3l9e8QiDS6sgK8HuBCTWnxHvRtT8joqEfqC0BeEYxJ6g9AXhL4g9AXBBaF4gxUgqUZAKJYjnNMRcPFuBsCFESAqOwUurPvEdsbhQkNfkNMBFz+b4tPFnwt0gS7Qjfeq4MYvARBWbHyFiOEJrNkD4MLxEdxtsj0ALmS4MATVDm5TdTBBlf3rVNGjUHl3nMp8y1TqjYkrFMgf+hUje+AiV2IAAAAASUVORK5CYII='
//     },
//     {
//         id: '4d570559-8003-446f-a0c7-080bc81d1076',
//         ma_noi_bo: 'com.blogtalkradio.Voltsillam',
//         ten: 'Wine - Sawmill Creek Autumn',
//         gia_von: 2.72,
//         gia_ban: 6096,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 7601,
//         so_luong_du_bao: 3200,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-10-04 20:46:28',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJBSURBVDjLpZNLSJRhFIaf/1JO3lAbZyjDbpiXRC1FFFsUtJSwhMBsFy3b2CZaRAiuWre2opDCBCtalBl5qzSjhAwkqlWljRZizuj/fee0mHKmaFF04PDCd+A557wfx1FV/id8gKvDX8+pctKqbBERrIARwVrBimKspKXFGsWKTHQdL6n3AUT1VGt9TvhfOp+//qZqbQIrEgbof3sZIwZjDYEYAhuk1Jq12pnGLoLAZqQBkj4cqzjxV92/JQyBsSkPrBUAekbm/gpwqK6A1XSA+QEAaNsX4cboLEebomtvg6P3KcsYJVrVgbc+l9hi4tcJAiuIKr4HNx/P4XkufU/mcFzwNcHOxF1C0SaejfYwn9tCeVEmq0ES4P5cQQHfc2htjOC7cKQhwuH6CE0bx4kU1ZG7aQ8FyyMc2LGY/FojKUBgBRXwXbgz+RnPS+q9p1N8mu4nJ5yHXbxFuLyNqaFurDEo6QAjqCrrPGiuLSTkOzTvDbPbGSBa1gKJ54xfuUZ2XpyMhQns3DhZGX4KYCRJ81yHgZcxAIbHHhIsLZCTv4ysvAMV7NIE2/d3EJ++hGfjaQBjkh64cLA6jO8aovO95BXXIMtTqMSpbS1FVj8QCs1QuK2JamfwNw9UqSzO5uNCnM3Ljwjll5CZE0PNPDgek30zgCCJGQp3baVs5QFDnZUVjqpyuvv1iAlszaqVrMBY2vUsDe0X8dz3qPkCpF+si7ehlNlXL5i53dnr/Omcxy7UxtRKpoqiImmpa4oIqjr5HZAwT/Ha9ghQAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '879e2fd4-a912-4640-a06a-e9878ca658b0',
//         ma_noi_bo: 'com.photobucket.Transcof',
//         ten: 'Bread - Multigrain',
//         gia_von: 8025.62,
//         gia_ban: 6941,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 4107,
//         so_luong_du_bao: 275,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-07-04 05:50:19',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ8SURBVDjLlZJZSJRRFMd/39JipZXZmCURrRBjKshkJW20CFGSQlAotENQr9FT1IP2HoFPFRFhCSMYGS1GEUUrkZHY2DItUs30kTnjrN9dehhzFAzqwuHcc+6f3z3ncIyL9/uPa80BqVWxUgqpQCiFlAqpNEKqESaRQiOVetZYv8gHYCutj9T5cgv4j3Piyrtlf+62VKoAoP3DBYQSCClwlcCVbtZLMfx2bEUjrisnjABoAHYt3fdPv8eSAldIsgCpAGh5EP4nwLaKfNIjAWIIALCzykPrwxA7VhUO5/yPQ9RVZmMnkhxdgSsVSmtsC/yPwliWSdvjMIYJ5pCo/WkYtKIodJ7C8V/wMh+oAMCUUqEB2zKoW+HBNqG20sN2n4canweAGp+H1RNusWBWiDlVu6ld8poXTYurAYxTbZ/0oU1F3O/5hWGM7tc2IZZS5DvX8ebdY3rpRmJ97xFRxcC7V/HAs679tisUWmvGWVBdPpPOLocNpdm1uP3yB4tzAkwt2YKMdzOxoJik7KNoTdkkEetvtoXKDNEyDTq7HIBhbxrgKuiLz2Ba/0dMy8EwY4zPcyAZZ/BbyrKFEJkZmLCupIB7rx3WejMVJMPdBB+dIX+6RokEpvoFZi7aTfPmciDZHYgctF2ZacE7dwrffiZYUJhD8Psg8vMNjM9Xmbe8Cp2+hSGj9NxMYSWTJFKSjx8i9Q0tQb8tpX54suVtWVqqya6QuEIiheLw7A7K128g7TRj21N5dSfNpU+bCedVMBiNd1y7UOMHQGs9pn1pq36SCjXpWGCrfn56ZfTsnoUNY+nsv63s16DTGRm4Oy/+M5bo7f1xdO+5t61j6X4DmUx477d3qncAAAAASUVORK5CYII='
//     },
//     {
//         id: '8a017d2b-c5d4-479c-adbd-c226a7d17398',
//         ma_noi_bo: 'uk.gov.Stronghold',
//         ten: 'Cheese - Roquefort Pappillon',
//         gia_von: 2552.09,
//         gia_ban: 5824,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 852,
//         so_luong_du_bao: 7545,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-09-26 06:29:43',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAK9SURBVBgZBcHLi1VlAADw3/edc+fRmOP4YEzERxQYZGaQaQ8qRDCK+gPcGC1rYbjRWtqiTaAULWrRItwVVAaFBUIvhqjAyixIE41JB8fxzsy9c+855ztfv1/IOYPDH1/bg5N3rxnb169b/bpVt62Vpu1iCTeRsIB5fIizJUDbNI/s2rhq39EnNwCAXtVO9qt2cmGQNlc1S8Pkys1lX1zqHcCREqBtmunVIwFcu510QlAGipLRTrRlNCpi1CnYvXncpsmJte//OPtWBEh1vXqipGlZqoJuze0h3QHdAfMrzC0ncPz0Vfu2T7h/fWdDCZDqeu2dI1FvSG+QxBiUKApiQSEoAi1CWjRzecEvV7uzJUCqqunJ8UJ3pdEfZjFmRSSmoIgUsqJALtxYHDr11d+LOFwCNFW1dXp1R3eQNZApUhAzEoWszFGbSZ2kqZrtn7762K0IkKrh1o2To3pVFiJFCCIiAiBkcqYZDqVqmKCEgye+HC+LODLaiaqURBlZRhJAQIzUKVnu9RssQgnNsNowMTEmBlrIhEAU5EwIXLx0xl+XP7fUXzAV+0V3+cbrHHyjhFQN7ygnRpSRIgapDeSsRQj8+udH5vtfe/rxh21ee69zFz4JM79fP7H3lU1r4hNHTq9vqurEnh1bXF/MrtxIbi0lvYqUsxCyny6c9uCOXVJMdt11QAq1vTsfhZfLVFX78ezPF/+xsFJaHmZ1yoZ1UDWtJrWWuv/phFWeue8lcHT/e8789i4+GytTXT/0wlMPjL92aC8ASJk6ZVXD88e7Lsz+4Pzsd44d+MCbZ180VozCoNi48+A9U5MTz80v1a7O9cwtDiz2a3WTFTEa6QQpDX3zxxnbpre52f9Xtzfn+/PfWrw9PBV2Hzq5HkewFeuwDlOYwuTYSKczNtYRRs5ZSTPaPEDok9+eeWf22P/PLlOL9Py8xgAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '32b176b5-0fac-4ba9-834b-5cb3a7d1f527',
//         ma_noi_bo: 'com.elegantthemes.Zoolab',
//         ten: 'Cauliflower',
//         gia_von: 7645.78,
//         gia_ban: 9795,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 4279,
//         so_luong_du_bao: 3596,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-03-06 13:43:01',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ5SURBVDjLfZJtSFNRGMenRkgY1BKiL30yEkqJrCjrgxBB5Qtmyy3NcGoUuqD5skEm+ZZizpTUmZEw33ML06lzGoQKtRRETXM2Z1LOTBs6LNNw9/w7d+IiuevAj3vO4fx/z+E5lweAtxVRvp5Pqaf8psAF3RQfngtBa1OvCet2Bq5Ge/80K5nkCntR7AwhsP0imF8msCwRfF4k+GQlmFxgYF7YEKerDJzV90vKexwHZm0EX2hw6juBaZ6B8RuDsa8MRiwbggL1IP57A7b6NK36kYbH5xiM0vCwhRXYHYKMmnd/gwlH+dvunPTOehy623ZLlrfO9oCVbA72JsMzjEPK2QP5Gb5UGewJxcXtKBLsQ2JKBkR5OkfHq/QfnKKlH2uONd0f/ecVioM8OzXyC+hRRKFAeBC3A3dAfHwn7ob71tCD5rnFlc3gKiVjM+cUlEbsqZ4xqLE81IT3Lx6gXyXDUMsjpGQqRip1Y2zwJ0W6tWfOyZUQQepEYxpZHW8FTFqsGdvRX5dORLlaKw0mcP0vTsHekAYPXkDFE3VxNplU3cREXQrMdRKoCnOI+5Gycu9zlR4uBbvON7l5nNbkykunGL0VkGvfQqo2QFJtwLNhIDHfZHc/UZvpFVThxik4FfEwNS2nDc+NBMkDwI0+4LoeiNQAV+sJcrsIxMnNJDD0noxTMFt4CAPqUiSp5xHbAcRoCIQ1BBFVBGFPAYFiAYPNSkxl+4JTYFYGv6mVxyBU2oe4LiC+GxDrKPR7rQU4G9eBl/ejMVEW1sspMDUk8V+VxPsHRDZkHbjcZvGL7lrxj+pe8xN2rviEa63HLlUVvS6JPWxqlPC5BH8A3ojcdBpMJSoAAAAASUVORK5CYII='
//     },
//     {
//         id: 'dff11cea-950e-432e-a06c-40cc0fb45686',
//         ma_noi_bo: 'com.chron.Latlux',
//         ten: 'Creme De Cacao White',
//         gia_von: 4787.85,
//         gia_ban: 8162,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 1821,
//         so_luong_du_bao: 4935,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-07-05 22:02:27',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIzSURBVDjLhZNLbxJRGIZP22la6KLpou1GN11QKGW4DFTH1phorZcYi8R6w2ooAlUSjUStC39A49I/4sK9f8OFLoyCwZAAgsMwc87x9RuoSXWgLt7MvGfO+3zfnAsDwAaJT0094+PjlojHTc7YpWHz/g4x5hELCx16Qm5eg9zOQGZzEMeOQyQSsLze8v8Ab2TqOuTT55C5R5CPS5ClPYhsAfJJCXY0htbkZH4ggFp+LYIq5I00RDoDQz+DRlxHM3YSxsoq+K27EDt5WDMzlsmY5gIIn0/Il6+ocpECazDip2BrOuzYCViRBMzoCgT9El+/gEYgUHN3MDLSlMktCFKTQk5QRCgUjoOHYugsRyC3bqObvodv09NlF8DWtKYsvYDM5NCmijycgFA1iOUoeDAMM6BCXEmiq6+ioihv3YC5OdlbtJ0cOlHqgKryYAR8SYXtD/UAMr+LzvZ9VP3+7y6AMT//rq1R1UIRfOMyTAKYTnAxSOEQOLXe2kziq28RHxnbG7iNrdnZ991kCuJmGnK3CJmiNblKPvOAdiCHaiSKT4pybug5qDO2VB4bQ23tNIyNi+CFhzDObqC+fh5lVcXnQIAfeZB68nqloSiojI7ii67jA73XJibQIG8zJo8GeDxZUAeShi3ST+fzP/4HY9nhAMYscTC5Q2oxViFfOeQdgDUQ8IuxO04l+wBg9kP75PcP+V4XbZrrAlC4TmpRxarTBV0sy3ZuZ18W78OrpBYB639yvwHcmnk0jmx5QwAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '4bd2d6a9-43a7-42d3-83d5-d5b413537e9b',
//         ma_noi_bo: 'cn.com.china.Flowdesk',
//         ten: 'Beans - Long, Chinese',
//         gia_von: 9451.6,
//         gia_ban: 9683,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 177,
//         so_luong_du_bao: 7425,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-02-07 19:05:23',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIiSURBVBgZpcHfa81hHMDx9/l+n82x0zadyBabMFtKSS03XGxXUlMspqxcufGrcOMPQHHhR1v8B26Wxi7ccOlKLUlqTJvQ0H5gv8737DzP5/OxJ5RcyPJ6OTPjf7juCwOn93a0X/tSsoIZoIqpIiqYGmqKiaBmmAiiSl21XxwZmbg4eKPntmvf095XlS8k6/OsRGGxJH3AbVfylpS+LLBSmbeEZc7EuNLbwkqduPqOyJkpUSUIBoT5Z5Q/9mMSMFE0KCaKeI9Rg990mc3NWxEVIocZUS7JkVNB5p6wqmY9adU6zAfMB6SSkX19hTX0UFVoINIgRE5UiBJAwhSV6UfUbugizA6T5HchMkP52yhp4zFcQxdiSqSiRM7U+EFYmnpIdW0LkKLZZ3BLzH54zOotZ8g3HUINUk2JVIXIqQpRKI1S/nSfuqZ9WMggbQTvMXXownsWR26hPsOSOvJtR1BVIhe8J1oYv0l1/TaymVdoUKprWkAd9Q2dZDMvEF9h7uNzirsvka5aQ/CByJkoUb7YSShPQ5KQSyt8HR+irrGDUP5GpTRNUr+T5u7ruNomkjTFVImcqhIVNvbyi4YFZl/fpTT5kvL8JMUd56hvPczv1IzITbx9N3Dypu9REVQUEaF3Yz9ta4s8fZMw9Pks2XCKySBqiqmiSjTAMjd4o+cocJSfxu9t369e7mQzcv7gqbEHB/k7xx/Uy4R6OdB6fOwl/+A7Obk497M21x8AAAAASUVORK5CYII='
//     },
//     {
//         id: '0ba0ae1c-e0cf-40ac-a9e0-4799aa6ce984',
//         ma_noi_bo: 'com.foxnews.Konklux',
//         ten: 'Wine - Ej Gallo Sonoma',
//         gia_von: 6189.13,
//         gia_ban: 4064,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 802,
//         so_luong_du_bao: 7208,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-08-30 20:06:42',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAI4SURBVDjLhVPNaxNREJ8k2yxuDMGEkGi0QltREC9+HQQPKl5EkBz8D7xI/QDxIh4KpeA5lII3IbeAF6UKHjU5SS2op1JKTFiN5GOD6WY/s/t8M5u3bkvBWWZneJn3m99vdhJrNBqrjLEH3MH3fRBRuOd55NN8qVwuL0PU6vX6N13X2f+s3++zWq1WxwZRlxAZbf37CBiP/IzcxwL4l1+bZcRkvxGAG2DAscxMAIAPRaBXS3NIguu6VNdefHoPDPP97Ku1X1KAynXzwuGuBdudMSiHkmHnoxmJxwRdnEwm0L7/5HkK3BV9bGzwo0uBhKmeTEqGCwvyHimYDwcBgxPq7yKMjTe2Y68cjnsXf1y/s0gMEjEAj1e2h+4e/X5kHv6Xr3C+rS7ounHbNa3HcpJVYGw+JADsfuuMHGok6VNWaMmkDNrnFEx2deAMXpz88DrWvHyzApZ1WkJdwhzHgW63C5qmEeVsNgv5fJ4D8JkguGEBGxtBsWECs20gAPEp8bKqquFC8f2gPJ1Og692QHIdYIb1rHn2yqO0IsHoj70VFwywcDAYUCyVSuQI0uv1KMZuXIVP2SM7YJrrM75XYY4NYNmr8agE0VloF/MRM9mcP95hln1XTjAY/RxuzPW21iQcnJCAmpF2q9UKgfBMAGCzuebm0k7uVAecyVvaxCgDHBh25XtPF3K5HBSLxRBAfKX5wfbLcJXxEH9UFIW8UCjAQYY1B/4XOIOP1Wr1HDLBgmgUecTf7Qf4C2kj+HVimC2aAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '7b5f5e4e-0fd8-4659-b172-6a2a90f45230',
//         ma_noi_bo: 'gov.noaa.Cardify',
//         ten: 'Lamb - Shoulder, Boneless',
//         gia_von: 6750.19,
//         gia_ban: 6840,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 6801,
//         so_luong_du_bao: 2866,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-05-20 13:07:29',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFuSURBVDjLpZPdS8JgFIf9F6UoieiDogKllUNhM4IIKYhCRAqlIkqKmYVGERZFUZB9kBgJ9uH9dARdeDfn6T3ndbJFN7kfPDDGe55zONvrAgCXE1zuacW7nK3UQ3ufIO8yku8gbZchuFWCwOYriBsv4E8UYCr+DMLqI/hi9yAnbutYR4JwumJEcyr8FyFybZAAO3fC2NIVkEBOfkAnGVm44AJp541eFItFAjOU0QhMvBAhms2m/Uz4rCVgC+tEMDB3ygW47b+CBSaGYdjA9M+ecAF+qt9dzU5YDCmRwMLv0jiB6Zs54gJx3T62dVTq1hI0Gg2bwCNnuEBYe2qPbB0TC6zout4G0x1Ic8Fk7IELBC+Bh83F4bM5DRZal9glprjAF83zreXvOCzlrxKBUVUVqtUqPddqNdA0vie3XwHzLigTKzf0Z40uXsJw+BwG53Nsy8e0KE8oC73SIfQED9jY+6yzAqwGUUjg+DY6FfwA5i1AjZKvAWgAAAAASUVORK5CYII='
//     },
//     {
//         id: '4d55c54f-3da9-4093-a817-4669afcf17a0',
//         ma_noi_bo: 'com.baidu.Opela',
//         ten: 'Coffee - Egg Nog Capuccino',
//         gia_von: 679.1,
//         gia_ban: 9674,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5378,
//         so_luong_du_bao: 4758,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-11-23 12:59:19',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKASURBVDjLjZJfaFJRHMf3sOhlSG8RvfbQS0G9RNBLPRUEPfQQPQQrVkH1VC/BegkEL1O7ouhwORS1beI2///Z9TKk2EMEM3QqTkQKdU5S58VhE9q33zmkrRhjB36ce889n+/5/r73jAAYOaoikYgqGAxKfr9fWFpaGv3/+3HghM/n6y4uLioej0eYm5sbPZZAOBxWBQIBBu/W63V0u10QvOdyuQSHw3HySIEBvLy8vFur1UDPoBagKArsdvsvm80WslqtJw4VCIVCKtosD2AGzs/Pg9pBu93mTghWLBaLYDKZRg+FKazdarUKss9sgxxhvViFGMnC/+UbzGbzvtFoTIqieGoIU1gqAmQKi8PkAG63GySKZrMJ80oeE+8/45VrHd8rNRCs6HS6a4fC5AAUFu+90WggmUziR7OFSU8Kno95BOVP0Gq1fUEQbnABOk32er397e1tkAicTifvncEkjtXVVZTLZWQLJXwIyAzeI/jyMESCpYWFhf1KpYJisQhJkjjMHESjUZRKJbgiVry0PMCTd3dwX329e+v1xTdDAUpZSafT2NnZwebmJgqFAnfAks/lcrD5DHjrfYjwhhlfaxLExDPcEy/gyvMzOi5AYW2tra0hFouh1Wohn89zkWw2i1QqhXHhJgIZAwI5I9jQyY8hyk+ZQI8L0M06PTs7208kEvzXsdQ7nQ4ymQxkWcbtyUuIbthwcATTZibw9w7MzMyMTU9PN1jP7BeyvuPxOCis3tUXZ39qpUfQSOMc1qyM/+tgUHSzxgwGwxbrnwWo0Wh6arX6HG1U39Wdh16a4Cezmb0PMzhYer1+bGpqaotghcGDddosUCnM9p9ZYOu/ASUg4G4xOdG6AAAAAElFTkSuQmCC'
//     },
//     {
//         id: 'faf45761-832c-4153-b743-f6818496981c',
//         ma_noi_bo: 'com.yolasite.Biodex',
//         ten: 'Mountain Dew',
//         gia_von: 1288.96,
//         gia_ban: 3436,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 445,
//         so_luong_du_bao: 792,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-08-03 05:02:26',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ9SURBVDjLjZHdS1NhHMcF/4G8FgoWEQgVIhFKkNZC7CLadL0osxwy1CbNt522JAzfljrb9Ax1LtfUTTcSCkTCJV3VRe2it4vV2Vp7zs45bpAXkZAv9O05A6E3bRe/qx/fz+f7e54cADnlY6LmLJuMKUd5qF0izgwTFPbErfLuf5NT7hQV58dFlA6TWMkgyTveG7de84gouBULZQUotRPFSRsJFfcnGGpljt6Oh6+MJ3GwncuuAbVqqDVWNpSQrWHDjISSO5+Qb4goswKcHk6AWtcOm7kiatVogyNQeXs3dI8v52YF0DiTOMRwaweMHFM5O/TV/sqKwRfdUHvqnqndFbkVjrLEzYfiABMUHKaA9L49IJIWv0CM04TNAKg1tt/4AfnXI2H1g56X1uddWIo+QueTDtDw+qne4i1TQJDa54VvI6E0RpdTuLsowTD1eTUD+LNS9ZxqwLLUgoWIH753UzhhKdxuC6YVRr+weWOa33Y+XUNHII0Gr6T8J0Aeat6YfjuJ+6+dOGYs+G70S1yzl2w0echy/+IXtPpTqHfvAqBhgdaWzXIY5/r6tjqDSTS642+o1WwOpmGaT6HOJTK7NtiZZl+qqHVWQMNkfFPvFiqplWmj9lbfKrROYW9A00wqr9Er8l1BHvXO6IKO5TLfqh0TYKKQ6lF+Yk8ArWs1+xLQsdH1Oge3Umv/uFJji6wYXHGY51K4aOdDuwL0U5K+w0erj8dAg0d+3V2y87BQQJWNhP8C6D1SXuZOeuNVB/dDa48GalhhXybo4BXUyjS4BHQvpFHLJnFhgCh/A+gmRT19XdTSO2tYIt8JGiySd1X3+IlKG4FqiMjBnZn4CaGVtXQxzDRMAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '3b73f2d1-5b8a-4e73-b637-8058f4387977',
//         ma_noi_bo: 'jp.jugem.Tin',
//         ten: 'Soup Campbells - Italian Wedding',
//         gia_von: 3590.17,
//         gia_ban: 7948,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5621,
//         so_luong_du_bao: 8316,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-04-21 07:27:25',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHcSURBVDjLhZPZihpBFIbrJeY2wbcQmjxdIGSSTC4zQxLyAK4o7igoKm7TPW49LoiYjqLG3DWpZmx7/tQpsR1xycW5qTr/9/+n+jTTdR3dbhftdhutVgvNZhOapkFVVTQajSsA7FKxTqcDx3GOajqdSki1Wr0IYeRMAsMwpPNkMnEhdCZSoFQqnYUwikzN5EYH9XpdNU0Ttm3LcwJWKhXk8/mTEEauu0YhfhKRDcuysDBt5H5tk4zHYxSLReRyuSMII+dd5M1mAxL//uvgw8Mz3t4DWWN7NxqNKAXS6fQBhIkZ+Wq1kk3r9Rpz4XytPeNLF/iqAx8f9pDhcEgpEI/HXQir1WpvxIx8uVzKps7Kls53AvCjB3x7PIQMBgNKgUgkIiGSUi6XFTEjXywWsunxj433qoM7fQ+51oDMzy2k1+tRCoRCoSt3lkKhoIgZ+Xw+P4J8F4DPTeDm3oK92aZIJpMIBAKvD15UzKdks1k+m81cyDsB+SRGuG2tYVpPL8Ued4SXlclklFQqxWkTCaILyG3bgWXvnf1+v8d9xFPLkUgklFgsxmkTd5+YxOL8QHwWQBWNRr3ipTktWL/fPym+CKAKh8PeYDDISezz+TwnV/l/v6tw9Qrxq3P3/wBazDrstPR7KQAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '8638fa91-9f42-4099-a00f-6e3024a9dcca',
//         ma_noi_bo: 'com.reference.Trippledex',
//         ten: 'Flower - Daisies',
//         gia_von: 982.26,
//         gia_ban: 5625,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2987,
//         so_luong_du_bao: 6322,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2018-12-17 14:25:55',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGvSURBVDjLxZPbSgJRGIV9BB+h+yikuomIJigo6UBnKtJOUFSkSBIhMUZmBywUNDtQG6KMCrITXVnzCANSYUNNOoMzzEjuR/jbXWjQjN0UdLFuNnt9e/9r8RsAwPAbGf4c0BsSi8b2JOS5UN9cpwo7d6Kw82fqW19IRK0rqaIfAb1B0eS7zeC1mwzu9AtU7pwYKfe5iukzBZsXeJMuoCcoGH3EGI5loXPjy5yTeZGnCBhmj2Vc53oxagBdfsG+y2BwRhS20LzD2yK7eq0C5eTsGsD0gczs3GeBfJcuBKid5WjvpQrta0lGA5hAEhO+y0KThy8IqHZw9GJUJY/oALr8KRSOvUN3QIgWApjdr1FPVPkcAWkAjW6eWr7KwExExj9kgB2HEpSNPlK6NTYv8WjpQoGaGW7wu7li7GnQeSRDtf0Z6dbYHUgxxGhqcPNofD+NK6cS+arKR5+M/SEBV9kSqNT6YKp3cdoMnBEZquzPdOV0gupYT7JtvmS+zhYvz5Jw2RJLnCoeiNPWTRE0AMeRBLYDCaZQGiaJxvfS+Usj2yIMEVm3RLAQ84Ae4N+28QM8btMbbDzl6wAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '9c6348cb-c2e9-4931-bb1b-32e59004c1a0',
//         ma_noi_bo: 'com.instagram.Bytecard',
//         ten: 'Pumpkin - Seed',
//         gia_von: 1249.17,
//         gia_ban: 6615,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 7229,
//         so_luong_du_bao: 1891,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-07-01 01:30:54',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKsSURBVBgZBcFLiJZlGADQ87zfN/N7GSe7UBiIEmUaRKRJRogGpmEmRglRCIZEi2oTRbuojQQRdDGVCDICN4ELISMoJG/jhNm9DLOyMLMyLSXHme9/n86JzHTns3vWTx7uPVTCAkxCgEAGWXW1Hjt7Oj+Kf888vXfLqnGAyEwrN+7f+84TC+aVEtOTRgKQCenP8xO2Hzrr918u/vft10cu27dl1UVooTRx/WDbTP/mD02JIMhKP6uuSxcm+pZeN82tc1qj9c8pWeeeWfL4e5d+vOnuiwUiYigzm8GmGCi0QVNoggiKQDVjeNCqhTPMvrY3uR2e+hYUSAIKSlAilAglQolQmnDy7wtmDU244bLwyG2XawaatdACZNI2hJARaj8poW0YzOKH02m8dmodt/ianmhLBy1kkmijSCkRJQSykk1hkFJDrY1UiAJaSFREEEIikZWBBhBF9EOWVJEJtFA7MqkIZCYoBTW0TZLh1PHvvfvGC4aGhsycmJi0bt3mewrUmjKTJDMBSCCEU8eP2rtzm4WLFnvx5U2OXb1hrNfrvd1Cv9JPslIBJKjJrz8e8eGON82bf7ublqzx6Ib1+jGnPXnm5GALtUs103OfjQupBplB5ZLTX5j2+XbdrEV2TV/l1XVrPP7gaqMHR9rfum52C13X1+8zY3jA/CuLmvSTs9+P+Gr3Vj9NusryFQ849NgKy+6+3yejI74cWDv26QcrTxToLtba1ZrjfUZO9I2c7Iz+3nf0/W0Onxvir589v2ymW1av9893B7y2eWudyLaDFibG+qemDjZXPHPj+NTMLJKK+w4dtvvjA86fO+fg/j127dzhlde31sm9gXP9rp6AFiLypeVP7bk3Im5OOQUgFmw0c9bcXq/lrjsWxnfTHh5b+uS+sVrr4bbYDv8DSZ8/CcPtqE4AAAAASUVORK5CYII='
//     },
//     {
//         id: '05dc5fb9-ce69-43d9-87b7-25960fe083c4',
//         ma_noi_bo: 'com.technorati.Stringtough',
//         ten: 'Mackerel Whole Fresh',
//         gia_von: 7615.21,
//         gia_ban: 9378,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 1221,
//         so_luong_du_bao: 50,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-04-02 00:38:09',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAMCSURBVDjLTdBNaFxVGMbx/zn33plOZjIT8zmGOnFM1VoNJYFWYi2CtVSMdKUbK+LKhSAGXYhd2m6FQkEXLbiwETcWiq3WpiiSBLRqTa122pEmMZPmw8lkMt93zrnnXhdJbB54l++Ph0cEQQDAm1/l9gOnHmnbMVzXPnXto32fhueXgAqwChigCBSAz4ErNpvxPe/pvcnY8PvPdbE9NeUn6spPFF2zU2moNA1zq1W+vVs7DIxuB3riIQFAbt3gCIEtwLIh7EhSYYklJY4Fgzsj9Cai7WeuLX4stwCjdTxqg+dDRQlKGtabUHI3rtCAf6sGgA/H5hlOR3mq0+mytwHtrSFJrQk11yClwAYsC6QFFgJLgA8IU+anmSLX50uL9wGlehIRi1LDo94MkDLAkiCNwJJgEbCj/AN/j3/G250D1CZ/5BWdHPsf8JTq64k7lNwADyAAywhksLF9vPI17WvXiAy8TiI9yPrs4zSunH1jW4NmXzIRJrNiEBIkG88SaKlcJuX8SezRA6zdzRASitZ4klhHKmEDvHjicsS2ZCjsSJQxSAIgIADCtSnS9i8k0kdoLn1JqEXwz/RttKsKbqP6jATwmqorLEBujkQAAohUJtglrpLofwl38QzCKeLEWtHVRV+Xl17Y9875rNys32LjY0uwpAAhMfOXSJmrJHYdxb33KdLRqPLDrEzc4PTC4dtD741PA8iDo2OdnlIn9u9OsVwOmFsxlLKXSOqf6X5yBLV8FisU0Cz3kZ/8ndzAR2Sq3TNb29lGqUPAyG+ZWYoNG2fhG14dyOP5vSzdPM0D3SHctYfITd1CHvqEhZyLUSq/BUij9dDLB56IfHF8hJOvPcYeLrLn2bcI5ybJXphi+rs17nx/g4n2D4i09VKp1jFaF+430Hp2ebXEufEMbbEI2Zk86q+LpPcepJQvcO/mDM8fv8CDoX7CNuTXKhitF7YAMXjsVCcwCvQBHf25k0eG0l1i3+60mFPR4HxuSLhOB/FohLZ4C3/cyWWBY9fPvfsrwH+7HFmMUqkOrwAAAABJRU5ErkJggg=='
//     },
//     {
//         id: 'a3621b90-b267-4797-b0f1-b0269bab0c4a',
//         ma_noi_bo: 'com.taobao.Cardify',
//         ten: 'Parasol Pick Stir Stick',
//         gia_von: 6083.15,
//         gia_ban: 398,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 8078,
//         so_luong_du_bao: 4706,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-03-29 12:32:07',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAChSURBVCjPpZExCgIxEEVfZI/jGWy8gFewX6w9h7C1V1iwXRsvYCN4DUEEJ3HzLbIRF4zNZsq8/+bDOPH/zZgKVABHASzdVj0vAp6A4bH60CBEp5s6iV9TZZWAjULO0rqvFekbdq7QQUQisFZKG08Mw+prMwL2JUOkJwIr2Sd/cSMgGdqyIZVcDIbUJBDqR+6QgFPJAGcA5spZz32A3eRrvgFwMGHf7+AlJwAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '5f49df09-9295-4aa4-b941-29d315db77ea',
//         ma_noi_bo: 'com.weather.Holdlamis',
//         ten: 'Icecream - Dstk Super Cone',
//         gia_von: 7534.69,
//         gia_ban: 1901,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5054,
//         so_luong_du_bao: 2364,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-01-04 22:48:24',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFESURBVBgZBcG9S9RxAAfg565DI51uCBocpGhoqM1VaAjByXAImvoXDtr6D4JAaG2oyXtpKJGEltYcGntDErEhEI3kvDP7fb+fnqcVAAAAQAeg39XLqsVcyl62bTw8AkTE5tqb8WHOU1MzzUFej1+uR4SIzeWPOcu/TPI7JznNecZ5ngcrEa3YnJ/7fHehY6Kqqiq+eedgP7cH4zZ6dxZmnamKoiqGnpjTXcxj2tSVq/4qGkXRGOlrfDcvK7TJ0qypoiiKob5G9cWsukSHoCiqamQgiiqKoE12p2YUxVBf0aiK6ybs0qbu/HJZMTRQFEWjuOFU3aFNnn06vLCnr1EURbHq1PF+ntIKXiz/+fDTFV/90HHNTWdOTO69fU8rYH0tr7rzc2YUF8aOx3m0NYJWAPe76VmttzK1bzsbW0dAKwAAAID/tYu/URIDsoEAAAAASUVORK5CYII='
//     },
//     {
//         id: 'ba65cf14-1b76-4fd3-824b-f06a023c41ee',
//         ma_noi_bo: 'org.archive.Stronghold',
//         ten: 'Lid - 0090 Clear',
//         gia_von: 4976.32,
//         gia_ban: 3754,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 4489,
//         so_luong_du_bao: 635,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-10-27 10:21:53',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEgSURBVDjLrZMxSkQxEIa/J2IhVmKlF/EWVl7FW3iNrS28wna29iKIKNgIvrfZzPy/Rd7uIu5jFzQQApnMN18mpLPNX8bhrgO396+bCoaUubk67/YGAFycHiEbCZ7fFz9iB/sAVslKU9Pbr3D30NuAEgzYcH153EWaDJBMpqhlApCCs5MDBFjw9ikAIkSkUJqQKDW2A2xIt1WGaPnUKiJa9UxPG0SCtAFktv1l9doi05TI7QCNBtJPg2VNaogcLWrdYdAARtkItSRRW/WQqXXCINQaKbsZjOdKNXWsLkFMPWOmSHWbHnjVxGzJ2cCSJgwMLx9Jji+y+iKxNI9PX78SV6P7l880m81cSmEYBhaLBX3f/5rDMKzjpRTm83n3LwbfdX8jZ1EmeqAAAAAASUVORK5CYII='
//     },
//     {
//         id: '0e6ce70d-5c0f-4417-9afd-cd0fb4754e36',
//         ma_noi_bo: 'net.php.Veribet',
//         ten: 'Bar Special K',
//         gia_von: 5544.37,
//         gia_ban: 1835,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2387,
//         so_luong_du_bao: 7799,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-11-11 16:04:04',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJhSURBVDjLdZPda85hGMc/v+f5Pc8e2wE2QtiMyZh5K4RSpCkHxJnkSElOxJFy4kDyF3DopR15yUst2pRpJw4cSJLETCTb0mxre57f775eHPyevdBcdXfV3X19ru/36rojd+fwuZtt7n7PYQRnt+O4A+5kyaePaSAko19e3rm0GiAme3DaobV9Q2M0NDyK+1QRZDDDDX6PTlBOHPO4mWpkANjbvmFltG/TShqXteMZAXPLulrWffGCWmpLMXuOnOEvAO4L29uaePr6EyMjk7gZADalwh035/fYJJUkZXZULRDFxZi1G5toWVKPKrgbZo6qo2aIOeVK4O793rkAjqrxdWiMYq5ApVIhJCli2b2QJy4UWVRXg7nPAQBMDdFAkiQc3dGSyc/U4e7cevGBUCrwT/2MgqCGBkE0R2fve5IgiDoqhhBRKBZJJRvqnAARIw2B1MBzNUSFAuQciwwzI9WIVP8LgCCKVIQkKKJGUKvmDL5+4BFrPj5g29AAv4olujviix3dcm1GgRohCSRBMzvqpFVIa/9jdiV9tJ48Q01zG+W33bzv67nSc6AwkZttIaQZIBWjHJQ0KIkYy991sm7fMUqfe4luH6e2/yGrmhryHvn5eGphUlEkEZJgBDNUnGBKCM788UFKS5vh0IUZ75eXkbdo1fQMVB1NNbNghogh4og4Y7UNTL7pou7JWZLyTyaB8bE8mufH9AzI5di+cxMeRag6oo5V8+iWE7x71UVj/TzifIHxYWFgMFLHr08Bep51vTqV/bxZ+4+Dw3NfwX7byuZvPTSkYPncd8dvHOyWq38AFgvYQWlFsCQAAAAASUVORK5CYII='
//     },
//     {
//         id: '982406bc-6fe6-42b3-a8c3-e17d909bcbed',
//         ma_noi_bo: 'cn.360.Trippledex',
//         ten: 'Pear - Asian',
//         gia_von: 6953.16,
//         gia_ban: 5573,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2220,
//         so_luong_du_bao: 3923,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-09-15 11:00:09',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKGSURBVDjLpZNPSFRhFMV/33vfzDjOvBmlRkuz0THKIjGN2qSrFkFEBUFBG1fhtkW7aB1UUNDCyIJaRC3aRAXWxkroHxpCRGaY2uS/GWfUGUfffPPe+1poUbvCs77n3HPvuVdorVkP5P8Ujz3ae0IVtj80w80U5l4i7MlO8a8OfvQmTsvAyXvBjR1EG1pZGHvD8PNbs/JCz7u+snKrdS5fCi3ZjuFp8NC4nsbTGldrmq234kx7p4hWtJEd/YxfKKzIJsoq4zEZq4zWdR3bHimWtCgLmH91FYDKvEKlM0QThyhOP8BfLpgYGsb1/Fwe25c0UjknoRxP3OubJjmnKBQ1ExmPZNYjOdaHSvUSbTyMPdWD8M3jC1tgz2Hu7WK5YvdWo1B0RcBnULs5wvPBFAtLJaojgpXxx5QvPCO67Sj2ZDeGr4TK1TP1YoiB6vPE6psAhFy2HQASm8IIDb0DKdo3DOLLvaaq/Qhq5hamX2Mvxpnp/8DgtmtsrGtE6FWeUbDd1TxNSNSEiWaeYWbfo9wapj9ex8OmkK0l2f+JgcQVahsaCf4RviysrCoJAU7JwTd9n13Hb/PlwTlG+l8T2NqCPZ9mvu0ivnAMQztIn/y9ZWO56KIBpRxms3lGvqVRn57Q0NJBKLSDyaFR9IFLNDXvoX6zRXYhj+c4aA1ogVwuOtr1tEhl8tTFLO58TXH1Zjf7dzbgj7fQfOou/sgWPDSy3I+ssphK51ipCIL2tCxkJ8eLyok3bQmKcNAQN54mMdZGEkKsOfUQvw4DSbzS8sZn8iqX/jEl1VJ64uDZ3sqAFQrJgCmkNDFMgWmAYQgMucpb00KAdh2lVhbnM+nR5Hex3m80WCd+AqUYHPPwkaN5AAAAAElFTkSuQmCC'
//     },
//     {
//         id: '689c34ce-824d-4621-bb60-d0f170ae9fec',
//         ma_noi_bo: 'gov.house.Toughjoyfax',
//         ten: 'Oven Mitts - 15 Inch',
//         gia_von: 3734.27,
//         gia_ban: 9233,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2771,
//         so_luong_du_bao: 9158,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-03-26 15:20:51',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJFSURBVDjLzZLdS9NRGMf3Fwy6jLrPKKerSAsqXzKn0xY4ZrIhbvulKwtrOCdYuWDOza02Ai96AxEEaXixTMkllDdKUjhrrmDMlMFGuLbTXO7N/b6d3+rCYFLQTQ98eTiH83yel/PwAPD+Rbz/A8D2dsupvlIRTjmdluS0XWT7WifJXu4gGUZN0q2tJHWxhSSbpGSrQRJJnKtT5AE0QEaVwMwLYH4eWF4G/H7A50Pu9StExsYQHR1FfGQEsQcPEXQ4ELzdj83T1Yl4+SkJB3iLJ4+AyUnA6QRWVgCPB5iYQE6nQ1CjQYhhEFWrsaFQ4F1jIz6ZzfB33QARlgU5QAnbo11kLSaAZsP6OvI2N4ecVIqQWIwv9fX4RrVaVYWPAwNYZdpBSo6HYweFsvwMaL97aL/TOUM/4HIB4TCwtARWLkeEBsYoJCYSIWAy4bOSAREcC0SLSkt/+4Wspp2fUammtvV6YGEB8HrB0tJJTQ0StbXYGBrCGg2OHT4aiB4QFBf8xpRcwU/KmqcyPfqfADqDRGUlUlYrnhoYdNtlbPs9CVqMFfG6XsHNgnuwdf4C/7tI7E733QI7Po6sxQKnQYk7TiWee4fhCblhf3kFzfZilHXutRVcjs2Ks/vjJ8/409puJK9roTJWw/XBAZfvfn6+ttlLsM92cIDkrhtGjpQfov2+of2uNfQJMe19jJ327P0wB/i7dT1xdV/S6lZh0N2WDx6caftzBTtFHxqbbEW462bymTnPnXedwS4QM1WcK/uXN3P3PwAfNsr5/6zP/QAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '93705a93-eaad-46cf-a643-a529a12c0724',
//         ma_noi_bo: 'gov.dot.Viva',
//         ten: 'Energy Drink - Franks Pineapple',
//         gia_von: 9207.11,
//         gia_ban: 4358,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2597,
//         so_luong_du_bao: 9806,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-11-03 15:38:28',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKZSURBVDjLjY/Na1R3GIWf+zV3JolJ40iMptZMGrGtVLoQpK66qagLV0VwI3XThS4EQQQ31Y34F7gQFOqqdCMtdGGCRXAhQmskgVSNmYAOmkySmczHvTP33t/vfbsUkyl44OwOD+dxVJXNSWfHJ1X0oSqjKuKpkKjoPMK1/m8rv38wVtUt7cx81sre3VWN51Tbj1Ub05qt3NXmo92vNm99ekStDPifHILuWzB1yOq44TbEyOebtz0BYkGSCk62AdrFdpYgcbFmq+7/PFBs8x+ylftI0kbiBkHxMGLgIx8IbjhGMPglmiwjQR+igjWyFZDM7B1U0XkVGVXBU6uJow6m9S+mNod229i4RWHiYG8FsXLFH7k0Fuw8CdoFG4VZtEj84hqFHUfQ/DJeWAc12IxeAL3sjxwH0wTbBNvGL4yQRet47jzaaWGjFoEzgs16KFgDSISaNmiKJKuQdjBGyA1NovkqNqyxOrtB5S/D4u1ArKcV4ObRKXPDFyPYaAG78RRJV9DkDd7gBDZVktpzNI5Ye9Ygqo1x6MzPhKUDTmd2as/8o+nrT84WJlybKU5QxCuU8Pu/wB/4BtRiMiUc3kdu+y7e/F1l8rtT5Bcf4vxymr7yPcb3Fp24Zn70rREc1yWLF9DuOzRdIRw7gUnvkUVr2HoVUxfyoyU4cfG9+9VdSJvAtxm/ddZmTuW3fYUEw6DjxOtlvHA7tm83+Z0H8IZeEj/7k/4/zpF0lomBVtNDC07Hu/BD4VM3N3jMzQ/g+A5ZWqO1+pJWZeFB4/Xz+vqLpzt8vy+qvqqGbuCSeRGNdaW87OEPuVNO+ddiSQw/iZXvreVrMcyJ1Wmx3Dp4vr4EsHR7uFSby9/ZKK8dISKnBdKg6D0e2J87+x98zpgrhVsXPQAAAABJRU5ErkJggg=='
//     },
//     {
//         id: 'bc1148eb-a777-462b-a200-532ad63e2fa6',
//         ma_noi_bo: 'com.forbes.Transcof',
//         ten: 'Mop Head - Cotton, 24 Oz',
//         gia_von: 4497.11,
//         gia_ban: 8206,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5647,
//         so_luong_du_bao: 1483,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-11-22 00:53:41',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHWSURBVDjLzZPdS1NxGMf3L3TbXV5EEN5030UJpTdClBBKSgh2Y5cyW0QXISY2eiGxklYgGoaE2YtFdTjHvZyO25i6uReOuRc3T7TNnOFOw8bHs2MmZUEQRRefm9+P74fn9zzPzwJY/gTLPxUsjB04Hh06ifq4i+m7R5jp29/82+HFiT2NmmBlZfYpfMrwcXYU+Urte/PS4XDUGLw14Gc8G+4gF7pIaXEcTeylGHzEl4SL4L02fUsQ9vtl0mnVJJOpML9JbITl0AXKRRfFd+3kp84SGWwlMHC6PHXj2N4twYd4PIzH40KSJBOn04lX6GM5eI6yLrM234KeamI1bCNxv54HA/bStyZuCiIoimwG3W430lgvmtf6NdyMnmykEDqPeqsOLSJWnqZ/J0gmY/h8XmRZZnL8KuEXHUbZk+jxVj6nTrFiVKL21zLnFclmMzsFqZRKIODn5VA3c89tzExcI600sBZvIj/dSex2vRmORiPkctq2oNJlQXhlHC6Rzy/xsKcGVhNE75xAsO3GbZTssR8lu+CjUMga5ExEUTAnZPlxZJfaqinJNykp11G6DjFyporB/h5+NeIdC9NwcJfe3bJv/c3luvXX9sPSE2t11f/zF/6KYAOj9QWRU1s5XQAAAABJRU5ErkJggg=='
//     },
//     {
//         id: 'c8f11ab1-dc23-4b54-b2d7-b2ad614bf554',
//         ma_noi_bo: 'com.ft.Tres-Zap',
//         ten: 'Dr. Pepper - 355ml',
//         gia_von: 1804.32,
//         gia_ban: 4722,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 6223,
//         so_luong_du_bao: 8617,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-05-29 02:00:07',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGgSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+Trew0DfrwJCehfCceqU8fsy48ttS05xAkMLANgYP39N23K/3fq+n9wpkTXugsFQP8+B/r3DdC/pciS77WN1r9T0/v9Vkl7PU4DnKrPPJi85uJ/oH9fkpUXHCqOF9iVHn5gU7S/gG6ZiaoGAADG9LhB7Kzu8AAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '07f26011-4f1c-4ee6-a49c-d99413e55202',
//         ma_noi_bo: 'uk.ac.ox.Bitchip',
//         ten: 'Wine - Duboeuf Beaujolais',
//         gia_von: 4087.34,
//         gia_ban: 2967,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5838,
//         so_luong_du_bao: 5706,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-07-06 00:11:18',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIJSURBVDjLjZNPSNNhGMc/7+/3Ww5LmRaho0AdQgWpbRUqHmohVIdBp6Cbp7DWpQi8VyRBt+jQpdNuHYLAWEEiORl0CCsbIlGH5YKFbXPE/rzv+3QYipIOH3h5eB94Pu/3ff4gIuzl5EZGZKe4wx7t1cTEjnElIpuXTCYjxhi01mx4rTX5fJ5UKkUwGCQUChGLxdRGjreVVq/X6evrA2ArWESIRqOICMlkcpuCbQCtNQCzy42fGQFjwYrFGLh4QlOtVncH1Go1RIRw158dCwmBvQHS2Q6sWLRtKNAGrBWuDGkqlUpzAMCZ7t+7tLOjuYJqtYqIMPejEyOCtmBNoxZihaun680VFE0BEWHsaP6/1z98m2P6xTyl1gLXHpwvFcprj2YeLt7fBNxNT/bsC/i5JJex1pJIJHAcB6UU6y05Wnv+cm7sLEc6+5ldetmW/vL+3nA8GNicRGttr+NzyRQ/IyJ4nkc4HCYSiZDTKwwdG8Q4hsHucYyqM3xyFOCmA3A7db0do94d9B3ibfY11lo8z8PzPBzHYa2cx6cOEDt+C4A7F54ROjwA4PcAjDaTRltZKS+r/bV2ktkZrM/iui5KKQqlNZZWF1hcnWdq/DnTbybwuy0AFSUixONxabZIP/mK25tjdGCM/q5TrPz6yMKnebLfi4+3LVMzG44Hp4EbQBuwDjxNP1md+gdPcFmX7csAmAAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '7e70cc03-2f47-421a-87b1-c0aa61e66852',
//         ma_noi_bo: 'com.fastcompany.Solarbreeze',
//         ten: 'Absolut Citron',
//         gia_von: 7141.68,
//         gia_ban: 8211,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 599,
//         so_luong_du_bao: 3710,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2018-12-16 01:40:26',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKsSURBVDjLpVNdSFNhGH7P2dnZz3FOxfnD/Ns0pzXNEgbdSEhlhBeV9AP90IUV3XhR0EV3CUEXgRBBfxS18CIYXWStKJIo2NxSmw1zGLYlOja3Ua25nW1n39c7KVEwCzrw8vC97/s833Pe7/sYSin8z8euV/R4PPzo6KhqvR7mTw76Hrw/LpDFa5Yqjp1MFELsh9g3cHTr3X9ygOQehYLczynlmpmkIEgcK3A8vXPsytOevwoguRVI2qYtUUHk8ywYmeDEt3AU8msWJFv32Zut6wrsrRMvVHBpCHwMQJUgje3v7mrTSjFXfl3KJKGzjpxaNQOv13tDkqQODMhmszyG0Ww2Mzb763RbY5UiEAiAsTwFTMJFlNUn2JfDjmQmk/mSTqcBMcIiUdLpdM0o1oyJ8zzPM06nc6S+Uqvw+/2HBRUHheKreYOpk4357nkFQVCLonjJZDI1o4iRRdJAMBiERCKhx4Q9Go22I+5bWFjYjI3Pq+mT6yW1XfrCyi1Qxodq9OzIIawP+3y+vIMPrMVimUFyHB3MY2GPQqEYQ+JOtVo9UcT4e2W55BlNaRHk4o+hvLVXq5JJD0k21WUwGPICb5eGiERnPB7XI9H+e+dwaM5Sr/RYKjYdABDHwW0dhIKiFBTTebqrZopzOBwZnJdnSQAJt1Y4mEDcUalJuQtKzQc1xUk8VT8AJZBLvAPD9nOMnCxeZIgYQgH38k20Wq1ZtKRBcp2ahL+3CK7hho6TTUq5C0hm7teZyUEmtEP4EwvP7EOzpy+/qF2+B0jk0EkKcYqXIsGyDbub1JooUCmGRBmMPZrGLgJEnAZdYy00qCPlb/rNG9d8C+6r2+ZajgzqZWwABb5ihq66ezKVCcKTHpge6rdxa70FKZ1Rjt/uSVFC8dfJiqDLCHmktOInriB9Oz6CFbUAAAAASUVORK5CYII='
//     },
//     {
//         id: 'ab544c20-9381-449a-87f2-06dd0daf265d',
//         ma_noi_bo: 'org.joomla.Daltfresh',
//         ten: 'Garbage Bag - Clear',
//         gia_von: 1752.68,
//         gia_ban: 4545,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 2246,
//         so_luong_du_bao: 6625,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-04-23 14:13:25',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+RreV94S96UiE9pff7/I1scPnlW6NWgBCLQvxKOVaeO2ZcfW2pbcogTGFgGwMD6+2/alP+rYhz+Na5O/L/lytT/F57t+t+/O+t/eL/uf/NsyR4G17oLBUD/Pgf69w3Qv6XILnqvbbT+nZre74RWlz8bL0/4v/HapP8g0LMn9X//nnSQAd8ZnKrPPJi85uJ/oH9f4opOn2rD/9uuzPmPDDZdmgoy4D+DQ8XxArvSww9sivYX4DLAMkf6e/eupP/tuxLAmtt3JiBcQEzqAypsCe7R+N+7KwVsM4gG8cFhQGwSBiruAOJPIGdD6Q6QOAAJO6JfeUJqowAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '24e89ff7-b7b9-4e21-ac19-d0bb5fd57930',
//         ma_noi_bo: 'gov.fda.Alphazap',
//         ten: 'Mushroom - Enoki, Dry',
//         gia_von: 9228.31,
//         gia_ban: 8642,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 7058,
//         so_luong_du_bao: 76,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-11-20 07:23:04',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHySURBVDjLpVNBaxpBGH2z7rrb6poNG0lSi0VyiA1I6a2XUtBLyD/oyV/gvSD03Gv/Q7El5NZDhBDx1lPpQUyRbg9SoUYTsEs21XVdM9/QWdZePGTgY2beft/33pudYcvlEvcZCu45VLno9Xrni8WizANBEGA+n0fh+34Us9mM5la1Wq1QHSMLvPhVMplsZ7NZkfT/iNvUNA3tdhv9fr9Sq9VaQgFnfGtZFobDIXRdj4rihbQeDAbIZDIoFApEWudwi3U6HcFu2zZc112RS1aokDGGRCIh2E3TRCqVQrPZhOM4FZXYSTp5pSTDMERI9rgSmsMwxHQ6RalUQrfbravE5HkexuOxOCCZRIdJs9xLjPaqqiKdTguV1KBM3WlDCdySkKwoyooC2Sifz4u8XC5HNWWFWMkrBX2QITGJP//2CdbVT1gTB896H6JfrFIDWUQMxWIxkkysEn/y6wJP/3yFzTYxe5TGFceiBvQHpL+4XHl45uf3SO15sPU9oMxg9D0cOCdCHWs0Gqfcy2HslkUzxaF9jH3NwcbONnR3Eziii8Mb/7jF98nDS7buMf1+wRraG7w2sQP92gJecnCDx5jf2Hc3H9c/Jh+j5Rnwd3fELYXA5T/8SwC4GK1X8Jg94E9uAhNJxVeC7ewWYHDkhrcOYd0B0mCFUhT4PX8AAAAASUVORK5CYII='
//     },
//     {
//         id: '4f2b25f1-501b-4468-8be5-76ccdfa5e2fe',
//         ma_noi_bo: 'ru.vkontakte.Fix San',
//         ten: 'Wine - Rosso Toscano Igt',
//         gia_von: 583.72,
//         gia_ban: 9055,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Sản phẩm lưu kho',
//         so_luong_thuc_te: 5760,
//         so_luong_du_bao: 3131,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-11-18 17:12:02',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAC4SURBVCjPdZFbDsIgEEWnrsMm7oGGfZrohxvU+Iq1TyjU60Bf1pac4Yc5YS4ZAtGWBMk/drQBOVwJlZrWYkLhsB8UV9K0BUrPGy9cWbng2CtEEUmLGppPjRwpbixUKHBiZRS0p+ZGhvs4irNEvWD8heHpbsyDXznPhYFOyTjJc13olIqzZCHBouE0FRMUjA+s1gTjaRgVFpqRwC8mfoXPPEVPS7LbRaJL2y7bOifRCTEli3U7BMWgLzKlW/CuebZPAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '9aa01797-fd03-44e3-be7b-43d63e446b93',
//         ma_noi_bo: 'com.naver.Zathin',
//         ten: 'Juice - Ocean Spray Kiwi',
//         gia_von: 276.46,
//         gia_ban: 9239,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 2665,
//         so_luong_du_bao: 1854,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-10-07 20:18:50',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAK5SURBVBgZBcFPaJZ1HADwz+95n3e6uTnREGdljRKtGCYiHTLxkIUmQeeCOnXzVnQIoi5BQV08TMo6GIiHiKI6ZEWgszzEmtpqSDP7s9ycm9NN977vnuf37fNJEWH/G6df6l676vki2YXVSCAhEpFVOU8uzMX36daNV88MH+oApIhw8O2zZz45vOuhokjrgoYAIALC7NKKEz8vmP67fee3XyfWjwwfakMJRSNt6yob68avaRQpkYhMHVlVheWV2r6tffYPjNi4eLyncWCodf7jI1Jr6sUSUkq9EdHoajQkIZALZOpEIWlPf27r4jndQy/oH9xp4c9tJk4de7eEIEGBlAgJREqKRP/yKXVcsH7r4+Ynf9eVOvrWbtK7YUt/CRBB2SBJIiW5Doqkd3nEllWj+gef1r56UldP8tfYhJt3UhTtuR0FRBAoU6FISYFGkaxePG1LfKv/gYNa/30oNW9o9vbpzvOOXj+wsvvwZ5cKCGSkRJGSIiWtK19af/uU/gef1ZoaVjRXdG7db+bMed173zJVD2QoIFdEkBG4fflrPYs/2vjIMzrTxzS6QvvWfWZGRs3tGZY2bFdnoICcQ0QQTI+e1L3wk5W82dWLR2Qtt+fvNnNuwuLeo1LvgNXNpK4CFFBn6iAysxc/8vCel636Z8SlL84a+2be+Hdjlh57R9WzWaDZKFSdCpSQq5AjvPlLx9DkrM74VwZ3POHm7JzJsUk/7PvU9Sv3yipwYlPTSjuDEqqqVtcMrG0a/+Oa9z8Ytnv7oOXNOyw9edyjffeIIIIL1yqRw0qrAiVU7ZyrnKNTS+te/9flFCYlkJdIS5UcRJEUOSnLlKs6V1DCSqueWdPVuOu1oc6aiCgEGdDfXYIIuptJSnKzkRbrKk9BCSnFe0+9cvq5lNLOED0AgkAIIEAr5zxaFk7A/5IUWNTkV3l/AAAAAElFTkSuQmCC'
//     },
//     {
//         id: '5a0dbfc0-9f12-4e99-9037-1652a10780ad',
//         ma_noi_bo: 'com.lycos.Ventosanzap',
//         ten: 'Plums - Red',
//         gia_von: 2165.26,
//         gia_ban: 2535,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 562,
//         so_luong_du_bao: 6989,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-05-10 20:38:47',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIwSURBVDjLhZHLaxNRGMUjaRDBjQtBxAdZFEQE/wEFUaurLm1FfEGzENwpturG6qIFrYUKXbUudOODNqIiTWqvFEwXKo1UUVRqS2NM0kmaZPKYPKbJ8XzTiUQxceDH3HvnO+e73xnH8X7fLjJInjbgEekiOwA4/sbBD0Ov5sIqY5SVXiO/Rpospw01HphXrOttZPBMxCkWJ3NltZItq3i2pOKZklrWi9Z5SMuKwf2GBtJVxJotiqWLKpIqqHCyYO3/Z/A8UyirBDtLcZTi6Y+RdxdHAsnTAy/NM0TerCuRlE2Y9El+YjCWoLBkViyxdL40OpNmLuBo0Gvk12AuYC5gLqB2XAw8A2NBFZzXVHm1YnHq1qQpYs4PjgbmAuYC5gLe0jrnWGLwzZqDi33ksSTunw3JvKZ0FbFmi5gLeDswF2v/h4Ftcm8yaIl9JMtcwFys4midOJQwEOX6ZyInBos18QYJk0yQVhJjLiiald/iTw+GMHN2N6YOuTB9YieCozfE4EvNYDO5Ttz2vn/Q+x5zC3EwEyw9GcaH7v0ovLiN6mcf8g8v4O35vRg+edTr+Ne/tU2OEV03SvB3uGFQjDvtQM8moM+N+M0D8B92LjQ0sE2+MhdMHXShOutF/ZO6toXnLdVm4o1yA1KYOLI+lrvbBVBU7HYgSZbOOeFvc4abGWwjXrLndefW3jeeVjPS44Z2xYXvnnVQ7S2rvjbn1aYj1BPo3H6ZHRfl2nz/ELGc/wJRo/MQHUFwBgAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '4cbbe4b5-ccc7-41a9-b49f-5346a3c709e3',
//         ma_noi_bo: 'fr.pagesperso-orange.Andalax',
//         ten: 'Flour - Bread',
//         gia_von: 9711.13,
//         gia_ban: 1717,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 6286,
//         so_luong_du_bao: 4312,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-04-04 21:25:40',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAITSURBVDjLlZLNi1JhFMaHiP6AvN0Ls4vob2jauYiIFoGBtnFlG1fSws1gDBdXLhIhdaEuvILfjtfK0cyvRflBeHWUEBW1nHGKpmkcpWw+ani6r9DCutn0wuGFl/P8znPOeZcALP0ekUhE7vf7HR6PZ+ByuQZ2u91hsVjkUrl/PITDYbnP5xMajQb6/T46nQ5KpRJMJpNgNBrlkoB4PL7M8zwbCoWaXq93RMStVguVSgXlchmCICCXy8FgMDgkAdFolK1Wq+j1emi326jX6ygUCsjn80ilUkgkEigWi9Dr9ac6nY7TarUrc4BAINDsdruo1WpzQtEZRDiCwSDE1pDJZBCLxaDRaLg5gDispnhmvRKrJJFU/SUWBwqO4+B2u5HNZqFWq8dzAKfTyRIh6ZVAksnkrDpxkk6nIW4F4nxmrghMpVLNO7Barctms5m12Wx46bw23XRf/TF5JsP4qQwHT2QYRWXYW7+Ix6vXT5VKJadQKFYk1/g1x5z/kmUU0+otnHy04Hi4hu8HHD4n6elegr7/z38wyTA3xy+Y7mHvAb69UWDauI0PiSuQEkoCRil663CwhuMddlad3EfbD/F+4zIWAvaf0+dEm48+bdDYjdMYC3dn4snmvYViya9MYoe/NNx/fQdb69R4EKGYMwOGPHVhO0qt7r66gXdhKrJIKAkQq6nehqijflCmOov4ry38T/wEpFjkJMz8PioAAAAASUVORK5CYII='
//     },
//     {
//         id: '9e7a845c-7004-4218-8b07-24a8dfc7c2de',
//         ma_noi_bo: 'com.wufoo.Bamity',
//         ten: '7up Diet, 355 Ml',
//         gia_von: 9336.88,
//         gia_ban: 3957,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 4503,
//         so_luong_du_bao: 1370,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: false,
//         ngay_tao: '2019-12-06 14:35:22',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIxSURBVDjLfZLPi1JRFMffPzGLNkW7Ni1aJUitI4IWLVpm0RTUohazqkVU0GhjGcGU1NA6dFQ0FX09QVHxVzr+eE9RRMw0NVslPcmn8517nulkOj44XO6953y+33Pf4SRJgiiKyOfzyOVyyGazyGQySKfTSKVSawC4VcEVCgWMx+OFaDabKiQej6+EcKRMBY1GQ1Wu1+szCJ0xF4hEIkdCOLJMyaRGB8lkMt3v96EoinpOwFgshmAwuBTCkeo0kRX/YZYbg8EAnb6CwLeJk1qthnA4jEAgsADhSHlqeTQagYp//B7j+d4+nn4BhMbkrlqtkgv4/f45CMd6lHu9npo0HA7RZsqGzD7eiMA7CdjaO4RUKhVyAY/HM4NwiUTiHOtR7na7alKhp6jKZgb4UALeF+ch5XKZXMDpdKoQlRKNRrWsR7nT6ahJxZ8K9OkxzNIhxJAB+K8TSKlUIhew2+1rs15CoZCW9Si32+0FyA4DPPpkx/23Otx6eRk6/QU8MW9gd3f3xNyLsv60giDIrVZrBnnGIA8cH/HYeh1ucRvZ7zxMn+/gquk0zt49Zlz4rzzPa30+n0yTSBCJQa4ZLsJZeAVn8TXNCozCOkzCbQIMlk6X1+vVut1umSaRIJcenoFX3MG/nyu/TYCjZ9zlcmnYS8s0YOfvncQWfwObvE4t3vTrVjuYhsPh0NhsNnnDtI4rxlN4wd9UlWml/dI3+D+sVqvGYrEcZ8l6Fr/I9t9VT/cHUXogzRNu46kAAAAASUVORK5CYII='
//     },
//     {
//         id: 'ec3b5183-4019-4c7d-96b9-bcbc800d56af',
//         ma_noi_bo: 'com.lulu.Alphazap',
//         ten: 'Nori Sea Weed',
//         gia_von: 2495.64,
//         gia_ban: 3826,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 9863,
//         so_luong_du_bao: 7051,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-09-23 11:52:50',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALHSURBVDjLpZLtS5NRGMYf6H8RIoT8VB+CZaKUsHIGGYgRpYn4IVdgmhW+zNnUTTZwC6l9SahmDjNTLJdtTtO11tBiOed8mS9zbW5zm9szd56r5zxKtM8euODmuq/7d87hHAYAcxwxxwY86pudlPX/itTpnAeVPXZyS2UnN1XfSEW3jZR3zZHrnbPk2tOv5Kp8hpTIpom41ZoRt5mTFxuHfackqklGPeDypNKEiyYyYDPIUvo/HVARIMMrtn+AQDhFCh4MLDFNLxa4Pd7Qjvjg8bPYTXBw7xB4AgTeIMFaiGAjzGE7ymE/DawGWLQb1rG4EUVJ9yzH1GmdYHn0u+9RaD/44N1hEU1yWYOBGIckP+wLsug0rqN3IoJgPEUBYO6o7AJgYZNg2H4IobskWAiDoTjH94HNEAvlkA96cwRjrgxCsSNAuXxOALj8BD+3CAZmQlAYPMJudJDef2uXRcdrD55PhvBlKSPoH6Dk8bQA+M3f27nG4r3tD15NB/Fs1Ifl7SS8/iR0fN0/FcSb6QAs7hRmVo4ASh5QVG9BigfM+1iM/wjC5o6g3/gRis5u6PV6NMsUeGtywbYUhlxx6Cm6lBgaGcMVxRSYc3c/cwn+zSacQcyvxtHTq4darYbf78fy8jLMZjOUPWpotH1YXHQLntVqFTKXa2Rg8ipeendjKbIRTHCj4yY8bGqCY2EB7fIOiMViVFVVQaPRoLGxUaip19rWzlGI9H49Yehvyrs9FD5b9yl9RnKPNLfKBEDhE9OB6EIhTCYTHA4HLBYLBgcHcZ73LrWMxymguaWVZP3r3Nzcmurq6jAFlJaWjufn56eLioogkUgE0Zp6tEcBNJsFOMkv/ogupVIZNhgMIaPRCJ1OB6lUioaGBqGmHu3RDM1mAXJyck6IRKLTxcXFK5WVlfHa2tokL7asrOwGrwK+3qMe7dEMzf4FvOYAdxCFF/YAAAAASUVORK5CYII='
//     },
//     {
//         id: 'e43c1802-4620-42d3-83f4-e13c1beba40d',
//         ma_noi_bo: 'com.kickstarter.Ventosanzap',
//         ten: 'Sauce - Soya, Dark',
//         gia_von: 4693.31,
//         gia_ban: 1819,
//         nhom_san_pham: 'nhom3,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 6734,
//         so_luong_du_bao: 7837,
//         don_vi_tinh: 'Cái',
//         trang_thai: true,
//         ngay_tao: '2019-08-09 09:17:10',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEoSURBVDjLrZMxSgRBEEXfiBiIkRjpRTyAuZGH8ALewmtsbOAVNjM1F0FEwURwZ9vq+t+gZ3aR3RFEC5qGLv6v11XdnW3+ErsAs9nMpRT6vme5XLJYLDZW3/erfCmF+XzeAXT/QgBwc7ewASUYsOHidL+7vn1eVzCkzNX5cbdhkIKjgx0EWPDyrpXu5HAP2Ujw+LrcTmBDuu0y1LV+JVaaSG83qAnS2iBzPDdZQTKZIsqEgQYC6TtBraKmUJoqUaL+TNAMjLI5RIhaW/VMTxNUtUbKbgTDFT7DK4pMU8bExhRSpLp1DzwaJFFFDhQRUwSGp7ckh4mM7ytKUqNVrzIREwSXZwfdtpdWwsRQXWpT2WowFRHZxNl6I+l3BvXT3D98bAjH+PNn+gIL+yQjrYYUIQAAAABJRU5ErkJggg=='
//     },
//     {
//         id: '4cdd2070-2fc7-4eac-9f4c-f703457f8991',
//         ma_noi_bo: 'com.usnews.Tresom',
//         ten: 'Apricots - Dried',
//         gia_von: 3503.34,
//         gia_ban: 911,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 7214,
//         so_luong_du_bao: 4017,
//         don_vi_tinh: 'Cái',
//         trang_thai: false,
//         ngay_tao: '2019-06-26 10:16:44',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVDjLpZO/T1NhFIafQss1aaSpUmgbCxpriE3q6Ayjk5oYpf6ILk5d9S9wwsRRDVFXN22ijg6YaBxMiUPjUBqCLe2l9goxcpHC/c5x6C20KRtneofzPnnfk+8LqCpHmSGOOMGuePLB3o8iqihgDKCgIoiCkY42Ao9uTgb6AADJ6AiKogIKqCiioD0QEVhpbg8mMKqo+gYBVUXVT+ODRBQRxewdUsGYTkRUEfGNXS39kD0jgwA10llUEBFU8A2yb+wCPe8QgCcgRhF/qbPc1XKQREE8HQSIJxjR/Z7a01n9St0k3mEAzwNjhN12m83qEqHtFeLjJ1ld+8Wx1CzD1ihu5R2nT8XY+eeQz68+LJVKzw4AxrC3C8vf3pMeV27cu4PrupxdW+PTl8/sMELu+hVGQkFs26ZYtB5Xq9VYoPuUH7xc0cDmdwK/i+Ru51h49YZ6pUgiESebzWLbNo7jYNvrHE9cIH/3EoVC4U9PAmXj5w/OxcMAnLl4i+XSV+bm5ohGo2xtbdFqtXj6fIHRyWuASyQSsXpuIPw1Eep2BYCljy9ADPPz84TDHajruqgIu5XXMHuZer2+E+j9jZlM5nw6nX47MzMTT6VSEgwGTzSbTcrlMpZlMTU1xcTEBJ7nbdRqtaHFxcX1PsD09PRwIpHIhsPhQjKZjFmWNQwMOY5zH6iPjY0VgFC73TaNRqPluu7V/620dUeTJTHnAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '5de94b7f-5879-4f82-8d7a-734c19329a2b',
//         ma_noi_bo: 'com.github.Voltsillam',
//         ten: 'Fudge - Chocolate Fudge',
//         gia_von: 3890.96,
//         gia_ban: 683,
//         nhom_san_pham: 'nhom2',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 6936,
//         so_luong_du_bao: 2077,
//         don_vi_tinh: 'Chiếc',
//         trang_thai: true,
//         ngay_tao: '2019-07-29 18:46:36',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJQSURBVDjLjZNvSBNxGMeX9O+FOAkaLbehozdGRGiMQqTIlEqJMIig3oxl0YxcgYt6FUZRryLYwpFWCr2wXgjBIMJMYhFjgZSiEXOg5c5N593udne7u+2+3V3tT22SBx/uxe/5fu7uuefRAdCpKJdJoVHB9h9qFSryuSJBYzqdpiRJymYyGZRDOYfH43lULCkW2NRwKpUCy7J5kskkSJJELBbTJARBwOv15iW58AZVoBbwPA9BELS7CsMwoCgK8XhcE3AcB/UhPp/vtyQnGBi03pYXjyAbPQuRD2sSbmUFVN9NLJ5ux9DryZJP0nqiChzjl48Oh9oYRPTAXBVksgnS0hRWu7uxXG/EfL0ZZ9yjGHgb1t4kGo0WBO6AvcUVsFP9oTZZjlQCP7ZA/r4JpHM3lup2Im6pRsRai2PX/GjoDWEk8BWJRKIg6P147mfP+CW63d16RUyOQP5SA6rLAsKyA0TNNizvM4D9/A4Tk2Ec7nuPE0+vgqbpgqBnzLl6vv8N3+x4eEsS0mAvHAJhMoAw6kHUVUF4rkeWHAKXZtA15kDL6C6tkXmBffiZs/P+NE7dC4pBhwsJY6USVjBtBO/bCswrbfq2GS+Ce9DwyooHoRvaPPzVxI67IVfHnQA+2JqQMFQgur0anP8J5IVmYEopmdbh5YQO1wMu0BxdKlB/44GLg48/HT8J8uBesH6/ViDxC5DnWiHPWjAz0wleYCGKokaJIDdI/6JMZ1nWEshr7UEZsnnBH8l+ZfpY9WA9YaWW0ba3SGBWJetY5xzq6pt/AY6/mKmzshF5AAAAAElFTkSuQmCC'
//     },
//     {
//         id: '6e66f53d-94a4-4277-9219-5ecfbe4b595a',
//         ma_noi_bo: 'org.gmpg.Trippledex',
//         ten: 'Soup - Campbells Tomato Ravioli',
//         gia_von: 3101.87,
//         gia_ban: 315,
//         nhom_san_pham: 'nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 1869,
//         so_luong_du_bao: 9199,
//         don_vi_tinh: 'Đôi',
//         trang_thai: false,
//         ngay_tao: '2019-03-13 07:40:17',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJKSURBVDjLldFfSFNRHAdw88mXnuqtHu/dufe2e/dHjUl/YEYRRsFaLViYFkUY+CcDwcqiJAjKoCiKCqRw+GCEa6vIdKOJLBPaiqy9CBlkYUUbq83Z3f32uzMX0kZ64AvnHM79nN/53RIAJYXC8+wux/EZQRDiFsu6Y8XOFdxkjG3lOE6jgABYrVbwvFS3BEA6YbNVQTLKWL9hI2RZAWGPFw3IcuVh184axMbDePrEC7vdrgOhJVQgb488MEGdMCH9zozOlgpwBtMr6kvZogBRUvYH7jdjMrQF09HjON3RDoulgvrAnP8FqFTW1NT8PvkjhamPn6BqQCj0jPog6894azCwVUUBuqGUDg15vV7oQ1WzFBWJRBzV1Zt0QK/CT8iyggAdsLlce9RkMkFLDdmsmos+Hx4OwWazgX6xRoi9GHDd4/Hkbs9k0nT7rzygx+FwUBU8hXX+AzBeXG21mOPBYCgHpNMpAmb/ANncXm3tvtwzCLi6ABi5pazwX1QORHsFtedGC6Y+f899+BcgIuJE/W69kQyN9fLNBUCsz9o/E5aBiILROxycjm14Mx7D3NAwSwWkAmvxoYdh9LKAn37xa7LfuCsPTNxT/OqYgpkRGVpYwu2z5Xj+IoL54fUNYLCrHBgUKCI0yrc+YywPvO42RqcfykgO6YCMLz4TTrbW4Whrm+Z279UYW4PzhwxAQELKJ2HsghR81GbenAd626WV1xrFmq4j4jmav7zUIExWKnwVNaxsLsLygzukjoFTQrq7Qbxyxi2WzvfgNzL+mVcak7YgAAAAAElFTkSuQmCC'
//     },
//     {
//         id: '43cda594-a6cd-4af2-8e85-b2110b4d5210',
//         ma_noi_bo: 'gov.fda.Temp',
//         ten: 'Turnip - White',
//         gia_von: 1236.1,
//         gia_ban: 3369,
//         nhom_san_pham: 'nhom2,nhom1',
//         loai_san_pham: 'Tiêu dùng',
//         so_luong_thuc_te: 475,
//         so_luong_du_bao: 8762,
//         don_vi_tinh: 'Đôi',
//         trang_thai: true,
//         ngay_tao: '2019-11-09 15:36:56',
//         anh: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEgSURBVDjLrZMxSkQxEIa/J2IhVmKlF/EWVl7FW3iNrS28wna29iKIKNgIvrfZzPy/Rd7uIu5jFzQQApnMN18mpLPNX8bhrgO396+bCoaUubk67/YGAFycHiEbCZ7fFz9iB/sAVslKU9Pbr3D30NuAEgzYcH153EWaDJBMpqhlApCCs5MDBFjw9ikAIkSkUJqQKDW2A2xIt1WGaPnUKiJa9UxPG0SCtAFktv1l9doi05TI7QCNBtJPg2VNaogcLWrdYdAARtkItSRRW/WQqXXCINQaKbsZjOdKNXWsLkFMPWOmSHWbHnjVxGzJ2cCSJgwMLx9Jji+y+iKxNI9PX78SV6P7l880m81cSmEYBhaLBX3f/5rDMKzjpRTm83n3LwbfdX8jZ1EmeqAAAAAASUVORK5CYII='
//     }
// ];

// function get(params: any) {
//     let ret = deepCopy(DATA) as DemoModel[];
//     if (params.filter) {
//         const filterObj = JSON.parse(params.filter);
//         if (filterObj.fullTextSearch) {
//             filterObj.fullTextSearch = filterObj.fullTextSearch.toLocaleLowerCase().trim();
//             ret.filter(x =>
//                 x.ma_noi_bo.toLocaleLowerCase().includes(filterObj.fullTextSearch) ||
//                 x.ten.toLocaleLowerCase().includes(filterObj.fullTextSearch) ||
//                 x.nhom_san_pham.toLocaleLowerCase().includes(filterObj.fullTextSearch) ||
//                 x.don_vi_tinh.toLocaleLowerCase().includes(filterObj.fullTextSearch)
//             );
//         }
//     }
//     if (params.sorter) {
//         const isASC = params.sort.includes('-') ? false : true;
//         const paramSort = params.sort.replace('-', '').replace('+', '');
//         ret = ret.sort((a, b) => {
//             if (isASC) { return a[paramSort] - b[paramSort]; }
//             return b[paramSort] - a[paramSort];
//         });
//     }
//     if (params.page && params.size) {
//         if (params.page * params.size + params.size > ret.length) {
//             params.page = 0;
//         }
//         ret = ret.splice(params.page * params.size, params.size);
//     }
//     return {
//         code: 200,
//         data: {
//             totalElements: 50,
//             numberOfElements: ret.length,
//             content: ret
//         }
//     };
// }

// export const DEMO = {
//     'GET /api/demo': (req: MockRequest) => {
//         const ps = +(req.queryString.ps || 10);
//         const data = get(req.queryString);
//         return data;
//     },
// };
