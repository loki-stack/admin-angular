// import { MockRequest, MockStatusError } from '@delon/mock';

// export const USERS = {
//     // Support object or array values
//     'GET /users': { users: [1, 2], total: 2 },
//     // GET: can be ingored
//     '/users/1': { users: [1, 2], total: 2 },
//     // POST
//     'POST /users/1': { uid: 1 },
//     // Get request parameters: queryString、headers、body
//     '/qs': (req: MockRequest) => req.queryString.pi,
//     // Routing parameters
//     '/users/:id': (req: MockRequest) => req.params, // /users/100, output: { id: 100 }
//     // Send Status Error
//     '/404': () => {
//         throw new MockStatusError(404);
//     },
//     // Regular expressions need to be wrapped with `()`
//     '/data/(.*)': (req: MockRequest) => req,
// };
