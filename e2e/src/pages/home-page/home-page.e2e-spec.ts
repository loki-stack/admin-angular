import { browser } from 'protractor';
import { LoginPage } from './../login-page/login-page.po';
import { HomePage } from './home-page.po';

describe(' Home Page', () => {
  const homePage = new HomePage();
  const loginPage = new LoginPage();
  beforeEach(() => {
    loginPage.navigateToLogin();
    browser.driver.sleep(1000);
    browser.waitForAngular();
    loginPage.getUserNameTextbox().sendKeys('Admin');
    loginPage.getPasswordTextbox().sendKeys('1Qaz2wsx');
    loginPage.getSubmitButton().click();
    browser.driver.sleep(1000);
    browser.waitForAngular();
    homePage.navigateToHome();
  });

  it('Should have the first card name', () => {
    expect(homePage.getFistCard()).toEqual('Bar');
  });
});
