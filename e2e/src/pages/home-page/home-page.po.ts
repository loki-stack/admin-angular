import { browser, by, element, ElementArrayFinder, ElementFinder, promise } from 'protractor';

export class HomePage {

  navigateToHome(): promise.Promise<any> {
    return browser.get('/#/admin/home');
  }

  getFistCard(): promise.Promise<string> {
    return element(by.css('page-grid nz-card:first-child .ant-card-head-title')).getText();
  }

}
