import { browser } from 'protractor';
import { LoginPage } from './login-page.po';

describe('Login Page', () => {
  const loginPage = new LoginPage();

  beforeEach(() => {
    loginPage.navigateToLogin();
  });

  it('Login form should be valid', () => {
    loginPage.getUserNameTextbox().sendKeys('Admin');
    loginPage.getPasswordTextbox().sendKeys('1Qaz2wsx');
    const form = loginPage.getForm().getAttribute('class');
    expect(form).toContain('ng-valid');
  });

  it('Login form should be invalid', () => {
    loginPage.getUserNameTextbox().sendKeys('');
    loginPage.getPasswordTextbox().sendKeys('');

    const form = loginPage.getForm().getAttribute('class');

    expect(form).toContain('ng-invalid');
  });

});
