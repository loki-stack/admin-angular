import { browser, by, element, ElementArrayFinder, ElementFinder, promise } from 'protractor';

export class LoginPage {

  navigateToLogin(): promise.Promise<any> {
    return browser.get('/#/passport/login');
  }

  getFistCard(): promise.Promise<string> {
    return element(by.css('page-grid nz-card:first-child .ant-card-head-title')).getText();
  }
  getUserNameTextbox() {
    return element(by.name('userName'));
  }
  getPasswordTextbox() {
    return element(by.name('password'));
  }

  getForm() {
    return element(by.css('#loginForm'));
  }

  getSubmitButton() {
    return element(by.css('#btnSubmit'));
  }

}
