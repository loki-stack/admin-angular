const SwaggerParser = require("@apidevtools/swagger-parser");
const fs = require("fs");


const createFolder = (folder) => {
    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }
};
const parseMocked = async (uri, module) => {
    createFolder(`../_mock`);
    let api = await SwaggerParser.parse(uri);
    let result = "";
    let tag = "";
    let start = true;
    for (const key in api.paths) {
        if (api.paths.hasOwnProperty(key)) {
            const path = api.paths[key];

            for (const key2 in path) {
                if (path.hasOwnProperty(key2)) {

                    const method = path[key2];

                    if (tag !== method.tags[0]) {
                        if (start) {
                            start = false
                        } else {
                            result += `//#endregion\n`;
                        }

                        result += `\n\n`;
                        tag = method.tags[0];
                        result += `//#region Module: ${tag} \n`;
                    }
                    let detail = JSON.stringify(method);
                    result += `// Function: ${method.summary} \n`;
                    result += `// OperationId: ${method.operationId} \n`;

                    result += `'${key2.toUpperCase()} ${key}': (req: MockRequest) => { \n
                        // ${detail}
                    }, \n`;
                }
            }
        }
    }
    let holder = `
        import { MockRequest, MockStatusError} from '@delon/mock';
        export const ${module}API = {
            ${result}
            //#endregion\n
        };
    `;

    fs.writeFileSync(`../_mock/_${module}_api.ts`, holder);
};

exports.parseSwagger = async (uri, module) => {
    await parseMocked(uri, module);
};
parseMocked("./admin-service.json", "admin")