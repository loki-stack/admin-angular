import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TitleService, VERSION as VERSION_ALAIN } from '@delon/theme';

import { SwPush } from '@angular/service-worker';
import { SysPushService } from '@services';
import { GET_CONFIG } from '@shared';
import { NzModalService } from 'ng-zorro-antd/modal';
import { VERSION as VERSION_ZORRO } from 'ng-zorro-antd/version';
import { filter } from 'rxjs/operators';
import { UpdateService } from './services/update/update.service';

const CONFIG = GET_CONFIG();

@Component({
  selector: 'app-root',
  template: ` <router-outlet></router-outlet> `,
})
export class AppComponent implements OnInit {
  private _subscription: PushSubscription;
  public operationName: string;
  constructor(
    el: ElementRef,
    renderer: Renderer2,
    private router: Router,
    private titleSrv: TitleService,
    private modalSrv: NzModalService,
    private pushService: SysPushService,
    private sw: UpdateService,
    private swPush: SwPush,
  ) {
    swPush.subscription.subscribe((subscription) => {
      this._subscription = subscription;
      this.operationName = this._subscription === null ? 'Subscribe' : 'Unsubscribe';
    });
    renderer.setAttribute(el.nativeElement, 'ng-alain-version', VERSION_ALAIN.full);
    renderer.setAttribute(el.nativeElement, 'ng-zorro-version', VERSION_ZORRO.full);
    renderer.setAttribute(el.nativeElement, 'build-version', CONFIG.buildVersion);
  }

  ngOnInit() {
    console.log('%c       ', `font-size:300px;width:100%; background:url(assets/images/security.gif) no-repeat;background-size: contain;`);
    console.log('%cDừng lại ngay!.', 'background: red; color: yellow; font-size: 100px');
    console.log('%cViệc chỉnh sửa các thành phần của ứng dụng có thể gây hại cho hoạt động ứng dụng.', '  color: blue; font-size: 40px');
    this.router.events.pipe(filter((evt) => evt instanceof NavigationEnd)).subscribe(() => {
      this.titleSrv.setTitle();
      this.modalSrv.closeAll();
    });
    // check the service worker for updates

    this.sw.checkForUpdates();
    this.subscribe();
  }
  private subscribe() {
    this.pushService.get().subscribe((res: any) => {
      this.swPush
        .requestSubscription({
          serverPublicKey: res.data,
        })
        .then((subscription) =>
          this.pushService.post(subscription).subscribe(
            () => {},
            (error) => console.error(error),
          ),
        )
        .catch((error) => console.error(error));
    });
  }

  private unsubscribe(endpoint) {
    this.swPush
      .unsubscribe()
      .then(() =>
        this.pushService.delete(endpoint).subscribe(
          () => {},
          (error) => console.error(error),
        ),
      )
      .catch((error) => console.error(error));
  }
}
