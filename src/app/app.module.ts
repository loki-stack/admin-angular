// tslint:disable: no-duplicate-imports

import { APP_INITIALIZER, LOCALE_ID, NgModule, Provider, forwardRef } from '@angular/core';
import { ApiInterceptor, GET_STORAGE, StorageService } from '@services';
import { AuthGuard, I18NService, PipesModule } from '@core';
import { DELON_LOCALE, en_US as delonEnUS } from '@delon/theme';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NZ_DATE_LOCALE, NZ_I18N, en_US, vi_VN } from 'ng-zorro-antd/i18n';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { ALAIN_I18N_TOKEN } from '@delon/theme';
import { AdminApiConfiguration } from '@services/admin/admin-api-configuration';
import { AdminApiModule } from '@services/admin/admin-api.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { DELON_VI } from '@core/i18n';
import { DefaultInterceptor } from '@core';
import { GET_CONFIG } from '@shared';
import { GlobalConfigModule } from './global-config.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JsonSchemaModule } from '@shared/json-schema/json-schema.module';
import { LayoutModule } from './layout/layout.module';
import { RoutesModule } from './routes/routes.module';
import { STWidgetModule } from './shared/st-widget/st-widget.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { SharedModule } from './shared/shared.module';
import { SimpleInterceptor } from '@delon/auth';
import { StartupService } from '@core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from '../environments/environment';
import ngEn from '@angular/common/locales/en';
import ngVi from '@angular/common/locales/vi';
import { registerLocaleData } from '@angular/common';

// #region default language

const LANGS = {
  'vi-VN': {
    ng: ngVi,
    zorro: vi_VN,
    delon: DELON_VI,
    abbr: 'vi',
  },
  'en-US': {
    ng: ngEn,
    zorro: en_US,
    delon: delonEnUS,
    abbr: 'en',
  },
};

const currentLanguage = GET_STORAGE().getItem('currentLanguage') || 'vi-VN';
const LANG = LANGS[currentLanguage];
// register angular

registerLocaleData(LANG.ng, LANG.abbr);
const LANG_PROVIDES = [
  { provide: LOCALE_ID, useValue: LANG.abbr },
  { provide: NZ_I18N, useValue: LANG.zorro },
  { provide: NZ_DATE_LOCALE, useValue: LANG.date },
  { provide: DELON_LOCALE, useValue: LANG.delon },
];
// #endregion

// #region i18n services

// 加载i18n语言文件
export function I18nHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `assets/tmp/i18n/`, '.json');
}

const I18NSERVICE_MODULES = [
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: I18nHttpLoaderFactory,
      deps: [HttpClient],
    },
  }),
];

const I18NSERVICE_PROVIDES = [{ provide: ALAIN_I18N_TOKEN, useClass: I18NService, multi: false }];

// #endregion

// #region JSON Schema form (using @delon/form)
const FORM_MODULES = [JsonSchemaModule];
// #endregion

// #region Http Interceptors

const INTERCEPTOR_PROVIDES = [
  { provide: HTTP_INTERCEPTORS, useClass: SimpleInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: DefaultInterceptor, multi: true },
];
// #endregion

// #region Startup Service

export function StartupServiceFactory(startupService: StartupService) {
  return () => startupService.load();
}
const APPINIT_PROVIDES = [
  StartupService,
  {
    provide: APP_INITIALIZER,
    useFactory: StartupServiceFactory,
    deps: [StartupService],
    multi: true,
  },
];
// #endregion
// #region global third module
const GLOBAL_THIRD_MODULES: any[] = [PipesModule, AdminApiModule];
const GLOBAL_THIRD_PROVIDER: any[] = [
  {
    provide: AdminApiConfiguration,

    useValue: { rootUrl: environment.mocked ? '' : GET_CONFIG().adminHost },
  },
];
// #endregion

export const API_INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useExisting: forwardRef(() => ApiInterceptor),
  multi: true,
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    HttpClientModule,
    GlobalConfigModule.forRoot(),
    CoreModule,
    SharedModule,
    LayoutModule,
    RoutesModule,
    STWidgetModule,
    ...I18NSERVICE_MODULES,
    ...GLOBAL_THIRD_MODULES,
    ...FORM_MODULES,
  ],
  providers: [
    StorageService,
    AuthGuard,
    ApiInterceptor,
    API_INTERCEPTOR_PROVIDER,
    ...GLOBAL_THIRD_PROVIDER,
    ...LANG_PROVIDES,
    ...INTERCEPTOR_PROVIDES,
    ...I18NSERVICE_PROVIDES,
    ...APPINIT_PROVIDES,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
