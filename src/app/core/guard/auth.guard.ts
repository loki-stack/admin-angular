import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { ApiInterceptor, StorageService } from '@services';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private storageService: StorageService, private apiInterceptor: ApiInterceptor) { }

  canActivate() {
    if (this.storageService.CheckLogin()) {
      this.apiInterceptor.bearer(this.storageService.GetLoginInfo()!.tokenString);
      this.apiInterceptor.applicationId(this.storageService.GetLoginInfo()!.applicationId);
      return true;
    }
    this.router.navigate(['passport/login']);
    return false;
  }
}
