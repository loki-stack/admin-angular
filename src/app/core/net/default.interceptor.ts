import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { NzNotificationService } from 'ng-zorro-antd/notification';

const CODEMESSAGE = {
  200: 'OK',
  201: 'Created',
  202: 'Accepted',
  204: 'No Conten',
  400: 'Bad Request',
  401: 'Unauthorized',
  403: 'Forbidden',
  404: 'Not Found',
  406: 'Not Acceptable',
  410: 'Gone',
  422: 'Unprocessable Entity',
  500: 'Internal Server Error',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Timeout',
  0: 'Lỗi mạng',
};

/**
 * Interceper `app.module.ts`
 */
@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  private getLocales(input: string): string {
    if (input) {
      return this.injector.get(ALAIN_I18N_TOKEN).fanyi(input);
    }
    return '';
  }

  private get notification(): NzNotificationService {
    return this.injector.get(NzNotificationService);
  }

  private goTo(url: string) {
    setTimeout(() => this.injector.get(Router).navigateByUrl(url));
  }

  private checkStatus(ev: any) {
    if ((ev.status >= 200 && ev.status < 300) || ev.status === 401) {
      return;
    }

    const errortext = CODEMESSAGE[ev.status] || ev.statusText;
    let errormess = 'Lỗi mạng';
    if (ev && ev.error && ev.error.message) {
      errormess = ev.error.message;
    }
    this.notification.error(this.getLocales('Đã xảy ra lỗi') + `: ${this.getLocales(errortext)}`, this.getLocales(errormess));
  }

  private handleData(ev: HttpResponseBase) {
    if (ev.status > 0) {
      this.injector.get(_HttpClient).end();
    }
    this.checkStatus(ev);
    switch (ev.status) {
      case 200:
        // handler response
        //  Error{ status: 1, msg: 'Get error' }
        //  Success：：{ status: 0, response: {  } }
        // if (event instanceof HttpResponse) {
        //     const body: any = event.body;
        //     if (body && body.status !== 0) {
        //         this.msg.error(body.msg);
        //         // this.http.get('/').subscribe()
        //         return throwError({});
        //     } else {
        //         return of(new HttpResponse(Object.assign(event, { body: body.response })));
        //         return of(event);
        //     }
        // }
        break;

      case 401:
        // check login
        const router = this.injector.get(Router);
        if (router.url === '/passport/login') {
          // Do nothing
        } else {
          this.notification.error(
            this.getLocales('Chưa đăng nhập hoặc đăng nhập đã hết hạn, vui lòng đăng nhập lại.'),
            this.getLocales('Unauthorized'),
          );
          // Call token service
          (this.injector.get(DA_SERVICE_TOKEN) as ITokenService).clear();
          this.goTo('/passport/login');
        }

        break;
      case 400:
        break;
      case 403:
      case 404:
      case 500:
        // console.warn(this.getLocales('Lỗi không xác định, kiểm tra CORS'), ev);
        // this.goTo(`/exception/${ev.status}`);
        break;
      case 0:
        // this.goTo(`/exception/${ev.status}`);
        break;
      default:
        // if (ev instanceof HttpErrorResponse) {
        // console.warn(this.getLocales('Lỗi không xác định, kiểm tra CORS'), ev);
        // return throwError(ev);
        // }
        break;
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = req.url;
    if (!url.startsWith('https://') && !url.startsWith('http://')) {
      url = environment.SERVER_URL + url;
    }

    const newReq = req.clone({ url });
    return next.handle(newReq).pipe(
      mergeMap((event: any) => {
        if (event instanceof HttpResponseBase) {
          this.handleData(event);
        }
        return of(event);
      }),
      catchError((err: HttpErrorResponse) => {
        this.handleData(err);
        return throwError(err);
      }),
    );
  }
}
