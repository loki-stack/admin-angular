import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ArrayGroupPipe } from './array-group.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [ArrayGroupPipe]
})
export class PipesModule { }
