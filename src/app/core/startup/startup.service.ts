import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ACLService } from '@delon/acl';
import { ALAIN_I18N_TOKEN, Menu, MenuService, SettingsService, TitleService } from '@delon/theme';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, zip } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { I18NService } from '../i18n/i18n.service';

import { CacheService } from '@delon/cache';
import { BsdNavigationService, StorageService } from '@services';
import { LoginResponse, NavigationModel } from '@services/admin/models';
import { BsdParameterService } from '@services/admin/services';
import { NzIconService } from 'ng-zorro-antd/icon';
import { ICONS } from '../../../style-icons';
import { ICONS_AUTO } from '../../../style-icons-auto';
import { ApiInterceptor } from './../../services/api-interceptor';
import { ROLE_CONSTANT } from './../../shared/utils/const';

/**
 * Start up
 */
@Injectable()
export class StartupService {
  constructor(
    iconSrv: NzIconService,
    private storageService: StorageService,
    private apiInterceptor: ApiInterceptor,
    private navigationService: BsdNavigationService,
    private menuService: MenuService,
    private translate: TranslateService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    private settingService: SettingsService,
    private aclService: ACLService,
    private titleService: TitleService,
    private parameterService: BsdParameterService,
    private httpClient: HttpClient,
    private cacheService: CacheService,
  ) {
    iconSrv.addIcon(...ICONS_AUTO, ...ICONS);
  }
  private loadRolePermission(loginInfo: LoginResponse) {
    const data = loginInfo.listRole;
    const roles = Array.isArray(data) ? data.map((x) => x.code) : [];
    this.aclService.setRole(roles);
    // console.('roles', roles);
  }
  private loadRightPermission(loginInfo: LoginResponse) {
    const data = loginInfo.listRight;
    const rights = Array.isArray(data) ? data.filter((x) => x.enable === true).map((x) => x.code) : [];
    this.aclService.setAbility(rights);
  }

  private loadParameter() {
    this.parameterService.getAll().subscribe(
      (responseData) => {
        if (responseData.code === 200) {
          this.cacheService.set('parameter', responseData.data);
          // console.debug('parameter', responseData.data);
        }
      },
      (error) => {
        console.error(error);
      },
    );
  }

  private loadMenu(loginInfo: LoginResponse) {
    this.navigationService.getTree().subscribe(
      (responseData) => {
        if (responseData.code === 200) {
          const menuResult = {
            text: 'Admin',
            i18n: 'menu.admin',
            group: true,
            reuse: true,
            hideInBreadcrumb: true,
            children: [],
          };
          responseData.data.map((item) => {
            const model = this.mapMenu(item, loginInfo);
            if (model) {
              menuResult.children.push(model);
            }
            return item;
          });
          this.menuService.add([menuResult]);
          this.menuService.change.subscribe(() => {
            setTimeout(() => {
              // load menu complete
            }, 1000);
          });
        }
      },
      (error) => {
        console.error(error);
      },
    );
  }

  load(): Promise<any> {
    const loginInfo = this.storageService.GetLoginInfo();
    // validateLoginInfo
    if (this.storageService.CheckLogin() && loginInfo && loginInfo.userModel) {
      this.apiInterceptor.bearer(this.storageService.GetLoginInfo().tokenString);
      this.apiInterceptor.applicationId(this.storageService.GetLoginInfo().applicationId);
      // Setting App
      this.settingService.setApp({
        name: '',
        description: 'Quản trị hệ thống',
      });
      // Setting User
      this.settingService.setUser({
        name: loginInfo.userModel.userName,
        avatar: loginInfo.userModel.avatarUrl,
        email: loginInfo.userModel.email,
      });

      // Setting Title
      this.titleService.default = 'T2 Project';
      this.titleService.prefix = 'Stup.vn ';

      // Setting
      this.loadRolePermission(loginInfo);
      this.loadRightPermission(loginInfo);
      debugger
      this.loadMenu(loginInfo);
      // parameter
      this.loadParameter();
    }

    // only works with promises
    // https://github.com/angular/angular/issues/15088
    return new Promise((resolve) => {
      zip(this.httpClient.get(`assets/tmp/i18n/${this.i18n.defaultLang}.json`), this.httpClient.get('assets/tmp/app-data.json'))
        .pipe(
          catchError(([langData, appData]) => {
            resolve(null);
            return [langData, appData];
          }),
        )
        .subscribe(
          ([langData]) => {
            // setting language data
            this.translate.setTranslation(this.i18n.defaultLang, langData);
            this.translate.setDefaultLang(this.i18n.defaultLang);

            // Setting App
            // this.settingService.setApp(res.app);
            // Setting User
            // this.settingService.setUser(res.user);
            // Set permission
            // this.aclService.setFull(true);
            // Setting Menu
            // this.menuService.add(res.menu);
            // Setting Title
            // this.titleService.default = '';
            // this.titleService.suffix = res.app.name;
          },
          () => {},
          () => {
            resolve(null);
          },
        );
    });
  }

  mapMenu(apiMenu: NavigationModel, loginInfo: LoginResponse): Menu {
    const rights = loginInfo.listRight;
    const roles = loginInfo.listRole;
    debugger
    // SYS_ADMIN view all menu
    if (roles.find((x) => x.id === ROLE_CONSTANT.SYS_ADMIN) || rights.find((x) => x.code === apiMenu.code + '.VIEW')) {
      const result: Menu = {
        text: apiMenu.name,
        i18n: apiMenu.name,
        icon: apiMenu.iconClass
          ? apiMenu.iconClass.includes('anticon anticon-')
            ? apiMenu.iconClass
            : 'anticon anticon-' + apiMenu.iconClass
          : 'anticon anticon-plus',
        reuse: true,
        link: '/admin/' + apiMenu.urlRewrite,
        disabled: !apiMenu.status,
      };
      if (apiMenu.subChild && Array.isArray(apiMenu.subChild) && apiMenu.subChild.length > 0) {
        result.children = [];
        delete result.link;
        result.externalLink = '/admin/' + apiMenu.urlRewrite;
        apiMenu.subChild.map((item) => {
          const model = this.mapMenu(item, loginInfo);
          if (model) {
            result.children.push(model);
          }
          return item;
        });
      }
      return result;
    }

    return null;
  }
}
