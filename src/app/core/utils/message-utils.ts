export class MessageUtil {
    static validRequired = 'Bạn cần nhập thông tin';
    static validEmail = 'Email không đúng định dạng';
    static validEmailAlreadyInUse = 'Email đã tồn tại';
    static validMax = 'Dữ liệu nhập không được lớn hơn';
    static validMin = 'Dữ liệu nhập không được nhỏ hơn';
}
