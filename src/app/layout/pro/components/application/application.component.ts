import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ALAIN_I18N_TOKEN, Menu, MenuService, SettingsService } from '@delon/theme';
import { NzModalService } from 'ng-zorro-antd/modal';
import { I18NService } from '../../../../core';
import { ApiInterceptor, BsdNavigationService, StorageService } from '../../../../services';
import { ApplicationModel, LoginResponse, NavigationModel } from '../../../../services/admin/models';

@Component({
  selector: 'layout-pro-application',
  templateUrl: 'application.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetApplicationComponent implements OnInit {
  constructor(
    public settings: SettingsService,
    private router: Router,
    private apiInterceptor: ApiInterceptor,
    private storageService: StorageService,
    private navigationService: BsdNavigationService,
    private menuService: MenuService,
    private modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {}
  loginInfo: LoginResponse;
  currentApp: ApplicationModel;
  listApp: ApplicationModel[] = [];
  ngOnInit(): void {
    this.loginInfo = this.storageService.GetLoginInfo();
    this.listApp = this.loginInfo ? this.loginInfo.listApplication : [];

    this.currentApp = this.listApp.find((x) => x.id === this.loginInfo.applicationId);
  }
  mapMenu(apiMenu: NavigationModel): Menu {
    const result: Menu = {
      text: apiMenu.name,
      i18n: apiMenu.name,
    };
    if (apiMenu.subChild && Array.isArray(apiMenu.subChild) && apiMenu.subChild.length > 0) {
      result.icon = apiMenu.iconClass;
      result.children = [];
      apiMenu.subChild.map((item) => {
        const model = this.mapMenu(item);
        result.children.push(model);
        return item;
      });
    } else {
      result.reuse = true;
      result.link = '/admin' + apiMenu.urlRewrite;
    }
    return result;
  }
  private loadMenu(loginInfo: any) {
    this.menuService.clear();
    this.navigationService.getTree().subscribe(
      (responseData) => {
        if (responseData.code === 200) {
          const menuResult = {
            text: 'Admin',
            i18n: 'menu.admin',
            group: true,
            hideInBreadcrumb: true,
            children: [],
          };
          responseData.data.map((item) => {
            const model = this.mapMenu(item);
            menuResult.children.push(model);
            return item;
          });
          this.menuService.add([menuResult]);
        }
      },
      (error) => {
        console.error(error);
      },
    );
  }
  selectApp() {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Thay đổi cửa hàng quản trị'),
      nzContent: this.i18n.fanyi('Tiếp tục sẽ mất tác vụ đang thao tác. Tiếp tục đổi?'),
      nzOkText: this.i18n.fanyi('Đồng ý'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOnOk: () => {
        this.storageService.Login({
          ...this.loginInfo,
          applicationModel: this.currentApp,
          applicationId: this.currentApp.id,
        });
        this.apiInterceptor.applicationId(this.currentApp.id);
        this.router.navigateByUrl('/');
        this.loadMenu(this.storageService.GetLoginInfo());
      },
      nzOnCancel: () => {
        this.currentApp = this.listApp.find((x) => x.id === this.loginInfo.applicationId);
      },
    });
  }
}
