import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewRef } from '@angular/core';
import { format } from 'date-fns';

@Component({
  selector: 'layout-pro-clock',
  styleUrls: ['clock.component.scss'],
  templateUrl: 'clock.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetClockComponent implements OnInit, OnDestroy {
  constructor(
    private cdr: ChangeDetectorRef,
  ) { }
  interval: any;
  DisplayTime = format(new Date, 'dd/MM/yyyy HH:mm:ss Z');
  ngOnInit(): void {
    this.interval = setInterval(() => {
      this.DisplayTime = format(new Date, 'dd/MM/yyyy HH:mm:ss Z');
      if (!(this.cdr as ViewRef).destroyed) {
        this.cdr.detectChanges();
      }
    }, 1000);
  }
  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

}
