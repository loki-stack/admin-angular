import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { SettingsService } from '@delon/theme';
import { environment } from '@env/environment';
@Component({
  selector: 'layout-pro-footer',
  templateUrl: './footer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProFooterComponent implements OnInit {
  get year() {
    return this.setting.app.year;
  }
  innerWidth: number;
  innerHeight: number;
  isDebug: boolean;
  constructor(private setting: SettingsService) {

  }
  ngOnInit() {
    this.isDebug = environment.debug;
    if (this.isDebug) {
      this.innerWidth = window.innerWidth;
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.isDebug) {
      this.innerWidth = window.innerWidth;
    }
  }
}
