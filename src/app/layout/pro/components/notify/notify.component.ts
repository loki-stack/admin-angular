import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { NoticeIconList, NoticeItem } from '@delon/abc/notice-icon';

import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ModalHelper } from '@delon/theme';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { Subscription } from 'rxjs';
import { StorageService } from './../../../../services/storage/storage.service';

@Component({
  selector: 'layout-pro-notify',
  templateUrl: './notify.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetNotifyComponent implements OnInit, OnDestroy {
  data: NoticeItem[] = [
    {
      title: 'Hòm thư',
      list: [],
      emptyText: 'Không có thông báo',
      emptyImage: 'assets/images/notification.svg',
      clearText: 'Xem chi tiết',
    },
    {
      title: 'Góp ý',
      list: [],
      emptyText: 'Không có thông báo',
      emptyImage: 'assets/images/notification.svg',
      clearText: 'Xem chi tiết',
    },
  ];
  count = 0;
  count1 = 0;
  count2 = 0;
  loading = false;
  subscription: Subscription;
  interVal: any;
  constructor(
    public router: Router,
    private cdr: ChangeDetectorRef,
    // public inboxService: CMSInboxService,
    public storageService: StorageService,
    // public feedbackService: CMSFeedBackT2Service,
    public modal: ModalHelper,
    @Inject(DOCUMENT) public document: Document,
  ) {}

  ngOnInit(): void {
    this.loadData();
    this.interVal = setInterval(() => {
      this.loadData();
    }, 15000);
    this.subscription = this.storageService.notifyObservable$.subscribe((res) => {
      if (res === 'notify') {
        this.loadData();
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    clearInterval(this.interVal);
  }

  updateNoticeData(notices: NoticeIconList[], title: string) {
    this.data.find((w) => w.title === title).list = [];
    notices.forEach((item) => {
      const newItem = { ...item };
      if (newItem.datetime) {
        newItem.datetime = formatDistanceToNow(new Date(item.datetime), {
          locale: (window as any).__locale__,
        });
      }
      if (newItem.extra && newItem.status) {
        newItem.color = {
          error: 'red',
          warning: 'gold',
        }[newItem.status];
      }
      this.data.find((w) => w.title === title).list.push(newItem);
    });
  }

  loadData() {
    // this.loadInboxData();
    // this.loadFeedbackData();
  }
  // loadFeedbackData() {
  //   this.loading = true;
  //   const payload = {
  //     page: 1,
  //     size: 10000,
  //     Status: 'NEW',
  //   };
  //   this.feedbackService.getByFilter(JSON.stringify(payload)).subscribe((responseData) => {
  //     if (responseData.code === 200) {
  //       this.count1 = responseData.data.totalElements;
  //       this.count = this.count1 + this.count2;
  //       const dataReturn = responseData.data.content;
  //       const itemList = dataReturn.map((x: any) => {
  //         const result = {
  //           id: x.id,
  //           description: x.title,
  //           type: 'Góp ý',
  //           // title: `${x.name}`,
  //           title: `${x.name}\n(${x.email})`,
  //           // extra: x.email,
  //           // extra: formatDistanceToNow(new Date(x.createdDate), {
  //           //   locale: (window as any).__locale__,
  //           // }),
  //           datetime: x.createdDate,
  //           item: {
  //             ...x,
  //           },
  //           // avatar:
  //         } as NoticeIconList;
  //         return result;
  //       });
  //       this.updateNoticeData(itemList, 'Góp ý');
  //       this.loading = false;
  //       this.cdr.detectChanges();
  //     }
  //   });
  // }
  // loadInboxData() {
  //   const payload = {
  //     page: 1,
  //     size: 10000,
  //     status: 0,
  //     IsUnRead: true,
  //     SenderId: USER_CONSTANT.SYS_ADMIN_ID,
  //   };
  //   this.inboxService.getInboxThreadFilter(JSON.stringify(payload)).subscribe((responseData) => {
  //     if (responseData.code === 200) {
  //       this.count2 = responseData.data.content.filter((x) => x.status === 0).length;
  //       this.count = this.count1 + this.count2;
  //       const dataReturn = responseData.data.content;
  //       const itemList = dataReturn
  //         .filter((x) => x.status === 0)
  //         .map((x: any) => {
  //           const result = {
  //             id: x.id,
  //             description: x.lastMessage,
  //             type: 'Hòm thư',
  //             title: x.recipientInfo.fullName,
  //             avatar: x.recipientInfo.avatarFullPath,
  //             datetime: x.lastUpdate,
  //             // extra: formatDistanceToNow(new Date(x.lastUpdate), {
  //             //   locale: (window as any).__locale__,
  //             // }),

  //             item: {
  //               ...x,
  //             },
  //           } as NoticeIconList;
  //           return result;
  //         });
  //       this.updateNoticeData(itemList, 'Hòm thư');
  //       this.loading = false;
  //       this.cdr.detectChanges();
  //     }
  //   });
  // }

  clear(type: string) {
    if (type === 'Hòm thư') {
      this.router.navigate(['/admin/inbox/']);
    } else {
      this.router.navigate(['/admin/feedback/']);
    }
    // tricky close notification
    const node = this.document.getElementsByName('cdk-overlay-backdrop');
    if (node && node.length > 0) {
      node[0].click();
    }
  }

  select(res: any) {
    // if (res.title === 'Hòm thư') {
    //   this.actionViewInbox(res.item.item);
    //   // this.router.navigate(['/admin/inbox/']);
    // } else {
    //   // this.router.navigate(['/admin/feedback/']);
    //   this.actionViewFeedBack(res.item.item);
    // }
    // tricky close notification
    const node = this.document.getElementsByName('cdk-overlay-backdrop');
    if (node && node.length > 0) {
      node[0].click();
    }
  }
  // actionViewFeedBack(item: any): any {
  //   this.modal.createStatic(FeedbackComponentsItemComponent, { i: item }).subscribe(() => {});
  // }
  // actionViewInbox(item: any): any {
  //   this.modal.createStatic(InboxComponentsItemComponent, { i: item }).subscribe(() => {});
  // }
}
