import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewRef } from '@angular/core';
import { Router } from '@angular/router';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenModel, ITokenService } from '@delon/auth';
import { SettingsService } from '@delon/theme';
import { SysAuthenticationService, StorageService } from '@services';
import { GET_CONFIG } from '@shared';

const CONFIG = GET_CONFIG();
@Component({
  selector: 'layout-pro-user',
  templateUrl: 'user.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetUserComponent implements OnInit, OnDestroy {
  NameKey: string;
  NameAvatar: string;
  fileHost: string;
  constructor(
    private cdr: ChangeDetectorRef,
    public settings: SettingsService,
    private storageService: StorageService,
    private authenticationService: SysAuthenticationService,
    private reuseTabService: ReuseTabService,
    private router: Router,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {
    this.fileHost = CONFIG.fileHost;
    this.storageService.notifyObservable$.subscribe((res) => {
      if (res === 'UPDATE_JWT') {
        this.authenticationService.jwtInfo().subscribe(
          (responseData) => {
            if (responseData.code === 200) {
              this.storageService.Login(responseData.data);
              this.reuseTabService.clear();
              const token = {
                email: 'loki@susu.com',
                id: 10000,
                name: 'admin',
                time: 1560939613287,
                token: responseData.data.tokenString,
              } as ITokenModel;
              this.tokenService.set(token);
              this.NameKey = this.settings.user ? this.settings.user.name[0].toUpperCase() : 'N';
              this.NameAvatar = this.settings.user ? this.fileHost + '/' + this.settings.user.avatar : './assets/default-user.png';
            }
          },
          (error) => {},
        );
      }
    });
  }

  ngOnInit(): void {
    this.NameKey = this.settings.user ? this.settings.user.name[0].toUpperCase() : 'N';
    this.NameAvatar = this.settings.user ? this.fileHost + '/' + this.settings.user.avatar : './assets/default-user.png';

    // mock
    // const token = this.tokenService.get() || {
    //   token: 'nothing',
    //   name: 'Thor',
    //   avatar: './assets/logo-color.svg',
    //   email: 'mri.hannibal@gmai.com',
    // };
    // this.settings.user.name = 'Thor';
    // this.tokenService.set(token);
  }
  ngOnDestroy(): void {
    this.cdr.detach();
  }
  logout() {
    this.tokenService.clear();
    this.storageService.Logout();
    this.router.navigateByUrl(this.tokenService.login_url);
  }
}
