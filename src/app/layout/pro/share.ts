// #region shared components

import { ProPageGridComponent } from './components/page-grid/page-grid.component';
import { ProPageHeaderWrapperComponent } from './components/page-header-wrapper/page-header-wrapper.component';
export const PRO_SHARED_COMPONENTS = [ProPageGridComponent, ProPageHeaderWrapperComponent];

// #endregion
