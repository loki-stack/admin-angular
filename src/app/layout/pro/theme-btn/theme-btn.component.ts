import { Platform } from '@angular/cdk/platform';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2 } from '@angular/core';
import { AlainConfigService } from '@delon/util';
import { StorageService } from '@services';

type SiteTheme = 'default' | 'dark' | 'compact';

@Component({
  selector: 'layout-theme-btn',
  templateUrl: './theme-btn.component.html',
  styleUrls: ['./theme-btn.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutThemeBtnComponent implements OnInit, OnDestroy {
  @Input() theme: SiteTheme = 'default';
  @Output() themeChange = new EventEmitter<SiteTheme>();
  private el: HTMLLinkElement;

  constructor(
    private renderer: Renderer2,
    private configSrv: AlainConfigService,
    private storageService: StorageService,
    @Inject(DOCUMENT) public document: Document,
    private platform: Platform,
  ) {}

  ngOnInit(): void {
    this.initTheme();
  }

  private initTheme(): void {
    if (!this.platform.isBrowser) {
      return;
    }
    this.theme = (this.storageService.getItem('site-theme') as SiteTheme) || 'default';
    this.updateChartTheme();
    this.onThemeChange(this.theme);
  }

  private updateChartTheme(): void {
    this.configSrv.set('chart', { theme: this.theme === 'dark' ? 'dark' : '' });
  }

  onThemeChange(theme: SiteTheme): void {
    if (!this.platform.isBrowser) {
      return;
    }
    this.theme = theme;
    this.themeChange.emit(this.theme);
    this.renderer.setAttribute(this.document.body, 'data-theme', theme);
    const dom = this.document.getElementById('site-theme');
    if (dom) {
      dom.remove();
    }
    this.storageService.removeItem('site-theme');
    if (theme !== 'default') {
      const el = (this.el = this.document.createElement('link'));
      el.type = 'text/css';
      el.rel = 'stylesheet';
      el.id = 'site-theme';
      el.href = `assets/style.${theme}.css`;

      this.storageService.setItem('site-theme', theme);
      this.document.body.append(el);
    }
    this.updateChartTheme();
  }

  ngOnDestroy(): void {
    if (this.el) {
      this.document.body.removeChild(this.el);
    }
  }
}
