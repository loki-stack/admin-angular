import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { ACLGuard } from '@delon/acl';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'HOME',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      // /**
      //  * Routing đến các trang quản trị
      //  */
      {
        path: 'user-profile',
        loadChildren: () => import('./pages/user-profile/user-profile.module').then((m) => m.UserProfileModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'user-profile',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      // /**
      //  * Routing đến các trang quản trị
      //  */
      {
        path: 'user',
        loadChildren: () => import('./pages/user/user.module').then((m) => m.UserModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'user',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'right',
        loadChildren: () => import('./pages/right/right.module').then((m) => m.RightModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'right',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'role',
        loadChildren: () => import('./pages/role/role.module').then((m) => m.RoleModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'role',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'navigation',
        loadChildren: () => import('./pages/navigation/navigation.module').then((m) => m.NavigationModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'navigation',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'permission',
        loadChildren: () => import('./pages/permission/permission.module').then((m) => m.PermissionModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'permission',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'parameter',
        loadChildren: () => import('./pages/parameter/parameter.module').then((m) => m.ParameterModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'parameter',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
      {
        path: 'logs',
        loadChildren: () => import('./pages/logs/logs.module').then((m) => m.LogsModule),
        canLoad: [ACLGuard],
        data: {
          animation: 'logs',
          reuse: true,
          // guard: {
          //   role: ['SYS_ADMIN'],
          //   // ability: [ 10, 'USER-EDIT' ],
          //   mode: 'oneOf', // allOf
          // } as ACLType,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
