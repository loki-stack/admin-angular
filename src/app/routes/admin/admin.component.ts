import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

import { bounceInLeft } from 'ng-animate';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less'],
  animations: [trigger('bounce', [transition('* <=> *', useAnimation(bounceInLeft))])],
})
export class AdminComponent implements OnInit {
  constructor() {}
  ngOnInit() {}
  getState(outlet) {
    return outlet.activatedRouteData;
  }
}
