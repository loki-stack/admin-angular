import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { UserComponentsChangePasswordComponent } from './pages/user/components/change-password/change-password.component';

const COMPONENTS = [AdminComponent];
const COMPONENTS_NOROUNT = [UserComponentsChangePasswordComponent];

@NgModule({
  imports: [SharedModule, AdminRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class AdminModule { }
