import { AfterContentInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { _HttpClient } from '@delon/theme';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
})
export class HomeComponent implements OnInit, OnChanges {
  constructor() {}
  selectHour = false;
  selectMin = true;
  hour = 0;
  min = 0;
  hourStr = '00';
  minStr = '00';
  isAM = true;
  @Input() time: Date;
  @Output() updateTime = new EventEmitter<any>();
  ngOnInit(): void {
    // this.setMin(this.min);
    // this.setMin(this.hour);
    // this.getHtmlHour();
    // this.getHtmlMin();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.time) {
      this.hour = this.time.getUTCHours();
      this.min = this.time.getMinutes();
      this.setHour(this.hour);
      this.setMin(this.min);
    }
  }

  toDegrees(angle) {
    return angle * (180 / Math.PI);
  }
  toRadians(angle) {
    return angle * (Math.PI / 180);
  }
  getHtmlHour() {
    let html = '';
    for (let index = 0; index < 12; index++) {
      html =
        html +
        `
      <div class="hour hour_${index}" [class.active]="hour===${index}" (click)="setHour(${index})" [ngStyle]="getPosition2(${index})">
        ${index}
      </div>
      `;
    }
    console.log(html);
  }
  getHtmlMin() {
    let html = '';
    for (let index = 0; index < 60; index++) {
      html =
        html +
        `
      <div class="min min_${index}" [class.active]="min===${index}" (click)="setMin(${index})" [ngStyle]="getPosition3(${index})">
        ${index}
      </div>
      `;
    }
    console.log(html);
  }
  getPosition(hour) {
    const delta = 30;
    const r = 180;
    const r2 = 20;
    const x = Math.cos(this.toRadians(hour * delta)) * r;
    const y = Math.sin(this.toRadians(hour * delta)) * r;
    const bottom = r + x - r2 + 20;
    const left = r + y - r2 + 20;
    return {
      left: left + 'px',
      bottom: bottom + 'px',
    };
  }
  getPosition2(hour) {
    const delta = 30;
    const r = 225;
    const r2 = 25;
    const x = Math.cos(this.toRadians(hour * delta)) * r;
    const y = Math.sin(this.toRadians(hour * delta)) * r;
    const bottom = r + x - r2 + 25;
    const left = r + y - r2 + 25;
    return {
      left: left + 'px',
      bottom: bottom + 'px',
    };
  }
  getPosition3(min) {
    const delta = 6;
    const r = 225;
    const r2 = 25;
    const x = Math.cos(this.toRadians(min * delta)) * r;
    const y = Math.sin(this.toRadians(min * delta)) * r;
    const bottom = r + x - r2 + 25;
    const left = r + y - r2 + 25;
    return {
      left: left + 'px',
      bottom: bottom + 'px',
    };
  }
  setMin(e) {
    this.min = e;
    this.minStr = (this.min > 9 ? '' : 0) + this.min.toString();
    this.time.setMinutes(e);
    this.updateTime.emit(this.time);
  }
  setHour(e) {
    this.hour = e;
    this.hourStr = (this.hour > 9 ? '' : 0) + this.hour.toString();
    this.time.setHours(e);
    this.updateTime.emit(this.time);
  }
  focusHour() {
    this.selectHour = true;
    this.selectMin = false;
  }
  focusMin() {
    this.selectHour = false;
    this.selectMin = true;
  }
}
