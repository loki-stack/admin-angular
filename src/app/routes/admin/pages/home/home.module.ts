import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

const COMPONENTS = [HomeComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, HomeRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class HomeModule { }
