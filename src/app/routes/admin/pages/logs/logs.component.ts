import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, ModalHelper, _HttpClient } from '@delon/theme';
import { BsdLogsService } from '@services';
import { COLUMN_WIDTH, TemplateFilter, TemplateList } from '@shared';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogsComponent extends TemplateList {
  levelEnum = [
    {
      value: 2,
      i18n: 'Info',
      color: '#108ee9',
    },
    {
      value: 3,
      i18n: 'Warning',
      color: '#f50',
    },
    {
      value: 4,
      i18n: 'Error',
      color: '#f5222d',
    },
    {
      value: 5,
      i18n: 'Fatal',
      color: 'black',
    },
  ];
  @ViewChild('expandTpl') private expandTpl: TemplateRef<any>;

  constructor(
    public msg: NzMessageService,
    public route: ActivatedRoute,
    public modal: ModalHelper,
    public modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService,
    public cdr: ChangeDetectorRef,
    public router: Router,
    public service: BsdLogsService,
  ) {
    super(msg, route, modal, modalService, i18n, cdr, router);
  }
  initConfig() {
    this.lgConfig.actionsSelf = [];
    this.lgConfig.actionsMulti = [];
    this.lgConfig.actionsSingle = [];
    this.lgConfig.tagFilter = JSON.parse(JSON.stringify(this.levelEnum));
    this.lgConfig.columns = [
      {
        index: 'application',
        i18n: 'Dịch vụ',
        width: '100px',
      },
      {
        index: 'timestamp',
        i18n: 'Ngày ghi nhận',
        width: COLUMN_WIDTH.DATETIME,
        type: 'date-time',
        onTop: true,
      },
      {
        index: 'level',
        i18n: 'Loại',
        width: '85px',
        type: 'tag',
        onTop: true,
        config: {
          enum: this.levelEnum,
        },
      },
      {
        index: 'message',
        onTop: true,
        i18n: 'Nội dung',
        config: {
          maxRow: 3,
        },
      },
    ];

    this.lgConfig.expandColumn = {
      render: this.expandTpl,
    };
    this.lgConfig.toolSize = '20px';
  }

  actionSearch(filter: TemplateFilter) {
    if (filter.listSelectedTag && filter.listSelectedTag.length > 0) {
      const filterObj = JSON.parse(filter.filter);
      filterObj.listLevel = filter.listSelectedTag;
      filter.filter = JSON.stringify(filterObj);
    }
    const payload = {
      page: filter.page,
      size: filter.size,
      sort: filter.sort,
      filter: filter.filter,
    };
    const cmd = this.service.getFilter(payload);
    return cmd;
  }

  processData(input: any[]): any[] {
    const output = [...input];
    output.forEach((x) => {
      const log_eventObj = JSON.parse(x.log_event);
      x.application = log_eventObj.Properties.Application;
      x.content = log_eventObj.Properties.Application;
      x.log_eventObj = log_eventObj;
    });
    return output;
  }
}
