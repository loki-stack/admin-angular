import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { LogsRoutingModule } from './logs-routing.module';
import { LogsComponent } from './logs.component';

const COMPONENTS = [LogsComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, LogsRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class LogsModule { }
