import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { IdmRightService } from '@services';
import { TemplateItem, trimObject } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';

import { NzModalRef } from 'ng-zorro-antd/modal';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-right-components-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less'],
})
export class RightComponentsItemComponent extends TemplateItem implements OnInit {
  @ViewChild('sf') public sf: SFComponent;
  i: any;
  groupCode: any;
  navCode: any;
  type: any;
  formData: any;
  isLoadingForm = false;
  loading = false;
  layout = 'horizontal';
  title = 'Thêm';
  schema: SFSchema = {
    properties: {
      code: {
        type: 'string',
        title: this.i18n.fanyi('Mã quyền'),
        format: 'regex',
        pattern: `^[a-zA-Z0-9_-]{1,99}$`,
      },
      name: { type: 'string', title: this.i18n.fanyi('Tên quyền') },
      description: { type: 'string', title: this.i18n.fanyi('Ghi chú') },
    },

    required: ['code', 'name', 'groupCode'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        pattern: this.i18n.fanyi('Trường mã viết liền không dấu'),
      },
      spanLabelFixed: 140,
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
    },
    $description: {
      grid: {
        gutter: 15,
        span: 24,
      },
      widget: 'textarea',
      autosize: { minRows: 2, maxRows: 6 },
    },
    $groupCode: {
      widget: 'autocomplete',
    },
  };
  constructor(
    private modal: NzModalRef,
    private rightService: IdmRightService,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    @Inject(DOCUMENT) public document: Document,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {
    super(document);
  }

  ngOnInit(): void {
    this.navCode = this.groupCode?.replace('NAVIGATION.', '');
    if (this.i.id) {
      this.title = 'Cập nhật';
      if (this.type === 'VIEW') {
        this.title = 'Xem chi tiết';
      }
    }
    this.loadFormData(this.i.id);
  }
  loadFormData(id) {
    if (id) {
      this.i.code = this.i.code.replace(this.navCode + '_', '');
      this.formData = {
        ...this.i,
      };
    } else {
      this.formData = {};
      this.formData.permission = {};
    }
  }

  save(value: any) {
    value = trimObject(value);
    this.isLoadingForm = true;
    value.code = this.navCode + '_' + value.code;
    if (this.title === 'Thêm') {
      const payload = {
        model: {
          ...value,
          groupCode: this.groupCode,
        },
      } as IdmRightService.CreateParams;
      this.rightService
        .create(payload)
        .pipe(
          tap(
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
          ),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Thêm thành công'));
              this.modal.close(true);
            }
          },
          (error) => {
            console.error(error);
          },
        );
    } else {
      const payload = {
        id: this.i.id,
        model: {
          ...value,
        },
      } as IdmRightService.UpdateParams;
      this.rightService
        .update(payload)
        .pipe(
          tap(
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
          ),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.modal.close(true);
            }
          },
          (error) => {
            console.error(error);
          },
        );
    }
  }

  close() {
    this.modal.destroy();
  }
}
