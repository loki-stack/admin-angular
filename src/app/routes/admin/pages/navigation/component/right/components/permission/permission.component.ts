import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { IdmRightService, IdmRoleService, IdmUserService, SysApplicationService } from '@services';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-right-components-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.less'],
})
export class RightComponentsPermissionComponent implements OnInit, AfterViewInit {
  i: any;
  data: any[] = [];
  listUser = [];
  listRole = [];
  listApp = [];
  selected = {
    listRole: [],
    listUser: [],
    listRoleRaw: [],
    listUserRaw: [],
    appId: null,
  };
  q: any = {};
  constructor(
    public msg: NzMessageService,
    private userService: IdmUserService,
    private roleService: IdmRoleService,
    private rightService: IdmRightService,
    private appService: SysApplicationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {
    // this.route.queryParams.subscribe((params) => {
    //   this.q = { ...this.q, ...params };
    //   this.loadData();
    // });
  }

  ngAfterViewInit() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector('.full-page [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.full-page .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }
  setAutoFocus() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector('.ant-form [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }
  ngOnInit() {
    this.getData();
  }

  getData() {
    this.loadData();
  }
  loadData() {
    this.getAllApp();
    this.getAllUser();
    this.getAllRole();
  }
  getAllUser() {
    this.userService.getAll ().subscribe((responseData) => {
      if (responseData.code === 200) {
        this.listUser = responseData.data;
      }
    });
  }

  getAllRole() {
    this.roleService.getAll ().subscribe((responseData) => {
      if (responseData.code === 200) {
        this.listRole = responseData.data;
      }
    });
  }
  getAllApp() {
    this.appService.getAll ().subscribe((responseData) => {
      if (responseData.code === 200) {
        this.listApp = responseData.data;
      }
    });
  }

  onSelectedApp(app) {
    this.selected.appId = app.id;
    this.getRoleHasRight(this.selected.appId);
    this.getUserHasRight(this.selected.appId);
  }

  getRoleHasRight(appId) {
    const payload = {
      id: this.i.id,
      applicationId: appId,
    } as IdmRightService.GetRoleMapRightParams;
    this.rightService.getRoleMapRight(payload).subscribe((responseData) => {
      if (responseData.code === 200) {
        this.selected.listRole = responseData.data.map((x) => x.id);
        this.selected.listRoleRaw = [...this.selected.listRole];
      }
    });
  }

  getUserHasRight(appId) {
    const payload = {
      id: this.i.id,
      applicationId: appId,
    } as IdmRightService.GetUserMapRightParams;
    this.rightService.getUserMapRight(payload).subscribe((responseData) => {
      if (responseData.code === 200) {
        this.selected.listUser = responseData.data.map((x) => x.id);
        this.selected.listUserRaw = [...this.selected.listUser];
      }
    });
  }
  changeRole(event) {
    const isAdded = this.selected.listRoleRaw.length < event.length;
    if (isAdded) {
      const listAddRole = event.filter((x: any) => !this.selected.listRoleRaw.includes(x));

      const payload = {
        applicationId: this.selected.appId,
        id: this.i.id,
        listRoleId: listAddRole,
      } as IdmRightService.AddRightMapRoleParams;
      this.rightService.addRightMapRole(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          this.getRoleHasRight(this.selected.appId);
          this.getUserHasRight(this.selected.appId);
        }
      });
    } else {
      const listDeleteRole = this.selected.listRoleRaw.filter((x: any) => !event.includes(x));
      const payload = {
        applicationId: this.selected.appId,
        id: this.i.id,
        listRoleId: listDeleteRole,
      } as IdmRightService.DeleteRightMapRoleParams;
      this.rightService.deleteRightMapRole(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          this.getRoleHasRight(this.selected.appId);
          this.getUserHasRight(this.selected.appId);
        }
      });
    }
  }

  changeUser(event) {
    const isAdded = this.selected.listUserRaw.length < event.length;
    if (isAdded) {
      const listAddUser = event.filter((x: any) => !this.selected.listUserRaw.includes(x));

      const payload = {
        applicationId: this.selected.appId,
        id: this.i.id,
        listUserId: listAddUser,
      } as IdmRightService.AddRightMapUserParams;
      this.rightService.addRightMapUser(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          this.getRoleHasRight(this.selected.appId);
          this.getUserHasRight(this.selected.appId);
        }
      });
    } else {
      const listDeleteUser = this.selected.listUserRaw.filter((x: any) => !event.includes(x));
      const payload = {
        applicationId: this.selected.appId,
        id: this.i.id,
        listUserId: listDeleteUser,
      } as IdmRightService.DeleteRightMapUserParams;
      this.rightService.deleteRightMapUser(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          this.getRoleHasRight(this.selected.appId);
          this.getUserHasRight(this.selected.appId);
        }
      });
    }
  }
}
