import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, ModalHelper, _HttpClient } from '@delon/theme';
import { IdmRightService } from '@services';
import { COMMON_CONSTANT, TemplateFilter, TemplateList } from '@shared';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import { RightComponentsItemComponent } from './components/item/item.component';
import { RightComponentsPermissionComponent } from './components/permission/permission.component';

@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RightComponent extends TemplateList {
  @Input() groupCode;
  constructor(
    public msg: NzMessageService,
    public route: ActivatedRoute,
    public modal: ModalHelper,
    public modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService,
    public cdr: ChangeDetectorRef,
    public router: Router,
    public mainService: IdmRightService,
  ) {
    super(msg, route, modal, modalService, i18n, cdr, router);
    this.addComponent = RightComponentsItemComponent;
    this.editComponent = RightComponentsItemComponent;
  }
  initConfig() {
    this.lgConfig.columns = [
      {
        index: 'code',
        i18n: 'Mã',
        width: '120px',
        onTop: true,
      },
      {
        index: 'name',
        i18n: 'Tên quyền',
        width: '140px',
        onTop: true,
      },
      {
        index: 'isSystem',
        i18n: 'Loại',
        width: '140px',
        type: 'tag',
        onTop: true,
        config: {
          enum: COMMON_CONSTANT.enumRoleType,
        },
      },
      {
        index: 'description',
        i18n: 'Ghi chú',
        width: '250px',
      },
    ];
    this.lgConfig.actionsMulti = [];
    this.lgConfig.actionsSingle = [
      {
        icon: 'edit',
        if: (data: any) => !data.isSystem,
        action: (data: any) => this.edit(data),
        i18n: 'Sửa',
        i18nDescription: 'Sửa bản ghi',
      },
      {
        icon: 'cluster',
        action: (data: any) => this.permission(data),
        i18n: 'Phân quyền',
        i18nDescription: 'Phân quyền bản ghi',
      },
      {
        icon: 'delete',
        if: (data: any) => !data.isSystem,
        action: (data: any) => this.delete(data),
        i18n: 'Xóa',
        type: 'danger',
        i18nDescription: 'Xóa bản ghi',
      },
    ];
  }

  permission(item: any): any {
    this.modal.createStatic(RightComponentsPermissionComponent, { i: item, groupCode: this.groupCode }).subscribe(() => {
      this.lgOnSearch();
    });
  }
  actionDelete(item: any): Observable<any> {
    const cmd = this.mainService.delete (item.id);
    return cmd;
  }

  actionDeleteMulti(selectedData: any[]): Observable<any> {
    const listId = selectedData.map((x) => x.id);
    const cmd = this.mainService.deleteRange (listId);
    return cmd;
  }

  actionSearch(filter: TemplateFilter) {
    const filterObj = {
      groupCode: this.groupCode,
    };
    const payload = {
      page: filter.page,
      size: filter.size,
      sort: filter.sort,
      filter: JSON.stringify(filterObj),
    };
    const cmd = this.mainService.getFilter(payload);
    return cmd;
  }
}
