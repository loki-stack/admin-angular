import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFSchemaEnumType, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN } from '@delon/theme';
import { BsdNavigationService, IdmRoleService } from '@services';
import { NavigationCreateModel, NavigationModel } from '@services/admin/models.js';
import { trimObject } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzFormatEmitEvent, NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/tree';
import { forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RightComponent } from './component/right/right.component.js';
import ICON_DATAS from './icon.json';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent implements OnInit, AfterViewInit {
  @ViewChild('sf') public sf: SFComponent;
  @ViewChild('rightComponent') public rightComponent: RightComponent;

  listIcon = [];
  activetab = 0;
  loading = true;
  selectedParent: any;
  searchValue = '';
  nodes = [];
  formData: any;
  layout = 'horizontal';
  isShowPermission = false;
  activedNode: NzTreeNode;
  schema: SFSchema = {
    properties: {
      id: { type: 'string', title: this.i18n.fanyi('Id'), readOnly: true },
      parentId: { type: 'string', title: this.i18n.fanyi('Điều hướng cha') },
      name: { type: 'string', title: this.i18n.fanyi('Tên điều hướng') },
      code: {
        type: 'string',
        title: this.i18n.fanyi('Mã điều hướng'),
        format: 'regex',
        pattern: `^[a-zA-Z0-9_-]{1,99}$`,
      },
      order: { type: 'number', title: this.i18n.fanyi('Thứ tự') },
      status: { type: 'boolean', title: '', default: true },
      iconClass: { type: 'string', title: this.i18n.fanyi('Biểu tượng') },
      urlRewrite: { type: 'string', title: this.i18n.fanyi('Đường dẫn') },
    },
    required: ['name', 'code', 'name', 'order'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        pattern: this.i18n.fanyi('Trường mã viết liền không dấu'),
      },
      spanLabelFixed: 140,
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
    },
    $id: {},
    $parentId: {
      placeholder: this.i18n.fanyi('Chọn điều hướng cha'),
      widget: 'tree-select',
      allowClear: true,
    },
    $name: {
      placeholder: this.i18n.fanyi('Nhập tên điều hướng'),
    },
    $code: {
      placeholder: this.i18n.fanyi('Nhập mã điều hướng'),
    },
    $order: {
      placeholder: this.i18n.fanyi('Nhập STT'),
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 8,
        lg: 8,
        xl: 8,
      },
    },
    $status: {
      placeholder: this.i18n.fanyi('Trạng thái'),
      class: 'status-item',
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 4,
        lg: 4,
        xl: 4,
      },
      // checkedChildren: this.i18n.fanyi('ON'),
      // unCheckedChildren: this.i18n.fanyi('OFF'),
    },
    $iconClass: {
      placeholder: this.i18n.fanyi('Chọn icon'),
      widget: 'custom',
      // widget: 'autocomplete',
      // debounceTime: 100,
      // asyncData: (input: string) =>
      //   of(input ? ICON_DATAS.filter(x => x.name.includes(input)).slice(0, 10) : ICON_DATAS.slice(0, 10)),
    },
    $urlRewrite: {
      placeholder: this.i18n.fanyi('Nhập đường dẫn'),
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 24,
        lg: 24,
        xl: 24,
      },
    },
    $permission: {
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 24,
        lg: 24,
        xl: 24,
      }, 
    },
  };

  constructor(
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService,
    private roleService: IdmRoleService,
    private navigationService: BsdNavigationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {
    this.listIcon = ICON_DATAS.map((x) => x.id);
  }

  ngAfterViewInit() {
    try {
      // disabled="true"
      setTimeout(() => {
        let elm = this.document.querySelector('.full-page [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.full-page .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }
  setAutoFocus() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector('.ant-form [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }

  ngOnInit() {
    this.formData = null;
    this.cdr.detectChanges();
    const loadAllMenu = this.getMenu();
    this.loading = true;
    forkJoin([loadAllMenu]).pipe(
      tap(() => {
        this.loading = false;
        this.cdr.detectChanges();
      }),
    );
  }
 

  getMenu(): Observable<any> {
    this.loading = true;
    const cmd = this.navigationService.getTree();
    cmd
      .pipe(
        tap(() => {
          this.loading = false;
          this.cdr.detectChanges();
        }),
      )
      .subscribe((responseData) => {
        if (responseData.code === 200) {
          const result = responseData.data.map((x) => this.mapMenuObject(x));
          this.nodes = result;
          this.schema.properties.parentId.enum = responseData.data.map((x) => this.mapMenuObjectTree(x));
          if (this.nodes.length > 0) {
            this.cdr.detectChanges();
          }
        }
      });
    return cmd;
  }

  mapMenuObjectTree(item: NavigationModel): SFSchemaEnumType {
    return {
      title: item.name,
      key: item.id,
      isLeaf: !item.subChild,
      children: !item.subChild ? null : item.subChild.map((x) => this.mapMenuObjectTree(x)),
    } as SFSchemaEnumType;
  }

  mapMenuObject(item: NavigationModel): NzTreeNodeOptions {
    return {
      ...item,
      title: item.name,
      icon: item.iconClass,
      selectable: true,
      key: item.id,
      isLeaf: !item.subChild,
      expanded: true,
      children: !item.subChild ? null : item.subChild.map((x) => this.mapMenuObject(x)),
    } as NzTreeNodeOptions;
  }

  onClickTree(event: NzFormatEmitEvent): void {
    this.selectMenu(event.node.origin);
    this.activedNode = event.node;
  }

  selectMenu(item: any) {
    this.setAutoFocus();
    this.isShowPermission = false;
    if (!item) {
      this.formData = item;
    } else {
      this.formData = {
        ...item
      };

      this.activetab = 1;
    }

    this.cdr.detectChanges();
  }

  findParent(id, menu) {
    menu.forEach((x) => {
      if (x.id === id) {
        this.selectedParent = x;
        return;
      } else {
        if (Array.isArray(x.subChild) && x.subChild.length > 0) {
          this.findParent(id, x.subChild);
        }
      }
    });
  }

  save(value: any) {
    value = trimObject(value);
    if (value.id) {
      // Update
      this.selectedParent = null;
      this.findParent(value.parentId, this.nodes);
      const payload = {
        id: value.id,
        request: {
          ...value,
          parentModel: this.selectedParent,
        },
      } as BsdNavigationService.UpdateParams;
      this.loading = true;
      this.navigationService
        .update(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.cdr.detectChanges();
              this.getMenu().pipe(
                tap(() => {
                  this.loading = false;
                  this.cdr.detectChanges();
                }),
              );
            }
          },
          (error) => {
            console.error(error);
          },
        );
    } else {
      // Create
      this.selectedParent = null;
      this.findParent(value.parentId, this.nodes);
      const payload = {
        ...value,
        parentModel: this.selectedParent,
      } as NavigationCreateModel;
      this.navigationService
        .create(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.loading = true;
              this.cdr.detectChanges();
              this.getMenu().pipe(
                tap(() => {
                  this.loading = false;
                  this.cdr.detectChanges();
                }),
              );
            }
          },
          (error) => {
            console.error(error);
          },
        );
    }
  }

  addNew() {
    this.selectMenu({});
  }

  delete(item: any): any {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        this.navigationService.delete (item.origin.id).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.cdr.detectChanges();
            this.getMenu();
            this.formData = null;
            this.activedNode = null;
          },
          (error) => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
          },
        );
      },
      nzOnCancel: () => {},
    });
  }
  onInput(value: string): void {
    this.listIcon = ICON_DATAS.map((x) => x.id).filter((option) => option.toLowerCase().indexOf(value.toLowerCase()) === 0);
    this.cdr.detectChanges();
  }
  toggglePermission(isPermission) {
    this.isShowPermission = isPermission;
  }
}
