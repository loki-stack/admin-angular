import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { RightComponentsItemComponent } from './component/right/components/item/item.component';
import { RightComponentsPermissionComponent } from './component/right/components/permission/permission.component';
import { RightComponent } from './component/right/right.component';
import { NavigationRoutingModule } from './navigation-routing.module';
import { NavigationComponent } from './navigation.component';

const COMPONENTS = [NavigationComponent];
const COMPONENTS_NOROUNT = [RightComponent, RightComponentsItemComponent, RightComponentsPermissionComponent];

@NgModule({
  imports: [SharedModule, NavigationRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class NavigationModule { }
