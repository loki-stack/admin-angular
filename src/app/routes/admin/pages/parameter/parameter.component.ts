import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN } from '@delon/theme';
import { ParameterCreateModel } from '@services/admin/models';
import { BsdParameterService } from '@services/admin/services';
import { trimObject } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd/tree';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterComponent implements OnInit, AfterViewInit {
  @ViewChild('sf') public sf: SFComponent;
  activetab = 0;
  loading = true;
  selectedParent: any;
  searchValue = '';
  nodes = [];
  formData: any;
  layout = 'horizontal';
  selectedRows = [];
  activedNode: NzTreeNode;
  schema: SFSchema = {
    properties: {
      id: { type: 'string', title: this.i18n.fanyi('Id'), readOnly: true },
      name: { type: 'string', title: this.i18n.fanyi('Tên tham số') },
      value: { type: 'string', title: this.i18n.fanyi('Giá trị tham số') },
      groupCode: { type: 'string', title: this.i18n.fanyi('Nhóm') },
      isSystem: { type: 'boolean', title: this.i18n.fanyi('Hệ thống'), readOnly: true, default: false },
      description: { type: 'string', title: this.i18n.fanyi('Ghi chú') },
    },
    required: ['name', 'value', 'groupCode'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
      },
      spanLabelFixed: 120,
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
    },
    $id: {},
    $name: {
      placeholder: this.i18n.fanyi('Nhập tên tham số'),
    },
    $value: {
      placeholder: this.i18n.fanyi('Nhập giá trị tham số'),
    },
    $description: {
      placeholder: this.i18n.fanyi('Nhập ghi chú'),
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 24,
        lg: 24,
        xl: 24,
      },
      widget: 'textarea',
      autosize: { minRows: 6, maxRows: 12 },
    },
  };

  constructor(
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService,
    private parameterService: BsdParameterService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) { }

  ngAfterViewInit() {
    try {
      // disabled="true"
      setTimeout(() => {
        let elm = this.document.querySelector('.full-page [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.full-page .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) { }
  }
  setAutoFocus() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector('.ant-form [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) { }
  }

  ngOnInit() {
    this.formData = null;
    this.cdr.detectChanges();
    this.getMenu();
  }

  getMenu() {
    this.loading = true;
    return this.parameterService
      .getAll ()
      .pipe(
        tap(() => {
          this.loading = false;
          this.cdr.detectChanges();
        }),
      )
      .subscribe((responseData) => {
        if (responseData.code === 200) {
          this.nodes = this.mapMenuObject(responseData.data);
          if (this.nodes.length > 0) {
            this.cdr.detectChanges();
          }
        } else {
          this.nodes = [];
          this.cdr.detectChanges();
        }
      });
  }

  mapMenuObject(arr: any[]) {
    const groups = {};
    for (const item of arr) {
      const groupName = item.groupCode;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      groups[groupName].push({
        ...item,
        title: item.name,
        selectable: true,
        key: item.id,
        isLeaf: true,
        expanded: true,
        checked: false,
      });
    }

    const result = [];
    // tslint:disable-next-line: forin
    for (const groupName in groups) {
      result.push({
        title: groupName,
        selectable: false,
        disableCheckbox: true,
        key: groupName,
        expanded: true,
        isGroup: true,
        children: groups[groupName],
      });
    }
    return result;
  }

  onClickTree(event: NzFormatEmitEvent): void {
    this.selectMenu(event.node.origin);
    this.activedNode = event.node;
  }

  onCheckBox(event: NzFormatEmitEvent): void {
    this.selectedRows = event.keys;
  }

  selectMenu(item: any) {
    this.setAutoFocus();
    if (!item) {
      this.formData = item;
    } else {
      if (item.isGroup) {
        return;
      }
      this.formData = {
        ...item,
      };
      this.activetab = 1;
      // this.schema.properties.code.readOnly = item.isSystem;
      this.schema.properties.name.readOnly = item.isSystem;
      this.schema.properties.groupCode.readOnly = item.isSystem;
    }
    this.cdr.detectChanges();
  }

  save(value: any) {
    value = trimObject(value);
    this.loading = true;
    if (value.id) {
      // Update
      this.selectedParent = null;
      const payload = {
        id: value.id,
        model: {
          ...value,
        },
      } as BsdParameterService.UpdateParams;
      this.parameterService
        .update(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.cdr.detectChanges();
              this.getMenu();
            }
          },
          (error) => {
            console.error(error);
          },
        );
    } else {
      // Create
      const payload = {
        ...value,
      } as ParameterCreateModel;
      this.parameterService
        .create(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Thêm thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.cdr.detectChanges();
              this.getMenu();
            }
          },
          (error) => {
            console.error(error);
          },
        );
    }
  }

  addNew() {
    this.selectMenu({});
  }

  deleteMulti() {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa những bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        const listDeleteId = [];
        this.selectedRows.map((item) => {
          listDeleteId.push(item);
          return item;
        });
        this.parameterService.deleteRange (listDeleteId).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.cdr.detectChanges();
            this.getMenu();
            this.selectedRows = [];
            this.formData = null;
            this.activedNode = null;
          },
          () => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
          },
        );
      },
      nzOnCancel: () => { },
    });
  }

  delete(item: any): any {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        this.parameterService.delete (item.id).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.cdr.detectChanges();
            this.getMenu();
            this.formData = null;
            this.activedNode = null;
          },
          () => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
          },
        );
      },
      nzOnCancel: () => { },
    });
  }
}
