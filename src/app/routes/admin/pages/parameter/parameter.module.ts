import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { ParameterRoutingModule } from './parameter-routing.module';
import { ParameterComponent } from './parameter.component';

const COMPONENTS = [ParameterComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, ParameterRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class ParameterModule { }
