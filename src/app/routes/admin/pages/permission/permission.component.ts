import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFSchemaEnumType, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { BsdNavigationService, IdmRightService, IdmRoleService, IdmUserService, SysApplicationService } from '@services';
import { NavigationModel, RightModel } from '@services/admin/models';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PermissionComponent implements OnInit, AfterViewInit {
  @ViewChild('sf') public sf: SFComponent;
  loading = true;
  searchValue = '';
  nodes: any;
  nodes2: any;
  treeNav: any;
  treeMenu: any;
  allright: RightModel[];
  formData: any;
  layout = 'vertical';
  selected: any;
  schema: SFSchema = {
    properties: {
      user: { type: 'string', title: this.i18n.fanyi('Người dùng') },
      application: { type: 'string', title: this.i18n.fanyi('Hệ thống') },
      roles: { type: 'string', title: this.i18n.fanyi('Nhóm quyền đã gán') },
    },
  };
  ui: SFUISchema = {
    '*': {
      grid: {
        span: 24,
      },
    },
    $user: {
      autofocus: true,
      placeholder: this.i18n.fanyi('Chọn người dùng'),
      widget: 'select',
      asyncData: () => this.loadAllUsers(),
      change: (value: any) => this.selectUser(value),
    },
    $application: {
      placeholder: this.i18n.fanyi('Chọn hệ thống'),
      widget: 'select',
      asyncData: () => this.loadAllApps(),
      change: (value: any) => this.selectApps(value),
    },
    $roles: {
      placeholder: this.i18n.fanyi('Gán nhóm người dùng'),
      widget: 'select',
      mode: 'tags',
      asyncData: () => this.loadAllRoles(),
      change: (value: any) => this.selectRoles(value),
    },
  };
  constructor(
    public msg: NzMessageService,
    private roleService: IdmRoleService,
    private userService: IdmUserService,
    private rightService: IdmRightService,
    private cdr: ChangeDetectorRef,
    private appService: SysApplicationService,
    private navigationService: BsdNavigationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {
    this.formData = {};
    this.selected = {
      userId: null,
      appId: null,
      roles: [],
    };
    this.loadAllRights().subscribe((responseData) => {
      this.nodes = responseData;
      this.cdr.detectChanges();
    });
  }

  ngAfterViewInit() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector('.ant-form [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) { }
  }
  setAutoFocus() {
    try {
      // disabled="true"
      setTimeout(() => {
        let elm = this.document.querySelector('.full-page [autofocus=\'true\']:not(:disabled)') as any;
        if (!elm) {
          elm = this.document.querySelector('.full-page .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) { }
  }
  ngOnInit() {
    this.formData = {};
  }
  loadAllRoles(): Observable<SFSchemaEnumType[]> {
    return new Observable((observer) => {
      this.roleService.getAll().subscribe((responseData) => {
        if (responseData.code === 200) {
          const result = responseData.data
            .filter((x) => x.isSystem)
            .map((x) => {
              return {
                value: x.id,
                label: x.name,
              } as SFSchemaEnumType;
            });
          observer.next(result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }
  loadAllUsers(): Observable<SFSchemaEnumType[]> {
    return new Observable((observer) => {
      this.userService.getAll().subscribe((responseData) => {
        if (responseData.code === 200) {
          const result = responseData.data.map((x) => {
            return {
              value: x.id,
              label: x.name,
            } as SFSchemaEnumType;
          });
          observer.next(result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }
  loadAllApps(): Observable<SFSchemaEnumType[]> {
    return new Observable((observer) => {
      this.appService.getAll().subscribe((responseData) => {
        if (responseData.code === 200) {
          const result = responseData.data.map((x) => {
            return {
              value: x.id,
              label: x.name,
            } as SFSchemaEnumType;
          });
          observer.next(result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }
  loadAllRights(): Observable<SFSchemaEnumType[]> {
    return new Observable((observer) => {
      this.rightService.getAll().subscribe((responseData) => {
        if (responseData.code === 200) {
          this.allright = responseData.data;
          const result = this.mapRightObject(responseData.data, [], null);
          observer.next(result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }

  mapRightObject(arr: RightModel[], listSelectRight: any[], isNavigation) {
    const groups = {};
    for (const right of arr) {
      const groupName = right.groupCode;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      // current right
      const currentSelectRight = listSelectRight.find((x) => x.id === right.id) || {};
      groups[groupName].push({
        ...right,
        ...currentSelectRight,
        title: right.name,
        selectable: true,
        key: right.id,
        expanded: true,
        checked: currentSelectRight.enable,
        trueChecked: currentSelectRight.enable,
      });
    }

    const result = [];
    for (const groupName in groups) {
      if (groupName.includes('NAVIGATION.') === isNavigation || isNavigation === null) {
        result.push({
          title: groupName,
          selectable: true,
          // disableCheckbox: true,
          key: groupName,
          expanded: true,
          isGroup: true,
          children: groups[groupName],
        });
      }
    }
    return result;
  }

  loadPermission() {
    if (this.selected.appId && this.selected.userId) {
      this.setAutoFocus();
      this.loadCurrentRoles();
      this.loadCurrentRight();
    }
  }

  loadCurrentRoles() {
    const payload = {
      applicationId: this.selected.appId,
      id: this.selected.userId,
    } as IdmUserService.GetRoleMapUserParams;
    this.userService.getRoleMapUser(payload).subscribe((responseData) => {
      if (responseData.code === 200) {
        const listRoleId = responseData.data.map((x) => x.id);
        this.sf.setValue('/roles', listRoleId);
        this.selected.roles = listRoleId;
      }
    });
  }

  loadCurrentRight() {
    this.loading = true;
    const payload = {
      applicationId: this.selected.appId,
      id: this.selected.userId,
    } as IdmUserService.GetRightMapUserParams;
    this.userService.getRightMapUser(payload).subscribe((responseData) => {
      if (responseData.code === 200) {
        this.nodes = this.mapRightObject(this.allright, responseData.data, false);
        this.nodes2 = this.mapRightObject(this.allright, responseData.data, true);
        this.getNavigation();
      }
    });
  }

  selectApps = (value: any) => {
    this.selected.appId = value;
    this.loadPermission();
  }
  selectUser = (value: any) => {
    this.selected.userId = value;
    this.loadPermission();
  }

  selectRoles = (value: any) => {
    const isAdded = this.selected.roles.length < value.length;
    if (this.selected.appId && this.selected.userId) {
      if (isAdded) {
        const listAddRoles = value.filter((x: any) => !this.selected.roles.includes(x));

        this.selected.roles = value;
        const payload = {
          applicationId: this.selected.appId,
          id: this.selected.userId,
          listRoleId: listAddRoles,
        } as IdmUserService.AddUserMapRoleParams;
        this.userService.addUserMapRole(payload).subscribe((responseData) => {
          if (responseData.code === 200) {
            this.loadCurrentRight();
            this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          } else {
            this.loadCurrentRight();
            this.msg.error(this.i18n.fanyi('Cập nhật thất bại'));
          }
        });
      } else {
        const listDeleteRoles = this.selected.roles.filter((x: any) => !value.includes(x));
        this.selected.roles = value;
        const payload = {
          applicationId: this.selected.appId,
          id: this.selected.userId,
          listRoleId: listDeleteRoles,
        } as IdmUserService.DeleteUserMapRoleParams;
        this.userService.deleteUserMapRole(payload).subscribe((responseData) => {
          if (responseData.code === 200) {
            this.loadCurrentRight();
            this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          } else {
            this.loadCurrentRight();
            this.msg.error(this.i18n.fanyi('Cập nhật thất bại'));
          }
        });
      }
    } else {
      this.selected.roles = value;
    }
  }
  onTreeCheckBox = (e: { node: { origin: any } }) => {
    const item = e.node.origin;
    if (item.isGroup) {
      item.children.map((x) => {
        if (x.trueChecked !== item.checked) {
          x.trueChecked = item.checked;
          this.onTreeCheckBox({
            node: {
              origin: x,
            },
          });
        }
        return x;
      });
      return;
    }
    item.trueChecked = item.checked;
    if (item.checked) {
      const payload = {
        applicationId: this.selected.appId,
        id: item.id,
        listUserId: [this.selected.userId],
      } as IdmRightService.AddRightMapUserParams;
      this.rightService.addRightMapUser(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.loadCurrentRight();
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
        } else {
          this.loadCurrentRight();
          this.msg.error(this.i18n.fanyi('Cập nhật thất bại'));
        }
      });
    } else {
      const payload = {
        applicationId: this.selected.appId,
        id: item.id,
        listUserId: [this.selected.userId],
      } as IdmRightService.DeleteRightMapUserParams;
      this.rightService.deleteRightMapUser(payload).subscribe((responseData) => {
        if (responseData.code === 200) {
          this.loadCurrentRight();
          this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
        } else {
          this.loadCurrentRight();
          this.msg.error(this.i18n.fanyi('Cập nhật thất bại'));
        }
      });
    }
  }

  getNavigation() {
    if (this.treeMenu) {
      this.treeNav = this.treeMenu.map((x) => this.mapMenuObjectTree(x));
      this.loading = false;
      this.cdr.detectChanges();
      return;
    }
    const cmd = this.navigationService.getTree();
    cmd.subscribe((responseData) => {
      if (responseData.code === 200) {
        this.treeMenu = responseData.data;
        this.treeNav = responseData.data.map((x) => this.mapMenuObjectTree(x));
        this.loading = false;
        this.cdr.detectChanges();
      }
    });
    return cmd;
  }
  mapMenuObjectTree(item: NavigationModel): SFSchemaEnumType {
    const findGroup = this.nodes2.find((x) => x.title === 'NAVIGATION.' + item.code);
    if (findGroup) {
      return {
        // icon: item.iconClass,
        title: item.name,
        key: item.id,
        isLeaf: !item.subChild,
        children: findGroup.children,
        expanded: true,
        isGroup: true,
        selectable: false,
        disableCheckbox: true,
      } as SFSchemaEnumType;
    } else {
      return {
        // icon: item.iconClass,
        title: item.name,
        key: item.id,
        isLeaf: !item.subChild,
        expanded: true,
        isGroup: true,
        selectable: false,
        disableCheckbox: true,
        children: !item.subChild ? null : item.subChild.map((x) => this.mapMenuObjectTree(x)),
      } as SFSchemaEnumType;
    }
  }
}
