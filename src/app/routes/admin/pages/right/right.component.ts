import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFSchemaEnumType, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN } from '@delon/theme';
import { IdmRightService } from '@services';
import { RightModel } from '@services/admin/models.js';
import { trimObject } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd/tree';
import { tap } from 'rxjs/internal/operators/tap';

@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RightComponent implements OnInit, AfterViewInit {
  @ViewChild('sf') public sf: SFComponent;
  activetab = 0;
  loading = true;
  selectedParent: any;
  searchValue = '';
  nodes = [];
  formData: any;
  layout = 'horizontal';
  selectedRows = [];
  activedNode: NzTreeNode;
  schema: SFSchema = {
    properties: {
      code: {
        type: 'string',
        title: this.i18n.fanyi('Mã quyền'),
        format: 'regex',
        pattern: `^[a-zA-Z0-9_-]{1,99}$`,
      },
      name: { type: 'string', title: this.i18n.fanyi('Tên quyền') },
      groupCode: {
        type: 'string',
        title: this.i18n.fanyi('Nhóm'),
      },
      isSystem: { type: 'boolean', title: this.i18n.fanyi('Hệ thống'), readOnly: true, default: false },
      description: { type: 'string', title: this.i18n.fanyi('Ghi chú') },
    },

    required: ['code', 'name', 'groupCode'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        pattern: this.i18n.fanyi('Trường mã viết liền không dấu'),
      },
      spanLabelFixed: 120,
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
    },
    $description: {
      grid: {
        gutter: 15,
        span: 24,
      },
      widget: 'textarea',
      autosize: { minRows: 6, maxRows: 12 },
    },
    $groupCode: {
      widget: 'autocomplete',
    },
  };

  constructor(
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService,
    private rightService: IdmRightService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {}

  ngAfterViewInit() {
    try {
      // disabled="true"
      setTimeout(() => {
        let elm = this.document.querySelector(".full-page [autofocus='true']:not(:disabled)") as any;
        if (!elm) {
          elm = this.document.querySelector('.full-page .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }
  setAutoFocus() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector(".ant-form [autofocus='true']:not(:disabled)") as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }

  ngOnInit() {
    this.formData = null;
    this.cdr.detectChanges();
    this.getMenu();
  }

  getGroup() {
    return this.nodes.map((x) => {
      return {
        label: x.title,
        value: x.title,
      } as SFSchemaEnumType;
    });
  }

  getMenu() {
    this.loading = true;
    return this.rightService
      .getAll ()
      .pipe(
        tap(() => {
          this.loading = false;
          this.cdr.detectChanges();
        }),
      )
      .subscribe((responseData) => {
        if (responseData.code === 200) {
          this.nodes = this.mapMenuObject(responseData.data);
          if (this.nodes.length > 0) {
            this.cdr.detectChanges();
          }
        } else {
          this.nodes = [];
          this.cdr.detectChanges();
        }
      });
  }

  mapMenuObject(arr: RightModel[]) {
    const groups = {};
    for (const right of arr) {
      const groupName = right.groupCode;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      // current right
      groups[groupName].push({
        ...right,
        title: right.name,
        selectable: true,
        key: right.id,
        isLeaf: true,
        expanded: true,
        checked: false,
      });
    }

    const result = [];
    for (const groupName in groups) {
      if (!groupName.includes('NAVIGATION.')) {
        result.push({
          title: groupName,
          selectable: false,
          disableCheckbox: true,
          key: groupName,
          expanded: true,
          isGroup: true,
          children: groups[groupName],
        });
      }
    }
    return result;
  }

  onClickTree(event: NzFormatEmitEvent): void {
    this.selectMenu(event.node.origin);
    this.activedNode = event.node;
  }

  onCheckBox(event: NzFormatEmitEvent): void {
    this.selectedRows = event.keys;
  }

  selectMenu(item: any) {
    this.setAutoFocus();
    if (!item) {
      this.formData = {};
      this.schema.properties.code.readOnly = false;
      this.schema.properties.name.readOnly = false;
      this.schema.properties.groupCode.readOnly = false;
      this.schema.properties.groupCode.enum = this.getGroup();
    } else {
      if (item.isGroup) {
        return;
      }
      this.formData = {
        ...item,
      };
      this.activetab = 1;
      this.schema.properties.code.readOnly = true;
      this.schema.properties.name.readOnly = item.isSystem;
      this.schema.properties.groupCode.readOnly = item.isSystem;
      this.schema.properties.groupCode.enum = this.getGroup();
    }
    this.cdr.detectChanges();
  }

  save(value: any) {
    value = trimObject(value);
    this.loading = true;
    if (value.id) {
      // Update
      this.selectedParent = null;
      const payload = {
        id: value.id,
        model: {
          ...value,
        },
      } as IdmRightService.UpdateParams;
      this.rightService
        .update(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.cdr.detectChanges();
              this.getMenu();
            }
          },
          (error) => {
            console.error(error);
          },
        );
    } else {
      // Create
      const payload = {
        model: {
          ...value,
        },
      } as IdmRightService.CreateParams;
      this.rightService
        .create(payload)
        .pipe(
          tap(() => {
            this.loading = false;
            this.cdr.detectChanges();
          }),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Thêm thành công'));
              this.formData = null;
              this.activedNode = null;
              this.activetab = 0;
              this.cdr.detectChanges();
              this.getMenu();
            }
          },
          (error) => {
            console.error(error);
          },
        );
    }
  }

  addNew() {
    this.selectMenu(null);
  }

  deleteMulti() {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa những bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        const listDeleteId = [];
        this.selectedRows.map((item) => {
          listDeleteId.push(item);
          return item;
        });
        this.rightService.deleteRange (listDeleteId).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.cdr.detectChanges();
            this.getMenu();
            this.selectedRows = [];
            this.formData = null;
            this.activedNode = null;
          },
          () => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
          },
        );
      },
      nzOnCancel: () => {},
    });
  }

  delete(item: any): any {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        this.rightService.delete (item.id).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.cdr.detectChanges();
            this.getMenu();
            this.formData = null;
            this.activedNode = null;
          },
          () => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
          },
        );
      },
      nzOnCancel: () => {},
    });
  }
}
