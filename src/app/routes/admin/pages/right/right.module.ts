import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { RightRoutingModule } from './right-routing.module';
import { RightComponent } from './right.component';

const COMPONENTS = [RightComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, RightRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class RightModule { }
