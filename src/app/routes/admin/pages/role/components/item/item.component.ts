import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFSchemaEnumType, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { BsdNavigationService, IdmRightService, IdmRoleService } from '@services';
import { TemplateItem, trimObject } from '@shared';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { tap } from 'rxjs/operators';
import { NavigationModel, RightModel } from '../../../../../../services/admin/models';

@Component({
  selector: 'app-role-components-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less'],
})
export class RoleComponentsItemComponent extends TemplateItem implements OnInit {
  @ViewChild('sf') public sf: SFComponent;
  i: any;
  type: any;
  formData: any;
  nodes = [];
  nodes2 = [];
  loading = true;
  treeNav: any;
  treeMenu: any;
  allright: RightModel[];
  isLoadingForm = false;
  selectedRights = [];
  layout = 'horizontal';
  title = 'Thêm';
  searchValue: string;
  schema: SFSchema = {
    properties: {
      code: {
        type: 'string',
        title: this.i18n.fanyi('Mã nhóm'),
        format: 'regex',
        pattern: `^[a-zA-Z0-9_-]{1,99}$`,
      },
      name: { type: 'string', title: this.i18n.fanyi('Tên nhóm') },
      description: { type: 'string', title: this.i18n.fanyi('Ghi chú') },
      // permission: {
      //   type: 'object',
      //   properties: {
      //     rights: { title: 'Phân quyền', type: 'string' },
      //   },
      // },
    },
    required: ['code', 'name'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        pattern: this.i18n.fanyi('Trường mã viết liền không dấu'),
      },
      spanLabelFixed: 140,
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
    },
    $code: {
      autofocus: true,
    },
    $description: {
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 24,
        lg: 24,
        xl: 24,
      },
      widget: 'textarea',
      autosize: { minRows: 2, maxRows: 6 },
    },
    // $permission: {
    //   class: 'displaynone',
    //   grid: {
    //     gutter: 15,
    //     xs: 24,
    //     sm: 24,
    //     md: 24,
    //     lg: 24,
    //     xl: 24,
    //   },
    //   $rights: {
    //     spanLabelFixed: 140,
    //     grid: {
    //       gutter: 15,
    //       xs: 24,
    //       sm: 24,
    //       md: 24,
    //       lg: 24,
    //       xl: 24,
    //     },
    //     widget: 'custom',
    //   },
    // },
  };
  constructor(
    private modal: NzModalRef,
    private rightService: IdmRightService,
    private roleService: IdmRoleService,
    private msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private navigationService: BsdNavigationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {
    super(document);
  }

  ngOnInit(): void {
    if (this.i.id) {
      this.title = 'Cập nhật';
      if (this.i.isSystem) {
        this.type = 'VIEW';
      }
      if (this.type === 'VIEW') {
        this.title = 'Xem chi tiết';
      }
    }
    this.loadFormData(this.i.id);
  }
  loadFormData(id) {
    const cmd = this.loadAllRights();
    if (id) {
      this.formData = {
        ...this.i,
      };
      // cmd.subscribe(() => {
      //   this.loadCurrentRight(this.i.id);
      // });
    } else {
      this.formData = {};
      // this.formData.permission = {};
    }
  }

  getNavigation() {
    if (this.treeMenu) {
      this.treeNav = this.treeMenu.map((x) => this.mapMenuObjectTree(x));
      this.loading = false;
      this.cdr.detectChanges();
      return;
    }
    const cmd = this.navigationService.getTree();
    cmd.subscribe((responseData) => {
      if (responseData.code === 200) {
        this.treeMenu = responseData.data;
        this.treeNav = responseData.data.map((x) => this.mapMenuObjectTree(x));
        this.loading = false;
        this.cdr.detectChanges();
      }
    });
    return cmd;
  }
  loadCurrentRight(id) {
    this.isLoadingForm = true;
    const payload = {
      id,
    } as IdmRoleService.GetRightMapRoleParams;
    this.roleService
      .getRightMapRole(payload)
      .pipe(
        tap(
          () => (this.isLoadingForm = false),
          () => (this.isLoadingForm = false),
          () => (this.isLoadingForm = false),
        ),
      )
      .subscribe((responseData) => {
        if (responseData.code === 200) {
          this.nodes2 = this.mapRightObject(this.allright, responseData.data, true);
          this.nodes = this.mapRightObject(this.allright, responseData.data, false);

          this.selectedRights = responseData.data.map((x) => x.id);
          this.cdr.detectChanges();
          this.formData.permission = {};
          this.formData.permission.rights = [...this.selectedRights];
          this.sf.setValue('/permission/rights', this.selectedRights);
          this.getNavigation();
        }
      });
  }

  loadAllRights() {
    const cmd = this.rightService.getAll ();
    cmd.subscribe((responseData) => {
      if (responseData.code === 200) {
        this.allright = responseData.data;
        this.getNavigation();
      }
    });
    return cmd;
  }

  mapRightObject(arr: RightModel[], listSelectRight: any[], isNavigation) {
    const groups = {};
    for (const right of arr) {
      const groupName = right.groupCode;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      // current right
      const currentSelectRight = listSelectRight.find((x) => x.id === right.id);
      groups[groupName].push({
        ...right,
        title: right.name,
        selectable: true,
        key: right.id,
        expanded: true,
        checked: currentSelectRight || false,
        trueChecked: currentSelectRight || false,
      });
    }

    const result = [];
    for (const groupName in groups) {
      if (groupName.includes('NAVIGATION.') === isNavigation || isNavigation === null) {
        result.push({
          title: groupName,
          selectable: true,
          // disableCheckbox: true,
          key: groupName,
          expanded: true,
          isGroup: true,
          children: groups[groupName],
        });
      }
    }
    return result;
  }

  mapMenuObjectTree(item: NavigationModel): SFSchemaEnumType {
    const findGroup = this.nodes2.find((x) => x.title === 'NAVIGATION.' + item.code);
    if (findGroup) {
      return {
        // icon: item.iconClass,
        title: item.name,
        key: item.id,
        isLeaf: !item.subChild,
        children: findGroup.children,
        expanded: true,
        isGroup: true,
        selectable: false,
        disableCheckbox: true,
      } as SFSchemaEnumType;
    } else {
      return {
        // icon: item.iconClass,
        title: item.name,
        key: item.id,
        isLeaf: !item.subChild,
        expanded: true,
        isGroup: true,
        selectable: false,
        disableCheckbox: true,
        children: !item.subChild ? null : item.subChild.map((x) => this.mapMenuObjectTree(x)),
      } as SFSchemaEnumType;
    }
  }
  save(value: any) {
    value = trimObject(value);
    this.isLoadingForm = true;
    if (this.title === 'Thêm') {
      const payload = {
        model: {
          ...value,
          // listAddRoleId: value.permission.rights,
        },
      } as IdmRoleService.CreateParams;
      this.roleService
        .create(payload)
        .pipe(
          tap(
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
          ),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Thêm thành công'));
              this.modal.close(true);
            }
          },
          (error) => {
            console.error(error);
          },
        );
    } else {
      const payload = {
        id: this.i.id,
        model: {
          ...value,
          // listAddRightId: value.permission.rights.filter((x) => !this.formData.permission.rights.includes(x)),
          // listDeleteRightId: this.formData.permission.rights.filter((x) => !value.permission.rights.includes(x)),
        },
      } as IdmRoleService.UpdateParams;
      this.roleService
        .update(payload)
        .pipe(
          tap(
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
            () => (this.isLoadingForm = false),
          ),
        )
        .subscribe(
          (response) => {
            if (response.code === 200) {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
              this.modal.close(true);
            }
          },
          (error) => {
            console.error(error);
          },
        );
    }
  }

  close() {
    this.modal.destroy();
  }

  onTreeCheckBox = (e: { node: { origin: any } }) => {
    const item = e.node.origin;
    if (item.isGroup) {
      item.children.map((x) => {
        if (x.trueChecked !== item.checked) {
          x.trueChecked = item.checked;
          this.onTreeCheckBox({
            node: {
              origin: x,
            },
          });
        }
        return x;
      });
      return;
    }
    item.trueChecked = item.checked;
    if (item.checked) {
      this.selectedRights.push(item.id);
    } else {
      const index = this.selectedRights.indexOf(item.id);
      if (index > -1) {
        this.selectedRights.splice(index, 1);
      }
    }

    this.sf.setValue('/permission/rights', this.selectedRights);
  };
}
