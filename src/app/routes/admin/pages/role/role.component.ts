import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, ModalHelper, _HttpClient } from '@delon/theme';
import { IdmRoleService, IdmUserService } from '@services';
import { COMMON_CONSTANT, TemplateFilter, TemplateList } from '@shared';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import { RoleComponentsItemComponent } from './components/item/item.component';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleComponent extends TemplateList {
  constructor(
    public msg: NzMessageService,
    public route: ActivatedRoute,
    public modal: ModalHelper,
    public modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService,
    public cdr: ChangeDetectorRef,
    public router: Router,
    public mainService: IdmRoleService,
  ) {
    super(msg, route, modal, modalService, i18n, cdr, router);
    this.addComponent = RoleComponentsItemComponent;
    this.editComponent = RoleComponentsItemComponent;
  }
  initConfig() {
    this.lgConfig.columns = [
      {
        index: 'code',
        i18n: 'Mã',
        width: '120px',
        onTop: true,
      },
      {
        index: 'name',
        i18n: 'Tên nhóm',
        width: '140px',
        onTop: true,
      },
      {
        index: 'isSystem',
        i18n: 'Loại',
        width: '140px',
        type: 'tag',
        onTop: true,
        config: {
          enum: COMMON_CONSTANT.enumRoleType,
        },
      },
      {
        index: 'description',
        i18n: 'Ghi chú',
        width: '250px',
      },
    ];
    // this.lgConfig.tagFilter = COMMON_CONSTANT.enumRoleType;
  }
  actionDelete(item: any): Observable<any> {
    const cmd = this.mainService.delete (item.id);
    return cmd;
  }

  actionDeleteMulti(selectedData: any[]): Observable<any> {
    const listId = selectedData.map((x) => x.id);
    const cmd = this.mainService.deleteRange (listId);
    return cmd;
  }

  actionSearch(filter: TemplateFilter) {
    const payload = {
      page: filter.page,
      size: filter.size,
      sort: filter.sort,
      filter: filter.filter,
    };
    const cmd = this.mainService.getFilter(payload);
    return cmd;
  }
}
