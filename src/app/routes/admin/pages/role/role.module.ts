import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { RoleComponentsItemComponent } from './components/item/item.component';
import { RoleRoutingModule } from './role-routing.module';
import { RoleComponent } from './role.component';

const COMPONENTS = [RoleComponent];
const COMPONENTS_NOROUNT = [RoleComponentsItemComponent];

@NgModule({
  imports: [SharedModule, RoleRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class RoleModule { }
