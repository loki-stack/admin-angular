import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { I18NService } from '@core';
import { SFComponent, SFSchema, SFUISchema } from '@delon/form';
import { ALAIN_I18N_TOKEN, ModalHelper, _HttpClient } from '@delon/theme';
import { SysApplicationService, IdmUserService, StorageService, MdmNodeUploadService } from '@services';
import { GET_CONFIG, trimObject, USER_CONSTANT } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { forkJoin, Observable, of } from 'rxjs';
import { UserComponentsChangePasswordComponent } from '../user/components/change-password/change-password.component';

const CONFIG = GET_CONFIG();

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class UserProfileComponent implements OnInit {
  menus: any[] = [
    {
      key: 'info',
      title: 'Chi tiết tài khoản',
    },
  ];
  @ViewChild('sf') public sf: SFComponent;
  i: any;
  formData: any;
  isLoadingForm = false;
  layout = 'horizontal';
  title = 'Thêm';
  loading = false;
  avatarUrl = null;
  fileHost = '';
  allApp: any[];
  schema: SFSchema = {
    properties: {
      common: {
        type: 'object',
        properties: {
          group1: {
            type: 'object',
            properties: {
              id: { type: 'string', title: '', ui: { hidden: true } },
              userName: {
                type: 'string',
                title: this.i18n.fanyi('Tài khoản'),
                format: 'regex',
                readOnly: true,
                pattern: `^[a-z0-9_-]{0,99}$`,
              },
              password: { type: 'string', title: this.i18n.fanyi('Mật khẩu') },
              name: { type: 'string', title: this.i18n.fanyi('Tên người dùng') },
              phoneNumber: {
                type: 'string',
                title: this.i18n.fanyi('Số điện thoại'),
                format: 'regex',
                pattern: `^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$`,
              },
              email: { type: 'string', title: this.i18n.fanyi('Emai'), format: 'email' },
              birthdate: { type: 'string', format: 'date', title: this.i18n.fanyi('Ngày sinh') },
              apps: { type: 'string', title: this.i18n.fanyi('Hệ thống') },
            },
            required: ['name', 'userName', 'phoneNumber', 'email'],
          },
          group2: {
            type: 'object',
            properties: {
              avatarUrl: {
                type: 'string',
                title: this.i18n.fanyi('Avatar'),
              },
              type: { type: 'number', title: this.i18n.fanyi('Chức vụ'), readOnly: true },
            },
          },
        },
        required: ['group1'],
      },
    },
    required: ['common'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        email: this.i18n.fanyi('Email sai định dạng'),
      },
    },
    $common: {
      class: 'ant-row custom-two-panel',
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
      $group1: {
        class: 'ant-row',
        spanLabelFixed: 140,
        grid: {
          gutter: 15,
          xs: 24,
          sm: 24,
          md: 18,
          lg: 18,
          xl: 18,
        },
        $userName: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          autofocus: true,
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Tên người dùng chữ cái thường viết liền không dấu dài khoảng 3-16 ký tự'),
          },
        },
        $name: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
        },
        $phoneNumber: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Số điện thoại không đúng định dạng'),
          },
        },
        $password: {
          widget: 'custom',
        },
        $email: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          type: 'email',
        },

        $birthdate: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          widget: 'loki-date',
        },
        $apps: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          widget: 'text',
          visibleIf: { '/common/group2/type': [USER_CONSTANT.IsAdmin] },
          spanLabelFixed: 140,
        },
      },
      $group2: {
        class: 'ant-row',
        grid: {
          gutter: 15,
          xs: 24,
          sm: 24,
          md: 6,
          lg: 6,
          xl: 6,
        },
        $avatarUrl: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          spanLabelFixed: 1,
          class: 'text-center',
          widget: 'custom',
        },
        $type: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          spanLabelFixed: 1,
          widget: 'radio',
          styleType: 'button',
          buttonStyle: 'outline',
          class: 'custom-radio',
          size: 'small',
          asyncData: () =>
            of([
              { label: this.i18n.fanyi('Quản trị'), value: USER_CONSTANT.IsSuperAdmin },
              { label: this.i18n.fanyi('Người dùng'), value: USER_CONSTANT.IsAdmin },
            ]).pipe(),
        },
      },
    },
  };
  constructor(
    private uploadService: MdmNodeUploadService,
    private userService: IdmUserService,
    private appService: SysApplicationService,
    public msg: NzMessageService,
    private storageService: StorageService,
    private cdr: ChangeDetectorRef,
    private modal: ModalHelper,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {
    this.fileHost = CONFIG.fileHost;
  }

  ngOnInit(): void {
    const loginInfo = this.storageService.GetLoginInfo();
    this.userService.getById(loginInfo.userId).subscribe((results) => {
      if (results.code === 200) {
        this.i = results.data;
        this.avatarUrl = this.i.logo;
        this.loadFormData(this.i.id);
      }
    });
  }

  loadFormData(id) {
    const getUserProfileApp = id
      ? this.appService.getAllByUserId(id)
      : new Observable<any>((observer) => {
        observer.next({});
        observer.complete();
      });
    const loadAllApps = this.appService.getAll();

    forkJoin([getUserProfileApp, loadAllApps]).subscribe((results) => {
      let apps = '';
      if (results[1].code === 200 && id && results[0].code === 200) {
        const listUserApp = results[0].data.map((x) => x.id);
        apps = results[1].data
          .filter((x) => listUserApp.includes(x.id))
          .map((x) => x.name)
          .join(', ');
      }
      this.avatarUrl = this.i.avatarUrl;
      this.formData = {
        id: this.i.id,
        common: {
          group1: {
            userName: this.i.userName,
            password: this.i.password,
            name: this.i.name,
            phoneNumber: this.i.phoneNumber,
            email: this.i.email,
            birthdate: new Date(this.i.birthdate),
            apps,
          },
          group2: {
            avatarUrl: this.i.avatarUrl,
            type: this.i.type,
          },
        },
      };
      this.cdr.detectChanges();
    });
  }

  updateLogin() {
    this.storageService.notifyOther('UPDATE_JWT');
  }
  save(value: any) {
    value = trimObject(value);
    this.isLoadingForm = true;

    const payload = {
      id: this.i.id,
      model: {
        ...value.common.group1,
        ...value.common.group2,
      },
    } as IdmUserService.UpdateParams;
    this.userService.update(payload).subscribe(
      (response) => {
        if (response.code === 200) {
          setTimeout(() => {
            this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
          }, 500);
        } else {
          this.isLoadingForm = false;
          this.cdr.detectChanges();
        }
      },
      (error) => {
        this.isLoadingForm = false;
        this.cdr.detectChanges();
        console.error(error);
      },
    );
  }

  customReq = (item: any) => {
    if (!item.file.name.match(/.(jpg|jpeg|png|gif)$/i)) {
      this.msg.error('Định dạng file không hợp lệ!');
      return;
    }
    this.loading = true;
    const uploadData = {
      destinationPhysicalPath: 'avatar',
      file: item.file as any,
    };
    return this.uploadService.uploadFileBlobPhysical(uploadData).subscribe(
      (response) => {
        if (response.code === 200) {
          this.loading = false;
          this.avatarUrl = response.data.physicalPath;
          this.sf.setValue('/common/group2/avatarUrl', this.avatarUrl);
        }
      },
      (error) => {
        console.error(error);
        this.loading = false;
      },
    );
  };

  changePassword(): any {
    this.modal.createStatic(UserComponentsChangePasswordComponent, { i: this.i }).subscribe(() => { });
  }
}
