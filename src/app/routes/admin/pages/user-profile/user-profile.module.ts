import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';

const COMPONENTS = [UserProfileComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, UserProfileRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class UserProfileModule { }
