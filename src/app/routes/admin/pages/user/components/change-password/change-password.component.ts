import { Component, Inject, OnInit } from '@angular/core';
import { I18NService } from '@core';
import { ACLService } from '@delon/acl';
import { FormProperty, PropertyGroup, SFComponent, SFSchema, SFUISchema, SFValue } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { IdmUserService } from '@services';
import { trimObject } from '@shared';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-user-components-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less'],
})
export class UserComponentsChangePasswordComponent implements OnInit {
  i: any;
  formData: any;
  isLoadingForm = false;
  layout = 'horizontal';
  title = 'Đổi mật khẩu';
  schema: SFSchema = {
    properties: {
      oldPassword: { type: 'string', title: this.i18n.fanyi('Mật khẩu cũ') },
      newPassword: { type: 'string', title: this.i18n.fanyi('Mật khẩu mới') },
      confirmPassword: { type: 'string', title: this.i18n.fanyi('Xác nhận mật khẩu') },
    },
    required: ['oldPassword', 'newPassword', 'confirmPassword'],
  };
  ui: SFUISchema = {
    '*': {
      spanLabelFixed: 170,
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
      },
    },
    $oldPassword: {
      type: 'password',
    },
    $newPassword: {
      type: 'password',
    },
    $confirmPassword: {
      type: 'password',
      liveValidate: true,
      validator: (value: SFValue, formProperty: FormProperty, form: PropertyGroup) => {
        const listValidate = [];
        if (form.value && form.value.newPassword !== value) {
          listValidate.push({
            keyword: 'verify',
            message: this.i18n.fanyi('Mật khẩu không trùng khớp'),
          });
        }
        return listValidate;
      },
    },
  };

  constructor(
    public acl: ACLService,
    private modal: NzModalRef,
    private userService: IdmUserService,
    public msg: NzMessageService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {}

  ngOnInit(): void {
    this.formData = {};
    this.title = 'Đổi mật khẩu';
  }

  save(value: any) {
    value = trimObject(value);
    this.isLoadingForm = true;
    const payload = {
      id: this.i.id,
      oldPassword: value.oldPassword,
      password: value.newPassword,
    } as IdmUserService.ChangePasswordParams;
    this.userService.changePassword(payload).subscribe(
      (response) => {
        if (response.code === 200) {
          this.msg.success(this.i18n.fanyi('Cập nhật mật khẩu thành công'));
          this.modal.close(true);
        } else {
          this.isLoadingForm = false;
        }
      },
      (error) => {
        this.isLoadingForm = false;
        console.error(error);
      },
    );
  }

  close() {
    this.modal.destroy();
  }
}
