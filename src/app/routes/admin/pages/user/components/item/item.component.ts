import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { I18NService } from '@core';
import { FormProperty, PropertyGroup, SFComponent, SFSchema, SFSchemaEnumType, SFUISchema, SFValue } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { SysApplicationService, IdmRoleService,  IdmUserService, StorageService, MdmNodeUploadService } from '@services';
import { APP_CONSTANT, GET_CONFIG, RIGHT_CONSTANT, ROLE_CONSTANT, TemplateItem, trimObject, USER_CONSTANT } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Observable, of } from 'rxjs';

const CONFIG = GET_CONFIG();
@Component({
  selector: 'app-user-components-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class UserComponentsItemComponent extends TemplateItem implements OnInit {
  @ViewChild('sf') public sf: SFComponent;
  i: any;
  formData: any;
  isLoadingForm = false;
  layout = 'horizontal';
  title = 'Thêm';
  loading = false;
  avatarUrl = null;
  fileHost = '';
  allApp: any[];
  allRole: any[];
  schema: SFSchema = {
    properties: {
      common: {
        type: 'object',
        properties: {
          group1: {
            type: 'object',
            properties: {
              userName: {
                type: 'string',
                title: this.i18n.fanyi('Tài khoản'),
                format: 'regex',
                pattern: `^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+$`,
                minLength: 5,
                maxLength: 30,
              },
              id: { type: 'string', title: '', ui: { hidden: true } },
              password: {
                type: 'string',
                title: this.i18n.fanyi('Mật khẩu'),
                format: 'regex',
                pattern: `^[\\S]*$`,
                minLength: 5,
                maxLength: 30,
              },
              passwordVerify: {
                type: 'string',
                title: this.i18n.fanyi('Nhập lại mật khẩu'),
              },
              name: { type: 'string', title: this.i18n.fanyi('Tên người dùng') },
              phoneNumber: {
                type: 'string',
                title: this.i18n.fanyi('Số điện thoại'),
                format: 'regex',
                pattern: `^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$`,
              },
              email: { type: 'string', title: this.i18n.fanyi('Email'), format: 'email' },
              birthdate: { type: 'string', format: 'date', title: this.i18n.fanyi('Ngày sinh') },
            },
            required: ['name', 'userName', 'phoneNumber', 'email'],
          },
          group2: {
            type: 'object',
            properties: {
              avatarUrl: {
                type: 'string',
                title: this.i18n.fanyi('Avatar'),
              },
              type: { type: 'number', title: this.i18n.fanyi('Chức vụ') },
            },
          },
        },
        required: ['group1'],
      },
      permission: {
        type: 'object',
        properties: {
          roles: { title: 'Gán nhóm', type: 'string' },
          apps: { title: 'Gán hệ thống', type: 'string' },
        },
      },
    },
    required: ['common'],
  };
  ui: SFUISchema = {
    '*': {
      errors: {
        required: this.i18n.fanyi('Trường bắt buộc'),
        email: this.i18n.fanyi('Email sai định dạng'),
      },
    },
    $common: {
      class: 'ant-row custom-two-panel',
      grid: {
        gutter: 15,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
      },
      $group1: {
        class: 'ant-row',
        spanLabelFixed: 150,
        grid: {
          gutter: 15,
          xs: 24,
          sm: 24,
          md: 18,
          lg: 18,
          xl: 18,
        },
        $userName: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          autofocus: true,
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Tên đăng nhập viết liền không dấu, không chứa ký tự đặc biệt.'),
            minLength: this.i18n.fanyi('Tên đăng nhập phải dài từ 6 đến 30 ký tự'),
            maxLength: this.i18n.fanyi('Tên đăng nhập phải dài từ 6 đến 30 ký tự'),
          },
        },
        $name: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
        },
        $phoneNumber: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Số điện thoại không đúng định dạng'),
          },
        },
        $password: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Mật khẩu viết liền không dấu'),
            minLength: this.i18n.fanyi('Mật khẩu nhập phải dài từ 6 đến 30 ký tự'),
            maxLength: this.i18n.fanyi('Mật khẩu nhập phải dài từ 6 đến 30 ký tự'),
          },
          showRequired: true,
          visibleIf: { '/common/group1/id': (value: any) => value == null },
          type: 'password',
        },
        $passwordVerify: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          errors: {
            required: this.i18n.fanyi('Trường bắt buộc'),
            pattern: this.i18n.fanyi('Mật khẩu viết liền không dấu'),
            minLength: this.i18n.fanyi('Mật khẩu nhập phải dài từ 6 đến 30 ký tự'),
            maxLength: this.i18n.fanyi('Mật khẩu nhập phải dài từ 6 đến 30 ký tự'),
          },
          showRequired: true,
          visibleIf: { '/common/group1/id': (value: any) => value == null },
          type: 'password',
          liveValidate: true,
          validator: (value: SFValue, formProperty: FormProperty, form: PropertyGroup) => {
            const listValidate = [];
            if (form.value && form.value.common.group1.password !== value) {
              listValidate.push({
                keyword: 'verify',
                message: this.i18n.fanyi('Mật khẩu không trùng khớp'),
              });
            }
            return listValidate;
          },
        },
        $email: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          type: 'email',
          errors: {
            format: this.i18n.fanyi('Email sai định dạng'),
          },
        },

        $birthdate: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          widget: 'loki-date',
        },
      },
      $group2: {
        class: 'ant-row',
        grid: {
          gutter: 15,
          xs: 24,
          sm: 24,
          md: 6,
          lg: 6,
          xl: 6,
        },
        $avatarUrl: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          spanLabelFixed: 1,
          class: 'text-center',
          widget: 'custom',
        },
        $type: {
          grid: {
            xs: 24,
            sm: 24,
            md: 24,
            lg: 24,
            xl: 24,
          },
          spanLabelFixed: 1,
          widget: 'radio',
          styleType: 'button',
          class: 'custom-radio displaynone',
          size: 'small',
          asyncData: () =>
            of([
              { label: this.i18n.fanyi('Quản trị HT'), value: USER_CONSTANT.IsSuperAdmin },
              { label: this.i18n.fanyi('Quản trị viên'), value: USER_CONSTANT.IsAdmin },
            ]).pipe(),
        },
      },
    },
    $permission: {
      class: 'ant-row',
      $apps: {
        class: 'displaynone',
        grid: {
          gutter: 0,
          xs: 0,
          sm: 0,
          md: 0,
          lg: 0,
          xl: 0,
        },
        visibleIf: { '/common/group2/type': [USER_CONSTANT.IsAdmin] },
        spanLabelFixed: 140,
        widget: 'transfer',
        showSearch: true,
        itemUnit: ' ',
        itemsUnit: ' ',
        titles: ['HT', 'HT đã chọn'],
        liveValidate: true,
        showRequired: true,
        validator: (value: SFValue, formProperty: FormProperty, form: PropertyGroup) => {
          const listValidate = [];
          if ((!value || value.length === 0) && form.value && form.value.common.group2.type !== USER_CONSTANT.IsAdmin) {
            listValidate.push({
              keyword: 'required',
              message: this.i18n.fanyi('Cần chọn ít nhất một hệ thống'),
            });
          }
          return listValidate;
        },
      },
      $roles: {
        grid: {
          gutter: 0,
          xs: 24,
          sm: 24,
          md: 24,
          lg: 24,
          xl: 24,
        },
        spanLabelFixed: 140,
        widget: 'transfer',
        showSearch: true,
        itemUnit: ' ',
        itemsUnit: ' ',
        titles: ['Nhóm', 'Nhóm đã chọn'],
        liveValidate: true,
      },
    },
  };
  constructor(
    private modal: NzModalRef,
    private uploadService: MdmNodeUploadService,
    private userService: IdmUserService,
    private roleService: IdmRoleService,
    private storageService: StorageService,
    private appService: SysApplicationService,
    public msg: NzMessageService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DOCUMENT) public document: Document,
  ) {
    super(document);
    this.fileHost = CONFIG.fileHost;
  }

  ngOnInit(): void {
    if (this.i.id) {
      this.title = 'Cập nhật';
      this.avatarUrl = this.i.logo;
      delete this.schema.properties.common.properties.group1.properties.passwordVerify;
    }
    this.loadFormData(this.i.id);
  }

  loadFormData(id) {
    const getUserApp = id
      ? this.appService.getAllByUserId (id)
      : new Observable<any>((observer) => {
          observer.next({});
          observer.complete();
        });
    const loadAllApps = this.appService.getAll ();

    const getUserRoles = id
      ? this.userService.getRoleMapUser ({
          id,
        })
      : new Observable<any>((observer) => {
          observer.next({});
          observer.complete();
        });
    const loadAllRole = this.roleService.getAll ();

    forkJoin([loadAllApps, getUserApp, loadAllRole, getUserRoles]).subscribe((results) => {
      if (results[0].code === 200) {
        this.schema.properties.permission.properties.apps.enum = results[0].data.map((x) => {
          return {
            value: x.id,
            title: x.name,
          } as SFSchemaEnumType;
        });
        this.allApp = results[0].data.map((x) => x.id);
      }
      if (results[2].code === 200) {
        this.schema.properties.permission.properties.roles.enum = results[2].data.map((x) => {
          return {
            value: x.id,
            title: x.name,
          } as SFSchemaEnumType;
        });
        this.allRole = results[2].data.map((x) => x.id);
      }

      if (id) {
        let apps = [];
        let roles = [];
        this.avatarUrl = this.i.avatarUrl;
        if (id && results[1].code === 200) {
          apps = results[1].data.map((x) => x.id);
        }
        if (id && results[3].code === 200) {
          roles = results[3].data.map((x) => x.id);
        }
        delete this.schema.properties.common.properties.group1.properties.password;
        this.formData = {
          id: this.i.id,
          common: {
            group1: {
              userName: this.i.userName,
              password: this.i.password,
              name: this.i.name,
              phoneNumber: this.i.phoneNumber,
              email: this.i.email,
              birthdate: !!this.i.birthdate ? new Date(this.i.birthdate) : null,
            },
            group2: {
              avatarUrl: this.i.avatarUrl,
              type: this.i.type,
            },
          },
          permission: {
            apps,
            roles,
          },
        };
      } else {
        this.formData = {
          common: {
            group2: {
              type: USER_CONSTANT.IsAdmin,
            },
          },
          permission: {
            apps: APP_CONSTANT.MAIN_APP,
          },
        };
      }
    });
  }

  callBackSave(id, value) {
    let listApplicationId = [];
    const listRoleId = [];
    if (this.i.id) {
      // nếu không có sự thay đổi app hoặc loại tài khoản
      if (
        this.i.type === value.common.group2.type &&
        this.formData.permission.apps === value.permission.apps &&
        this.formData.permission.roles === value.permission.roles
      ) {
        // this.msg.success(this.i18n.fanyi('Cập nhật quyền thành công'));
        this.updateLogin();
        this.modal.close(true);
        return;
      }
    }
    const listRightId = [RIGHT_CONSTANT.AccessAppId];
    // nếu là quản trị hệ thống tự động add full app+
    if (value.common.group2.type === USER_CONSTANT.IsSuperAdmin) {
      listApplicationId = this.allApp;
      // listRoleId = [ROLE_CONSTANT.SYS_ADMIN];
    } else {
      listApplicationId = value.permission.apps;
      // listRoleId = [ROLE_CONSTANT.ADMIN];
    }

    const payload = {
      id,
      request: {
        listRightId,
        listApplicationId,
        listRoleId,
      },
    } as IdmUserService.UpdateSystemRightRoleMapAppsParams;
    this.userService.updateSystemRightRoleMapApps(payload).subscribe(
      (response) => {
        if (response.code === 200) {
          this.updateLogin();
          // this.msg.success(this.i18n.fanyi('Cập nhật quyền thành công'));
          this.modal.close(true);
        }
        this.isLoadingForm = false;
      },
      (error) => {
        this.isLoadingForm = false;
        console.error(error);
      },
    );
  }

  updateLogin() {
    if (this.i.id === this.storageService.GetLoginInfo().userId) {
      this.storageService.notifyOther('UPDATE_JWT');
    }
  }
  save(value: any) {
    value = trimObject(value);
    this.isLoadingForm = true;
    if (this.title === 'Thêm') {
      const payload = {
        model: {
          ...value.common.group1,
          ...value.common.group2,
          listAddRoleId: [...value.permission.roles],
        },
      } as IdmUserService.CreateParams;
      this.userService.create(payload).subscribe(
        (response) => {
          if (response.code === 200) {
            setTimeout(() => {
              this.msg.success(this.i18n.fanyi('Thêm thành công'));
            }, 500);
            this.callBackSave(response.data.id, value);
          } else {
            this.isLoadingForm = false;
          }
        },
        (error) => {
          this.isLoadingForm = false;
          console.error(error);
        },
      );
    } else {
      // constt; listAddRoleId = [...];
      const payload = {
        id: this.i.id,
        model: {
          ...value.common.group1,
          ...value.common.group2,
          // listAddRoleId
          listAddRoleId: value.permission.roles.filter((x) => !this.formData.permission.roles.includes(x)),
          listDeleteRoleId: this.formData.permission.roles.filter((x) => !value.permission.roles.includes(x)),
        },
      } as IdmUserService.UpdateParams;
      this.userService.update(payload).subscribe(
        (response) => {
          if (response.code === 200) {
            setTimeout(() => {
              this.msg.success(this.i18n.fanyi('Cập nhật thành công'));
            }, 500);
            this.callBackSave(this.i.id, value);
          } else {
            this.isLoadingForm = false;
          }
        },
        (error) => {
          this.isLoadingForm = false;
          console.error(error);
        },
      );
    }
  }

  close() {
    this.modal.destroy();
  }

  customReq = (item: any) => {
    this.loading = true;
    console.log(item.file);
    if (!item.file.name.match(/.(jpg|jpeg|png|gif)$/i)) {
      this.msg.error('Định dạng file không hợp lệ!');
      return;
    }

    const uploadData = {
      destinationPhysicalPath: 'avatar',
      file: item.file as any,
    }  ;
    return this.uploadService.uploadFileBlobPhysical(uploadData).subscribe(
      (response) => {
        if (response.code === 200) {
          this.loading = false;
          this.avatarUrl = response.data.physicalPath;
          this.sf.setValue('/common/group2/avatarUrl', this.avatarUrl);
        }
      },
      (error) => {
        console.error(error);
      },
    );
  };
}
