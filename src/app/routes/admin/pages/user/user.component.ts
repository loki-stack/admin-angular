import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, ModalHelper, _HttpClient } from '@delon/theme';
import { IdmUserService } from '@services';
import { GET_CONFIG, TemplateFilter, TemplateList } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import { UserComponentsChangePasswordComponent } from './components/change-password/change-password.component';
import { UserComponentsItemComponent } from './components/item/item.component';

const CONFIG = GET_CONFIG();
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserComponent extends TemplateList {
  constructor(
    public msg: NzMessageService,
    public route: ActivatedRoute,
    public modal: ModalHelper,
    public modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService,
    public cdr: ChangeDetectorRef,
    public router: Router,
    public mainService: IdmUserService,
  ) {
    super(msg, route, modal, modalService, i18n, cdr, router);
    this.addComponent = UserComponentsItemComponent;
    this.editComponent = UserComponentsItemComponent;
  }
  initConfig() {
    this.lgConfig.avatarProp = 'imageDisplay';
    this.lgConfig.actionsSingle.unshift({
      icon: 'key',
      action: (data: any) => this.changePassword(data),
      i18n: 'Đổi mật khẩu',
      i18nDescription: 'Đổi mật khẩu bản ghi',
    });
    this.lgConfig.columns = [
      // {
      //   index: 'imageDisplay',
      //   i18n: 'Avatar',
      //   width: '70px',
      //   type: 'image',
      //   onTop: true,
      //   canSort: false,
      //   config: {
      //     nzFileType: 'image/png,image/jpeg,image/gif,image/bmp',
      //   },
      // },
      {
        index: 'name',
        i18n: 'Tên người dùng',
        sort_i18n: 'Tên',
        width: '140px',
        onTop: true,
      },
      {
        index: 'userName',
        i18n: 'Tài khoản',
        sort_i18n: 'Tài khoản',
        width: '140px',
        onTop: true,
      },
      {
        index: 'email',
        i18n: 'Email',
      },
      {
        index: 'phoneNumber',
        i18n: 'Số điện thoại',
        sort_i18n: 'SĐT',
        width: '140px',
      },
      // {
      //   index: 'type',
      //   i18n: 'Loại',
      //   type: 'tag',
      //   config: {
      //     enum: COMMON_CONSTANT.enumUserType,
      //   },
      // },
    ];
    // this.lgConfig.tagFilter = COMMON_CONSTANT.enumUserType;
    this.lgConfig.toolSize = '82px';
  }

  changePassword(item: any): any {
    this.modal.createStatic(UserComponentsChangePasswordComponent, { i: item }).subscribe(() => {
      this.lgOnSearch();
    });
  }

  actionDelete(item: any): Observable<any> {
    const cmd = this.mainService.delete(item.id);
    return cmd;
  }

  actionDeleteMulti(selectedData: any[]): Observable<any> {
    const listId = selectedData.map((x) => x.id);
    const cmd = this.mainService.deleteRange(listId);
    return cmd;
  }
  actionSearch(filter: TemplateFilter) {
    if (filter.listSelectedTag && filter.listSelectedTag.length > 0) {
      const filterObj = JSON.parse(filter.filter);
      filterObj.ListType = filter.listSelectedTag;
      filter.filter = JSON.stringify(filterObj);
    }
    const payload = {
      page: filter.page,
      size: filter.size,
      sort: filter.sort,
      filter: filter.filter,
    };
    const cmd = this.mainService.getFilter(payload);
    return cmd;
  }
  processData(input: any[]): any[] {
    const output = [...input];
    output.forEach((x) => (x.imageDisplay = x.avatarUrl ? CONFIG.fileHost + '/' + x.avatarUrl : null));
    return output;
  }
}
