import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { UserComponentsItemComponent } from './components/item/item.component';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

const COMPONENTS = [UserComponent];
const COMPONENTS_NOROUNT = [UserComponentsItemComponent];

@NgModule({
  imports: [SharedModule, UserRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class UserModule { }
