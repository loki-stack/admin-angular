import { Component, Inject, OnInit, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StartupService } from '@core';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenModel, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { SysAuthenticationService, ApiInterceptor, StorageService } from '@services';
@Component({
  selector: 'passport-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.less'],
})
export class UserCallbackComponent implements OnInit {
  constructor(
    private authenticationService: SysAuthenticationService,
    private storageService: StorageService,
    private apiInterceptor: ApiInterceptor,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.queryParams.subscribe((params) => {
      const tokenStr = params.token;
      this.apiInterceptor.bearer(tokenStr);
      this.signIn();
    });
  }

  ngOnInit() {}
  signIn() {
    this.authenticationService.jwtInfo().subscribe((responseData) => {
      if (responseData.code === 200) {
        this.storageService.Login(responseData.data);
        this.reuseTabService.clear();
        const token = {
          email: 'loki@susu.com',
          id: 10000,
          name: 'admin',
          time: 1560939613287,
          token: responseData.data.tokenString,
        } as ITokenModel;
        this.tokenService.set(token);
        this.router.navigate(['/admin/home']);
      }
    });
  }
}
