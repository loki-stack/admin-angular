import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { I18NService, StartupService } from '@core';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenModel, ITokenService } from '@delon/auth';
import { ALAIN_I18N_TOKEN, SettingsService } from '@delon/theme';
import { SysAuthenticationService, StorageService } from '@services';
import { LoginModel } from '@services/admin/models';
import { GET_CONFIG } from '@shared';
import { NzNotificationService } from 'ng-zorro-antd/notification';

const CONFIG = GET_CONFIG();
@Component({
  selector: 'passport-lock',
  templateUrl: './lock.component.html',
  styleUrls: ['./lock.component.less'],
})
export class UserLockComponent implements OnInit {
  f: FormGroup;
  isLoading = false;
  error: string;

  NameKey: string;
  NameAvatar: string;
  fileHost: string;
  constructor(
    fb: FormBuilder,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    private startupSrv: StartupService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    public settings: SettingsService,
    private authenticationService: SysAuthenticationService,
    private storageService: StorageService,
    public notification: NzNotificationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    private router: Router,
  ) {
    this.fileHost = CONFIG.fileHost;
    tokenService.clear();
    this.f = fb.group({
      password: [null, Validators.required],
    });
  }
  ngOnInit() {
    this.NameKey = this.settings.user ? this.settings.user.name[0].toUpperCase() : 'N';
    this.NameAvatar = this.settings.user ? this.fileHost + '/' + this.settings.user.avatar : './assets/default-user.png';
    try {
      this.storageService.setItem('lock-username', this.storageService.GetLoginInfo().userModel.userName);
    } catch (error) {}
    this.tokenService.clear();
    this.storageService.Logout();
  }

  submit() {
    // tslint:disable-next-line: forin
    this.isLoading = true;
    // tslint:disable-next-line: forin
    for (const i in this.f.controls) {
      this.f.controls[i].markAsDirty();
      this.f.controls[i].updateValueAndValidity();
    }
    // console.log(this.f.value);
    if (this.f.valid) {
      // this.tokenService.set({
      //   token: '123',
      // });
      // this.router.navigate(['/']);
      this.authenticationService
        .signInJwt({
          // type: this.type,
          userName: this.storageService.getItem('lock-username'),
          password: this.f.value.password,
        } as LoginModel)
        .subscribe(
          (responseData) => {
            if (responseData.code === 200) {
              this.storageService.Login(responseData.data);
              this.reuseTabService.clear();
              const token = {
                email: 'loki@susu.com',
                id: 10000,
                name: 'admin',
                time: 1560939613287,
                token: responseData.data.tokenString,
              } as ITokenModel;
              this.tokenService.set(token);
              this.startupSrv.load().then(() => {
                // tslint:disable-next-line: no-non-null-assertion
                const url = this.tokenService.referrer!.url || '/admin/home';
                this.router.navigateByUrl(url);
              });
            }
            this.isLoading = false;
          },
          (error) => {
            this.error = this.i18n.fanyi('Sai mật khẩu');
            this.notification.error(this.i18n.fanyi('Sai mật khẩu'), this.i18n.fanyi('Vui lòng kiểm tra dữ liệu đã nhập'));
            this.isLoading = false;
          },
        );
    }
  }
}
