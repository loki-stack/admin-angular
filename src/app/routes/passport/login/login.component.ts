import { Component, Inject, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { I18NService, StartupService } from '@core';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenModel, ITokenService, SocialOpenType, SocialService } from '@delon/auth';
import { ALAIN_I18N_TOKEN, SettingsService, _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { StorageService, SysAuthenticationService } from '@services';
import { LoginModel } from '@services/admin/models';
import { GET_CONFIG } from '@shared';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
const CONFIG = GET_CONFIG();
@Component({
  selector: 'passport-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  providers: [SocialService],
})
export class UserLoginComponent implements OnInit, OnDestroy {
  constructor(
    private fb: FormBuilder,
    private modalSrv: NzModalService,
    private router: Router,
    private settingsService: SettingsService,
    private socialService: SocialService,
    private authenticationService: SysAuthenticationService,
    private storageService: StorageService,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    public http: _HttpClient,
    public msg: NzMessageService,
    public notification: NzNotificationService,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {
  }
  // #region fields
  get remember() {
    return this.form.controls.remember;
  }

  get userName() {
    return this.form.controls.userName;
  }
  get password() {
    return this.form.controls.password;
  }
  get mobile() {
    return this.form.controls.mobile;
  }
  get captcha() {
    return this.form.controls.captcha;
  }
  form: FormGroup;
  error: string;
  isLoading = false;
  type = 0;

  // #region get captcha

  count = 0;
  interval$: any;
  ngOnInit() {
    const currentUser = this.storageService.getItem('username');
    this.form = this.fb.group({
      userName: [currentUser, [Validators.required, Validators.minLength(4)]],
      password: [null, Validators.required],
      // mobile: [null, [Validators.required, Validators.pattern(/^1\d{10}$/)]],
      // captcha: [null, [Validators.required]],
      remember: [true],
    });
    this.modalSrv.closeAll();
  }

  // #endregion

  switch(ret: any) {
    this.type = ret.index;
  }

  getCaptcha() {
    if (this.mobile.invalid) {
      this.mobile.markAsDirty({ onlySelf: true });
      this.mobile.updateValueAndValidity({ onlySelf: true });
      return;
    }
    this.count = 59;
    this.interval$ = setInterval(() => {
      this.count -= 1;
      if (this.count <= 0) {
        clearInterval(this.interval$);
      }
    }, 1000);
  }

  // #endregion

  submit() {
    this.error = '';
    if (this.type === 0) {
      this.userName.markAsDirty();
      this.userName.updateValueAndValidity();
      this.password.markAsDirty();
      this.password.updateValueAndValidity();
      if (this.userName.invalid || this.password.invalid) {
        return;
      }
    } else {
      this.mobile.markAsDirty();
      this.mobile.updateValueAndValidity();
      this.captcha.markAsDirty();
      this.captcha.updateValueAndValidity();
      if (this.mobile.invalid || this.captcha.invalid) {
        return;
      }
    }

    // 默认配置中对所有HTTP请求都会强制 [校验](https://ng-alain.com/auth/getting-started) 用户 Token
    // 然一般来说登录请求不需要校验，因此可以在请求URL加上：`/login?_allow_anonymous=true` 表示不触发用户 Token 校验
    // this.http
    //   .post('/login/account?_allow_anonymous=true', {
    //     type: this.type,
    //     userName: this.userName.value,
    //     password: this.password.value,
    //   })
    //   .subscribe((res: any) => {
    //     if (res.msg !== 'ok') {
    //       this.error = res.msg;
    //       return;
    //     }
    //     // 清空路由复用信息
    //     this.reuseTabService.clear();
    //     // 设置用户Token信息
    //     this.tokenService.set(res.user);
    //     // 重新获取 StartupService 内容，我们始终认为应用信息一般都会受当前用户授权范围而影响
    //     this.startupSrv.load().then(() => {
    //       let url = this.tokenService.referrer!.url || '/';
    //       if (url.includes('/passport')) url = '/';
    //       this.router.navigateByUrl(url);
    //     });
    //   });
    this.isLoading = true;
    this.authenticationService
      .signInJwt({
        // type: this.type,
        userName: this.userName.value,
        password: this.password.value,
        isAdminSite: true, // Fix only admin can access
      } as LoginModel)
      .subscribe(
        (responseData) => {
          if (responseData.code === 200) {
            if (this.remember.value) {
              this.storageService.setItem('username', this.userName.value);
            }
            this.storageService.Login(responseData.data);
            this.reuseTabService.clear();
            const token = {
              email: 'loki@susu.com',
              id: 10000,
              name: 'admin',
              time: 1560939613287,
              token: responseData.data.tokenString,
            } as ITokenModel;
            this.tokenService.set(token);
            this.startupSrv.load().then(() => {
              // tslint:disable-next-line: no-non-null-assertion
              const url = this.tokenService.referrer!.url || '/admin/home';
              this.router.navigateByUrl(url);
            });
          }
          this.isLoading = false;
        },
        (error) => {
          if (error.status === 401) {
            this.error = this.i18n.fanyi('Sai tài khoản hoặc mật khẩu');
            // this.notification.error(this.i18n.fanyi('Sai tài khoản hoặc mật khẩu'), this.i18n.fanyi('Vui lòng kiểm tra dữ liệu đã nhập'));
          }
          this.isLoading = false;
        },
      );
  }

  // #region social
  oathen(type: string) {
    if (type === 'FB') {
      window.location.href = `${CONFIG.oaFbUrl}?callbackUrl=${CONFIG.callbackUrl}`;
    }
    if (type === 'GG') {
      window.location.href = `${CONFIG.oaGGUrl}?callbackUrl=${CONFIG.callbackUrl}`;
    }
  }

  open(type: string, openType: SocialOpenType = 'href') {
    let url = ``;
    const callback = environment.production
      ? 'https://ng-alain.github.io/ng-alain/#/callback/' + type
      : 'http://localhost:4200/#/callback/' + type;

    switch (type) {
      case 'auth0':
        url = `//cipchk.auth0.com/login?client=8gcNydIDzGBYxzqV0Vm1CX_RXH-wsWo5&redirect_uri=${decodeURIComponent(callback)}`;
        break;
      case 'github':
        url = `//github.com/login/oauth/authorize?client_id=9d6baae4b04a23fcafa2&response_type=code&redirect_uri=${decodeURIComponent(
          callback,
        )}`;
        break;
      case 'weibo':
        url = `https://api.weibo.com/oauth2/authorize?client_id=1239507802&response_type=code&redirect_uri=${decodeURIComponent(callback)}`;
        break;
    }
    if (openType === 'window') {
      this.socialService
        .login(url, '/', {
          type: 'window',
        })
        .subscribe((res) => {
          if (res) {
            this.settingsService.setUser(res);
            this.router.navigateByUrl('/');
          }
        });
    } else {
      this.socialService.login(url, '/', {
        type: 'href',
      });
    }
  }

  getUserNameError() {
    return this.form.get('userName').hasError('required')
      ? this.i18n.fanyi('Vui lòng nhập tên đăng nhập')
      : this.form.get('userName').hasError('minlength')
        ? this.i18n.fanyi('Tên đăng nhập cần ít nhất 4 ký tự')
        : '';
  }
  getPasswordError() {
    return this.form.get('password').hasError('required') ? this.i18n.fanyi('Vui lòng nhập mật khẩu') : '';
  }
  // #endregion

  ngOnDestroy(): void {
    if (this.interval$) {
      clearInterval(this.interval$);
    }
  }
}
