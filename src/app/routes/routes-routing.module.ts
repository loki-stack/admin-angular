import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@core';
import { CallbackComponent } from './callback/callback.component';
import { LayoutPassportComponent } from '../layout/passport/passport.component';
import { LayoutProComponent } from '@brand';
import { NgModule } from '@angular/core';
import { UserCallbackComponent } from './passport/callback/callback.component';
import { UserLockComponent } from './passport/lock/lock.component';
import { UserLoginComponent } from './passport/login/login.component';
import { UserRegisterComponent } from './passport/register/register.component';
import { UserRegisterResultComponent } from './passport/register-result/register-result.component';
import { environment } from '@env/environment';

// layout

// single pages

// passport pages

const routes: Routes = [
  {
    path: '',
    component: LayoutProComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'admin', pathMatch: 'full' },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
      },
      // Exception
      {
        path: 'exception',
        loadChildren: () => import('./exception/exception.module').then((m) => m.ExceptionModule),
      },
    ],
  },
  // passport
  {
    path: 'passport',
    component: LayoutPassportComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      {
        path: 'login',
        component: UserLoginComponent,
        data: { title: 'Login', titleI18n: 'Đăng nhập' },
      },
      {
        path: 'register',
        component: UserRegisterComponent,
        data: { title: 'Register', titleI18n: 'Đăng ký tài khoản' },
      },
      {
        path: 'register-result',
        component: UserRegisterResultComponent,
        data: { title: 'Register Result', titleI18n: 'Đăng ký tài khoản' },
      },
      {
        path: 'callback',
        component: UserCallbackComponent,
        data: { title: 'callback', titleI18n: 'callback' },
      },
      {
        path: 'lock',
        component: UserLockComponent,
        data: { title: 'Lock', titleI18n: 'Khóa' },
      },
    ],
  },
  { path: 'callback/:type', component: CallbackComponent },
  { path: '**', redirectTo: 'exception/404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
      // NOTICE: If you use `reuse-tab` component and turn on keepingScroll you can set to `disabled`
      // Pls refer to https://ng-alain.com/components/reuse-tab
      scrollPositionRestoration: 'top',
      initialNavigation: 'enabled',
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
