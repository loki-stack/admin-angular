/* tslint:disable */
import { Injectable } from '@angular/core';

/**
 * Global configuration for AdminApi services
 */
@Injectable({
  providedIn: 'root',
})
export class AdminApiConfiguration {
  rootUrl: string = 'https://localhost:5001';
}

export interface AdminApiConfigurationInterface {
  rootUrl?: string;
}
