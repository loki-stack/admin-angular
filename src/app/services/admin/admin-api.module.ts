/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AdminApiConfiguration, AdminApiConfigurationInterface } from './admin-api-configuration';

import { BsdLogsService } from './services/bsd-logs.service';
import { BsdNavigationService } from './services/bsd-navigation.service';
import { BsdParameterService } from './services/bsd-parameter.service';
import { IdmRightService } from './services/idm-right.service';
import { IdmRoleService } from './services/idm-role.service';
import { IdmUserService } from './services/idm-user.service';
import { MdmNodeUploadService } from './services/mdm-node-upload.service';
import { SysApplicationService } from './services/sys-application.service';
import { SysAuthenticationService } from './services/sys-authentication.service';
import { SysCacheService } from './services/sys-cache.service';
import { SysPushService } from './services/sys-push.service';

/**
 * Provider for all AdminApi services, plus AdminApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    AdminApiConfiguration,
    BsdLogsService,
    BsdNavigationService,
    BsdParameterService,
    IdmRightService,
    IdmRoleService,
    IdmUserService,
    MdmNodeUploadService,
    SysApplicationService,
    SysAuthenticationService,
    SysCacheService,
    SysPushService
  ],
})
export class AdminApiModule {
  static forRoot(customParams: AdminApiConfigurationInterface): ModuleWithProviders<AdminApiModule> {
    return {
      ngModule: AdminApiModule,
      providers: [
        {
          provide: AdminApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
