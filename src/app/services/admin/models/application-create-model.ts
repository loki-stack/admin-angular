/* tslint:disable */
export interface ApplicationCreateModel {
  code?: string;
  description?: string;
  name?: string;
  templateApp?: string;
}
