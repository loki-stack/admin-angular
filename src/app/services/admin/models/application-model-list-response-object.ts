/* tslint:disable */
import { Code } from './code';
import { ApplicationModel } from './application-model';
export interface ApplicationModelListResponseObject {
  code?: Code;
  data?: Array<ApplicationModel>;
  message?: string;
  totalTime?: number;
}
