/* tslint:disable */
import { ApplicationModel } from './application-model';
export interface ApplicationModelPagination {
  content?: Array<ApplicationModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
