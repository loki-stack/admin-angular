/* tslint:disable */
import { Code } from './code';
import { ApplicationModel } from './application-model';
export interface ApplicationModelResponseObject {
  code?: Code;
  data?: ApplicationModel;
  message?: string;
  totalTime?: number;
}
