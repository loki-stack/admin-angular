/* tslint:disable */
import { Code } from './code';
import { ApplicationModelPagination } from './application-model-pagination';
export interface ApplicationModelResponsePagination {
  code?: Code;
  data?: ApplicationModelPagination;
  message?: string;
  totalTime?: number;
}
