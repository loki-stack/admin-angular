/* tslint:disable */
export interface ApplicationModel {
  code?: string;
  description?: string;
  id?: string;
  isDefault?: boolean;
  name?: string;
}
