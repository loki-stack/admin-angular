/* tslint:disable */
export interface ApplicationUpdateModel {
  code?: string;
  description?: string;
  name?: string;
}
