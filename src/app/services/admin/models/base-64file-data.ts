/* tslint:disable */
export interface Base64FileData {

  /**
   * File data base 64 string
   */
  fileData?: string;
  name?: string;
}
