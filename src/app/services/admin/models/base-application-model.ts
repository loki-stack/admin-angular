/* tslint:disable */
export interface BaseApplicationModel {
  code?: string;
  id?: string;
  name?: string;
}
