/* tslint:disable */
import { BaseUserModel } from './base-user-model';
export interface BaseNavigationModel {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  id?: string;
  idPath?: string;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  level?: number;
  name?: string;
  order?: number;
  path?: string;
  status?: boolean;
}
