/* tslint:disable */
import { Code } from './code';
import { BaseRightModel } from './base-right-model';
export interface BaseRightModelListResponseObject {
  code?: Code;
  data?: Array<BaseRightModel>;
  message?: string;
  totalTime?: number;
}
