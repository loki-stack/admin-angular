/* tslint:disable */
import { BaseUserModel } from './base-user-model';
import { BaseRoleModel } from './base-role-model';
export interface BaseRightModelOfUser {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  enable?: boolean;
  id?: string;
  inherited?: boolean;
  inheritedFromRoles?: string;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  listRole?: Array<BaseRoleModel>;
  listRoleId?: Array<string>;
  name?: string;
}
