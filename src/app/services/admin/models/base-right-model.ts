/* tslint:disable */
import { BaseUserModel } from './base-user-model';
export interface BaseRightModel {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  id?: string;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  name?: string;
}
