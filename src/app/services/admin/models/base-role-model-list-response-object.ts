/* tslint:disable */
import { Code } from './code';
import { BaseRoleModel } from './base-role-model';
export interface BaseRoleModelListResponseObject {
  code?: Code;
  data?: Array<BaseRoleModel>;
  message?: string;
  totalTime?: number;
}
