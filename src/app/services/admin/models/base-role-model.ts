/* tslint:disable */
export interface BaseRoleModel {
  code?: string;
  id?: string;
  isSystem?: boolean;
  name?: string;
}
