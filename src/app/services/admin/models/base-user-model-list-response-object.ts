/* tslint:disable */
import { Code } from './code';
import { BaseUserModel } from './base-user-model';
export interface BaseUserModelListResponseObject {
  code?: Code;
  data?: Array<BaseUserModel>;
  message?: string;
  totalTime?: number;
}
