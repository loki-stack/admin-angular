/* tslint:disable */
export interface BaseUserModel {
  applicationId?: string;
  avatarUrl?: string;
  birthdate?: string;
  email?: string;
  id?: string;
  lastActivityDate?: string;
  name?: string;
  password?: string;
  passwordSalt?: string;
  phoneNumber?: string;
  type?: number;
  userName?: string;
}
