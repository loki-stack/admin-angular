/* tslint:disable */
import { Code } from './code';
export interface BooleanResponseObject {
  code?: Code;
  data?: boolean;
  message?: string;
  totalTime?: number;
}
