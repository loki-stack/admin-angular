/* tslint:disable */
import { FileModel } from './file-model';
export interface FileModelPagination {
  content?: Array<FileModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
