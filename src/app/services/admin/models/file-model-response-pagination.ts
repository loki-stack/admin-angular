/* tslint:disable */
import { Code } from './code';
import { FileModelPagination } from './file-model-pagination';
export interface FileModelResponsePagination {
  code?: Code;
  data?: FileModelPagination;
  message?: string;
  totalTime?: number;
}
