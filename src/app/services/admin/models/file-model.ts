/* tslint:disable */
export interface FileModel {
  caption?: string;
  commentStatus?: string;
  createdByUserId?: string;
  createdOnDate?: string;
  description?: string;
  fileId?: string;
  fullPath?: string;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  order?: number;
  path?: string;
  status?: string;
  title?: string;
}
