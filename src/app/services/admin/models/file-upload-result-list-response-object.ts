/* tslint:disable */
import { Code } from './code';
import { FileUploadResult } from './file-upload-result';
export interface FileUploadResultListResponseObject {
  code?: Code;
  data?: Array<FileUploadResult>;
  message?: string;
  totalTime?: number;
}
