/* tslint:disable */
import { Code } from './code';
import { FileUploadResult } from './file-upload-result';
export interface FileUploadResultResponseObject {
  code?: Code;
  data?: FileUploadResult;
  message?: string;
  totalTime?: number;
}
