/* tslint:disable */

/**
 * File upload result
 */
export interface FileUploadResult {
  extension?: string;
  name?: string;
  physicalName?: string;
  physicalPath?: string;
  size?: number;
}
