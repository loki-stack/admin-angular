/* tslint:disable */
export interface LoginModel {
  isAdminSite?: boolean;
  password?: string;
  rememberMe?: boolean;
  username?: string;
}
