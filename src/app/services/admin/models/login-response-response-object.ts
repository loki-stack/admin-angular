/* tslint:disable */
import { Code } from './code';
import { LoginResponse } from './login-response';
export interface LoginResponseResponseObject {
  code?: Code;
  data?: LoginResponse;
  message?: string;
  totalTime?: number;
}
