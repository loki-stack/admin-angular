/* tslint:disable */
import { ApplicationModel } from './application-model';
import { BaseRightModelOfUser } from './base-right-model-of-user';
import { BaseRoleModel } from './base-role-model';
import { BaseUserModel } from './base-user-model';
export interface LoginResponse {
  applicationId?: string;
  applicationModel?: ApplicationModel;
  expiresAt?: string;
  issuedAt?: string;
  listApplication?: Array<ApplicationModel>;
  listRight?: Array<BaseRightModelOfUser>;
  listRole?: Array<BaseRoleModel>;
  tokenString?: string;
  userId?: string;
  userModel?: BaseUserModel;
}
