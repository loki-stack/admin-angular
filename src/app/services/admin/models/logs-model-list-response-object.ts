/* tslint:disable */
import { Code } from './code';
import { LogsModel } from './logs-model';
export interface LogsModelListResponseObject {
  code?: Code;
  data?: Array<LogsModel>;
  message?: string;
  totalTime?: number;
}
