/* tslint:disable */
import { LogsModel } from './logs-model';
export interface LogsModelPagination {
  content?: Array<LogsModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
