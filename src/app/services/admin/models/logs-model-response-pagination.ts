/* tslint:disable */
import { Code } from './code';
import { LogsModelPagination } from './logs-model-pagination';
export interface LogsModelResponsePagination {
  code?: Code;
  data?: LogsModelPagination;
  message?: string;
  totalTime?: number;
}
