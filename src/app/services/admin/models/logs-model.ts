/* tslint:disable */
import { BaseApplicationModel } from './base-application-model';
import { BaseUserModel } from './base-user-model';
export interface LogsModel {
  applicationId?: string;
  applicationModel?: BaseApplicationModel;
  exception?: string;
  level?: number;
  log_event?: string;
  message?: string;
  message_template?: string;
  timestamp?: string;
  userId?: string;
  userModel?: BaseUserModel;
}
