/* tslint:disable */
import { BaseNavigationModel } from './base-navigation-model';
export interface NavigationCreateModel {
  code?: string;
  iconClass?: string;
  name?: string;
  order?: number;
  parentModel?: BaseNavigationModel;
  status?: boolean;
  subUrl?: string;
  urlRewrite?: string;
}
