/* tslint:disable */
import { Code } from './code';
import { NavigationModel } from './navigation-model';
export interface NavigationModelListResponseObject {
  code?: Code;
  data?: Array<NavigationModel>;
  message?: string;
  totalTime?: number;
}
