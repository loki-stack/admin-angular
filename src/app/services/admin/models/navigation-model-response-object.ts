/* tslint:disable */
import { Code } from './code';
import { NavigationModel } from './navigation-model';
export interface NavigationModelResponseObject {
  code?: Code;
  data?: NavigationModel;
  message?: string;
  totalTime?: number;
}
