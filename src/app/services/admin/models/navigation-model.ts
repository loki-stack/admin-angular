/* tslint:disable */
import { BaseUserModel } from './base-user-model';
export interface NavigationModel {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  iconClass?: string;
  id?: string;
  idPath?: string;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  level?: number;
  name?: string;
  order?: number;
  parentId?: string;
  path?: string;
  status?: boolean;
  subChild?: Array<NavigationModel>;
  subUrl?: string;
  urlRewrite?: string;
}
