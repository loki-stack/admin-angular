/* tslint:disable */
import { BaseNavigationModel } from './base-navigation-model';
export interface NavigationUpdateModel {
  code?: string;
  fromSubNavigation?: string;
  iconClass?: string;
  name?: string;
  order?: number;
  parentModel?: BaseNavigationModel;
  status?: boolean;
  subUrl?: string;
  urlRewrite?: string;
}
