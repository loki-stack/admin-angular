/* tslint:disable */
export interface ParameterCreateModel {
  description?: string;
  groupCode?: string;
  id?: string;
  name?: string;
  value?: string;
}
