/* tslint:disable */
import { Code } from './code';
import { ParameterModel } from './parameter-model';
export interface ParameterModelListResponseObject {
  code?: Code;
  data?: Array<ParameterModel>;
  message?: string;
  totalTime?: number;
}
