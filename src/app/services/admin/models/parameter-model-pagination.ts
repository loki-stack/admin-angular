/* tslint:disable */
import { ParameterModel } from './parameter-model';
export interface ParameterModelPagination {
  content?: Array<ParameterModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
