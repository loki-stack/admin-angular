/* tslint:disable */
import { Code } from './code';
import { ParameterModel } from './parameter-model';
export interface ParameterModelResponseObject {
  code?: Code;
  data?: ParameterModel;
  message?: string;
  totalTime?: number;
}
