/* tslint:disable */
import { Code } from './code';
import { ParameterModelPagination } from './parameter-model-pagination';
export interface ParameterModelResponsePagination {
  code?: Code;
  data?: ParameterModelPagination;
  message?: string;
  totalTime?: number;
}
