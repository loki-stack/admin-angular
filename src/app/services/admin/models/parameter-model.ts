/* tslint:disable */
import { BaseUserModel } from './base-user-model';
export interface ParameterModel {
  applicationId?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  description?: string;
  groupCode?: string;
  id?: string;
  isSystem?: boolean;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  name?: string;
  value?: string;
}
