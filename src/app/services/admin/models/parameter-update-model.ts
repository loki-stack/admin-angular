/* tslint:disable */
export interface ParameterUpdateModel {
  description?: string;
  groupCode?: string;
  name?: string;
  value?: string;
}
