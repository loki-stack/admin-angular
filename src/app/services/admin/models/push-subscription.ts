/* tslint:disable */
export interface PushSubscription {
  endpoint?: string;
  keys?: {[key: string]: string};
}
