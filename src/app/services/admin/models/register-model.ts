/* tslint:disable */
export interface RegisterModel {
  avatarUrl?: string;
  birthdate?: string;
  email?: string;
  fullName?: string;
  password?: string;
  phoneNumber?: string;
  rememberMe?: boolean;
  userName?: string;
}
