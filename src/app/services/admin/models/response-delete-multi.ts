/* tslint:disable */
import { Code } from './code';
import { ResponseDelete } from './response-delete';
export interface ResponseDeleteMulti {
  code?: Code;
  data?: Array<ResponseDelete>;
  message?: string;
  totalTime?: number;
}
