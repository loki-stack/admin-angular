/* tslint:disable */
import { Code } from './code';
import { ResponseDeleteModel } from './response-delete-model';
export interface ResponseDelete {
  code?: Code;
  data?: ResponseDeleteModel;
  message?: string;
  totalTime?: number;
}
