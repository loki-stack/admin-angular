/* tslint:disable */
import { Code } from './code';
import { ResponseUpdateModel } from './response-update-model';
export interface ResponseUpdate {
  code?: Code;
  data?: ResponseUpdateModel;
  message?: string;
  totalTime?: number;
}
