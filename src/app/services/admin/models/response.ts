/* tslint:disable */
import { Code } from './code';
export interface Response {
  code?: Code;
  message?: string;
  totalTime?: number;
}
