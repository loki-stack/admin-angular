/* tslint:disable */
export interface RightCreateModel {
  applicatonId?: string;
  code?: string;
  description?: string;
  groupCode?: string;
  isSystem?: boolean;

  /**
   * Danh sách nhóm user thêm
   */
  listAddRoleId?: Array<string>;

  /**
   * Danh sách user thêm
   */
  listAddUserId?: Array<string>;
  name?: string;
  order?: number;
  status?: boolean;
  userId?: string;
}
