/* tslint:disable */
import { Code } from './code';
import { RightDetailModel } from './right-detail-model';
export interface RightDetailModelListResponseObject {
  code?: Code;
  data?: Array<RightDetailModel>;
  message?: string;
  totalTime?: number;
}
