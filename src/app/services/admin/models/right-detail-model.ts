/* tslint:disable */
import { BaseUserModel } from './base-user-model';
import { BaseRoleModel } from './base-role-model';
export interface RightDetailModel {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  description?: string;
  groupCode?: string;
  id?: string;
  isSystem?: boolean;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  listRole?: Array<BaseRoleModel>;
  listUser?: Array<BaseUserModel>;
  name?: string;
  order?: number;
  status?: boolean;
}
