/* tslint:disable */
import { Code } from './code';
import { RightModel } from './right-model';
export interface RightModelListResponseObject {
  code?: Code;
  data?: Array<RightModel>;
  message?: string;
  totalTime?: number;
}
