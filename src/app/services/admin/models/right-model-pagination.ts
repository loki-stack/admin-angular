/* tslint:disable */
import { RightModel } from './right-model';
export interface RightModelPagination {
  content?: Array<RightModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
