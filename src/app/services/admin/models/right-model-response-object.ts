/* tslint:disable */
import { Code } from './code';
import { RightModel } from './right-model';
export interface RightModelResponseObject {
  code?: Code;
  data?: RightModel;
  message?: string;
  totalTime?: number;
}
