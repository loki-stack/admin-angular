/* tslint:disable */
import { Code } from './code';
import { RightModelPagination } from './right-model-pagination';
export interface RightModelResponsePagination {
  code?: Code;
  data?: RightModelPagination;
  message?: string;
  totalTime?: number;
}
