/* tslint:disable */
import { BaseUserModel } from './base-user-model';
export interface RightModel {
  applicationId?: string;
  code?: string;
  createdByUser?: BaseUserModel;
  createdByUserId?: string;
  createdOnDate?: string;
  description?: string;
  groupCode?: string;
  id?: string;
  isSystem?: boolean;
  lastModifiedByUserId?: string;
  lastModifiedOnDate?: string;
  name?: string;
  order?: number;
  status?: boolean;
}
