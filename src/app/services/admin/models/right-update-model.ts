/* tslint:disable */
export interface RightUpdateModel {
  applicatonId?: string;
  code?: string;
  description?: string;
  groupCode?: string;
  id?: string;
  isSystem?: boolean;

  /**
   * Danh sách nhóm user thêm
   */
  listAddRoleId?: Array<string>;

  /**
   * Danh sách user thêm
   */
  listAddUserId?: Array<string>;

  /**
   * Danh sách nhóm user xóa
   */
  listDeleteRoleId?: Array<string>;

  /**
   * Danh sách user xóa
   */
  listDeleteUserId?: Array<string>;
  name?: string;
  order?: number;
  status?: boolean;
  userId?: string;
}
