/* tslint:disable */
export interface RoleCreateModel {
  applicatonId?: string;
  code?: string;
  description?: string;

  /**
   * Danh sách quyền thêm
   */
  listAddRightId?: Array<string>;

  /**
   * Danh sách user thêm
   */
  listAddUserId?: Array<string>;
  name?: string;
}
