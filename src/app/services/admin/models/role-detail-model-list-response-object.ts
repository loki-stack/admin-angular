/* tslint:disable */
import { Code } from './code';
import { RoleDetailModel } from './role-detail-model';
export interface RoleDetailModelListResponseObject {
  code?: Code;
  data?: Array<RoleDetailModel>;
  message?: string;
  totalTime?: number;
}
