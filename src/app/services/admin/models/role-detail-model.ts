/* tslint:disable */
import { BaseRightModel } from './base-right-model';
import { BaseUserModel } from './base-user-model';
export interface RoleDetailModel {
  code?: string;
  description?: string;
  id?: string;
  isSystem?: boolean;
  listRight?: Array<BaseRightModel>;
  listUser?: Array<BaseUserModel>;
  name?: string;
}
