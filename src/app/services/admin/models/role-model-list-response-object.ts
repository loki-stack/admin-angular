/* tslint:disable */
import { Code } from './code';
import { RoleModel } from './role-model';
export interface RoleModelListResponseObject {
  code?: Code;
  data?: Array<RoleModel>;
  message?: string;
  totalTime?: number;
}
