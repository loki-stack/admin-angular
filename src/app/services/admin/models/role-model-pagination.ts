/* tslint:disable */
import { RoleModel } from './role-model';
export interface RoleModelPagination {
  content?: Array<RoleModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
