/* tslint:disable */
import { Code } from './code';
import { RoleModel } from './role-model';
export interface RoleModelResponseObject {
  code?: Code;
  data?: RoleModel;
  message?: string;
  totalTime?: number;
}
