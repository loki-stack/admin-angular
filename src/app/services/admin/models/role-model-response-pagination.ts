/* tslint:disable */
import { Code } from './code';
import { RoleModelPagination } from './role-model-pagination';
export interface RoleModelResponsePagination {
  code?: Code;
  data?: RoleModelPagination;
  message?: string;
  totalTime?: number;
}
