/* tslint:disable */
export interface RoleModel {
  code?: string;
  description?: string;
  id?: string;
  isSystem?: boolean;
  name?: string;
}
