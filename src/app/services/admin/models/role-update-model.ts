/* tslint:disable */
export interface RoleUpdateModel {
  applicatonId?: string;
  code?: string;
  description?: string;

  /**
   * Danh sách quyền thêm
   */
  listAddRightId?: Array<string>;

  /**
   * Danh sách user thêm
   */
  listAddUserId?: Array<string>;

  /**
   * Danh sách quyền xóa
   */
  listDeleteRightId?: Array<string>;

  /**
   * Danh sách user xóa
   */
  listDeleteUserId?: Array<string>;
  name?: string;
}
