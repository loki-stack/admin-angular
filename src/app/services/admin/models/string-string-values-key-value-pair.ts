/* tslint:disable */
export interface StringStringValuesKeyValuePair {
  key?: string;
  value?: Array<string>;
}
