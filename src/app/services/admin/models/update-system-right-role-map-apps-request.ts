/* tslint:disable */
export interface UpdateSystemRightRoleMapAppsRequest {
  listApplicationId?: Array<string>;
  listRightId?: Array<string>;
  listRoleId?: Array<string>;
}
