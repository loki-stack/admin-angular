/* tslint:disable */
export interface UserCreateModel {
  applicationId?: string;
  avatarUrl?: string;
  birthdate?: string;
  email?: string;

  /**
   * preset
   */
  id?: string;

  /**
   * Danh sách quyền thêm
   */
  listAddRightId?: Array<string>;

  /**
   * Danh sách nhóm user thêm
   */
  listAddRoleId?: Array<string>;
  name?: string;
  password?: string;
  phoneNumber?: string;
  type?: number;
  userName?: string;
}
