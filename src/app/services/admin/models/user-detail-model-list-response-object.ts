/* tslint:disable */
import { Code } from './code';
import { UserDetailModel } from './user-detail-model';
export interface UserDetailModelListResponseObject {
  code?: Code;
  data?: Array<UserDetailModel>;
  message?: string;
  totalTime?: number;
}
