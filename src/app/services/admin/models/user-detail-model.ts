/* tslint:disable */
import { BaseRightModelOfUser } from './base-right-model-of-user';
import { BaseRoleModel } from './base-role-model';
export interface UserDetailModel {
  applicationId?: string;
  avatarUrl?: string;
  birthdate?: string;
  email?: string;
  id?: string;
  lastActivityDate?: string;
  listRight?: Array<BaseRightModelOfUser>;
  listRole?: Array<BaseRoleModel>;
  name?: string;
  password?: string;
  passwordSalt?: string;
  phoneNumber?: string;
  type?: number;
  userName?: string;
}
