/* tslint:disable */
import { Code } from './code';
import { UserModel } from './user-model';
export interface UserModelListResponseObject {
  code?: Code;
  data?: Array<UserModel>;
  message?: string;
  totalTime?: number;
}
