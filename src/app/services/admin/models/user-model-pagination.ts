/* tslint:disable */
import { UserModel } from './user-model';
export interface UserModelPagination {
  content?: Array<UserModel>;
  numberOfElements?: number;
  page?: number;
  size?: number;
  totalElements?: number;
  totalPages?: number;
}
