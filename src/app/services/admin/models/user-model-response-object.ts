/* tslint:disable */
import { Code } from './code';
import { UserModel } from './user-model';
export interface UserModelResponseObject {
  code?: Code;
  data?: UserModel;
  message?: string;
  totalTime?: number;
}
