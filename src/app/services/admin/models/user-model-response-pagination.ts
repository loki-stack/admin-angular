/* tslint:disable */
import { Code } from './code';
import { UserModelPagination } from './user-model-pagination';
export interface UserModelResponsePagination {
  code?: Code;
  data?: UserModelPagination;
  message?: string;
  totalTime?: number;
}
