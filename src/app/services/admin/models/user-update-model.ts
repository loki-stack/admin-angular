/* tslint:disable */
export interface UserUpdateModel {
  applicationId?: string;
  avatarUrl?: string;
  birthdate?: string;
  email?: string;
  id?: string;

  /**
   * Danh sách quyền thêm
   */
  listAddRightId?: Array<string>;

  /**
   * Danh sách nhóm user thêm
   */
  listAddRoleId?: Array<string>;

  /**
   * Danh sách quyền xóa
   */
  listDeleteRightId?: Array<string>;

  /**
   * Danh sách nhóm user xóa
   */
  listDeleteRoleId?: Array<string>;
  name?: string;
  password?: string;
  phoneNumber?: string;
  type?: number;
  userName?: string;
}
