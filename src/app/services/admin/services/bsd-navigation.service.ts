/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { NavigationModelListResponseObject } from '../models/navigation-model-list-response-object';
import { NavigationModelResponseObject } from '../models/navigation-model-response-object';
import { NavigationCreateModel } from '../models/navigation-create-model';
import { ResponseUpdate } from '../models/response-update';
import { NavigationUpdateModel } from '../models/navigation-update-model';
import { ResponseDelete } from '../models/response-delete';
@Injectable({
  providedIn: 'root',
})
class BsdNavigationService extends __BaseService {
  static readonly getTreePath = '/api/v1/bsd/navigations/tree';
  static readonly createPath = '/api/v1/bsd/navigations';
  static readonly updatePath = '/api/v1/bsd/navigations/{id}';
  static readonly deletePath = '/api/v1/bsd/navigations/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get tree
   * @return Success
   */
  getTreeResponse(): __Observable<__StrictHttpResponse<NavigationModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/bsd/navigations/tree`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<NavigationModelListResponseObject>;
      })
    );
  }
  /**
   * Get tree
   * @return Success
   */
  getTree(): __Observable<NavigationModelListResponseObject> {
    return this.getTreeResponse().pipe(
      __map(_r => _r.body as NavigationModelListResponseObject)
    );
  }

  /**
   * Create
   * @param body
   * @return Success
   */
  createResponse(body?: NavigationCreateModel): __Observable<__StrictHttpResponse<NavigationModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/bsd/navigations`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<NavigationModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param body
   * @return Success
   */
  create(body?: NavigationCreateModel): __Observable<NavigationModelResponseObject> {
    return this.createResponse(body).pipe(
      __map(_r => _r.body as NavigationModelResponseObject)
    );
  }

  /**
   * Update
   * @param params The `BsdNavigationService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  updateResponse(params: BsdNavigationService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/bsd/navigations/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `BsdNavigationService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  update(params: BsdNavigationService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/bsd/navigations/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }
}

module BsdNavigationService {

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: NavigationUpdateModel;
  }
}

export { BsdNavigationService }
