/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ParameterModelResponseObject } from '../models/parameter-model-response-object';
import { ParameterCreateModel } from '../models/parameter-create-model';
import { ResponseDeleteMulti } from '../models/response-delete-multi';
import { ParameterModelResponsePagination } from '../models/parameter-model-response-pagination';
import { ResponseUpdate } from '../models/response-update';
import { ParameterUpdateModel } from '../models/parameter-update-model';
import { ResponseDelete } from '../models/response-delete';
import { ParameterModelListResponseObject } from '../models/parameter-model-list-response-object';
@Injectable({
  providedIn: 'root',
})
class BsdParameterService extends __BaseService {
  static readonly createPath = '/api/v1/bsd/parameters';
  static readonly deleteRangePath = '/api/v1/bsd/parameters';
  static readonly getFilterPath = '/api/v1/bsd/parameters';
  static readonly updatePath = '/api/v1/bsd/parameters/{id}';
  static readonly deletePath = '/api/v1/bsd/parameters/{id}';
  static readonly getByIdPath = '/api/v1/bsd/parameters/{id}';
  static readonly getByCodePath = '/api/v1/bsd/parameters/code/{code}';
  static readonly getAllPath = '/api/v1/bsd/parameters/all';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Create
   * @param body
   * @return Success
   */
  createResponse(body?: ParameterCreateModel): __Observable<__StrictHttpResponse<ParameterModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/bsd/parameters`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParameterModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param body
   * @return Success
   */
  create(body?: ParameterCreateModel): __Observable<ParameterModelResponseObject> {
    return this.createResponse(body).pipe(
      __map(_r => _r.body as ParameterModelResponseObject)
    );
  }

  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRangeResponse(listId?: Array<string>): __Observable<__StrictHttpResponse<ResponseDeleteMulti>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (listId || []).forEach(val => {if (val != null) __params = __params.append('listId', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/bsd/parameters`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDeleteMulti>;
      })
    );
  }
  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRange(listId?: Array<string>): __Observable<ResponseDeleteMulti> {
    return this.deleteRangeResponse(listId).pipe(
      __map(_r => _r.body as ResponseDeleteMulti)
    );
  }

  /**
   * Get filter and pagination
   * @param params The `BsdParameterService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: BsdParameterService.GetFilterParams): __Observable<__StrictHttpResponse<ParameterModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/bsd/parameters`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParameterModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `BsdParameterService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: BsdParameterService.GetFilterParams): __Observable<ParameterModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as ParameterModelResponsePagination)
    );
  }

  /**
   * Update
   * @param params The `BsdParameterService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  updateResponse(params: BsdParameterService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/bsd/parameters/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `BsdParameterService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  update(params: BsdParameterService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/bsd/parameters/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }

  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getByIdResponse(id: string): __Observable<__StrictHttpResponse<ParameterModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/bsd/parameters/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParameterModelResponseObject>;
      })
    );
  }
  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getById(id: string): __Observable<ParameterModelResponseObject> {
    return this.getByIdResponse(id).pipe(
      __map(_r => _r.body as ParameterModelResponseObject)
    );
  }

  /**
   * Get by code value
   * @param code
   * @return Success
   */
  getByCodeResponse(code: string): __Observable<__StrictHttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/bsd/parameters/code/${encodeURIComponent(code)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * Get by code value
   * @param code
   * @return Success
   */
  getByCode(code: string): __Observable<string> {
    return this.getByCodeResponse(code).pipe(
      __map(_r => _r.body as string)
    );
  }

  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAllResponse(filter?: string): __Observable<__StrictHttpResponse<ParameterModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/bsd/parameters/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ParameterModelListResponseObject>;
      })
    );
  }
  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAll(filter?: string): __Observable<ParameterModelListResponseObject> {
    return this.getAllResponse(filter).pipe(
      __map(_r => _r.body as ParameterModelListResponseObject)
    );
  }
}

module BsdParameterService {

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: ParameterUpdateModel;
  }
}

export { BsdParameterService }
