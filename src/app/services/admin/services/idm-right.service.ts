/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RightDetailModelListResponseObject } from '../models/right-detail-model-list-response-object';
import { BaseUserModelListResponseObject } from '../models/base-user-model-list-response-object';
import { Response } from '../models/response';
import { BaseRoleModelListResponseObject } from '../models/base-role-model-list-response-object';
import { RightModelResponseObject } from '../models/right-model-response-object';
import { RightCreateModel } from '../models/right-create-model';
import { ResponseDeleteMulti } from '../models/response-delete-multi';
import { RightModelResponsePagination } from '../models/right-model-response-pagination';
import { ResponseUpdate } from '../models/response-update';
import { RightUpdateModel } from '../models/right-update-model';
import { ResponseDelete } from '../models/response-delete';
import { RightModelListResponseObject } from '../models/right-model-list-response-object';
@Injectable({
  providedIn: 'root',
})
class IdmRightService extends __BaseService {
  static readonly getDetailPath = '/api/v1/idm/rights/{id}/detail';
  static readonly getUserMapRightPath = '/api/v1/idm/rights/{id}/user';
  static readonly addRightMapUserPath = '/api/v1/idm/rights/{id}/user';
  static readonly deleteRightMapUserPath = '/api/v1/idm/rights/{id}/user';
  static readonly getRoleMapRightPath = '/api/v1/idm/rights/{id}/role';
  static readonly addRightMapRolePath = '/api/v1/idm/rights/{id}/role';
  static readonly deleteRightMapRolePath = '/api/v1/idm/rights/{id}/role';
  static readonly createPath = '/api/v1/idm/rights';
  static readonly deleteRangePath = '/api/v1/idm/rights';
  static readonly getFilterPath = '/api/v1/idm/rights';
  static readonly updatePath = '/api/v1/idm/rights/{id}';
  static readonly deletePath = '/api/v1/idm/rights/{id}';
  static readonly getByIdPath = '/api/v1/idm/rights/{id}';
  static readonly getAllPath = '/api/v1/idm/rights/all';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get by id with detail
   * @param params The `IdmRightService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetailResponse(params: IdmRightService.GetDetailParams): __Observable<__StrictHttpResponse<RightDetailModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/detail`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RightDetailModelListResponseObject>;
      })
    );
  }
  /**
   * Get by id with detail
   * @param params The `IdmRightService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetail(params: IdmRightService.GetDetailParams): __Observable<RightDetailModelListResponseObject> {
    return this.getDetailResponse(params).pipe(
      __map(_r => _r.body as RightDetailModelListResponseObject)
    );
  }

  /**
   * Get users map right
   * @param params The `IdmRightService.GetUserMapRightParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getUserMapRightResponse(params: IdmRightService.GetUserMapRightParams): __Observable<__StrictHttpResponse<BaseUserModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseUserModelListResponseObject>;
      })
    );
  }
  /**
   * Get users map right
   * @param params The `IdmRightService.GetUserMapRightParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getUserMapRight(params: IdmRightService.GetUserMapRightParams): __Observable<BaseUserModelListResponseObject> {
    return this.getUserMapRightResponse(params).pipe(
      __map(_r => _r.body as BaseUserModelListResponseObject)
    );
  }

  /**
   * Add right map users
   * @param params The `IdmRightService.AddRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapUserResponse(params: IdmRightService.AddRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add right map users
   * @param params The `IdmRightService.AddRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapUser(params: IdmRightService.AddRightMapUserParams): __Observable<Response> {
    return this.addRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete right map users
   * @param params The `IdmRightService.DeleteRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapUserResponse(params: IdmRightService.DeleteRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete right map users
   * @param params The `IdmRightService.DeleteRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapUser(params: IdmRightService.DeleteRightMapUserParams): __Observable<Response> {
    return this.deleteRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Get roles map right
   * @param params The `IdmRightService.GetRoleMapRightParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRoleMapRightResponse(params: IdmRightService.GetRoleMapRightParams): __Observable<__StrictHttpResponse<BaseRoleModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseRoleModelListResponseObject>;
      })
    );
  }
  /**
   * Get roles map right
   * @param params The `IdmRightService.GetRoleMapRightParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRoleMapRight(params: IdmRightService.GetRoleMapRightParams): __Observable<BaseRoleModelListResponseObject> {
    return this.getRoleMapRightResponse(params).pipe(
      __map(_r => _r.body as BaseRoleModelListResponseObject)
    );
  }

  /**
   * Add rights map role
   * @param params The `IdmRightService.AddRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapRoleResponse(params: IdmRightService.AddRightMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add rights map role
   * @param params The `IdmRightService.AddRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapRole(params: IdmRightService.AddRightMapRoleParams): __Observable<Response> {
    return this.addRightMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete right map roles
   * @param params The `IdmRightService.DeleteRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapRoleResponse(params: IdmRightService.DeleteRightMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete right map roles
   * @param params The `IdmRightService.DeleteRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapRole(params: IdmRightService.DeleteRightMapRoleParams): __Observable<Response> {
    return this.deleteRightMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Create
   * @param params The `IdmRightService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  createResponse(params: IdmRightService.CreateParams): __Observable<__StrictHttpResponse<RightModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/rights`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RightModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param params The `IdmRightService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  create(params: IdmRightService.CreateParams): __Observable<RightModelResponseObject> {
    return this.createResponse(params).pipe(
      __map(_r => _r.body as RightModelResponseObject)
    );
  }

  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRangeResponse(listId?: Array<string>): __Observable<__StrictHttpResponse<ResponseDeleteMulti>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (listId || []).forEach(val => {if (val != null) __params = __params.append('listId', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/rights`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDeleteMulti>;
      })
    );
  }
  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRange(listId?: Array<string>): __Observable<ResponseDeleteMulti> {
    return this.deleteRangeResponse(listId).pipe(
      __map(_r => _r.body as ResponseDeleteMulti)
    );
  }

  /**
   * Get filter and pagination
   * @param params The `IdmRightService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: IdmRightService.GetFilterParams): __Observable<__StrictHttpResponse<RightModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RightModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `IdmRightService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: IdmRightService.GetFilterParams): __Observable<RightModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as RightModelResponsePagination)
    );
  }

  /**
   * Update
   * @param params The `IdmRightService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  updateResponse(params: IdmRightService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `IdmRightService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  update(params: IdmRightService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }

  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getByIdResponse(id: string): __Observable<__StrictHttpResponse<RightModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RightModelResponseObject>;
      })
    );
  }
  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getById(id: string): __Observable<RightModelResponseObject> {
    return this.getByIdResponse(id).pipe(
      __map(_r => _r.body as RightModelResponseObject)
    );
  }

  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAllResponse(filter?: string): __Observable<__StrictHttpResponse<RightModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/rights/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RightModelListResponseObject>;
      })
    );
  }
  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAll(filter?: string): __Observable<RightModelListResponseObject> {
    return this.getAllResponse(filter).pipe(
      __map(_r => _r.body as RightModelListResponseObject)
    );
  }
}

module IdmRightService {

  /**
   * Parameters for GetDetail
   */
  export interface GetDetailParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for GetUserMapRight
   */
  export interface GetUserMapRightParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddRightMapUser
   */
  export interface AddRightMapUserParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteRightMapUser
   */
  export interface DeleteRightMapUserParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for GetRoleMapRight
   */
  export interface GetRoleMapRightParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddRightMapRole
   */
  export interface AddRightMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteRightMapRole
   */
  export interface DeleteRightMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for Create
   */
  export interface CreateParams {
    body?: RightCreateModel;
    applicationId?: string;
  }

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: RightUpdateModel;
    applicationId?: string;
  }
}

export { IdmRightService }
