/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RoleDetailModelListResponseObject } from '../models/role-detail-model-list-response-object';
import { BaseUserModelListResponseObject } from '../models/base-user-model-list-response-object';
import { Response } from '../models/response';
import { BaseRightModelListResponseObject } from '../models/base-right-model-list-response-object';
import { RoleModelResponseObject } from '../models/role-model-response-object';
import { RoleCreateModel } from '../models/role-create-model';
import { ResponseDeleteMulti } from '../models/response-delete-multi';
import { RoleModelResponsePagination } from '../models/role-model-response-pagination';
import { ResponseUpdate } from '../models/response-update';
import { RoleUpdateModel } from '../models/role-update-model';
import { ResponseDelete } from '../models/response-delete';
import { RoleModelListResponseObject } from '../models/role-model-list-response-object';
@Injectable({
  providedIn: 'root',
})
class IdmRoleService extends __BaseService {
  static readonly getDetailPath = '/api/v1/idm/roles/{id}/detail';
  static readonly getUserMapRolePath = '/api/v1/idm/roles/{id}/user';
  static readonly addUserMapRolePath = '/api/v1/idm/roles/{id}/user';
  static readonly deleteUserMapRolePath = '/api/v1/idm/roles/{id}/user';
  static readonly getRightMapRolePath = '/api/v1/idm/roles/{id}/right';
  static readonly addRightMapRolePath = '/api/v1/idm/roles/{id}/right';
  static readonly deleteRightMapRolePath = '/api/v1/idm/roles/{id}/right';
  static readonly createPath = '/api/v1/idm/roles';
  static readonly deleteRangePath = '/api/v1/idm/roles';
  static readonly getFilterPath = '/api/v1/idm/roles';
  static readonly updatePath = '/api/v1/idm/roles/{id}';
  static readonly deletePath = '/api/v1/idm/roles/{id}';
  static readonly getByIdPath = '/api/v1/idm/roles/{id}';
  static readonly getAllPath = '/api/v1/idm/roles/all';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get by id with detail
   * @param params The `IdmRoleService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetailResponse(params: IdmRoleService.GetDetailParams): __Observable<__StrictHttpResponse<RoleDetailModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/detail`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RoleDetailModelListResponseObject>;
      })
    );
  }
  /**
   * Get by id with detail
   * @param params The `IdmRoleService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetail(params: IdmRoleService.GetDetailParams): __Observable<RoleDetailModelListResponseObject> {
    return this.getDetailResponse(params).pipe(
      __map(_r => _r.body as RoleDetailModelListResponseObject)
    );
  }

  /**
   * Get users map role
   * @param params The `IdmRoleService.GetUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getUserMapRoleResponse(params: IdmRoleService.GetUserMapRoleParams): __Observable<__StrictHttpResponse<BaseUserModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseUserModelListResponseObject>;
      })
    );
  }
  /**
   * Get users map role
   * @param params The `IdmRoleService.GetUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getUserMapRole(params: IdmRoleService.GetUserMapRoleParams): __Observable<BaseUserModelListResponseObject> {
    return this.getUserMapRoleResponse(params).pipe(
      __map(_r => _r.body as BaseUserModelListResponseObject)
    );
  }

  /**
   * Add users map role
   * @param params The `IdmRoleService.AddUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addUserMapRoleResponse(params: IdmRoleService.AddUserMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add users map role
   * @param params The `IdmRoleService.AddUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addUserMapRole(params: IdmRoleService.AddUserMapRoleParams): __Observable<Response> {
    return this.addUserMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete users map role
   * @param params The `IdmRoleService.DeleteUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteUserMapRoleResponse(params: IdmRoleService.DeleteUserMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/user`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete users map role
   * @param params The `IdmRoleService.DeleteUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteUserMapRole(params: IdmRoleService.DeleteUserMapRoleParams): __Observable<Response> {
    return this.deleteUserMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Get rights map role
   * @param params The `IdmRoleService.GetRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRightMapRoleResponse(params: IdmRoleService.GetRightMapRoleParams): __Observable<__StrictHttpResponse<BaseRightModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseRightModelListResponseObject>;
      })
    );
  }
  /**
   * Get rights map role
   * @param params The `IdmRoleService.GetRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRightMapRole(params: IdmRoleService.GetRightMapRoleParams): __Observable<BaseRightModelListResponseObject> {
    return this.getRightMapRoleResponse(params).pipe(
      __map(_r => _r.body as BaseRightModelListResponseObject)
    );
  }

  /**
   * Add rights map role
   * @param params The `IdmRoleService.AddRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapRoleResponse(params: IdmRoleService.AddRightMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add rights map role
   * @param params The `IdmRoleService.AddRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapRole(params: IdmRoleService.AddRightMapRoleParams): __Observable<Response> {
    return this.addRightMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete rights map role
   * @param params The `IdmRoleService.DeleteRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapRoleResponse(params: IdmRoleService.DeleteRightMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete rights map role
   * @param params The `IdmRoleService.DeleteRightMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapRole(params: IdmRoleService.DeleteRightMapRoleParams): __Observable<Response> {
    return this.deleteRightMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Create
   * @param params The `IdmRoleService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  createResponse(params: IdmRoleService.CreateParams): __Observable<__StrictHttpResponse<RoleModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/roles`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RoleModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param params The `IdmRoleService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  create(params: IdmRoleService.CreateParams): __Observable<RoleModelResponseObject> {
    return this.createResponse(params).pipe(
      __map(_r => _r.body as RoleModelResponseObject)
    );
  }

  /**
   * Delete range
   * @param body List Id records
   * @return Success
   */
  deleteRangeResponse(body?: Array<string>): __Observable<__StrictHttpResponse<ResponseDeleteMulti>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/roles`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDeleteMulti>;
      })
    );
  }
  /**
   * Delete range
   * @param body List Id records
   * @return Success
   */
  deleteRange(body?: Array<string>): __Observable<ResponseDeleteMulti> {
    return this.deleteRangeResponse(body).pipe(
      __map(_r => _r.body as ResponseDeleteMulti)
    );
  }

  /**
   * Get filter and pagination
   * @param params The `IdmRoleService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: IdmRoleService.GetFilterParams): __Observable<__StrictHttpResponse<RoleModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RoleModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `IdmRoleService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: IdmRoleService.GetFilterParams): __Observable<RoleModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as RoleModelResponsePagination)
    );
  }

  /**
   * Update
   * @param params The `IdmRoleService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  updateResponse(params: IdmRoleService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `IdmRoleService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  update(params: IdmRoleService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }

  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getByIdResponse(id: string): __Observable<__StrictHttpResponse<RoleModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RoleModelResponseObject>;
      })
    );
  }
  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getById(id: string): __Observable<RoleModelResponseObject> {
    return this.getByIdResponse(id).pipe(
      __map(_r => _r.body as RoleModelResponseObject)
    );
  }

  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAllResponse(filter?: string): __Observable<__StrictHttpResponse<RoleModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/roles/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RoleModelListResponseObject>;
      })
    );
  }
  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAll(filter?: string): __Observable<RoleModelListResponseObject> {
    return this.getAllResponse(filter).pipe(
      __map(_r => _r.body as RoleModelListResponseObject)
    );
  }
}

module IdmRoleService {

  /**
   * Parameters for GetDetail
   */
  export interface GetDetailParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for GetUserMapRole
   */
  export interface GetUserMapRoleParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddUserMapRole
   */
  export interface AddUserMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteUserMapRole
   */
  export interface DeleteUserMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for GetRightMapRole
   */
  export interface GetRightMapRoleParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddRightMapRole
   */
  export interface AddRightMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteRightMapRole
   */
  export interface DeleteRightMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for Create
   */
  export interface CreateParams {
    body?: RoleCreateModel;
    applicationId?: string;
  }

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: RoleUpdateModel;
    applicationId?: string;
  }
}

export { IdmRoleService }
