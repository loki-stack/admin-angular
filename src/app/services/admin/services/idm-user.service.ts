/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { UserDetailModelListResponseObject } from '../models/user-detail-model-list-response-object';
import { BaseRoleModelListResponseObject } from '../models/base-role-model-list-response-object';
import { Response } from '../models/response';
import { BaseRightModelListResponseObject } from '../models/base-right-model-list-response-object';
import { BooleanResponseObject } from '../models/boolean-response-object';
import { UpdateSystemRightRoleMapAppsRequest } from '../models/update-system-right-role-map-apps-request';
import { UserModelResponseObject } from '../models/user-model-response-object';
import { UserCreateModel } from '../models/user-create-model';
import { ResponseDeleteMulti } from '../models/response-delete-multi';
import { UserModelResponsePagination } from '../models/user-model-response-pagination';
import { ResponseUpdate } from '../models/response-update';
import { UserUpdateModel } from '../models/user-update-model';
import { ResponseDelete } from '../models/response-delete';
import { UserModelListResponseObject } from '../models/user-model-list-response-object';
@Injectable({
  providedIn: 'root',
})
class IdmUserService extends __BaseService {
  static readonly checkNameAvailabilityPath = '/api/v1/idm/users/check_name/{name}';
  static readonly getDetailPath = '/api/v1/idm/users/{id}/detail';
  static readonly getRoleMapUserPath = '/api/v1/idm/users/{id}/role';
  static readonly addUserMapRolePath = '/api/v1/idm/users/{id}/role';
  static readonly deleteUserMapRolePath = '/api/v1/idm/users/{id}/role';
  static readonly getRightMapUserPath = '/api/v1/idm/users/{id}/right';
  static readonly addRightMapUserPath = '/api/v1/idm/users/{id}/right';
  static readonly deleteRightMapUserPath = '/api/v1/idm/users/{id}/right';
  static readonly enableRightMapUserPath = '/api/v1/idm/users/{id}/right/enable/{rightId}';
  static readonly disableRightMapUserPath = '/api/v1/idm/users/{id}/right/disable/{rightId}';
  static readonly checkRightMapUserPath = '/api/v1/idm/users/{id}/right/check';
  static readonly updateSystemRightRoleMapAppsPath = '/api/v1/idm/users/{id}/right_role/system';
  static readonly createPath = '/api/v1/idm/users';
  static readonly deleteRangePath = '/api/v1/idm/users';
  static readonly getFilterPath = '/api/v1/idm/users';
  static readonly changePasswordPath = '/api/v1/idm/users/{id}/change_password';
  static readonly updatePath = '/api/v1/idm/users/{id}';
  static readonly deletePath = '/api/v1/idm/users/{id}';
  static readonly getByIdPath = '/api/v1/idm/users/{id}';
  static readonly getAllPath = '/api/v1/idm/users/all';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Check name valid
   * @param params The `IdmUserService.CheckNameAvailabilityParams` containing the following parameters:
   *
   * - `name`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  checkNameAvailabilityResponse(params: IdmUserService.CheckNameAvailabilityParams): __Observable<__StrictHttpResponse<UserDetailModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/check_name/${encodeURIComponent(params.name)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserDetailModelListResponseObject>;
      })
    );
  }
  /**
   * Check name valid
   * @param params The `IdmUserService.CheckNameAvailabilityParams` containing the following parameters:
   *
   * - `name`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  checkNameAvailability(params: IdmUserService.CheckNameAvailabilityParams): __Observable<UserDetailModelListResponseObject> {
    return this.checkNameAvailabilityResponse(params).pipe(
      __map(_r => _r.body as UserDetailModelListResponseObject)
    );
  }

  /**
   * Get by id with detail
   * @param params The `IdmUserService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetailResponse(params: IdmUserService.GetDetailParams): __Observable<__StrictHttpResponse<UserDetailModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/detail`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserDetailModelListResponseObject>;
      })
    );
  }
  /**
   * Get by id with detail
   * @param params The `IdmUserService.GetDetailParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getDetail(params: IdmUserService.GetDetailParams): __Observable<UserDetailModelListResponseObject> {
    return this.getDetailResponse(params).pipe(
      __map(_r => _r.body as UserDetailModelListResponseObject)
    );
  }

  /**
   * Get roles map user
   * @param params The `IdmUserService.GetRoleMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRoleMapUserResponse(params: IdmUserService.GetRoleMapUserParams): __Observable<__StrictHttpResponse<BaseRoleModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseRoleModelListResponseObject>;
      })
    );
  }
  /**
   * Get roles map user
   * @param params The `IdmUserService.GetRoleMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRoleMapUser(params: IdmUserService.GetRoleMapUserParams): __Observable<BaseRoleModelListResponseObject> {
    return this.getRoleMapUserResponse(params).pipe(
      __map(_r => _r.body as BaseRoleModelListResponseObject)
    );
  }

  /**
   * Add users map role
   * @param params The `IdmUserService.AddUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addUserMapRoleResponse(params: IdmUserService.AddUserMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add users map role
   * @param params The `IdmUserService.AddUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addUserMapRole(params: IdmUserService.AddUserMapRoleParams): __Observable<Response> {
    return this.addUserMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete user map role
   * @param params The `IdmUserService.DeleteUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteUserMapRoleResponse(params: IdmUserService.DeleteUserMapRoleParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/role`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete user map role
   * @param params The `IdmUserService.DeleteUserMapRoleParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteUserMapRole(params: IdmUserService.DeleteUserMapRoleParams): __Observable<Response> {
    return this.deleteUserMapRoleResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Get rights map user
   * @param params The `IdmUserService.GetRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRightMapUserResponse(params: IdmUserService.GetRightMapUserParams): __Observable<__StrictHttpResponse<BaseRightModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BaseRightModelListResponseObject>;
      })
    );
  }
  /**
   * Get rights map user
   * @param params The `IdmUserService.GetRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `applicationId`:
   *
   * @return Success
   */
  getRightMapUser(params: IdmUserService.GetRightMapUserParams): __Observable<BaseRightModelListResponseObject> {
    return this.getRightMapUserResponse(params).pipe(
      __map(_r => _r.body as BaseRightModelListResponseObject)
    );
  }

  /**
   * Add right map user
   * @param params The `IdmUserService.AddRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapUserResponse(params: IdmUserService.AddRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Add right map user
   * @param params The `IdmUserService.AddRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  addRightMapUser(params: IdmUserService.AddRightMapUserParams): __Observable<Response> {
    return this.addRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Delete right map user
   * @param params The `IdmUserService.DeleteRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapUserResponse(params: IdmUserService.DeleteRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Delete right map user
   * @param params The `IdmUserService.DeleteRightMapUserParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  deleteRightMapUser(params: IdmUserService.DeleteRightMapUserParams): __Observable<Response> {
    return this.deleteRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Enable right map user
   * @param params The `IdmUserService.EnableRightMapUserParams` containing the following parameters:
   *
   * - `rightId`:
   *
   * - `id`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  enableRightMapUserResponse(params: IdmUserService.EnableRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right/enable/${encodeURIComponent(params.rightId)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Enable right map user
   * @param params The `IdmUserService.EnableRightMapUserParams` containing the following parameters:
   *
   * - `rightId`:
   *
   * - `id`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  enableRightMapUser(params: IdmUserService.EnableRightMapUserParams): __Observable<Response> {
    return this.enableRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Disable right of user
   * @param params The `IdmUserService.DisableRightMapUserParams` containing the following parameters:
   *
   * - `rightId`:
   *
   * - `id`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  disableRightMapUserResponse(params: IdmUserService.DisableRightMapUserParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right/disable/${encodeURIComponent(params.rightId)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Disable right of user
   * @param params The `IdmUserService.DisableRightMapUserParams` containing the following parameters:
   *
   * - `rightId`:
   *
   * - `id`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  disableRightMapUser(params: IdmUserService.DisableRightMapUserParams): __Observable<Response> {
    return this.disableRightMapUserResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Check right map user
   * @param params The `IdmUserService.CheckRightMapUserParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `rightId`:
   *
   * @return Success
   */
  checkRightMapUserResponse(params: IdmUserService.CheckRightMapUserParams): __Observable<__StrictHttpResponse<BooleanResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.rightId != null) __params = __params.set('rightId', params.rightId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right/check`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BooleanResponseObject>;
      })
    );
  }
  /**
   * Check right map user
   * @param params The `IdmUserService.CheckRightMapUserParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `rightId`:
   *
   * @return Success
   */
  checkRightMapUser(params: IdmUserService.CheckRightMapUserParams): __Observable<BooleanResponseObject> {
    return this.checkRightMapUserResponse(params).pipe(
      __map(_r => _r.body as BooleanResponseObject)
    );
  }

  /**
   * Update system right, role map user in list application
   * @param params The `IdmUserService.UpdateSystemRightRoleMapAppsParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   *
   * @return Success
   */
  updateSystemRightRoleMapAppsResponse(params: IdmUserService.UpdateSystemRightRoleMapAppsParams): __Observable<__StrictHttpResponse<Response>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/right_role/system`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Response>;
      })
    );
  }
  /**
   * Update system right, role map user in list application
   * @param params The `IdmUserService.UpdateSystemRightRoleMapAppsParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   *
   * @return Success
   */
  updateSystemRightRoleMapApps(params: IdmUserService.UpdateSystemRightRoleMapAppsParams): __Observable<Response> {
    return this.updateSystemRightRoleMapAppsResponse(params).pipe(
      __map(_r => _r.body as Response)
    );
  }

  /**
   * Create
   * @param params The `IdmUserService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  createResponse(params: IdmUserService.CreateParams): __Observable<__StrictHttpResponse<UserModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/idm/users`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param params The `IdmUserService.CreateParams` containing the following parameters:
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  create(params: IdmUserService.CreateParams): __Observable<UserModelResponseObject> {
    return this.createResponse(params).pipe(
      __map(_r => _r.body as UserModelResponseObject)
    );
  }

  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRangeResponse(listId?: Array<string>): __Observable<__StrictHttpResponse<ResponseDeleteMulti>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (listId || []).forEach(val => {if (val != null) __params = __params.append('listId', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDeleteMulti>;
      })
    );
  }
  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRange(listId?: Array<string>): __Observable<ResponseDeleteMulti> {
    return this.deleteRangeResponse(listId).pipe(
      __map(_r => _r.body as ResponseDeleteMulti)
    );
  }

  /**
   * Get filter and pagination
   * @param params The `IdmUserService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: IdmUserService.GetFilterParams): __Observable<__StrictHttpResponse<UserModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `IdmUserService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: IdmUserService.GetFilterParams): __Observable<UserModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as UserModelResponsePagination)
    );
  }

  /**
   * Update password
   * @param params The `IdmUserService.ChangePasswordParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `password`:
   *
   * - `oldPassword`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  changePasswordResponse(params: IdmUserService.ChangePasswordParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.password != null) __params = __params.set('password', params.password.toString());
    if (params.oldPassword != null) __params = __params.set('oldPassword', params.oldPassword.toString());
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}/change_password`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update password
   * @param params The `IdmUserService.ChangePasswordParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `password`:
   *
   * - `oldPassword`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  changePassword(params: IdmUserService.ChangePasswordParams): __Observable<ResponseUpdate> {
    return this.changePasswordResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Update
   * @param params The `IdmUserService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  updateResponse(params: IdmUserService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    if (params.applicationId != null) __params = __params.set('applicationId', params.applicationId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `IdmUserService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * - `applicationId`:
   *
   * @return Success
   */
  update(params: IdmUserService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }

  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getByIdResponse(id: string): __Observable<__StrictHttpResponse<UserModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserModelResponseObject>;
      })
    );
  }
  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getById(id: string): __Observable<UserModelResponseObject> {
    return this.getByIdResponse(id).pipe(
      __map(_r => _r.body as UserModelResponseObject)
    );
  }

  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAllResponse(filter?: string): __Observable<__StrictHttpResponse<UserModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/idm/users/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UserModelListResponseObject>;
      })
    );
  }
  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAll(filter?: string): __Observable<UserModelListResponseObject> {
    return this.getAllResponse(filter).pipe(
      __map(_r => _r.body as UserModelListResponseObject)
    );
  }
}

module IdmUserService {

  /**
   * Parameters for CheckNameAvailability
   */
  export interface CheckNameAvailabilityParams {
    name: string;
    applicationId?: string;
  }

  /**
   * Parameters for GetDetail
   */
  export interface GetDetailParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for GetRoleMapUser
   */
  export interface GetRoleMapUserParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddUserMapRole
   */
  export interface AddUserMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteUserMapRole
   */
  export interface DeleteUserMapRoleParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for GetRightMapUser
   */
  export interface GetRightMapUserParams {

    /**
     * Id record
     */
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for AddRightMapUser
   */
  export interface AddRightMapUserParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for DeleteRightMapUser
   */
  export interface DeleteRightMapUserParams {

    /**
     * Id record
     */
    id: string;
    body?: Array<string>;
    applicationId?: string;
  }

  /**
   * Parameters for EnableRightMapUser
   */
  export interface EnableRightMapUserParams {
    rightId: string;
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for DisableRightMapUser
   */
  export interface DisableRightMapUserParams {
    rightId: string;
    id: string;
    applicationId?: string;
  }

  /**
   * Parameters for CheckRightMapUser
   */
  export interface CheckRightMapUserParams {
    id: string;
    rightId?: string;
  }

  /**
   * Parameters for UpdateSystemRightRoleMapApps
   */
  export interface UpdateSystemRightRoleMapAppsParams {
    id: string;
    body?: UpdateSystemRightRoleMapAppsRequest;
  }

  /**
   * Parameters for Create
   */
  export interface CreateParams {
    body?: UserCreateModel;
    applicationId?: string;
  }

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for ChangePassword
   */
  export interface ChangePasswordParams {

    /**
     * Id record
     */
    id: string;
    password?: string;
    oldPassword?: string;
    applicationId?: string;
  }

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: UserUpdateModel;
    applicationId?: string;
  }
}

export { IdmUserService }
