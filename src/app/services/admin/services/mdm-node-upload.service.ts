/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { FileModelResponsePagination } from '../models/file-model-response-pagination';
import { FileUploadResultResponseObject } from '../models/file-upload-result-response-object';
import { Base64FileData } from '../models/base-64file-data';
import { FileUploadResultListResponseObject } from '../models/file-upload-result-list-response-object';
import { StringStringValuesKeyValuePair } from '../models/string-string-values-key-value-pair';
@Injectable({
  providedIn: 'root',
})
class MdmNodeUploadService extends __BaseService {
  static readonly getFilterPath = '/api/v1/core/nodes';
  static readonly uploadFileBase64PhysicalPath = '/api/v1/core/nodes/upload/physical/base64';
  static readonly uploadFileBase64ManyPhysicalPath = '/api/v1/core/nodes/upload/physical/base64/many';
  static readonly uploadFileBlobPhysicalPath = '/api/v1/core/nodes/upload/physical/blob';
  static readonly uploadFileBlobManyPhysicalPath = '/api/v1/core/nodes/upload/physical/blob/many';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get filter and pagination
   * @param params The `MdmNodeUploadService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: MdmNodeUploadService.GetFilterParams): __Observable<__StrictHttpResponse<FileModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/core/nodes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FileModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `MdmNodeUploadService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: MdmNodeUploadService.GetFilterParams): __Observable<FileModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as FileModelResponsePagination)
    );
  }

  /**
   * Upload Base64
   * @param params The `MdmNodeUploadService.UploadFileBase64PhysicalParams` containing the following parameters:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * - `body`:
   *
   * @return Success
   */
  uploadFileBase64PhysicalResponse(params: MdmNodeUploadService.UploadFileBase64PhysicalParams): __Observable<__StrictHttpResponse<FileUploadResultResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.destinationPhysicalPath != null) __params = __params.set('destinationPhysicalPath', params.destinationPhysicalPath.toString());
    __body = params.body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/core/nodes/upload/physical/base64`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FileUploadResultResponseObject>;
      })
    );
  }
  /**
   * Upload Base64
   * @param params The `MdmNodeUploadService.UploadFileBase64PhysicalParams` containing the following parameters:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * - `body`:
   *
   * @return Success
   */
  uploadFileBase64Physical(params: MdmNodeUploadService.UploadFileBase64PhysicalParams): __Observable<FileUploadResultResponseObject> {
    return this.uploadFileBase64PhysicalResponse(params).pipe(
      __map(_r => _r.body as FileUploadResultResponseObject)
    );
  }

  /**
   * Upload many file base 64
   * @param params The `MdmNodeUploadService.UploadFileBase64ManyPhysicalParams` containing the following parameters:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * - `body`:
   *
   * @return Success
   */
  uploadFileBase64ManyPhysicalResponse(params: MdmNodeUploadService.UploadFileBase64ManyPhysicalParams): __Observable<__StrictHttpResponse<FileUploadResultListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.destinationPhysicalPath != null) __params = __params.set('destinationPhysicalPath', params.destinationPhysicalPath.toString());
    __body = params.body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/core/nodes/upload/physical/base64/many`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FileUploadResultListResponseObject>;
      })
    );
  }
  /**
   * Upload many file base 64
   * @param params The `MdmNodeUploadService.UploadFileBase64ManyPhysicalParams` containing the following parameters:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * - `body`:
   *
   * @return Success
   */
  uploadFileBase64ManyPhysical(params: MdmNodeUploadService.UploadFileBase64ManyPhysicalParams): __Observable<FileUploadResultListResponseObject> {
    return this.uploadFileBase64ManyPhysicalResponse(params).pipe(
      __map(_r => _r.body as FileUploadResultListResponseObject)
    );
  }

  /**
   * Upload a file
   * @param params The `MdmNodeUploadService.UploadFileBlobPhysicalParams` containing the following parameters:
   *
   * - `width`:
   *
   * - `isResize`:
   *
   * - `height`:
   *
   * - `file`:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * @return Success
   */
  uploadFileBlobPhysicalResponse(params: MdmNodeUploadService.UploadFileBlobPhysicalParams): __Observable<__StrictHttpResponse<FileUploadResultResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.width != null) __params = __params.set('width', params.width.toString());
    if (params.isResize != null) __params = __params.set('isResize', params.isResize.toString());
    if (params.height != null) __params = __params.set('height', params.height.toString());
    if (params.file != null) { __formData.append('file', params.file as string | Blob);}
    if (params.destinationPhysicalPath != null) __params = __params.set('destinationPhysicalPath', params.destinationPhysicalPath.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/core/nodes/upload/physical/blob`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FileUploadResultResponseObject>;
      })
    );
  }
  /**
   * Upload a file
   * @param params The `MdmNodeUploadService.UploadFileBlobPhysicalParams` containing the following parameters:
   *
   * - `width`:
   *
   * - `isResize`:
   *
   * - `height`:
   *
   * - `file`:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * @return Success
   */
  uploadFileBlobPhysical(params: MdmNodeUploadService.UploadFileBlobPhysicalParams): __Observable<FileUploadResultResponseObject> {
    return this.uploadFileBlobPhysicalResponse(params).pipe(
      __map(_r => _r.body as FileUploadResultResponseObject)
    );
  }

  /**
   * Upload many file
   * @param params The `MdmNodeUploadService.UploadFileBlobManyPhysicalParams` containing the following parameters:
   *
   * - `formData`:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * @return Success
   */
  uploadFileBlobManyPhysicalResponse(params: MdmNodeUploadService.UploadFileBlobManyPhysicalParams): __Observable<__StrictHttpResponse<FileUploadResultListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    (params.formData || []).forEach(val => {if (val != null) __formData.append('formData', JSON.stringify(val))});
    if (params.destinationPhysicalPath != null) __params = __params.set('destinationPhysicalPath', params.destinationPhysicalPath.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/core/nodes/upload/physical/blob/many`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<FileUploadResultListResponseObject>;
      })
    );
  }
  /**
   * Upload many file
   * @param params The `MdmNodeUploadService.UploadFileBlobManyPhysicalParams` containing the following parameters:
   *
   * - `formData`:
   *
   * - `destinationPhysicalPath`: Destination path (optional)
   *
   * @return Success
   */
  uploadFileBlobManyPhysical(params: MdmNodeUploadService.UploadFileBlobManyPhysicalParams): __Observable<FileUploadResultListResponseObject> {
    return this.uploadFileBlobManyPhysicalResponse(params).pipe(
      __map(_r => _r.body as FileUploadResultListResponseObject)
    );
  }
}

module MdmNodeUploadService {

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for UploadFileBase64Physical
   */
  export interface UploadFileBase64PhysicalParams {

    /**
     * Destination path (optional)
     */
    destinationPhysicalPath?: string;
    body?: Base64FileData;
  }

  /**
   * Parameters for UploadFileBase64ManyPhysical
   */
  export interface UploadFileBase64ManyPhysicalParams {

    /**
     * Destination path (optional)
     */
    destinationPhysicalPath?: string;
    body?: Array<Base64FileData>;
  }

  /**
   * Parameters for UploadFileBlobPhysical
   */
  export interface UploadFileBlobPhysicalParams {
    width?: number;
    isResize?: boolean;
    height?: number;
    file?: Blob;

    /**
     * Destination path (optional)
     */
    destinationPhysicalPath?: string;
  }

  /**
   * Parameters for UploadFileBlobManyPhysical
   */
  export interface UploadFileBlobManyPhysicalParams {
    formData?: Array<StringStringValuesKeyValuePair>;

    /**
     * Destination path (optional)
     */
    destinationPhysicalPath?: string;
  }
}

export { MdmNodeUploadService }
