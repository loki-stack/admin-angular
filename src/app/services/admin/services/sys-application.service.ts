/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ResponseUpdate } from '../models/response-update';
import { ApplicationModelResponseObject } from '../models/application-model-response-object';
import { ApplicationCreateModel } from '../models/application-create-model';
import { ResponseDeleteMulti } from '../models/response-delete-multi';
import { ApplicationModelResponsePagination } from '../models/application-model-response-pagination';
import { ApplicationUpdateModel } from '../models/application-update-model';
import { ResponseDelete } from '../models/response-delete';
import { ApplicationModelListResponseObject } from '../models/application-model-list-response-object';
@Injectable({
  providedIn: 'root',
})
class SysApplicationService extends __BaseService {
  static readonly bootstrapPath = '/api/v1/sys/applications/bootstrap';
  static readonly createPath = '/api/v1/sys/applications';
  static readonly deleteRangePath = '/api/v1/sys/applications';
  static readonly getFilterPath = '/api/v1/sys/applications';
  static readonly create_1Path = '/api/v1/sys/applications/{id}';
  static readonly updatePath = '/api/v1/sys/applications/{id}';
  static readonly deletePath = '/api/v1/sys/applications/{id}';
  static readonly getByIdPath = '/api/v1/sys/applications/{id}';
  static readonly getAllPath = '/api/v1/sys/applications/all';
  static readonly getAllOwnerPath = '/api/v1/sys/applications/owner';
  static readonly getAllByUserIdPath = '/api/v1/sys/applications/user/{userId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Bootstrap data of application
   * @param overwrite overwrite data
   * @return Success
   */
  bootstrapResponse(overwrite?: boolean): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (overwrite != null) __params = __params.set('overwrite', overwrite.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/sys/applications/bootstrap`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Bootstrap data of application
   * @param overwrite overwrite data
   * @return Success
   */
  bootstrap(overwrite?: boolean): __Observable<ResponseUpdate> {
    return this.bootstrapResponse(overwrite).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Create
   * @param body
   * @return Success
   */
  createResponse(body?: ApplicationCreateModel): __Observable<__StrictHttpResponse<ApplicationModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/sys/applications`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param body
   * @return Success
   */
  create(body?: ApplicationCreateModel): __Observable<ApplicationModelResponseObject> {
    return this.createResponse(body).pipe(
      __map(_r => _r.body as ApplicationModelResponseObject)
    );
  }

  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRangeResponse(listId?: Array<string>): __Observable<__StrictHttpResponse<ResponseDeleteMulti>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (listId || []).forEach(val => {if (val != null) __params = __params.append('listId', val.toString())});
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/sys/applications`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDeleteMulti>;
      })
    );
  }
  /**
   * Delete range
   * @param listId List Id records
   * @return Success
   */
  deleteRange(listId?: Array<string>): __Observable<ResponseDeleteMulti> {
    return this.deleteRangeResponse(listId).pipe(
      __map(_r => _r.body as ResponseDeleteMulti)
    );
  }

  /**
   * Get filter and pagination
   * @param params The `SysApplicationService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilterResponse(params: SysApplicationService.GetFilterParams): __Observable<__StrictHttpResponse<ApplicationModelResponsePagination>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.sort != null) __params = __params.set('sort', params.sort.toString());
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    if (params.filter != null) __params = __params.set('filter', params.filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/sys/applications`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelResponsePagination>;
      })
    );
  }
  /**
   * Get filter and pagination
   * @param params The `SysApplicationService.GetFilterParams` containing the following parameters:
   *
   * - `sort`: Sort condition. Ex: "+id"
   *
   * - `size`: Number records in one page
   *
   * - `page`: Page number
   *
   * - `filter`: Filter Json Object. Ex: "{}"
   *
   * @return Success
   */
  getFilter(params: SysApplicationService.GetFilterParams): __Observable<ApplicationModelResponsePagination> {
    return this.getFilterResponse(params).pipe(
      __map(_r => _r.body as ApplicationModelResponsePagination)
    );
  }

  /**
   * Create
   * @param params The `SysApplicationService.Create_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   *
   * @return Success
   */
  create_1Response(params: SysApplicationService.Create_1Params): __Observable<__StrictHttpResponse<ApplicationModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/sys/applications/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelResponseObject>;
      })
    );
  }
  /**
   * Create
   * @param params The `SysApplicationService.Create_1Params` containing the following parameters:
   *
   * - `id`:
   *
   * - `body`:
   *
   * @return Success
   */
  create_1(params: SysApplicationService.Create_1Params): __Observable<ApplicationModelResponseObject> {
    return this.create_1Response(params).pipe(
      __map(_r => _r.body as ApplicationModelResponseObject)
    );
  }

  /**
   * Update
   * @param params The `SysApplicationService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  updateResponse(params: SysApplicationService.UpdateParams): __Observable<__StrictHttpResponse<ResponseUpdate>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/sys/applications/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseUpdate>;
      })
    );
  }
  /**
   * Update
   * @param params The `SysApplicationService.UpdateParams` containing the following parameters:
   *
   * - `id`: Id record
   *
   * - `body`:
   *
   * @return Success
   */
  update(params: SysApplicationService.UpdateParams): __Observable<ResponseUpdate> {
    return this.updateResponse(params).pipe(
      __map(_r => _r.body as ResponseUpdate)
    );
  }

  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  deleteResponse(id: string): __Observable<__StrictHttpResponse<ResponseDelete>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/sys/applications/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ResponseDelete>;
      })
    );
  }
  /**
   * Delete
   * @param id Id record
   * @return Success
   */
  delete(id: string): __Observable<ResponseDelete> {
    return this.deleteResponse(id).pipe(
      __map(_r => _r.body as ResponseDelete)
    );
  }

  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getByIdResponse(id: string): __Observable<__StrictHttpResponse<ApplicationModelResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/sys/applications/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelResponseObject>;
      })
    );
  }
  /**
   * Get by Id
   * @param id Id record
   * @return Success
   */
  getById(id: string): __Observable<ApplicationModelResponseObject> {
    return this.getByIdResponse(id).pipe(
      __map(_r => _r.body as ApplicationModelResponseObject)
    );
  }

  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAllResponse(filter?: string): __Observable<__StrictHttpResponse<ApplicationModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/sys/applications/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelListResponseObject>;
      })
    );
  }
  /**
   * Get all
   * @param filter undefined
   * @return Success
   */
  getAll(filter?: string): __Observable<ApplicationModelListResponseObject> {
    return this.getAllResponse(filter).pipe(
      __map(_r => _r.body as ApplicationModelListResponseObject)
    );
  }

  /**
   * Get all owner
   * @return Success
   */
  getAllOwnerResponse(): __Observable<__StrictHttpResponse<ApplicationModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/sys/applications/owner`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelListResponseObject>;
      })
    );
  }
  /**
   * Get all owner
   * @return Success
   */
  getAllOwner(): __Observable<ApplicationModelListResponseObject> {
    return this.getAllOwnerResponse().pipe(
      __map(_r => _r.body as ApplicationModelListResponseObject)
    );
  }

  /**
   * Get all by userId
   * @param userId Id user
   * @return Success
   */
  getAllByUserIdResponse(userId: string): __Observable<__StrictHttpResponse<ApplicationModelListResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/sys/applications/user/${encodeURIComponent(userId)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApplicationModelListResponseObject>;
      })
    );
  }
  /**
   * Get all by userId
   * @param userId Id user
   * @return Success
   */
  getAllByUserId(userId: string): __Observable<ApplicationModelListResponseObject> {
    return this.getAllByUserIdResponse(userId).pipe(
      __map(_r => _r.body as ApplicationModelListResponseObject)
    );
  }
}

module SysApplicationService {

  /**
   * Parameters for GetFilter
   */
  export interface GetFilterParams {

    /**
     * Sort condition. Ex: "+id"
     */
    sort?: string;

    /**
     * Number records in one page
     */
    size?: number;

    /**
     * Page number
     */
    page?: number;

    /**
     * Filter Json Object. Ex: "{}"
     */
    filter?: string;
  }

  /**
   * Parameters for Create_1
   */
  export interface Create_1Params {
    id: string;
    body?: ApplicationCreateModel;
  }

  /**
   * Parameters for Update
   */
  export interface UpdateParams {

    /**
     * Id record
     */
    id: string;
    body?: ApplicationUpdateModel;
  }
}

export { SysApplicationService }
