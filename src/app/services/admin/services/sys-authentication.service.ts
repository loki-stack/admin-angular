/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { LoginResponseResponseObject } from '../models/login-response-response-object';
import { RegisterModel } from '../models/register-model';
import { LoginModel } from '../models/login-model';
@Injectable({
  providedIn: 'root',
})
class SysAuthenticationService extends __BaseService {
  static readonly registerJwtPath = '/api/v1/authentication/jwt/register';
  static readonly jwtInfoPath = '/api/v1/authentication/jwt/info';
  static readonly signInJwtPath = '/api/v1/authentication/jwt/login';
  static readonly oaFacebookPath = '/api/v1/authentication/fb/oa';
  static readonly oaGooglePath = '/api/v1/authentication/gg/oa';
  static readonly callBackOaPath = '/api/v1/authentication/{type}/callback';
  static readonly signOutOaPath = '/api/v1/authentication/logout';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Register JWT
   * @param body
   * @return Success
   */
  registerJwtResponse(body?: RegisterModel): __Observable<__StrictHttpResponse<LoginResponseResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/authentication/jwt/register`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<LoginResponseResponseObject>;
      })
    );
  }
  /**
   * Register JWT
   * @param body
   * @return Success
   */
  registerJwt(body?: RegisterModel): __Observable<LoginResponseResponseObject> {
    return this.registerJwtResponse(body).pipe(
      __map(_r => _r.body as LoginResponseResponseObject)
    );
  }

  /**
   * Get JWT info
   * @return Success
   */
  jwtInfoResponse(): __Observable<__StrictHttpResponse<LoginResponseResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/authentication/jwt/info`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<LoginResponseResponseObject>;
      })
    );
  }
  /**
   * Get JWT info
   * @return Success
   */
  jwtInfo(): __Observable<LoginResponseResponseObject> {
    return this.jwtInfoResponse().pipe(
      __map(_r => _r.body as LoginResponseResponseObject)
    );
  }

  /**
   * Login
   * @param body
   * @return Success
   */
  signInJwtResponse(body?: LoginModel): __Observable<__StrictHttpResponse<LoginResponseResponseObject>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/authentication/jwt/login`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<LoginResponseResponseObject>;
      })
    );
  }
  /**
   * Login
   * @param body
   * @return Success
   */
  signInJwt(body?: LoginModel): __Observable<LoginResponseResponseObject> {
    return this.signInJwtResponse(body).pipe(
      __map(_r => _r.body as LoginResponseResponseObject)
    );
  }

  /**
   * Sign-In using OA Facebook
   * @param callbackUrl Url frontend callback. Ex: https//yourclient/callback.html
   */
  oaFacebookResponse(callbackUrl?: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (callbackUrl != null) __params = __params.set('callbackUrl', callbackUrl.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/authentication/fb/oa`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Sign-In using OA Facebook
   * @param callbackUrl Url frontend callback. Ex: https//yourclient/callback.html
   */
  oaFacebook(callbackUrl?: string): __Observable<null> {
    return this.oaFacebookResponse(callbackUrl).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Sign-In using OA Google
   * @param callbackUrl Url frontend callback. Ex: https//yourclient/callback.html
   */
  oaGoogleResponse(callbackUrl?: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (callbackUrl != null) __params = __params.set('callbackUrl', callbackUrl.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/authentication/gg/oa`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Sign-In using OA Google
   * @param callbackUrl Url frontend callback. Ex: https//yourclient/callback.html
   */
  oaGoogle(callbackUrl?: string): __Observable<null> {
    return this.oaGoogleResponse(callbackUrl).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Call back API OA
   * @param type undefined
   */
  callBackOaResponse(type: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/authentication/${encodeURIComponent(type)}/callback`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Call back API OA
   * @param type undefined
   */
  callBackOa(type: string): __Observable<null> {
    return this.callBackOaResponse(type).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Logout OA
   * @param callbackUrl undefined
   */
  signOutOaResponse(callbackUrl?: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (callbackUrl != null) __params = __params.set('callbackUrl', callbackUrl.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/authentication/logout`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Logout OA
   * @param callbackUrl undefined
   */
  signOutOa(callbackUrl?: string): __Observable<null> {
    return this.signOutOaResponse(callbackUrl).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module SysAuthenticationService {
}

export { SysAuthenticationService }
