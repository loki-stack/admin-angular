/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { AdminApiConfiguration as __Configuration } from '../admin-api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PushSubscription } from '../models/push-subscription';
@Injectable({
  providedIn: 'root',
})
class SysPushService extends __BaseService {
  static readonly getPath = '/api/v1/core/push';
  static readonly postPath = '/api/v1/core/push';
  static readonly testPushPath = '/api/v1/core/push/test/{message}';
  static readonly deletePath = '/api/v1/core/push/{endpoint}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get public key
   */
  getResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/core/push`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Get public key
   */
  get(): __Observable<null> {
    return this.getResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Add new subscription
   * @param body
   */
  postResponse(body?: PushSubscription): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/core/push`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Add new subscription
   * @param body
   */
  post(body?: PushSubscription): __Observable<null> {
    return this.postResponse(body).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Test send dummy message
   * @param message
   */
  testPushResponse(message: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/core/push/test/${encodeURIComponent(message)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Test send dummy message
   * @param message
   */
  testPush(message: string): __Observable<null> {
    return this.testPushResponse(message).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Delete endpoint
   * @param endpoint
   */
  deleteResponse(endpoint: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/core/push/${encodeURIComponent(endpoint)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Delete endpoint
   * @param endpoint
   */
  delete(endpoint: string): __Observable<null> {
    return this.deleteResponse(endpoint).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module SysPushService {
}

export { SysPushService }
