import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage/storage.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private storageService: StorageService) {}
  private nextAuthHeader: string;
  private nextAuthValue: string;
  private applicationIdHeader: string;
  /** Set to basic authentication */
  applicationId(value: string): void {
    this.applicationIdHeader = value;
  }
  basic(user: string, password: string): void {
    this.nextAuthHeader = 'Authorization';
    this.nextAuthValue = 'Basic ' + btoa(user + ':' + password);
  }

  /** Set to bearer authentication */
  bearer(token: string): void {
    this.nextAuthHeader = 'Authorization';
    this.nextAuthValue = 'Bearer ' + token;
    this.nextAuthValue = token;
  }

  /** Set to session key */
  nextAsSession(sessionKey: string): void {
    this.nextAuthHeader = 'Session';
    this.nextAuthValue = sessionKey;
  }

  /** Clear any authentication headers (to be called after logout) */
  clear(): void {
    this.nextAuthHeader = null;
    this.nextAuthValue = null;
  }

  /** Apply the current authorization headers to the given request */
  apply(req: HttpRequest<any>): HttpRequest<any> {
    const headers = {
      // Authorization: 'Bearer ' + CONFIG.tokenGateWay,
    };
    if (this.nextAuthHeader) {
      // headers[this.nextAuthHeader] = this.nextAuthValue;
      headers['X-Permission'] = this.nextAuthValue;
      headers['X-ApplicationId'] = '00000000-0000-0000-0000-000000000001';
    }
    if (this.applicationIdHeader) {
      headers['X-ApplicationId'] = this.applicationIdHeader;
    }
    // Apply the headers to the request
    return req.clone({
      setHeaders: headers,
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Apply the headers
    req = this.apply(req);
    this.storageService.notifyOther({
      loading: true,
    });
    // Also handle errors globally
    return next.handle(req).pipe(
      tap(
        (x) => {
          this.storageService.notifyOther({
            loading: false,
          });
          return x;
        },
        (_) => {
          this.storageService.notifyOther({
            loading: false,
          });
        },
      ),
    );
  }
}
