import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LoginResponse } from '../admin/models/login-response';
const StorageShim = require('node-storage-shim');
/** Check storage */

function setCookie(name: string, value: string, days: number) {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
}
function getCookie(name: string) {
  const nameEQ = name + '=';
  const ca = document.cookie.split(';');
  // tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}
function eraseCookie(name: string) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function checkLocalStorage(): boolean {
  const test = 'test';
  try {
    localStorage.setItem(test, test);
    localStorage.removeItem(test);
    return true;
  } catch (e) {
    return false;
  }
}
function checkSectionStorage(): boolean {
  const test = 'test';
  try {
    sessionStorage.setItem(test, test);
    sessionStorage.removeItem(test);
    return true;
  } catch (e) {
    return false;
  }
}

export const GET_STORAGE = () => {
  if (!isNotSSR()) {
    return new StorageShim();
  }

  // let preferStorage = "cookie";
  // let preferStorage = "localStorage";
  // let preferStorage = "sessionStorage";
  // tslint:disable-next-line: prefer-const
  let preferStorage = 'localStorage';
  const cookieStorage = {
    removeItem: (key: string) => {
      return eraseCookie(key);
    },
    setItem: (key: string, value: string) => {
      return setCookie(key, value, 100);
    },
    getItem: (key: string) => {
      return getCookie(key);
    },
  };
  switch (preferStorage) {
    case 'localStorage':
      if (checkLocalStorage()) {
        return {
          removeItem: (key: string) => {
            return localStorage.removeItem(key);
          },
          setItem: (key: string, value: string) => {
            return localStorage.setItem(key, value);
          },
          getItem: (key: string) => {
            return localStorage.getItem(key);
          },
        };
      }
      break;
    case 'sessionStorage':
      if (checkSectionStorage()) {
        return {
          removeItem: (key: string) => {
            return sessionStorage.removeItem(key);
          },
          setItem: (key: string, value: string) => {
            return sessionStorage.setItem(key, value);
          },
          getItem: (key: string) => {
            return sessionStorage.getItem(key);
          },
        };
      }
      break;
    default:
      break;
  }
  return cookieStorage;
};
function isNotSSR() {
  return typeof window !== 'undefined';
}
@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private notify = new Subject<any>();
  private storage = {};
  public loginInfo: LoginResponse = {};
  public isLogined = true;
  /*
   * Observable string streams
   */
  notifyObservable$ = this.notify.asObservable();

  constructor() {}
  //#region Storage Common
  /*
   * clear
   */
  public clear() {
    this.storage = {};
  }
  /**
   * setItem
   */
  public setItem(key: string, value: string) {
    if (GET_STORAGE()) {
      GET_STORAGE().setItem(key, value);
    } else {
      this.storage[key] = value;
    }
  }
  /**
   * getItem
   */
  public getItem(key: string) {
    if (GET_STORAGE()) {
      return GET_STORAGE().getItem(key);
    } else {
      return this.storage[key];
    }
  }
  /**
   * removeItem
   */
  public removeItem(key: string) {
    if (GET_STORAGE()) {
      GET_STORAGE().removeItem(key);
    } else {
      this.storage[key] = null;
    }
  }

  /** Get item */
  public getItemJSON(key: string) {
    const value = this.getItem(key);
    if (!value) {
      return null;
    }
    try {
      return JSON.parse(value);
    } catch (error) {
      return null;
    }
  }

  //#endregion
  //#region Storage LoginInfo
  private serializableStorage() {
    this.setItem('loginInfo', JSON.stringify(this.loginInfo));
    this.setItem('isLogined', JSON.stringify(this.isLogined));
  }
  private deserializableStorage() {
    this.loginInfo = JSON.parse(this.getItem('loginInfo'));
    this.isLogined = JSON.parse(this.getItem('isLogined'));
  }
  public Login(value: LoginResponse) {
    this.loginInfo = value;
    this.isLogined = true;
    this.serializableStorage();
  }
  public Logout() {
    this.loginInfo = {};
    this.isLogined = false;
    this.serializableStorage();
  }
  public CheckLogin() {
    this.deserializableStorage();
    return this.isLogined === true;
  }
  public GetLoginInfo() {
    this.deserializableStorage();
    return this.loginInfo;
  }
  //#endregion
  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }
}
