import { Inject, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN } from '@delon/theme';
import { NzModalService } from 'ng-zorro-antd/modal';
import { interval } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UpdateService {
  updateSubscription;
  isIgnoreUpdate = false;
  constructor(public updates: SwUpdate, public modalService: NzModalService, @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService) {}
  public checkForUpdates(): void {
    this.updateSubscription = this.updates.available.subscribe((event) => this.promptUser());
    if (this.updates.isEnabled) {
      // Required to enable updates on Windows and ios.
      this.updates.activateUpdate();

      interval(60 * 5 * 1000).subscribe(() => {
        this.updates.checkForUpdate().then((res) => {
          console.log('checking for updates');
        });
      });
    }

    // Important: on Safari (ios) Heroku doesn't auto redirect links to their https which allows the installation of the pwa like usual
    // but it deactivates the swUpdate. So make sure to open your pwa on safari like so: https://example.com then (install/add to home)
  }

  promptUser(): void {
    if (this.isIgnoreUpdate) {
      console.log('User ignore update');
      return;
    }
    this.updates.activateUpdate().then(() => {
      this.modalService.confirm({
        nzTitle: this.i18n.fanyi('Phát hiện phiên bản mới. Hệ thống sẽ tải lại trang. Bạn có muốn tiếp tục?'),
        nzOkText: this.i18n.fanyi('Tiếp tục'),
        nzCancelText: this.i18n.fanyi('Hủy'),
        nzOkType: 'danger',
        nzOnOk: () => {
          window.location.reload();
        },
        nzOnCancel: () => {
          this.isIgnoreUpdate = true;
        },
      });
    });
  }
}
