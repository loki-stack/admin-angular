import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-loki-control',
  styleUrls: ['./loki-control.component.less'],
  templateUrl: './loki-control.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LokiControlComponent),
      multi: true,
    },
  ],
})
export class LokiControlComponent implements OnInit, ControlValueAccessor, OnChanges {
  @Input() lcType = 'input';
  @Input() lcView = 'control';
  @Input() lcValue: any;
  @Input() lcConfig: any;
  @Input() maxRow: any;
  model: any;

  @ViewChild('control') public control: any;

  ngOnChanges(changes) {}
  constructor(private cdr: ChangeDetectorRef) {}
  ngOnInit() {}
  writeValue(obj: any): void {
    this.model = obj;
    this.cdr.detectChanges();
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {}

  onChange = (_) => {};
  onTouched = () => {};

  onChangeData(_e) {
    this.onChange(this.model);
  }
  getError(control) {
    if (control && control.invalid && (control.dirty || control.touched)) {
      return control.errors.required
        ? 'Trường bắt buộc'
        : control.errors.maxlength
        ? 'Trường vượt quá ký tự'
        : control.errors.minlength
        ? 'Trường ký tự ít'
        : 'Trường không hợp lệ';
    }
    return '';
  }
  // Tag
  getTagItem(item) {
    if (item.enum) {
      const cur = item.enum.find((x) => x.item.lcValue);
    }
    return item;
  }
}
