import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { ChangeDetectorRef, Component, ElementRef, forwardRef, HostListener, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import format from 'date-fns/format';
import isDate from 'date-fns/isDate';
import isValid from 'date-fns/isValid';
import parse from 'date-fns/parse';

@Component({
  selector: 'app-loki-date',
  styleUrls: ['./loki-date.component.less'],
  templateUrl: './loki-date.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LokiDateComponent),
      multi: true,
    },
  ],
})
export class LokiDateComponent implements OnInit, ControlValueAccessor {
  @Input() ldPlaceHolder = 'Nhập vào ngày';
  @Input() ldAllowClear = true;
  @Input() ldAutoFocus = true;
  @Input() ldDisabled = false;
  @Input() ldFormat = 'dd/MM/yyyy';
  ldMomentFormat: string;
  ldShowTime = false;

  isOpen = false;
  lokiDate: Date;
  lokiDateFomart: string;

  constructor(private elementRef: ElementRef, @Inject(DOCUMENT) public document: Document, private cdr: ChangeDetectorRef) {}
  ngOnInit() {
    this.getLdMomentFormat();
  }

  getLdMomentFormat() {
    switch (this.ldFormat) {
      case 'dd/MM/yyyy HH:mm:ss':
        this.ldMomentFormat = 'dd/MM/yyyy HH:mm:ss';
        this.ldShowTime = true;
        break;
      case 'dd/MM/yyyy HH:mm':
        this.ldMomentFormat = 'dd/MM/yyyy HH:mm';
        this.ldShowTime = true;
        break;
      case 'dd/MM/yyyy HH':
        this.ldMomentFormat = 'dd/MM/yyyy HH';
        this.ldShowTime = true;
        break;
      case 'dd/MM/yyyy':
        this.ldMomentFormat = 'dd/MM/yyyy';
        break;
      case 'MM/yyyy':
        this.ldMomentFormat = 'MM/yyyy';
        break;
      case 'yyyy':
        this.ldMomentFormat = 'YYYY';
        break;
      default:
        this.ldFormat = 'dd/MM/yyyy';
        this.ldMomentFormat = 'dd/MM/yyyy';
        break;
    }
    return this.ldMomentFormat;
  }

  writeValue(obj: any): void {
    this.lokiDate = obj;
    if (obj) {
      this.lokiDateFomart = format(this.lokiDate, this.getLdMomentFormat());
    }
    this.cdr.detectChanges();
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {}

  onChange = (_) => {};
  onTouched = () => {};

  togglePicker() {
    this.isOpen = !this.isOpen;
  }
  onOkPicker() {
    this.isOpen = false;
  }
  changePicker(date) {
    if (!this.ldShowTime) {
      this.isOpen = false;
    }
    this.lokiDateFomart = format(date, this.getLdMomentFormat());
    this.onChange(date);
  }
  changeInput() {
    try {
      const parsed = parse(this.lokiDateFomart, this.getLdMomentFormat(), new Date());
      if (isDate(parsed) && isValid(parsed)) {
        this.lokiDate = parsed;
      } else {
        this.lokiDate = null;
        this.lokiDateFomart = '';
      }

      this.onChange(this.lokiDate);
    } catch (error) {}
  }
  clearPicker() {
    this.lokiDate = null;
    this.lokiDateFomart = null;
    this.onChange(this.lokiDate);
  }

  @HostListener('document:click', ['$event'])
  clickDocument(event) {
    const elementList = this.document.getElementsByClassName('ant-calendar-picker-container');
    if (elementList.length > 0) {
      const element = elementList[0];
      const rect = element.getBoundingClientRect();
      // console.log(rect.top, rect.right, rect.bottom, rect.left);
      // console.log(event.clientX, event.clientY);
      const checkPicker =
        event.clientX > rect.left && event.clientX < rect.right && event.clientY < rect.bottom && event.clientY > rect.top;
      // console.log("checkPicker", checkPicker);
      const checkInput = this.elementRef.nativeElement.contains(event.target);
      // console.log("checkInput", checkInput);
      if (!checkPicker && !checkInput) {
        this.isOpen = false;
      }
    }
  }
}
