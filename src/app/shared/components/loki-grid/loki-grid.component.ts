import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { I18NService } from '@core';
import { SFComponent } from '@delon/form';
import { ALAIN_I18N_TOKEN, _HttpClient } from '@delon/theme';
import { MAKE_ID } from '@shared/utils/utils';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DEFAULT_COLUMN_ITEM, DEFAULT_LG_CONFIG, DEFAULT_LG_DATA, LGConfigModel, LGDataModel } from './loki-grid.model';

@Component({
  selector: 'app-loki-grid',
  templateUrl: './loki-grid.component.html',
  styleUrls: ['./loki-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LokiGridComponent implements OnInit, OnChanges, OnDestroy {
  /**
   * Cấu hình
   */
  @Input() lgConfig: LGConfigModel;
  /**
   * Dữ liệu
   */
  @Input() lgData: LGDataModel;
  /**
   * Dữ liệu tìm kiếm
   */
  @Input() lgSearchData: any;
  /**
   * Sự kiện tìm kiếm
   */
  @Output() lgOnSearch = new EventEmitter<any>();
  /**
   * Sự kiện sắp xếp
   */
  @Output() lgOnOrder = new EventEmitter<any>();

  /**
   * Sự kiện sửa dòng
   */
  @Output() lgOnComfirmEdit = new EventEmitter<any>();
  /**
   * Sự kiện Thêm dòng
   */
  @Output() lgOnComfirmAdd = new EventEmitter<any>();

  /**
   * Filter
   */
  isFilterAll = true;

  isOneFilter = false;

  selectingFilter: any;

  listSelected = [];
  /**
   * Select
   */
  isSelectAll = false;
  isHideSelectAll = false;

  /**
   * Visible
   */
  isVisibleAll = true;

  /**
   * Group
   */
  canGroup = false;
  groupPropName: any = 'Không nhóm';

  groupData = [];

  /**
   * Sort
   */
  sortDirName = '';

  sortPropName = '';

  /**
   * Edit row
   */
  editingRow: any;

  // Local variable
  searchFormData: any;
  listSelectedTag: any = [];

  scrollX: string | null = null;
  scrollY: string | null = null;
  gridID: string;
  isMobileTable = false;
  @ViewChild('sf') public sf: SFComponent;
  constructor(
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService,
    private router: Router,
    @Inject(DOCUMENT) public document: Document,
    @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
  ) {
    this.gridID = MAKE_ID(16);
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.scrollX = null;
        this.scrollY = null;
        this.updateChanges(true);
        setTimeout(() => {
          this.updateChanges();
        }, 10);
      }
    });
  }

  ngOnChanges(changes) {
    // Update config
    if (changes.lgConfig) {
      this.lgConfig = {
        ...DEFAULT_LG_CONFIG,
        ...this.lgConfig,
      };
      this.lgConfig.columns = this.lgConfig.columns.map((x) => {
        if (!x.sort_i18n && x.i18n) {
          x.sort_i18n = x.i18n;
        }
        return {
          ...DEFAULT_COLUMN_ITEM,
          ...x,
        };
      });

      this.canGroup = this.lgConfig.columns.find((x) => x.canGroup) !== undefined;
    }
    // Update data
    if (changes.lgData) {
      if (this.lgConfig.displayMode !== 'both' && !this.lgConfig.display) {
        this.lgConfig.display = this.lgConfig.displayMode;
      }
      const table = this.document.getElementById('loki-grid-' + this.gridID);
      const tableWidth = table ? table.clientWidth : window.innerWidth;
      if (tableWidth > 991) {
        this.isMobileTable = false;
      } else {
        this.isMobileTable = true;
      }
      if (!this.lgConfig.display) {
        if (tableWidth > 991) {
          this.lgConfig.display = 'list';
        } else {
          this.lgConfig.display = 'grid';
        }
      }
      this.lgData = {
        ...DEFAULT_LG_DATA,
        ...this.lgData,
      };
      // Process Data
      const currentCol = this.lgConfig.columns.find((x) => x.index === this.lgData.sortProp);
      if (currentCol && currentCol.clientSort) {
        this.lgData.datas = this.lgData.datas.sort((a: any, b: any) => {
          return currentCol.clientSort(a, b) * (this.lgData.sortDir === 'ascend' ? 1 : -1);
        });
      }
      this.lgConfig.loading = false;
      let columnGroup;
      if (this.lgData.groupProp) {
        columnGroup = this.lgConfig.columns.find((x) => x.canGroup && x.index === this.lgData.groupProp);
      }
      this.processGroup(columnGroup);
      this._caculateSortName();
    }
    // Search form data
    if (changes.lgSearchData) {
      if (this.lgSearchData && this.lgSearchData.searchFormData && Object.keys(this.lgSearchData.searchFormData).length > 0) {
        this.lgConfig.searchForm.isOpenAdvance = true;
        this.searchFormData = { ...this.lgSearchData.searchFormData };
      }

      if (this.lgSearchData && this.lgSearchData.listSelectedTag && this.lgSearchData.listSelectedTag.length > 0) {
        this.lgConfig.tagFilter.forEach((x) => {
          this.lgSearchData.listSelectedTag.forEach((y) => {
            if (y === x.value.toString()) {
              x.selected = true;
            }
          });
        });
        this.searchFormData = [...this.lgSearchData.listSelectedTag];
        this.listSelectedTag = this.lgSearchData.listSelectedTag;
      }
    }
  }
  ngOnInit() {
    // console.log('ngOnInit');
  }
  ngOnDestroy() {
    // console.log('ngOnDestroy');
  }
  updateChanges(isNotCall = false) {
    if (!isNotCall) {
      this.caculateScroll();
    }
    try {
      this.cdr.detectChanges();
    } catch (error) {}
  }
  processGroup(column?) {
    if (!column || !column.index) {
      this.groupData = [
        {
          hiddenTitle: true,
          datas: this.lgData.datas,
          expand: true,
        },
      ];
      this.groupPropName = 'Không nhóm';
      this.lgData.groupProp = null;
    } else {
      this.lgConfig.loading = true;
      this.groupPropName = column.i18n;
      this.lgData.groupProp = column.index;
      this.groupData = [];
      this.lgData.datas.forEach((item) => {
        item.checked = false;
        const findGroup = this.groupData.find((x) => x.name === item[this.lgData.groupProp]);
        if (findGroup) {
          findGroup.datas.push(item);
        } else {
          const group = {
            name: item[this.lgData.groupProp],
            datas: [item],
            expand: true,
            checked: false,
          };
          this.groupData.push(group);
        }
      });
    }
    this.lgConfig.loading = false;
    this.updateChanges();
  }

  private _caculateSortName() {
    if (!this.lgData.sortDir) {
      this.lgData.sortDir = 'ascend';
    }
    if (!this.lgData.sortProp) {
      const sortItem = this.lgConfig.columns.find((x) => x.canSort);
      this.lgData.sortProp = sortItem ? sortItem.index : '';
    }
    this.sortDirName = this.lgData.sortDir === 'ascend' ? 'Tăng dần' : 'Giảm dần';
    if (this.lgData.sortProp !== '') {
      const _current = this.lgConfig.columns.find((x) => x.index === this.lgData.sortProp);
      this.sortPropName = _current ? _current.i18n : '';
    }
  }
  private _buildQuery() {
    const filter = {
      pi: this.lgData.pi,
      ps: this.lgData.ps,
      sortDir: this.lgData.sortDir,
      sortProp: this.lgData.sortProp,
      conditions: this.lgData.conditions,
      filterText: this.lgData.filterText,
      searchFormData: null,
      listSelectedTag: null,
    };
    if (this.lgConfig.searchMode === 'allInOne') {
      // build query from search
      if (this.lgData.filterText) {
        const terms = this.lgData.filterText.split(';').map((x: string) => x.trim());
        const condition = {
          isFilterAll: this.isFilterAll,
          columns: this.lgConfig.columns.filter((x) => x.selectFilter),
          terms,
        };
        if (!Array.isArray(this.lgData.conditions)) {
          this.lgData.conditions = [];
        }
        if (!this.selectingFilter) {
          this.lgData.conditions.push(condition);
        } else {
          this.selectingFilter.isFilterAll = condition.isFilterAll;
          this.selectingFilter.columns = condition.columns;
          this.selectingFilter.terms = condition.terms;
          this.selectingFilter = false;
          this.selectingFilter = undefined;
        }
        this.lgData.filterText = '';
        filter.conditions = this.lgData.conditions;
      }
    } else {
      filter.conditions = null;
      filter.filterText = this.lgData.filterText;
      if (this.lgConfig.searchForm && this.lgConfig.searchForm.isOpenAdvance) {
        const searchFormData = this.sf.value;
        this.clean(searchFormData);
        filter.searchFormData = searchFormData;
      }
    }
    if (this.listSelectedTag != null && this.listSelectedTag.length > 0) {
      filter.listSelectedTag = this.listSelectedTag;
    }
    return filter;
  }

  clean(obj) {
    for (const propName in obj) {
      if (obj[propName] === null || obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }
  onSearch(reset_pagging = false) {
    this.lgConfig.loading = true;
    if (reset_pagging) {
      this.lgData.pi = 1;
    }
    const filter = this._buildQuery();
    this.lgOnSearch.emit({ filter, data: this.lgData });
  }
  onSort() {
    const currentCol = this.lgConfig.columns.find((x) => x.index === this.lgData.sortProp);
    if (currentCol && currentCol.clientSort) {
      this.lgConfig.loading = true;
      this.lgData.datas = this.lgData.datas.sort((a: any, b: any) => {
        return currentCol.clientSort(a, b) * (this.lgData.sortDir === 'ascend' ? 1 : -1);
      });
      this.lgConfig.loading = false;
      const filter = this._buildQuery();
      this.lgOnSearch.emit({ filter: { ...filter, clientSort: true }, data: this.lgData });
    } else {
      this.onSearch(true);
    }
  }
  onReset() {
    this.lgConfig.loading = true;
    this.lgData.filterText = '';
    this.searchFormData = {};
    this.listSelectedTag = [];
    if (this.lgConfig.tagFilter) {
      this.lgConfig.tagFilter.forEach((x) => (x.selected = false));
    }
    if (this.sf) {
      this.sf.reset();
    }
    const filter = this._buildQuery();
    this.lgOnSearch.emit({ filter, data: this.lgData });
  }
  checkAll(isChecked) {
    this.listSelected = [];
    this.groupData.forEach((group) => {
      group.checked = isChecked;
      group.datas.forEach((data) => {
        if (!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(data)) {
          data.checked = isChecked;
          if (isChecked) {
            this.listSelected.push(data);
          }
        }
      });
    });
  }
  checkAllGroup(group, isChecked) {
    this.listSelected = [];
    group.datas.forEach((data) => {
      if (!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(data)) {
        data.checked = isChecked;
        if (isChecked) {
          this.listSelected.push(data);
        }
      }
    });
  }
  checkItem(group) {
    group.checked = true;
    group.hide = false;
    this.isSelectAll = true;
    this.isHideSelectAll = false;
    if (group.datas.find((x) => !x.checked && (!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(x)))) {
      group.checked = false;
    }
    if (group.datas.find((x) => !(!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(x)))) {
      group.hide = true;
    }
    if (this.groupData.find((x) => !x.checked && (!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(x)))) {
      this.isSelectAll = false;
    }
    if (this.groupData.find((x) => !(!this.lgConfig.checkboxIf || this.lgConfig.checkboxIf(x)))) {
      this.isHideSelectAll = true;
    }
    this.getSelectedItem();
  }
  getSelectedItem() {
    this.listSelected = [];
    this.groupData.forEach((group) => {
      group.datas.forEach((data) => {
        if (data.checked) {
          this.listSelected.push(data);
        }
      });
    });
  }
  getColumn(filterProp) {
    if (filterProp === 'dummy') {
      return this.lgConfig.columns.filter((x) => x.selectVisible && !x.width);
    }
    const result = this.lgConfig.columns.filter((x) => x[filterProp]);
    return result;
  }
  getColumnDisplay(column) {
    let display = 'raw';
    if (column.format) {
      display = 'format';
    }
    if (column.render) {
      display = 'render';
    }
    if (column.canEdit) {
      display = 'editable';
    }
    return display;
  }
  onDropRow(event: CdkDragDrop<any[]>): void {
    const prevParent = event.previousContainer.data[0][this.lgData.groupProp];
    const curentParent = event.container.data[0][this.lgData.groupProp];
    if (prevParent !== curentParent) {
      this.modalService.confirm({
        nzTitle: this.i18n.fanyi('loki.table_drag', { from: prevParent, to: curentParent }),
        nzOkText: this.i18n.fanyi('Tiếp tục'),
        nzCancelText: this.i18n.fanyi('Hủy'),
        nzOkType: 'danger',
        nzOnOk: () => {
          const currentGroup = this.groupData.find((x) => x.name === curentParent);
          const prevGroup = this.groupData.find((x) => x.name === prevParent);
          const item = prevGroup.datas[event.previousIndex];
          prevGroup.datas.splice(event.previousIndex, 1);
          item[this.lgData.groupProp] = curentParent;
          currentGroup.datas.splice(event.currentIndex, 0, event.item);
          moveItemInArray(currentGroup, event.previousIndex, event.currentIndex);
          this.lgOnOrder.emit(this.groupData);
        },
        nzOnCancel: () => {},
      });
    } else {
      this.modalService.confirm({
        nzTitle: this.i18n.fanyi('loki.table_drag_position', { from: event.previousIndex, to: event.currentIndex }),
        nzOkText: this.i18n.fanyi('Tiếp tục'),
        nzCancelText: this.i18n.fanyi('Hủy'),
        nzOkType: 'danger',
        nzOnOk: () => {
          const currentGroup = this.groupData.find((x) => x.name === curentParent);
          moveItemInArray(currentGroup.datas, event.previousIndex, event.currentIndex);
          this.lgOnOrder.emit(this.groupData);
        },
        nzOnCancel: () => {},
      });
    }
  }

  getRowClass(data): string {
    if (this.lgConfig.getRowClass) {
      return this.lgConfig.getRowClass(data);
    }
    return '';
  }
  stChangePi(pi) {
    return this.stChange(
      {
        pi,
      },
      'pi',
    );
  }
  stChangePs(ps) {
    return this.stChange(
      {
        ps,
      },
      'ps',
    );
  }
  stChange(e, typeChanged) {
    switch (typeChanged) {
      case 'pi':
        this.lgData.pi = e;
        this.onSearch();
        break;
      case 'ps':
        this.lgData.ps = e;
        this.lgData.pi = 1;
        this.onSearch();
        break;
      case 'sort':
        const { sort } = e as any;
        const currentSort = e.sort.find((item) => item.value !== null);
        const sortField = (currentSort && currentSort.key) || null;
        const sortOrder = (currentSort && currentSort.value) || null;
        this.lgData.sortDir = sortOrder;
        this.lgData.sortProp = sortField;
        this.onSort();
        break;
      default:
        break;
    }
  }
  doSortGrid(option, value) {
    if (option === 'dir') {
      this.lgData.sortDir = value;
    } else {
      this.lgData.sortProp = value;
    }
    this.onSort();
  }
  reset() {
    this.lgData.pi = 1;
    this.lgData.conditions = [];
    this.lgData.filterText = '';
    setTimeout(() => this.onSearch());
  }

  removeCondition(index) {
    this.lgData.conditions.splice(index, 1);
  }
  selectCondition(condition) {
    if (this.selectingFilter) {
      this.selectingFilter.selecting = false;
    }
    this.selectingFilter = condition;
    this.selectingFilter.selecting = true;
    condition.columns.forEach((x) => {
      const findColumn = this.lgConfig.columns.find((y) => y.index === x.index);
      findColumn.selectFilter = x.selectFilter;
    });
    this.isFilterAll = condition.isFilterAll;
    this.lgData.filterText = condition.terms.join(';');
    this.updateChanges();
  }
  toggleAllFilter() {
    this.lgConfig.columns.forEach((x) => {
      if (x.canFilter) {
        x.selectFilter = this.isFilterAll;
      }
    });
    this.updateChanges();
  }
  toggleItemFilter() {
    setTimeout(() => {
      this.isFilterAll = true;
      this.isOneFilter = false;
      if (this.lgConfig.columns.find((x) => x.canFilter && !x.selectFilter)) {
        this.isFilterAll = false;
      }
      if (this.lgConfig.columns.filter((x) => x.canFilter && x.selectFilter).length === 1) {
        this.isOneFilter = true;
      }
      this.updateChanges();
    });
  }
  toggleAllVisible() {
    this.lgConfig.columns.forEach((x) => {
      if (!x.onTop) {
        x.selectVisible = this.isVisibleAll;
      }
    });
    this.updateChanges();
  }
  toggleItemVisible() {
    setTimeout(() => {
      this.isVisibleAll = true;
      if (this.lgConfig.columns.find((x) => !x.selectVisible)) {
        this.isVisibleAll = false;
      }
      this.updateChanges();
    });
  }
  dataEmit(_data, model) {
    model.raw = model.changed;
  }
  dataRevert(_data, model) {
    model.changed = model.raw;
  }

  toggleFormRow(row) {
    if (this.editingRow) {
      this.editingRow.editing = false;
      delete this.editingRow.model;
    }

    const model = { ...row };
    row.model = model;
    row.editing = true;

    this.editingRow = row;
  }

  saveRow(row) {
    const model = { ...row.model };
    for (const key in model) {
      if (model.hasOwnProperty(key)) {
        row[key] = model[key];
      }
    }
    if (this.editingRow) {
      this.editingRow.editing = false;
      delete this.editingRow.model;
    }

    this.editingRow = undefined;
  }
  revertRow(row) {
    delete row.model;
    if (this.editingRow) {
      this.editingRow.editing = false;
      delete this.editingRow.model;
    }
    this.editingRow = undefined;
  }
  getTotal(datas, column) {
    let total = 0;
    datas.forEach((item) => {
      total += item[column.index];
    });
    return total;
  }
  checkTag(i) {
    const index = this.listSelectedTag.indexOf(i.value);
    if (i.selected) {
      this.listSelectedTag.push(i.value);
    } else {
      this.listSelectedTag.splice(index, 1);
    }
    const currentSelected = JSON.stringify(this.listSelectedTag);
    setTimeout(() => {
      if (JSON.stringify(this.listSelectedTag) === currentSelected) {
        this.onSearch();
      }
    }, 500);
  }
  caculateScroll() {
    const maxSize = null;
    let y = 'calc(100vh - 330px)';
    let x = maxSize;
    if (this.lgConfig && this.lgConfig.searchForm && this.lgConfig.searchForm.isOpenAdvance) {
      y = maxSize;
    }
    if (this.lgData && this.lgConfig.display === 'list') {
      x = this.lgConfig.maxWidth || '900px';
    }
    const result = { x, y };
    this.scrollX = x;
    this.scrollY = y;
    if (this.lgConfig.noScroll || this.isMobileTable) {
      this.scrollY = maxSize;
    }
    // return result;
  }
  toggleDisplay() {
    this.caculateScroll();
    this.cdr.detectChanges();
  }
}
