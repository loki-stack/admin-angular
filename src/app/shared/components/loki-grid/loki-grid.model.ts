import { TemplateRef } from '@angular/core';
import { SFSchema, SFUISchema } from '@delon/form';
import { TABLE_CONFIG } from '@shared/utils/const';
/**
 * Cấu hình
 */
export interface LGConfigModel {
  /**
   * Loading state
   */
  loading?: boolean;
  /**
   * Cấu hình tìm kiếm
   */
  searchMode?: 'allInOne' | 'classic' | 'none';
  /**
   * Form tìm kiếm sử dụng sf form
   */
  searchForm?: LGSearchFormModel;
  /**
   * Nhóm tìm kiếm nhanh
   */
  tagFilter?: LGEnumModel[];

  /**
   * Cấu hình hiển thị
   */
  displayMode?: 'list' | 'grid' | 'both';
  /**
   * Cấu hình hiển thị
   */
  display?: 'list' | 'grid' | 'both';
  /**
   * Cấu hình cột
   */
  columns?: LGColumnModel[];
  /**
   * Thuộc tính làm avatar
   */
  avatarProp?: string;
  /**
   * Cho phép kéo thả
   */
  canDragDrop?: boolean;
  /**
   * Function lấy class của row
   */
  getRowClass?: (data: any[]) => string;
  /**
   * Cấu hình cột mở rộng
   */
  expandColumn?: LGColumnModel;
  /**
   * Cấu hình edit
   *  - `{{cell}}` Edit dữ liệu trên từng cell
   *  - `{{row}}` Edit dữ liệu trên cả row
   */
  editMode?: 'cell' | 'row' | 'none';
  /**
   * Cấu hình phân trang
   */
  paginationMode?: 'client' | 'server' | 'none';
  /**
   * Cấu hình chọn số trang 1 page. Ex [10,20,100]
   */
  pageSizes?: number[];
  /**
   * Cấu hình phân trang quick jump
   */
  showQuickJumper?: boolean;

  /**
   * Cấu hình dòng tổng quan (summary)
   */
  summaryMode?: 'top' | 'bottom' | 'both' | 'none';
  /**
   * Template định nghĩa hiển thị summary
   * @argument datas: Dữ liệu hiện tại
   * @argument columns: Cấu hình cột hiện tại
   */
  summaryRender?: TemplateRef<{
    datas: any;
    columns: any;
  }>;
  /**
   * Các action không liên quan tới dữ liệu select table. Ex Thêm, Import, Export.
   */
  actionsSelf?: LGActionSelfModel[];
  /**
   * Các action thao tác với 1 record
   */
  actionsSingle?: LGActionSingleModel[];
  /**
   * Các action thao tác khi chọn nhiều record
   */
  actionsMulti?: LGActionMultiModel[];
  /**
   * Điều kiện hiển thị checkbox
   */
  checkboxIf?: (data: any, index?: number) => boolean;

  /**
   * Các key thêm
   */
  // [key: string]: any;
  toolSize?: string;

  maxWidth?: string;
  /**
   * Không có tool bar
   */
  noToolbar?: boolean;

  /**
   * Không có tool bar
   */
  noScroll?: boolean;
}
/**
 * Dữ liệu
 */
export interface LGDataModel {
  /**
   * Vị trí hiện tại (Tính từ 0)
   */
  pi?: number;
  /**
   * Số bản ghi 1 trang
   */
  ps?: number;
  /**
   * Tổng số dữ liệu (server render)
   */
  total?: number;
  /**
   * Hướng sắp xếp
   */
  sortDir?: 'ascend' | 'descend' | 'none';
  /**
   * Thuộc tính sắp xếp
   */
  sortProp?: string;
  /**
   * Thuộc tính nhóm
   */
  groupProp?: string;

  /**
   * Text tìm kiếm
   */
  filterText?: string;

  /**
   * Dữ liệu tìm kiếm
   */
  conditions?: any[];
  /**
   * Dữ liệu tìm kiếm form
   */
  searchFormData?: any;
  /**
   * Dữ liệu tìm kiếm tag
   */
  listSelectedTag?: any[];
  /**
   * Dữ liệu
   */
  datas?: any[];
  /**
   * Màn hình đang hiển thị
   */
  display?: string;
  /**
   * Các key thêm
   */
  // [key: string]: any;
}
export interface LGActionBaseModel {
  /**
   * Biểu tượng để hiển thị
   */
  icon?: string;
  /**
   * Tiêu đề (key đa ngôn ngữ)
   */
  i18n?: string;
  /**
   * Ghi chú (key đa ngôn ngữ)
   */
  i18nDescription?: string;
  /**
   * Css-class
   */
  class?: string;
  /**
   * Type
   */
  type?: string;
}
export interface LGActionSelfModel extends LGActionBaseModel {
  /**
   * Hành động nhận parram từ model
   */
  action?: (data: any, index?: number) => any;
}
export interface LGActionSingleModel extends LGActionBaseModel {
  /**
   * Hành động nhận parram từ model
   * @argument data: Bản ghi
   * @argument index: Vị trí
   */
  action?: (data: any, index?: number) => any;
  /**
   * Điều kiện hiển thị
   */
  if?: (data: any, index?: number) => boolean;
}
export interface LGActionMultiModel extends LGActionBaseModel {
  /**
   * Hành động nhận parram từ model
   * @argument selectedData: Danh sách bản ghi đã chọn
   */
  action?: (selectedData: any[]) => any;
}
export interface LGSearchFormModel {
  /**
   * Giao diện
   */
  ui?: SFUISchema;
  /**
   * Thuộc tính
   */
  schema?: SFSchema;
  /**
   * Mở advanced
   */
  isOpenAdvance?: boolean;
  /**
   * Không cho phép toggle
   */
  disableToggle?: boolean;
}
/**
 * Cấu hình một cột
 */
export interface LGColumnModel {
  /**
   * index dữ liệu trong DB
   */
  index?: string;
  /**
   * Cách hiển thị dữ liệu
   */
  format?: (data?: any, column?: any, index?: number) => string;
  /**
   * Cách hiển thị dữ liệu truyền vào render
   */
  render?: TemplateRef<{
    data: any;
    column: any;
    model: any;
    dataEmit: any;
  }>;
  /**
   * Nhãn cột
   */
  i18n?: string;
  /**
   * Nhãn cột ngắn
   */
  sort_i18n?: string;
  /**
   * Cho phép lọc (searchMode:'allInOne')
   */
  canFilter?: boolean;
  /**
   * Cho phép nhóm
   */
  canGroup?: boolean;
  /**
   * Cho phép sắp xếp
   */
  canSort?: boolean;
  /**
   * Cho phép sửa
   */
  canEdit?: boolean;
  /**
   * Mặc định đang lọc (searchMode:'allInOne')
   */
  selectFilter?: boolean;
  /**
   * Mặc định đang hiển thị
   */
  selectVisible?: boolean;
  /**
   * Cột luôn luôn hiển thị
   */
  onTop?: boolean;
  /**
   * Chiều rộng
   */
  width?: string;
  /**
   * Chiều rộng min
   */
  minWidth?: string;

  /**
   * Ẩn title nếu ở grid
   */
  hideTitleInGrid?: boolean;

  /**
   * CSS class
   */
  class?: string;
  /**
   * Cột có tổng số
   */
  hasTotal?: boolean;
  /**
   * Function tính tổng
   */
  getTotal?: (datas: any, column: any) => string;
  /**
   * Loại control
   */
  type?:
    | 'select'
    | 'select-multiple'
    | 'select-tags'
    | 'date'
    | 'date-time'
    | 'time'
    | 'checkbox'
    | 'switch'
    | 'input'
    | 'textarea'
    | 'money'
    | 'image'
    | 'tag'
    | 'custom';
  /**
   * Cấu hình phục vụ control
   */
  config?: LGColumnConfigModel;
  /**
   * click action
   */
  click?: (data: any) => void;
  /**
   * tooltip function
   */
  tooltipFn?: (data: any, column: LGColumnModel) => string;
  /**
   *
   */
  clientSort?: ((a: any, b: any) => number) | null;
}

export interface LGData {
  [key: string]: any;
}

export interface LGColumnConfigModel {
  /**
   * Danh sách các item chọn ( Select, tag ...)
   */
  enum?: LGEnumModel[];
  /**
   * Prefix ( money )
   */
  prefix?: string;
  /**
   * action click
   */
  click?: (item: any) => void;

  [key: string]: any;
}
export interface LGEnumModel {
  /**
   * Giá trị
   */
  value?: any;
  /**
   * Nhãn
   */
  i18n?: string;
  /**
   * Màu ( bule, red ,green ...)
   */
  color?: string;
  /**
   * option đang chọn
   */
  selected?: boolean;
  [key: string]: any;
}

/**
 * Giá trị mặc định
 */
export const DEFAULT_COLUMN_ITEM: LGColumnModel = {
  canFilter: true,
  canGroup: false,
  canSort: true,
  canEdit: false,
  selectFilter: true,
  selectVisible: true,
  onTop: false,
  hasTotal: false,
  type: 'input',
  tooltipFn: (data, column) => data[column.index],
};
export const DEFAULT_ACTIONS_SELF: LGActionSelfModel[] = [];
export const DEFAULT_ACTIONS_SINGLE: LGActionSingleModel[] = [];
export const DEFAULT_ACTIONS_MULTI: LGActionMultiModel[] = [];
export const DEFAULT_COLUMNS: LGColumnModel[] = [];
export const DEFAULT_DATAS: any[] = [];
export const DEFAULT_LG_CONFIG: LGConfigModel = {
  loading: false,
  searchMode: 'classic',
  displayMode: 'both',
  columns: DEFAULT_COLUMNS,
  canDragDrop: false,
  editMode: 'none',
  paginationMode: 'server',
  pageSizes: TABLE_CONFIG.pageSizes,
  showQuickJumper: TABLE_CONFIG.showQuickJumper,
  summaryMode: 'none',
  actionsSelf: DEFAULT_ACTIONS_SELF,
  actionsSingle: DEFAULT_ACTIONS_SINGLE,
  actionsMulti: DEFAULT_ACTIONS_MULTI,
  noToolbar: false,
};

export const DEFAULT_LG_DATA: LGDataModel = {
  pi: 1,
  ps: DEFAULT_LG_CONFIG.pageSizes[0],
  total: 0,
  datas: DEFAULT_DATAS,
  display: 'list',
};
export const COLUMN_WIDTH = {
  IMAGE: '60px',
  EMAIL: '200px',
  NAME: '160px',
  DATE: '110px',
  PHONE: '120px',
  CODE: '80px',
  DATETIME: '130px',
};
