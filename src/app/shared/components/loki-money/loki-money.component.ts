import { CurrencyPipe } from '@angular/common';
import { ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { StorageService } from '@services';

@Component({
  selector: 'app-loki-money',
  styleUrls: ['./loki-money.component.less'],
  templateUrl: './loki-money.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LokiMoneyComponent),
      multi: true,
    },
  ],
})
export class LokiMoneyComponent implements OnInit, ControlValueAccessor {
  @Input() lmPlaceHolder = '';
  @Input() lmAutoFocus = false;
  @Input() lmDisabled = false;
  @Input() lmMin = null;
  @Input() lmMax = null;
  @Input() lmPrefix = null;
  lokiMoney: number;
  lokiMoneyFomart: string;
  constructor(private currencyPipe: CurrencyPipe, private storageService: StorageService, private cdr: ChangeDetectorRef) {}
  ngOnInit() {}

  numberOnly(event) {
    const pattern = /[0-9/.kK-]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  writeValue(obj: any): void {
    this.lokiMoney = obj;
    if (this.lmMax && this.lokiMoney > this.lmMax) {
      this.lokiMoney = this.lmMax;
    }
    if (this.lmMin && this.lokiMoney < this.lmMin) {
      this.lokiMoney = this.lmMin;
    }
    this.lokiMoneyFomart = this.currencyPipe.transform(this.lokiMoney, '', '', '1.0', '');
    if (this.lokiMoneyFomart) {
      this.lokiMoneyFomart = this.lokiMoneyFomart.trim();
      this.cdr.detectChanges();
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {}

  onChange = (_) => {};
  onTouched = () => {};

  onBlurMethod() {
    this.lokiMoneyFomart = this.currencyPipe.transform(this.lokiMoney, '', '', '1.0', '');
    if (this.lokiMoneyFomart) {
      this.lokiMoneyFomart = this.lokiMoneyFomart.trim();
    }
  }

  changeInput(dateStr) {
    try {
      const currentLanguage = this.storageService.getItem('currentLanguage') || 'vi-VN';
      const result = currentLanguage === 'vi-VN' ? dateStr.replace(/\./g, '').replace(/\,/g, '.') : dateStr.replace(/\,/g, '');
      this.lokiMoney = parseFloat(result);
      if (this.lmMax && this.lokiMoney > this.lmMax) {
        this.lokiMoney = this.lmMax;
      }
      if (this.lmMin && this.lokiMoney < this.lmMin) {
        this.lokiMoney = this.lmMin;
      }
      this.lokiMoneyFomart = this.currencyPipe.transform(this.lokiMoney, '', '', '1.0', '');
      if (this.lokiMoneyFomart) {
        this.lokiMoneyFomart = this.lokiMoneyFomart.trim();
      }

      this.onChange(this.lokiMoney);
    } catch (error) {}
  }
}
