import { ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-loki-search',
  styleUrls: ['./loki-search.component.less'],
  templateUrl: './loki-search.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LokiSearchComponent),
      multi: true,
    },
  ],
})
export class LokiSearchComponent implements OnInit, ControlValueAccessor {
  /**
   * Place holder
   */
  @Input() lsPlaceHolder: string;
  /**
   * Thuộc tính giá trị
   */
  @Input() lsValueProp: string;
  /**
   * Thuộc tính nhãn
   */
  @Input() lsLabelProp: string;

  /**
   * Thuộc tính hiển thị
   */
  @Input() lsContentProp: any;
  /**
   * Hàm tìm kiếm
   */
  @Input() lsSearchFn: (searchText: string) => Observable<any>;
  /**
   * Hàm lấy giá trị hiện tại
   */
  @Input() lsFindCurrenFn: (value: string) => Observable<any>;
  /**
   * Search từng phần
   */
  @Input() lsAsync = false;

  /**
   * Loại search
   */
  @Input() lsMode: 'multiple' | 'tags' | 'default' = 'default';
  /**
   *  Chiều cao option
   */
  @Input() lsOptionHeight = 32;

  model: any;
  collections: any[] = [];
  searchChange$ = new BehaviorSubject('');
  loading = false;
  constructor(private cdr: ChangeDetectorRef) {}
  ngOnInit() {
    this.loading = true;
    if (this.lsAsync) {
      this.searchChange$
        .asObservable()
        .pipe(debounceTime(500))
        .pipe(switchMap((value) => this.getData(value)))
        .subscribe((response) => {
          if (response.code === 200) {
            const lastCollections = [...this.collections];
            this.collections = response.data.content;
            if (this.lsMode === 'default') {
              if (this.model !== undefined && this.model !== null) {
                // kiểm tra dữ liệu đã có chưa
                if (!this.collections.find((x) => x[this.lsValueProp] === this.model)) {
                  // Kiểm tra cache
                  const cache = lastCollections.find((x) => x[this.lsValueProp] === this.model);
                  if (cache) {
                    this.collections.unshift(cache);
                  } else {
                    this.lsFindCurrenFn(this.model).subscribe((response2) => {
                      if (response2.code === 200) {
                        this.collections.unshift(response2.data);
                        this.cdr.detectChanges();
                      } else {
                        this.cdr.detectChanges();
                      }
                    });
                  }
                }
              }
            }
            this.loading = false;
            this.cdr.detectChanges();
          } else {
            this.collections = [];
            this.loading = false;
            this.cdr.detectChanges();
          }
        });
    } else {
      this.getData('').subscribe((response) => {
        if (response.code === 200) {
          this.collections = response.data.content;
          this.loading = false;
          this.cdr.detectChanges();
        } else {
          this.collections = [];
          this.loading = false;
          this.cdr.detectChanges();
        }
      });
    }
  }

  writeValue(obj: any): void {
    this.model = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {}

  onChange = (_) => {};
  onTouched = () => {};

  onSearch(searchText: string) {
    if (this.lsAsync) {
      this.loading = true;
      this.searchChange$.next(searchText);
    }
  }
  getData(searchText: string): Observable<any> {
    if (this.lsSearchFn) {
      return this.lsSearchFn(searchText);
    }
  }
  modelChange(event) {
    if (this.lsMode === 'default') {
      const modelObj = this.collections.find((x) => x[this.lsValueProp] === event);
      this.onChange(modelObj);
    } else {
      const modelObj = this.collections.filter((x) => event.includes(x[this.lsValueProp])) || [];
      this.onChange(modelObj);
    }
  }
  getDisplayData(item) {
    if (typeof this.lsContentProp === 'function') {
      return this.lsContentProp(item);
      // do something
    }
    return item[this.lsContentProp];
  }
}
