import {
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { _HttpClient } from '@delon/theme';
import { LTColumnModel } from './loki-table.model';

@Component({
  selector: 'app-loki-table',
  templateUrl: './loki-table.component.html',
  styleUrls: ['./loki-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,

  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => LokiTableComponent),
    multi: true
  }]
})
export class LokiTableComponent implements OnInit, OnChanges, ControlValueAccessor {


  @Input() columns: LTColumnModel[];

  datas: any[];

  constructor(
  ) {
  }
  writeValue(obj: any): void {
    if (!Array.isArray(obj)) {
      obj = [];
    }
    this.datas = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(): void {
  }

  onChange = (_) => { };
  onTouched = () => { };

  ngOnChanges(changes) {
  }

  deleteRow(index) {
    this.datas.splice(index, 1);
    this.onChange(this.datas);
  }
  addRow() {
    this.datas.push({
    });
    this.onChange(this.datas);
  }
  ngOnInit() {
  }

}
