
export interface LTColumnModel {
    i18n?: string;
    index?: string;
    width?: string;
    class?: string;
    config?: any;
    type?:
    'select' |
    'select-multiple' |
    'select-tags' |
    'date' |
    'checkbox' |
    'switch' |
    'input' |
    'textarea' |
    'money' |
    'custom';
}
