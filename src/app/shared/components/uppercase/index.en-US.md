---
order: 30
title: uppercase
type: Component
---


```html
<input nz-input [(ngModel)]="q" uppercase  />
```

## API

| Property           | Description                            | Type                | Default |
|--------------------|----------------------------------------|---------------------|---------|
