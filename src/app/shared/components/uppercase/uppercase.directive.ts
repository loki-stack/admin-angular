import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { Directive, ElementRef, HostListener } from '@angular/core';
@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[uppercase]',
  host: {
    '(input)': '$event',
  },
})
export class UppercaseDirective {
  lastValue: string;

  constructor(public ref: ElementRef, @Inject(DOCUMENT) public document: Document) {}

  @HostListener('input', ['$event']) onInput($event) {
    const start = $event.target.selectionStart;
    const end = $event.target.selectionEnd;
    $event.target.value = $event.target.value.toUpperCase();
    $event.target.setSelectionRange(start, end);
    $event.preventDefault();

    if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
      this.lastValue = this.ref.nativeElement.value = $event.target.value;
      // Propagation
      const evt = this.document.createEvent('HTMLEvents');
      evt.initEvent('input', false, true);
      // tslint:disable-next-line: deprecation
      event.target.dispatchEvent(evt);
    }
  }
}
