import { NgModule } from '@angular/core';
import { DelonFormModule, WidgetRegistry } from '@delon/form';
import { SharedModule } from '@shared';

// import { AddressWidget } from './widgets/address/address.widget';
// import { EditorWidget } from './widgets/editor/editor.widget';
// import { ImgWidget } from './widgets/img/img.widget';
import { LokiMoneyWidget } from './widgets/loki-money/loki-money.widget';

import { LokiDateWidget } from './widgets/loki-date/loki-date.widget';
import { LokiSearchWidget } from './widgets/loki-search/loki-search.widget';
import { LokiTextNumberWidget } from './widgets/loki-text-number/loki-text-number.widget';

export const SCHEMA_THIRDS_COMPONENTS = [
  // EditorWidget,
  // ImgWidget,
  // AddressWidget,
  LokiMoneyWidget,
  LokiTextNumberWidget,
  LokiSearchWidget,
  LokiDateWidget,
];

@NgModule({
  declarations: SCHEMA_THIRDS_COMPONENTS,
  entryComponents: SCHEMA_THIRDS_COMPONENTS,
  imports: [SharedModule, DelonFormModule.forRoot()],
  exports: [...SCHEMA_THIRDS_COMPONENTS],
})
export class JsonSchemaModule {
  constructor(widgetRegistry: WidgetRegistry) {
    // widgetRegistry.register(EditorWidget.KEY, EditorWidget);
    // widgetRegistry.register(ImgWidget.KEY, ImgWidget);
    // widgetRegistry.register(AddressWidget.KEY, AddressWidget);
    widgetRegistry.register(LokiMoneyWidget.KEY, LokiMoneyWidget);
    widgetRegistry.register(LokiTextNumberWidget.KEY, LokiTextNumberWidget);
    widgetRegistry.register(LokiDateWidget.KEY, LokiDateWidget);
    widgetRegistry.register(LokiSearchWidget.KEY, LokiSearchWidget);
  }
}
