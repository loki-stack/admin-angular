import { Component, OnInit } from '@angular/core';
import { ControlWidget } from '@delon/form';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sf-loki-date',
  template: `
    <sf-item-wrap [id]="id" [schema]="schema" [ui]="ui" [showError]="showError" [error]="error" [showTitle]="schema.title">
      <app-loki-date
        [ldPlaceHolder]="ldPlaceHolder"
        [ldAllowClear]="ldAllowClear"
        [ldAutoFocus]="ldAutoFocus"
        [ldDisabled]="ldDisabled"
        [ldFormat]="ldFormat"
        [ngModel]="value"
        name="sf.loki-date"
        (ngModelChange)="_change($event)"
      ></app-loki-date>
    </sf-item-wrap>
  `,
  preserveWhitespaces: false,
})

// tslint:disable-next-line: component-class-suffix
export class LokiDateWidget extends ControlWidget implements OnInit {
  static readonly KEY = 'loki-date';
  ldPlaceHolder = 'Nhập vào ngày';
  ldAllowClear = true;
  ldAutoFocus = true;
  ldDisabled = false;
  ldFormat = 'dd/MM/yyyy';
  _change(value: any) {
    this.setValue(value);
  }
  ngOnInit(): void {
    this.ldPlaceHolder = this.ui.ldPlaceHolder || 'Nhập vào ngày';
    this.ldAllowClear = this.ui.ldAllowClear || true;
    this.ldAutoFocus = this.ui.ldAutoFocus || true;
    this.ldDisabled = this.ui.ldDisabled || false;
    this.ldFormat = this.ui.ldFormat || 'dd/MM/yyyy';
  }
}
