import { Component, OnInit } from '@angular/core';
import { ControlWidget } from '@delon/form';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sf-loki-money',
  template: `
    <sf-item-wrap [id]="id" [schema]="schema" [ui]="ui" [showError]="showError" [error]="error" [showTitle]="schema.title">
      <app-loki-money
        [lmPlaceHolder]="lmPlaceHolder"
        [lmAutoFocus]="lmAutoFocus"
        [lmDisabled]="lmDisabled"
        [lmMin]="lmMin"
        [lmMax]="lmMax"
        [lmPrefix]="lmPrefix"
        [ngModel]="value"
        name="sf.loki-money"
        (ngModelChange)="_change($event)"
      ></app-loki-money>
    </sf-item-wrap>
  `,
  preserveWhitespaces: false,
})

// tslint:disable-next-line: component-class-suffix
export class LokiMoneyWidget extends ControlWidget implements OnInit {
  static readonly KEY = 'loki-money';

  lmPlaceHolder = '';
  lmAutoFocus = false;
  lmDisabled = false;
  lmMin = null;
  lmMax = null;
  lmPrefix = null;
  ngOnInit(): void {
    this.lmPlaceHolder = this.ui.lmPlaceHolder || 'Nhập vào số';
    this.lmAutoFocus = this.ui.lmAutoFocus;
    this.lmDisabled = this.ui.lmDisabled;
    this.lmMin = this.ui.lmMin;
    this.lmMax = this.ui.lmMax;
    this.lmPrefix = this.ui.lmPrefix;
  }
  _change(value: string) {
    this.setValue(value);
  }
}
