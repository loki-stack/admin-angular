import { Component, OnInit } from '@angular/core';
import { ControlWidget } from '@delon/form';
import { Observable } from 'rxjs';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sf-loki-search',
  template: `
    <sf-item-wrap [id]="id" [schema]="schema" [ui]="ui" [showError]="showError" [error]="error" [showTitle]="schema.title">
      <app-loki-search
        [lsPlaceHolder]="lsPlaceHolder"
        [lsValueProp]="lsValueProp"
        [lsLabelProp]="lsLabelProp"
        [lsContentProp]="lsContentProp"
        [lsSearchFn]="lsSearchFn"
        [lsAsync]="lsAsync"
        [lsFindCurrenFn]="lsFindCurrenFn"
        [lsMode]="lsMode"
        [lsOptionHeight]="lsOptionHeight"
        [ngModel]="value"
        name="sf.loki-search"
        (ngModelChange)="_change($event)"
      ></app-loki-search>
    </sf-item-wrap>
  `,
  preserveWhitespaces: false,
})

// tslint:disable-next-line: component-class-suffix
export class LokiSearchWidget extends ControlWidget implements OnInit {
  static readonly KEY = 'loki-search';

  /**
   * Place holder
   */
  lsPlaceHolder: string;
  /**
   * Thuộc tính giá trị
   */
  lsValueProp: string;
  /**
   * Thuộc tính nhãn
   */
  lsLabelProp: string;

  /**
   * Thuộc tính hiển thị
   */
  lsContentProp: any;
  /**
   * Search bất đồng bộ
   */
  lsAsync: boolean;
  /**
   * Loại seach
   */
  lsMode: 'multiple' | 'tags' | 'default';
  /**
   * Hàm tìm kiếm
   */
  lsSearchFn: (searchText: string) => Observable<any>;

  /**
   * Hàm lấy giá trị hiện tại
   */
  lsFindCurrenFn: (value: string) => Observable<any>;

  /**
   * Hàm model change
   */
  lsModelChange: (value: any) => Observable<any>;

  /**
   * Chiều cao option
   */
  lsOptionHeight = 32;

  _change(value: any) {
    if (this.lsMode === 'default') {
      this.setValue(value ? value[this.lsValueProp] : null);
    } else {
      this.setValue(value ? value.map((x) => x[this.lsValueProp]) : []);
    }
    if (this.lsModelChange) {
      this.lsModelChange(value);
    }
  }
  ngOnInit(): void {
    this.lsMode = this.ui.lsMode || 'default';
    this.lsAsync = this.ui.lsAsync || false;
    this.lsPlaceHolder = this.ui.lsPlaceHolder || 'Chọn bản ghi';
    this.lsValueProp = this.ui.lsValueProp || 'id';
    this.lsLabelProp = this.ui.lsLabelProp || 'name';
    this.lsContentProp = this.ui.lsContentProp || 'name';
    this.lsOptionHeight = this.ui.lsOptionHeight || '32';
    this.lsSearchFn = this.ui.lsSearchFn;
    this.lsFindCurrenFn = this.ui.lsFindCurrenFn;
    this.lsModelChange = this.ui.lsModelChange;
  }
}
