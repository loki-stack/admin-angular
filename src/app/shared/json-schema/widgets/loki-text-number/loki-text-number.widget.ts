import { Component } from '@angular/core';
import { ControlWidget } from '@delon/form';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sf-loki-text-number',
  template: `
    <sf-item-wrap [id]="id" [schema]="schema" [ui]="ui" [showError]="showError" [error]="error" [showTitle]="schema.title">
      {{ value | number }}
    </sf-item-wrap>
  `,
  preserveWhitespaces: false,
})

// tslint:disable-next-line: component-class-suffix
export class LokiTextNumberWidget extends ControlWidget {
  static readonly KEY = 'loki-text-number';

  _change(value: string) {
    this.setValue(value);
  }
}
