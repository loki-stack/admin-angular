/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
class DemoService {
    getFilterPath = '/api/demo';

    constructor(
        private http: HttpClient
    ) {

    }


    /**
     * @param params The `BSDParameterService.GetFilterParams` containing the following parameters:
     *
     * - `sort`: Thông tin sắp xếp (Array Json)
     *
     * - `size`: Số bản ghi giới hạn một trang
     *
     * - `page`: Số thứ tự trang tìm kiếm
     *
     * - `filter`: Thông tin lọc nâng cao (Object Json)
     *
     * @return Thành công
     */
    getFilterAsync(params: DemoService.GetFilterParams): __Observable<any> {
        let paramData = new HttpParams();
        if (params.sort != null) paramData = paramData.set('sort', params.sort.toString());
        if (params.size != null) paramData = paramData.set('size', params.size.toString());
        if (params.page != null) paramData = paramData.set('page', params.page.toString());
        if (params.filter != null) paramData = paramData.set('filter', params.filter.toString());
        let req = new HttpRequest<any>(
            'GET', this.getFilterPath,
            {
                params: paramData,
                responseType: 'json'
            });
        return this.http.request<any>(req);
    }


}

module DemoService {
    export interface GetFilterParams {
        sort: string,
        size: number,
        page: number,
        filter: string,
    }
}


export { DemoService }
