import { CommonModule, CurrencyPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DelonACLModule } from '@delon/acl';
import { DelonFormModule } from '@delon/form';
import { AlainThemeModule } from '@delon/theme';
import { TranslateModule } from '@ngx-translate/core';

import { SHARED_DELON_MODULES } from './shared-delon.module';
import { SHARED_ZORRO_MODULES } from './shared-zorro.module';

// #region third libs
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CountdownModule } from 'ngx-countdown';

const THIRDMODULES = [PipesModule, CountdownModule, DragDropModule];
// #endregion

// #region your componets & directives
import { PipesModule } from '@core';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PRO_SHARED_COMPONENTS } from '../layout/pro/share';
import { LangsComponent } from './components/langs/langs.component';
import { LokiControlComponent } from './components/loki-control/loki-control.component';
import { LokiDateComponent } from './components/loki-date/loki-date.component';
import { LokiGridComponent } from './components/loki-grid/loki-grid.component';
import { LokiMoneyComponent } from './components/loki-money/loki-money.component';
import { LokiSearchComponent } from './components/loki-search/loki-search.component';
import { LokiTableComponent } from './components/loki-table/loki-table.component';
import { QUICK_CHAT_COMPONENTS } from './components/quick-chat';
import { ScrollbarDirective } from './components/scrollbar/scrollbar.directive';
const I18NSERVICE_MODULES = [TranslateModule];

const COMPONENTS_ENTRY = [
  LangsComponent,
  // ImgComponent,
  // FileManagerComponent,
  // StatusLabelComponent,
  // AddressComponent,
  ...QUICK_CHAT_COMPONENTS,
];
const COMPONENTS = [
  // EditorComponent,
  LokiDateComponent,
  LokiSearchComponent,
  LokiControlComponent,
  LokiMoneyComponent,
  LokiGridComponent,
  LokiTableComponent,
  ...COMPONENTS_ENTRY,
  ...PRO_SHARED_COMPONENTS,
];
const DIRECTIVES = [
  // UppercaseDirective,
  // ImgDirective,
  //  DelayDirective,
  //   MasonryDirective,
  ScrollbarDirective,
  //   MouseFocusDirective
];
// #endregion

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AlainThemeModule.forChild(),
    LazyLoadImageModule,
    DelonACLModule,
    DelonFormModule,
    ...I18NSERVICE_MODULES,
    ...SHARED_DELON_MODULES,
    ...SHARED_ZORRO_MODULES,
    ...THIRDMODULES,
  ],
  declarations: [...COMPONENTS, ...DIRECTIVES],
  providers: [CurrencyPipe],
  entryComponents: COMPONENTS_ENTRY,
  exports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AlainThemeModule,
    DelonACLModule,
    DelonFormModule,
    ...I18NSERVICE_MODULES,
    ...SHARED_DELON_MODULES,
    ...SHARED_ZORRO_MODULES,
    ...THIRDMODULES,
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
})
export class SharedModule {}
