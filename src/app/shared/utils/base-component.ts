import { OnInit, OnDestroy, Directive } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Directive()
// tslint:disable-next-line: directive-class-suffix
export class BaseComponent implements OnInit, OnDestroy {
  ngOnInit() {}
  ngOnDestroy() {
    // dummy
  }
}
