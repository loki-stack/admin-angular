import { Platform } from '@angular/cdk/platform';
import { Directive, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Directive()
// tslint:disable-next-line: directive-class-suffix
export class BaseListView implements OnInit, OnDestroy {
  virtualScroll = false;
  scroll = {};
  constructor(public platform: Platform) {}
  ngOnInit() {
    if (!this.platform.isBrowser) {
      return;
    }
    this.virtualScroll = window.innerWidth < 1024 && window.innerWidth > 767;
    if (this.virtualScroll) {
      this.scroll = { x: window.innerWidth - 373 + 'px', y: window.innerHeight - 373 + 'px' };
    }
  }
  ngOnDestroy() {
    // dummy
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (!this.platform.isBrowser) {
      return;
    }
    this.virtualScroll = window.innerWidth < 1024 && window.innerWidth > 767;
    if (this.virtualScroll) {
      this.scroll = { x: window.innerWidth - 373 + 'px', y: window.innerHeight - 373 + 'px' };
    }
  }
}
