/**
 * Table list view config
 */
export const TABLE_CONFIG = {
  pageSizes: [10, 20, 30, 50, 100],
  showSize: true,
  front: false,
  showQuickJumper: true,
  multiSort: false,
};

export const COMMON_CONSTANT = {
  SEARCH_TIMEOUT: 500,
  enumItemStatus3: [
    {
      value: true,
      i18n: 'Hoạt động',
      color: '#108ee9',
    },
    {
      value: false,
      i18n: 'Không hoạt động',
      color: '#f5222d',
    },
  ],
  enumItemStatus2: [
    {
      value: 'True',
      i18n: 'Hoạt động',
      color: '#108ee9',
    },
    {
      value: 'False',
      i18n: 'Không hoạt động',
      color: '#f5222d',
    },
  ],
  enumItemStatus: [
    {
      value: 'ACTIVE',
      i18n: 'Hoạt động',
      color: '#108ee9',
    },
    {
      value: 'DEACTIVE',
      i18n: 'Không hoạt động',
      color: '#f5222d',
    },
  ],
  enumFeedbackStatus: [
    {
      value: 'NEW',
      i18n: 'Mới',
      color: '#108ee9',
    },
    {
      value: 'SEEN',
      i18n: 'Đã xem',
      color: '#f5222d',
    },
  ],
  enumPartner: [
    {
      value: JSON.stringify({ fromValue: 0 }),
      i18n: 'Phí cộng sự > 0',
      color: '#108ee9',
      selected: false,
    },
  ],
  enumTransaction: [
    {
      value: 'Open',
      i18n: 'Đang chờ',
      color: '#108ee9',
    },
    {
      value: 'Paid',
      i18n: 'Đã thanh toán',
      color: '#f5222d',
    },
    {
      value: 'Locked',
      i18n: 'Đã khóa',
      color: '#f50',
    },
    {
      value: 'Suspended',
      i18n: 'Đã hoàn tác',
      color: 'black',
    },
  ],
  enumUserType: [
    {
      value: -1,
      i18n: 'Quản trị HT',
      color: '#108ee9',
    },
    {
      value: 0,
      i18n: 'Quản trị viên',
      color: '#f5222d',
    },
  ],
  enumRoleType: [
    {
      value: true,
      i18n: 'Hệ thống',
      color: '#108ee9',
    },
    {
      value: false,
      i18n: 'Mặc định',
      color: '#f5222d',
    },
  ],
  enumAccountType: [
    {
      value: '00000000-0000-0000-0000-000000000005',
      i18n: 'Thành viên cao cấp',
      color: '#108ee9',
    },
    {
      value: '00000000-0000-0000-0000-000000000004',
      i18n: 'Cộng sự',
      color: '#f5222d',
    },
    // {
    //   value: '00000000-0000-0000-0000-000000000003',
    //   i18n: 'Quản trị viên',
    //   color: '#f50',
    // },
    {
      value: '00000000-0000-0000-0000-000000000002',
      i18n: 'Thành viên thường',
      color: 'black',
    },
  ],
  enumUpgradeType: [
    {
      value: 'TRA_PHI',
      i18n: 'Trả phí',
      color: '#108ee9',
    },
    {
      value: 'THU_CONG',
      i18n: 'Thủ công',
      color: '#f5222d',
    },
    {
      value: 'HA_CAP_KHI_HET_HAN',
      i18n: 'Hết hạn',
      color: '#f50',
    },
    {
      value: 'GIA_HAN',
      i18n: 'Gia hạn',
      color: 'black',
    },
  ],
  enumBannerPosition: [
    {
      value: 'Trái',
      i18n: 'Trái',
      color: '#108ee9',
    },
    {
      value: 'Phải',
      i18n: 'Phải',
      color: '#f5222d',
    },
  ],

  enumYesNo: [
    {
      value: true,
      i18n: 'Có',
      color: '#108ee9',
    },
    {
      value: false,
      i18n: 'Không',
      color: '#f5222d',
    },
  ],
  enumPostStatus: [
    {
      value: 'Draft',
      i18n: 'Lưu nháp',
      color: 'beige',
      viewPoint: '',
    },
    {
      value: 'Pending',
      i18n: 'Chờ duyệt',
      color: '#108ee9',
      viewPoint: 'MOD',
    },
    {
      value: 'PreApproved',
      i18n: 'Duyệt tạm',
      color: '#f50',
      viewPoint: 'MOD',
    },
    {
      value: 'Publish',
      i18n: 'Đã duyệt',
      color: '#87d068',
      viewPoint: 'MOD',
    },
    {
      value: 'Updating',
      i18n: 'Đang sửa',
      color: '#f5222d',
      viewPoint: '',
    },
    {
      value: 'Reject',
      i18n: 'Từ chối duyệt',
      color: 'black',
      viewPoint: 'MOD',
    },
  ],
  enumCommentStatus: [
    {
      value: 'Active',
      i18n: 'Duyệt tạm',
      color: '#f50',
    },
    {
      value: 'Publish',
      i18n: 'Đã duyệt',
      color: '#87d068',
    },
    {
      value: 'Delete',
      i18n: 'Từ chối duyệt',
      color: 'black',
    },
  ],
  enumRegisterPartner: [
    {
      value: 'WAITAPPROVAL',
      i18n: 'Chờ duyệt',
      color: '#f50',
    },
    {
      value: 'APPROVED',
      i18n: 'Đã duyệt',
      color: '#87d068',
    },
    {
      value: 'REJECTED',
      i18n: 'Từ chối duyệt',
      color: 'black',
    },
  ],
  inboxStatus: [
    {
      value: 'In',
      i18n: 'Nhận về',
      color: 'beige',
    },
    {
      value: 'out',
      i18n: 'Gửi đi',
      color: '#108ee9',
    },
  ],
};
export const USER_CONSTANT = {
  IsSuperAdmin: -1,
  IsAdmin: 0,
  IsGuest: 1,
  SYS_ADMIN_ID: '00000000-0000-0000-0000-000000000001',
};
export const ACCOUNT_CONSTANT = {
  HA_CAP_KHI_HET_HAN: 'HA_CAP_KHI_HET_HAN',
};
export const ROLE_CONSTANT = {
  SYS_ADMIN: '00000000-0000-0000-0000-000000000001',
  ADMIN: '00000000-0000-0000-0000-000000000002',
};
export const APP_CONSTANT = {
  MAIN_APP: '00000000-0000-0000-0000-000000000001',
};

export const DEPOSIT_CONSTANT = {
  DEPOSIT_MIN_AMOUNT: 15000,
};
export const DATE_FORMAT = 'dd/MM/yyy';
export const RIGHT_CONSTANT = {
  AccessAppId: '00000000-0000-0000-0000-000000000001',
  AccessAppCode: 'TRUY_CAP_HE_THONG',
  DefaultAppId: '00000000-0000-0000-0000-000000000002',
  DefaultAppCode: 'TRUY_CAP_MAC_DINH',
};

export const REGISTER_PARTNER = {
  WAITAPPROVAL: 'WAITAPPROVAL',
  APPROVED: 'APPROVED',
  REJECTED: 'REJECTED',
};
export const POST_STATUS = {
  Draft: 'Draft',
  Pending: 'Pending',
  PreApproved: 'PreApproved',
  Publish: 'Publish',
  Updating: 'Updating',
  Reject: 'Reject',
};
export const POST_COMMENT_STATUS = {
  Active: 'Active',
  Deactive: 'Deactive',
  Admin_Deactive: 'Admin_Deactive',
};
export const COMMENT_STATUS = {
  PreApproved: 'PreApproved',
  Publish: 'Publish',
  Reject: 'Delete',
};

export const FORM_VALIDATE = {
  trimRequied: (value, formProperty, form) => {
    if (value) {
      if (value !== '') {
        return true;
      }
    }
    return false;
  },
};

export default null;

export const GET_CONFIG = () => {
  try {
    return (window as any).__env_prod || (window as any).__env;
  } catch (error) {
    return {
      authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
      tokenGateWay: 'a9b94296-b757-3056-be4f-beeffb639ffe',
      fileHost: 'https://api.admin.stup.loki.vn/StaticFiles',
      oaFbUrl: 'https://api.admin.stup.loki.vn/api/v1/authentication/fb/oa',
      oaGGUrl: 'https://api.admin.stup.loki.vn/api/v1/authentication/gg/oa',
      callbackUrl: 'http://admin.stup.loki.vn/assets/pages/callback.html',
      adminHost: 'https://api.admin.stup.loki.vn',
      cmsHost: 'https://api.cms.stup.loki.vn',
      crmsHost: 'https://api.crm.stup.loki.vn',
      mvcHost: 'https://web.stup.loki.vn',
    };
  }
};
