import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Directive, OnDestroy } from '@angular/core';
import { Inject } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Directive()
// tslint:disable-next-line: directive-class-suffix
export class TemplateItem implements AfterViewInit, OnDestroy {
  constructor(@Inject(DOCUMENT) public document: Document) {}

  ngOnDestroy() {
    // dummy
  }
  ngAfterViewInit() {
    this.setAutoFocus();
  }
  setAutoFocus() {
    try {
      setTimeout(() => {
        let elm = this.document.querySelector(".ant-modal-body .ant-form [autofocus='true']:not(:disabled)") as any;
        if (!elm) {
          elm = this.document.querySelector('.ant-modal-body .ant-form .ant-input:not(:disabled)') as any;
        }
        if (elm) {
          elm.focus();
        }
      }, 500);
    } catch (error) {}
  }
}
