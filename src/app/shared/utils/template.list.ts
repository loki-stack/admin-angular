import { AfterViewInit, ChangeDetectorRef, Directive, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN, ModalHelper, ModalHelperOptions, _HttpClient } from '@delon/theme';
import { LGConfigModel, LGDataModel } from '@shared/components/loki-grid/loki-grid.model';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Observable, Subscription } from 'rxjs';
import { TABLE_CONFIG } from './const';
@AutoUnsubscribe()
@Directive()
// tslint:disable-next-line: directive-class-suffix
export class TemplateList implements OnInit, AfterViewInit, OnDestroy {
  cmd1: Subscription;

  lgConfig: LGConfigModel;
  lgData: LGDataModel;
  lgSearchData: any;
  /**
   * Component cho modal
   */
  addComponent: any;
  editComponent: any;
  /**
   * Sử dụng route để tìm kiếm
   */
  searchRoute = true;
  lastSearch: any = {
    result: {
      page: 1,
      size: TABLE_CONFIG.pageSizes[0],
      sort: '-LastModifiedOnDate',
    },
    objHelper: {
      data: {
        pi: 1,
        ps: TABLE_CONFIG.pageSizes[0],
      },
      filter: {},
    },
  };
  isFirstView = true;
  isFirstSearch = true;
  defaultSearch: any = {};
  constructor(
    public msg: NzMessageService,
    public route: ActivatedRoute,
    public modal: ModalHelper,
    public modalService: NzModalService,
    @Inject(ALAIN_I18N_TOKEN) public i18n: I18NService,
    public cdr: ChangeDetectorRef,
    public router: Router,
  ) {
    this.lgData = {
      sortDir: 'descend',
      sortProp: 'CreatedOnDate',
    };
  }
  ngOnInit() {
    if (this.searchRoute) {
      this.route.queryParams.subscribe((params) => {
        let searchFormData = {};
        let listSelectedTag = [];
        if (params.searchFormDataStr) {
          try {
            searchFormData = {
              ...this.defaultSearch,
              ...JSON.parse(params.searchFormDataStr),
            };
            listSelectedTag = [...params.listSelectedTag];
            if (params.listSelectedTag && !Array.isArray(params.listSelectedTag)) {
              listSelectedTag = [params.listSelectedTag];
            }
          } catch (error) {}
        }
        if (this.isFirstView) {
          this.lgOnSearch({
            filter: {
              ...params,
              searchFormData,
              listSelectedTag,
            },
          });
          this.isFirstView = false;
        }
      });
    } else {
      this.lgOnSearch({
        filter: {},
      });
    }
    this.postInit();
  }

  ngAfterViewInit() {
    // DEFAULT
    const displayMode = 'both';
    const actionsSingle = [
      {
        icon: 'edit',
        action: (data: any) => this.edit(data),
        i18n: 'Sửa',
        i18nDescription: 'Sửa bản ghi',
      },
      {
        icon: 'delete',
        action: (data: any) => this.delete(data),
        i18n: 'Xóa',
        type: 'danger',
        i18nDescription: 'Xóa bản ghi',
      },
    ];
    const actionsSelf = [
      {
        action: () => {
          this.add();
        },
        icon: 'plus',
        i18n: 'Thêm',
        i18nDescription: 'Thêm bản ghi',
      },
    ];
    const actionsMulti = [
      {
        action: (selectedData) => {
          this.deleteMulti(selectedData);
        },
        icon: 'delete',
        i18n: 'Xóa đã chọn',
        type: 'danger',
        i18nDescription: 'Xóa bản ghi đã chọn',
      },
    ];
    // Config
    this.lgConfig = {
      canDragDrop: false,
      displayMode,
      actionsSingle,
      actionsSelf,
      actionsMulti,
    };
    // overwrite config
    this.initConfig();
  }

  ngOnDestroy() {
    // dummy
  }
  lgOnSearch(event?: any) {
    let filter: any;
    let searchFormData = null;
    let sort = '-LastModifiedOnDate';
    let data = {} as any;
    let filterObj = {};
    if (event) {
      filter = event.filter;
      if (!filter.pi) {
        filter.pi = 1;
      }
      if (!filter.ps) {
        filter.ps = TABLE_CONFIG.pageSizes[0];
      }
      data = event.data;
      searchFormData = event.filter.searchFormData;
      let prop = event.filter.sortProp;
      if (!prop) {
        prop = 'LastModifiedOnDate';
      }
      prop = this.toPascalCase(prop);
      sort = event.filter.sortDir === 'ascend' ? '+' + prop : '-' + prop;
      filterObj = {
        fullTextSearch: event.filter.filterText,
        ...event.filter.searchFormData,
      };
      filter.searchFormDataStr = JSON.stringify(event.filter.searchFormData || {});
      delete filter.searchFormData;
      this.lastSearch.result = {
        page: filter.pi,
        size: filter.ps,
        sort,
        filter: JSON.stringify(filterObj),
        listSelectedTag: filter.listSelectedTag,
      };
      this.lastSearch.objHelper = {
        data,
        filter,
        searchFormData,
        listSelectedTag: filter.listSelectedTag,
      };
    }

    this.seach(this.lastSearch.result, this.lastSearch.objHelper);
  }

  clean(obj) {
    for (const propName in obj) {
      if (obj[propName] === null || obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }
  toPascalCase(input: string): string {
    return input[0].toUpperCase() + input.substring(1);
  }
  seach(filter: TemplateFilter, objHelper: any) {
    // try delay to cancel quick action

    try {
      this.cmd1.unsubscribe();
    } catch (error) {}
    if (filter.clientSort) {
      if (this.searchRoute) {
        this.router.navigate(['.'], { relativeTo: this.route, queryParams: { ...objHelper.filter } });
        this.cdr.detectChanges();
      }
      return;
    }
    this.cmd1 = this.actionSearch(filter).subscribe((responseData) => {
      if (responseData.code === 200) {
        let datas = responseData.data.content as any;
        datas = this.processData(datas);
        if (!this.lgData) {
          this.lgData = {};
        }
        this.lgData = {
          ...this.lgData,
          ...objHelper.data,
          datas,
          total: responseData.data.totalElements,
          pi: objHelper.filter.pi,
          ps: objHelper.filter.ps,
        };
        // ReadOnly
        if (this.isFirstSearch) {
          this.lgSearchData = {
            searchFormData: objHelper.searchFormData || {},
            listSelectedTag: objHelper.listSelectedTag,
          };
          this.isFirstSearch = false;
          this.lgData.filterText = objHelper.filter.filterText;
        }

        if (this.searchRoute) {
          this.router.navigate(['.'], { relativeTo: this.route, queryParams: { ...objHelper.filter } });
          this.cdr.detectChanges();
        }
      }
    });
  }
  add(): any {
    this.modal.createStatic(this.addComponent, { i: { id: null } }).subscribe(() => {
      this.lgOnSearch();
    });
  }
  deleteMulti(selectedData: any[]) {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa những bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        this.actionDeleteMulti(selectedData).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.lgOnSearch();
          },
          () => {
            this.msg.success(this.i18n.fanyi('Thất bại'));
            this.lgOnSearch();
          },
        );
      },
      nzOnCancel: () => {},
    });
  }
  delete(item: any): any {
    this.modalService.confirm({
      nzTitle: this.i18n.fanyi('Bạn có muốn xóa bản ghi này?'),
      nzOkText: this.i18n.fanyi('Tiếp tục'),
      nzCancelText: this.i18n.fanyi('Hủy'),
      nzOkType: 'danger',
      nzOnOk: () => {
        this.actionDelete(item).subscribe(
          () => {
            this.msg.success(this.i18n.fanyi('Xóa thành công'));
            this.lgOnSearch();
          },
          () => {
            this.msg.error(this.i18n.fanyi('Thất bại'));
            this.lgOnSearch();
          },
        );
      },
      nzOnCancel: () => {},
    });
  }
  edit(item: any): any {
    this.modal.createStatic(this.editComponent, { i: item }).subscribe(() => {
      this.lgOnSearch();
    });
  }
  /**
   * Overwrite method
   */
  actionDelete(item: any): Observable<any> {
    const cmd = new Observable((subscriber) => {
      setTimeout(() => {
        subscriber.next(item);
        subscriber.complete();
      }, 1000);
    });
    return cmd;
  }
  actionDeleteMulti(selectedData: any[]): Observable<any> {
    const cmd = new Observable((subscriber) => {
      setTimeout(() => {
        subscriber.next(selectedData);
        subscriber.complete();
      }, 1000);
    });
    return cmd;
  }
  actionSearch(filter: TemplateFilter): Observable<any> {
    const cmd = new Observable((subscriber) => {
      setTimeout(() => {
        subscriber.next(filter);
        subscriber.complete();
      }, 1000);
    });
    return cmd;
  }
  processData(input: any[]): any[] {
    return input;
  }
  initConfig() {}
  postInit() {}
  actionRow(item: any, components: any, option?: ModalHelperOptions): any {
    this.modal
      .createStatic(
        components,
        { i: item },
        {
          ...option,
        },
      )
      .subscribe(() => {
        this.lgOnSearch();
      });
  }
}

export interface TemplateFilter {
  page?: number;
  size?: number;
  sort?: string;
  filter?: string;
  listSelectedTag?: any[];
  [key: string]: any;
}
