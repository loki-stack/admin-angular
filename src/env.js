(function (window) {
  window.__env_prod = window.__env_prod || {};

  // API url
  window.__env_prod = {
    authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
    tokenGateWay: 'a9b94296-b757-3056-be4f-beeffb639ffe',
    fileHost: 'https://localhost:5001/StaticFiles',
    oaFbUrl: 'https://localhost:5001/api/v1/authentication/fb/oa',
    oaGGUrl: 'https://localhost:5001/api/v1/authentication/gg/oa',
    callbackUrl: 'http://admin.stup.loki.vn/assets/pages/callback.html',
    adminHost: 'https://localhost:5001',
  };
})(this);
