(function (window) {
  window.__env_prod = window.__env_prod || {};

  // API url
  window.__env_prod = {
    authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
    tokenGateWay: 'a9b94296-b757-3056-be4f-beeffb639ffe',
    fileHost: 'https://api.admin.stup.loki.vn/StaticFiles',
    oaFbUrl: 'https://api.admin.stup.loki.vn/api/v1/authentication/fb/oa',
    oaGGUrl: 'https://api.admin.stup.loki.vn/api/v1/authentication/gg/oa',
    callbackUrl: 'http://admin.stup.loki.vn/assets/pages/callback.html',
    adminHost: 'https://api.admin.stup.loki.vn',
    cmsHost: 'https://api.cms.stup.loki.vn',
    crmsHost: 'https://api.crm.stup.loki.vn',
    mvcHost: 'https://web.stup.loki.vn',
  };
})(this);
