export const environment = {
  SERVER_URL: `./`,
  production: false,
  mocked: false,
  useHash: true,
  debug: false,
  pro: {
    theme: 'dark',
    menu: 'top',
    contentWidth: 'fluid',
    fixedHeader: true,
    autoHideHeader: true,
    fixSiderbar: true,
    onlyIcon: true,
    collapsed: true,
  },
};
